using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using Autofac.Core.Lifetime;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using Snapme.Bots.Services;
using Snapme.Common;
using Snapme.Common.DI;
using Snapme.Data;
using Snapme.Data.Bots;
using Snapme.Data.Games;
using Snapme.Import;
using Snapme.Testing;
using Snapme.Tests;
using VkNet.Utils;

namespace Snapme.ManualTests
{
    public class PerfTest
    {
        [Test]
        public async Task CheckCtr()
        {
            LogHelper.InitLogger();
            var builder = new SnapmeBuilder(new[]
            {
                GetType().Assembly,
                typeof(Game).Assembly
            });
            var containerBuilder = builder.CreateBuilder("Local", Guid.NewGuid().ToString());
            containerBuilder.RegisterType<TestSpreadsheetReader>().AsImplementedInterfaces().AsSelf().SingleInstance();
            var container = containerBuilder.Build();

            var locker = new AsyncLock();
            var tasks = new List<Task>();
            ILifetimeScope scope;
            for (int i1 = 0; i1 < 100; i1++)
            {
                scope = container.BeginLifetimeScope(MatchingScopeLifetimeTags.RequestLifetimeScopeTag);
                for (int i = 0; i < 20; i++)
                {
                    var scope1 = scope;
                    var task = locker.Run(async () =>
                    {
                        var listAsync = await scope1.Resolve<SnapmeContext>().GameTaskActivities.Include(x => x.GameTask)
                            .OrderBy(x => x.Number).Take(10)
                            .ToListAsync();
                        return listAsync;
                    });
                    tasks.Add(task);
                    var task2 = locker.Run(async () =>
                    {
                        var bot = scope1.Resolve<GameService>().CreateBot();
                        var listAsync = scope1.Resolve<UserService>()
                            .GetOrCreateFakeUser(bot.Id, Guid.NewGuid().ToString() + "Fake");
                        await scope1.Resolve<GameService>().SaveChangesAsync();
                        return listAsync;
                    });
                    tasks.Add(task2);
                }
                Task.WaitAll(tasks.ToArray());
                scope.Dispose();
                scope = container.BeginLifetimeScope(MatchingScopeLifetimeTags.RequestLifetimeScopeTag);
            }
        }

        [Test]
        public async Task CheckPerf()
        {
            LogHelper.InitLogger();
            var builder = new SnapmeBuilder(new[]
            {
                GetType().Assembly,
                typeof(Game).Assembly
            });
            var containerBuilder = builder.CreateBuilder("Local", Guid.NewGuid().ToString());
            containerBuilder.RegisterType<TestSpreadsheetReader>().AsImplementedInterfaces().AsSelf().SingleInstance();
            containerBuilder.RegisterType<TestMessengerFactory>().AsImplementedInterfaces().AsSelf().SingleInstance();
            var container = containerBuilder.Build();
            await RunGame(container);
        }

        private static async Task RunGame(IContainer container)
        {
            var t = new TgmBotTester(container);
            var gameId = t.CreateGame(TestGameFlags.Default);
            t.InScope(s =>
            {
                AddTasksToGame(s.Resolve<SnapmeContext>(),
                    s.Resolve<SnapmeContext>().Games.Find(gameId), TestGameFlags.Default, "-1");
                AddTasksToGame(s.Resolve<SnapmeContext>(),
                    s.Resolve<SnapmeContext>().Games.Find(gameId), TestGameFlags.Default, "-2");
                AddTasksToGame(s.Resolve<SnapmeContext>(),
                    s.Resolve<SnapmeContext>().Games.Find(gameId), TestGameFlags.Default, "-3");
                AddTasksToGame(s.Resolve<SnapmeContext>(),
                    s.Resolve<SnapmeContext>().Games.Find(gameId), TestGameFlags.Default, "-4");
            });
            t.CreateAndStartGame();
            var r = new Random();
            var players = new List<TestUser>();
            for (var i = 0; i < 100; i++)
                if (r.Next(100) > 70)
                    players.Add(await t.JoinPlayerAndApproveSelfie(i, Gender.Boy));
                else
                    players.Add(await t.JoinPlayerAndApproveSelfie(i, Gender.Girl));

            for (int i = 0; i < 120; i++)
            {
                for (var j = 0; j < 20; j++)
                    t.ProcessPlayerMessage(players[r.Next(players.Count - 1)], "Photo" + i, "P" + i);
                for (var j = 0; j < 20; j++)
                    if (r.Next(10) > 8)
                        t.ProcessValidatorMessage("no Some reason");
                    else
                        t.ProcessValidatorMessage("yes");
                t.ProcessAdminMessage("status");
                t.RunBackgroundActions();
                t.RunBackgroundActions();
                t.RunBackgroundActions();
                t.RunBackgroundActions();
                t.RunBackgroundActions();
                LogHelper.Log(typeof(PerfTest)).Info("Emulated Time:" + i + " minutes passed");
                DateHelper.SetUtcNowForTests(DateTime.UtcNow + TimeSpan.FromMinutes(i));
            }

            await t.ProcessAdminMessage("status");
            await t.ProcessAdminMessage("finish");
        }


        public static void AddTasksToGame(SnapmeContext context, Game game, TestGameFlags flags, string suffix = null)
        {
            AddActivity(context, game, "Сделай селфи с Лениным", ActivityType.SelfieTask, suffix: suffix);
            AddActivity(context, game, "Сделай селфи с небом", ActivityType.SelfieTask, suffix: suffix);

            if ((flags & TestGameFlags.WithMeetings) != 0)
            {
                AddActivity(context, game, "Сделайте селфи со знаком Stop", ActivityType.MeetingTask, suffix: suffix);
                AddActivity(context, game, "Сделайте селфи со светофором", ActivityType.MeetingTask, suffix: suffix);
            }

            AddActivity(context, game, "Вы на фоне Пушкина", ActivityType.MissionTaskPlace, suffix: suffix);
            AddActivity(context, game, "Вы на фоне Оперного", ActivityType.MissionTaskPlace, suffix: suffix);
            AddActivity(context, game, "Вы играете в гольф", ActivityType.MissionTaskActivity, suffix: suffix);
            AddActivity(context, game, "Вы играете вниз головой", ActivityType.MissionTaskActivity, suffix: suffix);
            AddActivity(context, game, "Вы играете на трубе", ActivityType.MissionTaskActivity, suffix: suffix);
            AddActivity(context, game, "Вы танцуете", ActivityType.MissionTaskActivity, 2, suffix: suffix);
            AddActivity(context, game, "Ты играешь в домино", ActivityType.MissionSingleTask, suffix: suffix);
            AddActivity(context, game, "Ты играешь в тетрис", ActivityType.MissionSingleTask, 2, suffix: suffix);

            if ((flags & TestGameFlags.WithNoFinalActivities) == 0)
                AddActivity(context, game, "Ты одиноко стоишь у памятника", ActivityType.FinalSingleTask,
                    suffix: suffix);
            if ((flags & TestGameFlags.WithNoFinalActivities) == 0)
                AddActivity(context, game, "Ты на потолке", ActivityType.FinalTaskPlace, suffix: suffix);
        }

        public static void AddActivity(SnapmeContext context, Game game, string descriptionRu,
            ActivityType gameTaskType, int phase = 1, string suffix = null)
        {
            context.AddNew(new Activity
            {
                GameId = game.Id,
                DescriptionRu = descriptionRu,
                ActivityType = gameTaskType,
                Phase = phase
            });
        }
    }
}