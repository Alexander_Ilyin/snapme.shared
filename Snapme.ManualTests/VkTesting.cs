using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using NUnit.Framework;
using VkNet;
using VkNet.Enums.SafetyEnums;
using VkNet.Model;
using VkNet.Model.Attachments;
using VkNet.Model.Keyboard;
using VkNet.Model.RequestParams;

namespace Snapme.ManualTests
{
    public class VkTesting
    {
        [Test]
        public void CheckVk()
        {
//            response": {
//            "key": "1b5c21fb15d42dc6749cc9ba33bc674025c5d474",
//            "server": "https://lp.vk.com/wh180360354",
//            "ts": "2"
//        }
            var adminToken = "412163c9a4302d67c4da0d57ba8e2f1e47371000c978e417eb62f6206f25f263a9e32439202667983da02";
            var botToken = "dd77d6fdb3902882e9368949a6d0d3d4137f8951701d92a7508c849da6e9a0be0bcb2b1d862194d6ead2e";
            try
            {
                var services = new ServiceCollection();
                services.AddLogging(configure => configure.AddConsole().AddDebug().SetMinimumLevel(LogLevel.Trace));
                var service = services.BuildServiceProvider().GetService<ILogger<VkApi>>();
                ;
                var vkBotApi = new VkApi(service);
                vkBotApi.Authorize(new ApiAuthParams
                {
                    AccessToken =
                        botToken
                });

                var vkAdminApi = new VkApi(service);
                vkAdminApi.Authorize(new ApiAuthParams
                {
                    AccessToken =
                        adminToken
                });
                //var getLongPollServer = vkApi.Groups.GetLongPollServer(180652204);
                var getLongPollServer = vkAdminApi.Groups.GetLongPollServer(180685541);
//                var video = vkApi.Video.Save(new VideoSaveParams()
//                {
//                    Link = "https://www.youtube.com/watch?v=p84vkmo4hvE",
//                    Name = "VFROMB",
//                    
//                });

                vkBotApi.Messages.Send(new MessagesSendParams()
                {
                    Message = "Viiiideo From booot jsooon https://www.youtube.com/watch?v=RvWv9Rh_34E&list=PL8YZyma552VcePhq86dEkohvoTpWPuauk&index=4",
                    Attachments = new []{ new Link()
                    {
                        Uri = new Uri("https://www.youtube.com/watch?v=RvWv9Rh_34E&list=PL8YZyma552VcePhq86dEkohvoTpWPuauk&index=4"),
                        Title = "SOme link",
                        
                    }},
                    UserId = 467627475,
                    PeerId = 467627475,
                    RandomId = Guid.NewGuid().GetHashCode(),
//                    Attachments = new List<MediaAttachment>()
//                    {
//                        JsonConvert.DeserializeObject<Video>(File.ReadAllText("./video456239020", Encoding.UTF8))
//                    },
                    Keyboard = new MessageKeyboard()
                    {
                        OneTime = true,
                        
                        Buttons = new[]
                        {
                            new MessageKeyboardButton[]
                            {
                                new MessageKeyboardButton()
                                {
                                    
                                    Color = KeyboardButtonColor.Primary,
                                    Action = new MessageKeyboardButtonAction()
                                    {
                                        Label = "Yes",
                                        Payload = "{\"button\": \"1\"}",
                                        Type = KeyboardButtonActionType.Text,
                                        
                                        
                                    }
                                },
                                new MessageKeyboardButton()
                                {
                                    
                                    Color = KeyboardButtonColor.Primary,
                                    Action = new MessageKeyboardButtonAction()
                                    {
                                        Label = "No",
                                        Payload = "{\"button\": \"3\"}",
                                        Type = KeyboardButtonActionType.Text,
                                        
                                        
                                    }
                                },
                                
                            }
                        }
                    }
                });
                return;
                var pollHistory = vkAdminApi.Groups.GetBotsLongPollHistoryAsync(new BotsLongPollHistoryParams
                {
                    Wait = 2,
                    Ts = "5",
                    Key = getLongPollServer.Key,
                    Server = getLongPollServer.Server
                }).Result;
                foreach (var update in pollHistory.Updates)
                {
                    var v = update.Message?.Attachments.Select(x => x.Instance as Video).Concat(new[] {update.Video})
                        .Where(x => x != null);
                    foreach (var VARIABLE in v)
                    {
                        File.WriteAllText("./video" + VARIABLE.Id, JsonConvert.SerializeObject(VARIABLE),
                            Encoding.UTF8);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}