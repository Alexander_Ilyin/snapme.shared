﻿using System;
using System.Collections.Generic;
using System.Linq;
using TypeScripter.TypeScript;

namespace HrCore.ManualTests.Typescript
{
    public class CustomFormatter : TsFormatter
    {
        private readonly List<string> stringEmunsNames;

        public CustomFormatter(List<Type> stringEmuns)
        {
            stringEmunsNames = stringEmuns.Select(x => x.Name).ToList();
        }

        public override string Format(TsEnum tsEnum)
        {
            if (stringEmunsNames.Contains(tsEnum.Name.Name))
                return string.Empty;
            return base.Format(tsEnum);
        }
    }
}