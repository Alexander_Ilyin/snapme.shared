﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using HrCore.ManualTests.Typescript;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NUnit.Framework;
using Snapme.Common;
using Snapme.Data.Dto;
using TypeScripter;

namespace Snapme.ManualTests.Ts
{
    [Category("Manual")]
    [TestFixture]
    public class TsGeneratorTest
    {
        private static string RemoveNamespace(Match m)
        {
            var res = Regex.Replace(m.Value, @"([a-z0-9A-Z]*\.)*([a-z0-9A-Z]*)", "$2");
            return res;
        }

        private IEnumerable<Type> GetBaseTypes(Type type)
        {
            while (type != null)
            {
                yield return type;
                type = type.BaseType;
            }
        }

        private bool HasStringEnumConverterAttribute(PropertyInfo arg)
        {
            var attr = ((JsonConverterAttribute[]) arg.GetCustomAttributes(typeof(JsonConverterAttribute), false))
                .FirstOrDefault();
            var hasStringEnumConverterAttribute = attr != null && attr.ConverterType == typeof(StringEnumConverter);
            if (hasStringEnumConverterAttribute && !arg.PropertyType.IsEnum)
                throw new Exception("Invalid prop " + arg.DeclaringType + " " + arg.Name);
            return hasStringEnumConverterAttribute;
        }

        [Test]
        public void Test()
        {
            var assemblies = new[]
            {
                GetType().Assembly,
                typeof(GameDto).Assembly
            }.Distinct();
            var types = assemblies.SelectMany(x => x.GetTypes())
                .Where(x => x.GetCustomAttribute<TScriptedAttribute>() != null).ToList();

            var stringEmuns = types.SelectMany(x => x.GetProperties()).Where(HasStringEnumConverterAttribute)
                .Select(x => x.PropertyType).Distinct().ToList();


            types = types.Except(stringEmuns).SelectMany(x => GetBaseTypes(x)
                .Where(z => x.GetCustomAttribute<TScriptedAttribute>() != null)
                .Reverse()).Distinct().ToList();

            var scripter = new Scripter();
            scripter.UsingFormatter(new CustomFormatter(stringEmuns));
            var output = scripter
                .UsingTypeReader(new TsTypeReader())
                .AddTypes(types)
                .ToString();
            output = Regex.Replace(output, @"Snapme\.Data\.\S*", RemoveNamespace);
            output = Regex.Replace(output, @"System\.\S*", RemoveNamespace);
            output = output.Replace("<Guid>", "<string>");
            output = Regex.Replace(output, @"declare module.*?\{", "");
            output = Regex.Replace(output, @"^\}\s*$", "", RegexOptions.Multiline);
            output = output.Replace("const enum ", "export enum ");
            output = output.Replace("interface ", "export interface ");
            output = Regex.Replace(output, @"export\s*interface\s*IEnumerable.*?\}", "", RegexOptions.Singleline);
            output = Regex.Replace(output, @"export\s*interface\s*IComparable.*?\}", "", RegexOptions.Singleline);
            output = Regex.Replace(output, @"export\s*interface\s*IFormattable.*?\}", "", RegexOptions.Singleline);
            output = Regex.Replace(output, @"export\s*interface\s*IEquatable.*?\}", "", RegexOptions.Singleline);
            output = Regex.Replace(output, @"^\t{1}", "", RegexOptions.Multiline);

            foreach (var enumType in stringEmuns)
            {
                var names = Enum.GetNames(enumType).ToList();
                output += "\nexport class " + enumType.Name + " {";
                output = names.Aggregate(output,
                    (current, name) => current + string.Format("\n\tstatic {0} = '{0}';", name));
                output += "\n}\n\r";
            }

            output = output.Trim();
            output = output.Replace("\n\n", "");
            output = output.Replace("\r\n\r\n\r\n\r\n", "");
            output = output.Replace("?:", ":");
            output = output.Replace(":", "?:");
            output = output.Replace("\t", "  ");
            output = output.Replace("  {", " {");

            var outputPath = FsUtils.FindPath("./Snapme.Js/snapmeApp/src/contracts.tsx");
            File.WriteAllText(outputPath, output + "\n");
        }
    }
}