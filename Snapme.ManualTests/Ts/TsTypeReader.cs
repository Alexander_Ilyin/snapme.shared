using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;
using TypeScripter.Readers;

namespace HrCore.ManualTests.Typescript
{
    public class TsTypeReader : TypeReader
    {
        public override IEnumerable<ParameterInfo> GetParameters(MethodInfo method)
        {
            yield break;
        }

        public override IEnumerable<MethodInfo> GetMethods(TypeInfo type)
        {
            yield break;
        }

        public override IEnumerable<PropertyInfo> GetProperties(TypeInfo type)
        {
            return type.GetProperties().Where(x =>
                    x.GetIndexParameters().Length == 0
                    && !x.GetMethod.IsStatic
                    && Attribute.GetCustomAttributes(x, typeof(JsonIgnoreAttribute)).Length == 0)
                .Select(x => new TypeScriptPropertyInfo(x));
        }

        public override IEnumerable<TypeInfo> GetTypes(Assembly assembly)
        {
            yield break;
        }
    }
}