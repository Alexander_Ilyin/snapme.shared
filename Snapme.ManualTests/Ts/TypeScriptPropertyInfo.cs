﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using Newtonsoft.Json;

namespace HrCore.ManualTests.Typescript
{
    public class TypeScriptPropertyInfo : PropertyInfo
    {
        private readonly PropertyInfo propertyInfo;

        public TypeScriptPropertyInfo(PropertyInfo propertyInfo)
        {
            this.propertyInfo = propertyInfo;
        }

        public override string Name
        {
            get
            {
                var attr = Attribute.GetCustomAttribute(propertyInfo, typeof(JsonPropertyAttribute));
                if (attr != null)
                    return ((JsonPropertyAttribute) attr).PropertyName;
                return ToCamelCase(propertyInfo.Name) + "?";
            }
        }

        public override Type DeclaringType => propertyInfo.DeclaringType;

        public override Type ReflectedType => propertyInfo.ReflectedType;

        public override IEnumerable<CustomAttributeData> CustomAttributes => propertyInfo.CustomAttributes;

        public override int MetadataToken => propertyInfo.MetadataToken;

        public override Module Module => propertyInfo.Module;

        public override MemberTypes MemberType => propertyInfo.MemberType;

        public override Type PropertyType
        {
            get
            {
                var t = propertyInfo.PropertyType;
                if (t == typeof(bool?))
                    return typeof(bool);
                if (t.IsGenericType &&
                    t.GetGenericTypeDefinition() == typeof(Nullable<>))
                    t = t.GetGenericArguments()[0];
                if (t == typeof(Guid?))
                    return typeof(string);
                if (t == typeof(Guid))
                    return typeof(string);
                if (t == typeof(Guid[]))
                    return typeof(string[]);
                if (t == typeof(List<Guid>))
                    return typeof(string[]);

                if (t == typeof(Uri))
                    return typeof(string);
                if (t == typeof(byte[]))
                    return typeof(string);
                if (t == typeof(DateTimeOffset))
                    return typeof(string);
                if (t == typeof(DateTimeOffset?))
                    return typeof(string);
                if (t == typeof(DateTime))
                    return typeof(string);

                if (t.IsGenericType &&
                    t.GetGenericTypeDefinition() == typeof(List<>))
                    return t.GetGenericArguments()[0].MakeArrayType();

                if (t.IsGenericType &&
                    t.GetGenericTypeDefinition() == typeof(ICollection<>))
                    return t.GetGenericArguments()[0].MakeArrayType();

                if (t.IsGenericType &&
                    t.GetGenericTypeDefinition() == typeof(IEnumerable<>))
                    return t.GetGenericArguments()[0].MakeArrayType();

                if (t.IsGenericType &&
                    t.GetGenericTypeDefinition() == typeof(Dictionary<,>))
                    return typeof(object);

                if (typeof(Dictionary<string, string>).IsAssignableFrom(t))
                    return typeof(object);

                return t;
            }
        }

        public override PropertyAttributes Attributes => propertyInfo.Attributes;

        public override bool CanRead => propertyInfo.CanRead;

        public override bool CanWrite => propertyInfo.CanWrite;

        public override MethodInfo GetMethod => propertyInfo.GetMethod;

        public override MethodInfo SetMethod => propertyInfo.SetMethod;

        public new bool IsSpecialName => propertyInfo.IsSpecialName;

        public override object[] GetCustomAttributes(bool inherit)
        {
            return propertyInfo.GetCustomAttributes(inherit);
        }

        public override object[] GetCustomAttributes(Type attributeType, bool inherit)
        {
            return propertyInfo.GetCustomAttributes(attributeType, inherit);
        }

        public override bool IsDefined(Type attributeType, bool inherit)
        {
            return propertyInfo.IsDefined(attributeType, inherit);
        }

        public override IList<CustomAttributeData> GetCustomAttributesData()
        {
            return propertyInfo.GetCustomAttributesData();
        }

        private string ToCamelCase(string name)
        {
            return name.Substring(0, 1).ToLowerInvariant() + name.Substring(1);
        }

        public override object GetConstantValue()
        {
            return propertyInfo.GetConstantValue();
        }

        public override object GetRawConstantValue()
        {
            return propertyInfo.GetRawConstantValue();
        }

        public override void SetValue(object obj, object value, BindingFlags invokeAttr, Binder binder, object[] index,
            CultureInfo culture)
        {
            propertyInfo.SetValue(obj, value, invokeAttr, binder, index, culture);
        }

        public override MethodInfo[] GetAccessors(bool nonPublic)
        {
            return propertyInfo.GetAccessors(nonPublic);
        }

        public override MethodInfo GetGetMethod(bool nonPublic)
        {
            return propertyInfo.GetGetMethod(nonPublic);
        }

        public override MethodInfo GetSetMethod(bool nonPublic)
        {
            return propertyInfo.GetSetMethod(nonPublic);
        }

        public override ParameterInfo[] GetIndexParameters()
        {
            return propertyInfo.GetIndexParameters();
        }

        public new object GetValue(object obj)
        {
            return propertyInfo.GetValue(obj);
        }

        public override object GetValue(object obj, object[] index)
        {
            return propertyInfo.GetValue(obj, index);
        }

        public override object GetValue(object obj, BindingFlags invokeAttr, Binder binder, object[] index,
            CultureInfo culture)
        {
            return propertyInfo.GetValue(obj, invokeAttr, binder, index, culture);
        }

        public new void SetValue(object obj, object value)
        {
            propertyInfo.SetValue(obj, value);
        }

        public override void SetValue(object obj, object value, object[] index)
        {
            propertyInfo.SetValue(obj, value, index);
        }

        public override Type[] GetRequiredCustomModifiers()
        {
            return propertyInfo.GetRequiredCustomModifiers();
        }

        public override Type[] GetOptionalCustomModifiers()
        {
            return propertyInfo.GetOptionalCustomModifiers();
        }

        public new MethodInfo[] GetAccessors()
        {
            return propertyInfo.GetAccessors();
        }

        public new MethodInfo GetGetMethod()
        {
            return propertyInfo.GetGetMethod();
        }

        public new MethodInfo GetSetMethod()
        {
            return propertyInfo.GetSetMethod();
        }
    }
}