using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Autofac;
using NUnit.Framework;
using Snapme.Bots.Hosting;
using Snapme.Bots.Services;
using Snapme.Bots.Telegram;
using Snapme.Common;
using Snapme.Common.DI;
using Snapme.Data;
using Snapme.Data.Bots;
using Snapme.Data.Games;
using Snapme.Import;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Snapme.Tests
{
    public class AlexGameTest
    {
        [Test]
        public async Task GetContentTest()
        {
            var copyList = new SpreadsheetReader().GetContent(new TgmBotSettings()
            {
                InfoSpreadsheetId = "1uzDAZ8WabHKVOX2-BOT3DIibwLkB0fKhJufYGWKw9cA",
                InfoSpreadsheedListName = "Sheet1"
            });
        }


        [Test]
        public async Task StartGameBot()
        {
            try
            {
                LogHelper.InitLogger();
                var builder = new SnapmeBuilder(new[]
                {
                    GetType().Assembly,
                    typeof(GameService).Assembly
                });
                var container = builder.CreateContainer();
                var botId = await container.Get(scope => { return this.InitBot(scope); });
                await new TelegramBotHost(container).StartBot(botId);
                await Task.Delay(1000000);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private Task<Guid> InitBot(ILifetimeScope scope)
        {
            foreach (var tgmBot in scope.Resolve<SnapmeContext>().TgmBots)
                tgmBot.IsDelete = true;
            scope.Resolve<SnapmeContext>().SaveChanges();

            var bot = scope.Resolve<SnapmeContext>().AddNew(new TgmBot
            {
                BotTgmKey = "721997667:AAEMmqoW1ygQcOnyCNsLBtyW70WU9buN2EA"
            });

            var game = scope.Resolve<GameService>().CreateGame(bot.Id);
            game.MustBeProcessedByBot = true;
            game.TgmBotId = bot.Id;
            game.SetSettings(new GameSettings
            {
                ValidationChatIds = new List<long> { -390726805 },
                AdminChatId = -388617441,
                TaskSpreadsheedListName = "Game"
            });

            bot.FeedbackChatId = -276847545;
            bot.SetSettings(new TgmBotSettings
            {
                InfoSpreadsheetId = "1uzDAZ8WabHKVOX2-BOT3DIibwLkB0fKhJufYGWKw9cA",
                InfoSpreadsheedListName = "Sheet1",
            });


            scope.Resolve<SnapmeContext>().SaveChanges();
            game.SetStarted("all");
            scope.Resolve<SnapmeContext>().SaveChanges();
            return Task.FromResult(bot.Id);
        }
    }
}