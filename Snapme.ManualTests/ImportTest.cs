using System;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using Snapme.Bots.Games;
using Snapme.Common;
using Snapme.Import;
using Snapme.Tests;

namespace Snapme.ManualTests
{
    public class ImportTest : BotTestBase
    {
        [Test]
        public void Importd()
        {
            var locker = new AsyncLock();
            locker.Run(() => Op(1));
            locker.Run(() => Op(2));
            locker.Run(() => Op(4));
            Thread.Sleep(10000);
            //Console.WriteLine(GrammarFixer.FixGrammar("Привыет вввв  Как дела  ?"));
        }

        private async Task<int> Op(int p0)
        {
            Console.WriteLine("Start " + p0);
            await Task.Delay(700);
            Console.WriteLine("End " + p0);
            return 0;
        }

        [Test]
        public void Import()
        {
            //1036610378626-gokb6u4kdfa1eh9uesq1bnaf1ifk8d3q.apps.googleusercontent.com
            //Kle-mw9lN3YqmoFMeAz5n4nD
//            var records = new GameTaskSpreadsheetImporter().GetData("1mrVLtbVHqDjNAcgAyJOq-6MaZrzdPfdKJpoZqHfZdR4", "Sheet1");
        }
    }
}