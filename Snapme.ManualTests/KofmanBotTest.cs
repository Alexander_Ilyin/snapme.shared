﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Autofac;
using NUnit.Framework;
using Snapme.Bots.Hosting;
using Snapme.Bots.Services;
using Snapme.Common;
using Snapme.Common.DI;
using Snapme.Data;
using Snapme.Data.Bots;
using Snapme.Data.Games;
using Snapme.Import;

namespace Snapme.ManualTests
{
    public class KofmanBotTest
    {
        [Test]
        public async Task StartGameBot()
        {
            try
            {
                LogHelper.InitLogger();
                var builder = new SnapmeBuilder(new[]
                {
                    GetType().Assembly,
                    typeof(GameService).Assembly
                });
                var container = builder.CreateContainer();
                var botId = await container.Get(InitBot);
                await new TelegramBotHost(container).StartBot(botId);
                await Task.Delay(1000000000);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private Task<Guid> InitBot(ILifetimeScope scope)
        {
            var bot = scope.Resolve<SnapmeContext>().AddNew(new TgmBot
            {
                BotTgmKey = "816063718:AAGijYhr3LZZLa9NKMVT8h-Bp7m18dyZyPE" // @TestTheGameBot
            });
            
            bot.FeedbackChatId = -399207860;
            bot.SetSettings(new TgmBotSettings
            {
                // https://docs.google.com/spreadsheets/d/1u-z4fGJ6yxT2dmhh1luGLwv6oneq9Is5nQvHQz0E67A/edit#gid=331490549
                InfoSpreadsheetId = "1u-z4fGJ6yxT2dmhh1luGLwv6oneq9Is5nQvHQz0E67A",
                InfoSpreadsheedListName = "Content",
            });

            scope.Resolve<SnapmeContext>().SaveChanges();
            return Task.FromResult(bot.Id);
        }
    }
}