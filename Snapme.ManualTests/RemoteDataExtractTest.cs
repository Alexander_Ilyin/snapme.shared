using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Autofac.Core.Lifetime;
using NUnit.Framework;
using Snapme.Bots.Games;
using Snapme.Bots.Services;
using Snapme.Common;
using Snapme.Common.DI;
using Snapme.Data;

namespace Snapme.ManualTests
{
    public class RemoteDataExtractTest
    {
        [Test]
        public async Task GetDataTest()
        {
            LogHelper.InitLogger();
            var builder = new SnapmeBuilder(new[]
            {
                GetType().Assembly,
                typeof(GameService).Assembly
            });
            var container = builder.CreateContainer("prod-remote");
            await container.Run(async (scope) =>
            {
                var context = scope.Resolve<SnapmeContext>();
                var guid = Guid.Parse("9449e790-ae61-4680-8094-175db5a087c9");
                var game = context.Games.FirstOrDefault(x => x.Id == guid);
                scope.Resolve<GameTaskImportService>().Import(game);
                context.SaveChanges();
            });
        }
        
    }
}