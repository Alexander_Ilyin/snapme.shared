using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using NUnit.Framework;
using Snapme.Bots.Services;
using Snapme.Bots.Texts;
using Snapme.Common;
using Snapme.Common.DI;
using Snapme.Data.Games;
using Snapme.Import;

namespace Snapme.Tests
{
    public class ContentImportTest
    {
        private IContainer container;

        [SetUp]
        public virtual void Setup()
        {
            LogHelper.InitLogger();
            var builder = new SnapmeBuilder(new[]
            {
                GetType().Assembly,
                typeof(Game).Assembly,
                typeof(BotTestBase).Assembly
            }.Distinct().ToArray());
            container = builder.CreateBuilder("Tests", Guid.NewGuid().ToString()).Build();
        }

        [Test]
        public async Task TestImport()
        {
            var scope = container.BeginLifetimeScope();
            var bot = scope.Resolve<GameService>().CreateBot();
            var service = scope.Resolve<BotContentService>();
            service.ImportRecords(bot, new List<ContentSpreadsheetRecord>
            {
                new ContentSpreadsheetRecord {Type = "A", Description = "B"}
            });
        }
    }
}