using System;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using Snapme.Common;

namespace Snapme.ManualTests
{
    public class LockerTest
    {
        [Test]
        public void FOO()
        {
            var asyncLock = new AsyncLock();
            var v = Guid.NewGuid();
            var err = false;
            for (var d = 0; d < 300; d++)
                new Thread(() =>
                {
                    for (var i = 0; i < 100; i++)
                    {
                        asyncLock.Run(async () =>
                        {
                            var c = Guid.NewGuid();
                            v = c;

                            Console.WriteLine("AAA" + c);
                            if (Guid.NewGuid().GetHashCode() % 10 < 2)
                                throw new Exception("asdasd");
                            await Task.Delay(30);
                            if (Guid.NewGuid().GetHashCode() % 10 < 2)
                                throw new Exception("asdasd");
                            Console.WriteLine("ENDAAA" + c);
                            if (v != c)
                            {
                                err = true;
                                Console.WriteLine("ERRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR");
                            }

                            return 1;
                        });
                    }

                    ;
                }).Start();

            if (err)
                throw new Exception("3");
            Thread.Sleep(100000);
        }
    }
}