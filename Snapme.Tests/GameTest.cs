using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Autofac;
using NUnit.Framework;
using Snapme.Bots.Messages;
using Snapme.Bots.Texts;
using Snapme.Data;
using Snapme.Data.Bots;
using Snapme.Data.Games;
using Snapme.Import;
using Snapme.Testing;

namespace Snapme.Tests
{
    public class GameTest : BotTestBase
    {

        [Test]
        public async Task JoinPlayer1Player2_CheckUsersFiltersByQuery()
        {
            T.CreateAndStartGame();
            await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            await T.JoinPlayerAndApproveSelfie(2, Gender.Girl);
            await T.ProcessFeedbackMessage("users LName2");
            Assert.IsFalse(T.GetFeedbackMessages().Any(x=>x?.ToString()?.Contains("LName1")==true));
        }
        
        [Test]
        public async Task JoinPlayer1Player2_CheckUsersReturnsNumbers()
        {
            T.CreateAndStartGame();
            await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            await T.JoinPlayerAndApproveSelfie(2, Gender.Girl);
            await T.ProcessFeedbackMessage("users");
            T.CheckFeedbackReceivedLike(Regex.Escape("2. <a href='tg://user?id=2'>Name2 LName2</a>"));
        }


        [Test]
        public async Task SendPhotoInGame_CheckFeedbackNotReceived()
        {
            T.CreateAndStartGame();
            T.ProcessUserMessage(T.GetTestUser(1), new MessengerMessage() {PhotoId = "P1"});
            var feedbackMessages = T.GetFeedbackMessages();
            Assert.AreEqual(feedbackMessages.Count(x => x?.ForwardedMsg?.Data=="P1"), 0);
        }
        
        [Test]
        public async Task SendPhotoNotInGame_CheckFeedbackReceived()
        {
            T.ProcessUserMessage(T.GetTestUser(1), new MessengerMessage() {PhotoId = "P1",MsgInfo = new MessangerMsgInfo("P1")});
            var feedbackMessages = T.GetFeedbackMessages();
            Assert.AreEqual(feedbackMessages.Count(x => x?.ForwardedMsg?.Data=="P1"), 1);
        }

        [Test]
        public async Task ReplyToUserWithExtraScores_CheckScoresAdded()
        {
            var testUser = T.GetTestUser(1);
            T.CreateGame();
            await T.ProcessPlayerMessage(testUser, "/start", "B1");
            await T.ProcessPlayerMessage(testUser, "Ответ - баранина", "B1");
            await T.ProcessFeedbackMessage(new MessengerMessage() {ReplyTo = new MessangerMsgInfo("B1"), Text = "+3"});
            T.CheckPlayerReceived(testUser, MsgFormats.YouGotPScore, "3 балла");
            await T.ProcessAdminMessage("send-results");
            T.CheckPlayerReceivedLike(testUser, "Name1 LName1 : 3");
        }
        [Test]
        public async Task ReplyToPlayerWithExtraScores_CheckScoresAdded()
        {
            var testUser = T.GetTestUser(1);
            T.CreateAndStartGame();
            await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            await T.ProcessPlayerMessage(testUser, "Ответ - баранина", "B1");
            await T.ProcessFeedbackMessage(new MessengerMessage() {ReplyTo = new MessangerMsgInfo("B1"), Text = "+5"});
            await T.ProcessAdminMessage("send-results");
            T.CheckPlayerReceived(testUser, MsgFormats.YouGotPScore, "5 баллов");
            T.CheckPlayerReceivedLike(testUser, "Name1 LName1 : 10");
        }
        
        [Test]
        public async Task SendText_CheckFeedbackReceived()
        {
            T.ProcessUserMessage(T.GetTestUser(1), new MessengerMessage() {Text = "sdfsdfsf",MsgInfo = new MessangerMsgInfo("M1")});
            Assert.AreEqual(T.GetFeedbackMessages().Count(x => x.ToString().Contains("M1")), 1);
        }

        [Test]
        public async Task AddUser_JoinNotStartedGame_ReceiveAnnouncement()
        {
            T.CreateGame();
            var player = await T.JoinPlayer();
            T.LogAllMessages();
            T.CheckPlayerReceived(player, MsgFormats.Start);
        }


        [Test]
        public async Task AddUser_JoinGame_ReceiveSelfieTask()
        {
            T.CreateGame();
            var player = await T.JoinPlayer();
            T.CheckPlayerReceived(player, MsgFormats.Start);
        }

        [Test]
        public async Task AddUser_JoinGameAsAnanymous_ReceiveNameRequest()
        {
            T.CreateGame(TestGameFlags.RequireUserName);
            T.CreateAndStartGame();
            var player = T.GetTestUser(1, true);
            T.ProcessUserMessage(player, "/start");
            T.ProcessUserMessage(player, "/ready");
            T.CheckPlayerReceived(player, MsgFormats.SetUsername);
            T.CheckPlayerReceived(player, MsgFormats.WriteWhenReady);
        }


        [Test]
        public async Task JoinGameAsAnanymous_EnterUsernameButNotReady_EnsureNotReceivedTask()
        {
            T.CreateAndStartGame();
            var player = T.GetTestUser(1, true);
            T.ProcessUserMessage(player, "/start");
            T.ProcessUserMessage(player, "/ready");
            T.ProcessUserMessage(player, T.Simple("Готово"));
            Assert.IsNull(T.CurrentTaskId(player));
        }

        [Test]
        public async Task AddUser_JoinGameAsAnanymous_NoTaskIsAssigned()
        {
            T.CreateAndStartGame();
            var player = T.GetTestUser(1, true);
            T.ProcessUserMessage(player, "/start");
            T.ProcessUserMessage(player, "/ready");
            Assert.IsNull(T.CurrentTaskId(player));
        }

        [Test]
        public async Task AddUser_JoinGameAsAnanymous_JoinOtherUser_NoTaskIsAssigned()
        {
            T.CreateAndStartGame();
            var player = T.GetTestUser(1, true);
            T.ProcessUserMessage(player, "/start");
            T.ProcessUserMessage(player, "/ready");

            var player2 = await T.JoinPlayer(2);
            Assert.IsNull(T.CurrentTaskId(player));
        }

        [Test]
        public async Task AddUser_JoinGameAsAnanymous_EnterName_CheckSelfieTaskReceived()
        {
            T.CreateAndStartGame();
            var player = T.GetTestUser(1, true);
            T.ProcessUserMessage(player, "/start");
            player.TgmUserName = "updatedName";
            T.ProcessUserMessage(player, "Готово");
            T.ProcessUserMessage(player, "/ready");
            Assert.AreEqual(T.CurrentTask(player).TaskType, GameTaskType.Selfie);
        }

        [Test]
        public async Task SendMessionPhoto_RespondNo1_CheckPlayerReceivedReason()
        {
            T.CreateAndStartGame();
            var player1 = await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            var player2 = await T.JoinPlayerAndApproveSelfie(2, Gender.Girl);
            var player3 = await T.JoinPlayerAndApproveSelfie(3, Gender.Girl);
            await T.ProcessPlayerMessage(player1, "Photo", "P1");
            await T.ProcessValidatorMessage("yes");
            await T.ProcessPlayerMessage(player1, "Photo2", "P2");
            await T.ProcessValidatorMessage("no1");
            T.CheckPlayerReceived(player1, "Фото отклонено. Не выполнено условие: Вы играете на трубе");
        }

        [Test]
        public async Task SendMessionPhoto_RespondNo1_SendAnother_Approve_CheckGotMission()
        {
            T.CreateAndStartGame();
            var player1 = await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            var player2 = await T.JoinPlayerAndApproveSelfie(2, Gender.Girl);
            var player3 = await T.JoinPlayerAndApproveSelfie(3, Gender.Girl);
            await T.ProcessPlayerMessage(player1, "Photo", "P1");
            await T.ProcessValidatorMessage("yes");
            await T.ProcessPlayerMessage(player1, "Photo2", "P2");
            await T.ProcessValidatorMessage("no1");
            await T.ProcessPlayerMessage(player1, "Photo3", "P3");
            await T.ProcessValidatorMessage("yes");
            Assert.AreEqual(T.GetPartner(player1), player3);
        }
        
        [Test]
        public async Task FinihGame_ReceivePhoto_CheckValidatorNotReceived()
        {
            T.CreateAndStartGame();
            var player1 = await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            var player2 = await T.JoinPlayerAndApproveSelfie(2, Gender.Girl);
            await T.ProcessAdminMessage("finish");
            await T.ProcessPlayerMessage(player1, "XPhoto", "XPhoto1");
            Assert.IsTrue(T.GetValidatorMessages().All(x => !x.ToString().Contains("XPhoto1")));
        }
        
        [Test]
        public async Task SendSelfie_CheckModeratorReceivedText()
        {
            T.CreateAndStartGame();
            var player = await T.JoinPlayer();
            T.CheckPlayerReceived(player, "<b>Сделай селфи с небом.</b>");
            await T.ProcessPlayerMessage(player, "PhotoX", "F1");
            T.LogAllMessages();
            T.CheckValidatorReceivedPhoto("PhotoX");
            T.CheckValidatorReceived(
                "Сделай селфи с небом.[btn:yes boy|yes girl]" +
                "[btn:no На селфи должен быть один игрок]" +
                "[btn:no Фото не селфи|no Условие не выполнено]");
        }

        [Test]
        public async Task SendSelfie_ModeratorDecline_CheckNoSelfie()
        {
            T.CreateAndStartGame();
            var player = await T.JoinPlayer();
            await T.ProcessPlayerMessage(player, "Photo", "F1");
            await T.ProcessValidatorMessage("no Это не селфи");
            Assert.IsNull(T.SelfiePhotoId(player));
        }

        [Test]
        public async Task SendSelfie_ModeratorDecline_CheckNextTaskTakenFromQueue()
        {
            T.CreateAndStartGame();
            var player1 = await T.JoinPlayer(1);
            var player2 = await T.JoinPlayer(2);
            await T.ProcessPlayerMessage(player1, "Photo1", "F1");
            await T.ProcessPlayerMessage(player2, "Photo2", "F2");
            await T.ProcessValidatorMessage("no Это не селфи");
            T.CheckValidatorReceivedPhoto("Photo2");
            T.CheckValidatorReceived(
                "Сделай селфи с небом.[btn:yes boy|yes girl]" +
                "[btn:no На селфи должен быть один игрок]" +
                "[btn:no Фото не селфи|no Условие не выполнено]");
        }

        [Test]
        public async Task SendSelfie_ValidatorIsValidating_SendSelfie_CheckPlayerNotifiedPhotoBeingChecked()
        {
            T.CreateAndStartGame();
            var player = await T.JoinPlayer();
            await T.ProcessPlayerMessage(player, "Photo1", "F1");
            Assert.AreEqual(T.CurrentTask(player).GameTaskStatus, GameTaskStatus.Validating);
            await T.ProcessPlayerMessage(player, "Photo2", "F2");
            T.CheckPlayerReceived(player, MsgFormats.AlreadyCheckingYourPhoto);
        }

        [Test]
        public async Task ValidatorBusy_SendSelfie_SendSelfie_CheckValidatorRecievedLatest()
        {
            T.CreateAndStartGame();
            var player = await T.JoinPlayer(1);
            var player2 = await T.JoinPlayer(2);
            await T.ProcessPlayerMessage(player2, "PhotoX", "FX");

            await T.ProcessPlayerMessage(player, "Photo1", "F1");
            await T.ProcessPlayerMessage(player, "Photo2", "F2");
            await T.ProcessValidatorMessage("yes boy");
            T.LogAllMessages();
            T.CheckValidatorReceivedPhoto("Photo2");
        }

        [Test]
        public async Task CheckUserNameNotRequestedIfSettingOff()
        {
            T.CreateAndStartGame();
            var player = await T.JoinPlayer(1);
            var player2 = await T.JoinPlayer(2);
            await T.ProcessPlayerMessage(player2, "PhotoX", "FX");

            await T.ProcessPlayerMessage(player, "Photo1", "F1");
            await T.ProcessPlayerMessage(player, "Photo2", "F2");
            await T.ProcessValidatorMessage("yes boy");
            T.LogAllMessages();
            T.CheckValidatorReceivedPhoto("Photo2");
        }

        [Test]
        public async Task SendMissionPhoto_CheckPhotoForwardedToPartner()
        {
            T.CreateAndStartGame();
            var boy1 = await T.JoinPlayer(1);
            var girl2 = await T.JoinPlayer(2);
            await T.ProcessPlayerMessage(boy1, "Photo1", "F1");
            await T.ProcessValidatorMessage("yes boy");

            await T.ProcessPlayerMessage(girl2, "Photo2", "F2");
            await T.ProcessValidatorMessage("yes girl");

            T.LogAllMessages();
            await T.ProcessPlayerMessage(boy1, "Photo3", "F3");
            T.CheckPlayerReceivedPhoto(girl2, "Photo1", null);
        }


        [Test]
        public async Task SendSelfie_ModeratorAccept_NoGender__CheckGenderRequested()
        {
            T.CreateAndStartGame();
            var player = await T.JoinPlayer();
            await T.ProcessPlayerMessage(player, "Photo", "F1");
            await T.ProcessValidatorMessage("yes");
            T.LogAllMessages();
            T.CheckValidatorReceived(MsgFormats.SpecifyBoyGirl);
        }

        [Test]
        public async Task SendSelfie_ModeratorReject_CheckMissionTheSame()
        {
            T.CreateAndStartGame();
            var player = await T.JoinPlayer();
            var currentTaskId = T.CurrentTask(player).Id;
            await T.ProcessPlayerMessage(player, "Photo", "F1");
            await T.ProcessValidatorMessage("no Bad!");
            var newTaskId = T.CurrentTaskId(player);
            Assert.AreEqual(T.CurrentTask(player).GameTaskStatus, GameTaskStatus.NotReady);
            Assert.AreEqual(newTaskId, currentTaskId);
        }

        [Test]
        public async Task Join_Check_YourFirstTaskMsg_Received()
        {
            T.CreateAndStartGame();
            var player = await T.JoinPlayer();
            T.CheckPlayerReceived(player, MsgFormats.FirstTask);
        }


        [Test]
        public async Task Check_ListAll()
        {
            T.CreateAndStartGame();
            var player1 = await T.JoinPlayer(1);
            var player2 = await T.JoinPlayer(2);
            var player3 = await T.JoinPlayer(3);
            await T.ProcessFeedbackMessage("users");
            Assert.IsTrue(T.GetFeedbackMessages().Any(x => x.Text?.Contains("3") == true));
        }

        [Test]
        public async Task SendSelfie_ModeratorAcceptAsBoy_CheckClientReceivedConfirm()
        {
            T.CreateAndStartGame();
            var player = await T.JoinPlayer();
            await T.ProcessPlayerMessage(player, "Photo", "F1");
            await T.ProcessValidatorMessage("yes boy");
            T.CheckPlayerReceived(player, MsgFormats.PhotoAccepted);
        }


        [Test]
        public async Task SendManySelfies_ModeratorReceivedSingleSelfie()
        {
            T.CreateAndStartGame();
            var player1 = await T.JoinPlayer(1);
            var player2 = await T.JoinPlayer(2);
            var player3 = await T.JoinPlayer(3);
            await T.ProcessPlayerMessage(player1, "Photo 1", "F.1");
            await T.ProcessPlayerMessage(player2, "Photo 2", "F.2");
            await T.ProcessPlayerMessage(player3, "Photo 3", "F.3");
            Assert.AreEqual(T.GetValidatorMessages().Count(x => x.PhotoId != null || x.Text?.Contains("Photo") == true),
                1);
        }

        [Test]
        public async Task Add4Players_EnsureBoyGirlMatch()
        {
            T.CreateAndStartGame();

            var boy1 = await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            var boy2 = await T.JoinPlayerAndApproveSelfie(2, Gender.Boy);
            var girl1 = await T.JoinPlayerAndApproveSelfie(3, Gender.Girl);
            var girl2 = await T.JoinPlayerAndApproveSelfie(4, Gender.Girl);

            Assert.AreEqual(T.CurrentTask(boy1).TaskType, GameTaskType.Meeting);
            Assert.AreEqual(T.CurrentTask(boy2).TaskType, GameTaskType.Meeting);
            Assert.AreEqual(T.CurrentTask(girl1).TaskType, GameTaskType.Meeting);
            Assert.AreEqual(T.CurrentTask(girl2).TaskType, GameTaskType.Meeting);

            Assert.IsTrue(T.GetPartner(boy1) == girl1 || T.GetPartner(boy1) == girl2);
            Assert.IsTrue(T.GetPartner(boy2) == girl1 || T.GetPartner(boy2) == girl2);
            Assert.IsTrue(T.GetPartner(girl1) == boy1 || T.GetPartner(girl1) == boy2);
        }

        [Test]
        public async Task JoinBotBeforeGame_StartGame_CheckWelcomeReceived()
        {
            var boy1 = await T.JoinPlayer(1);
            T.CreateAndStartGame();
            T.CheckPlayerReceived(boy1, MsgFormats.GameStarted);
        }

        [Test]
        public async Task Add2Boys_Force_EnsureReceivedMeeting()
        {
            T.CreateAndStartGame();
            var boy1 = await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            var boy2 = await T.JoinPlayerAndApproveSelfie(2, Gender.Boy);
            await T.ProcessAdminMessage("force");
            Assert.AreEqual(T.CurrentTask(boy1).TaskType, GameTaskType.Meeting);
            Assert.AreEqual(T.CurrentTask(boy2).TaskType, GameTaskType.Meeting);
        }

        [Test]
        public async Task CheckCodes()
        {
            Assert.AreEqual(MsgFormats.PartnerIsSame.GetCode(), nameof(MsgFormats.PartnerIsSame));
        }

        [Test]
        public async Task Add2Players_EnsurePartnerReceivesPhoto()
        {
            T.CreateAndStartGame();
            var boy1 = await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            var girl1 = await T.JoinPlayerAndApproveSelfie(2, Gender.Girl);
            Assert.IsTrue(T.MessagesFor(boy1).Any(m => m.PhotoId?.Contains("Photo of 2") == true));
            Assert.IsTrue(T.MessagesFor(girl1).Any(m => m.PhotoId?.Contains("Photo of 1") == true));
        }

        [Test]
        public async Task AddNonGameUser_SendMessageAll_CheckReceived()
        {
            var user = T.ProcessUserMessage(1, "/start");
            await T.ProcessFeedbackMessage("message-all Hi guys!");
            T.CheckPlayerReceived(user, "Hi guys!");
        }

        [Test]
        public async Task AddGameUser_SendMessageAll_CheckReceived()
        {
            T.CreateAndStartGame();
            var user = T.ProcessUserMessage(1, "/start");
            await T.ProcessFeedbackMessage("message-all Hi guys!");
            T.CheckPlayerReceived(user, "Hi guys!");
        }

        [Test]
        public async Task AddPlayer_SendMessagePlayers_CheckReceived()
        {
            T.CreateAndStartGame();
            var user = await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            await T.ProcessAdminMessage("message-players Hi guys!");
            T.CheckPlayerReceived(user, "Hi guys!");
        }

        [Test]
        public async Task AddUser_SendMessagePlayers_CheckNotReceived()
        {
            var user = T.ProcessUserMessage(1, "/start");
            T.CreateAndStartGame();
            await T.ProcessAdminMessage("message-players Hi guys!");
            Assert.IsFalse(T.MessagesFor(user).Any(x => x.Text == "Hi guys!"));
        }

        [Test]
        public async Task GameWithMeetings_PlayersCompleteSelfies_EnsureTheyGetMeetingTaskWithHint()
        {
            T.CreateAndStartGame();
            var boy1 = await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            var girl1 = await T.JoinPlayerAndApproveSelfie(2, Gender.Girl);

            T.CheckPlayerReceived(boy1, MsgFormats.MeetingHint);
            T.CheckPlayerReceived(boy1, MsgFormats.YourTask);
            T.CheckPlayerReceived(boy1, "<b>Сделайте селфи со светофором.</b>");
            T.CheckPlayerReceived(girl1, MsgFormats.MeetingHint);
            T.CheckPlayerReceived(girl1, MsgFormats.YourTask);
            T.CheckPlayerReceived(girl1, "<b>Сделайте селфи со светофором.</b>");
        }

        [Test]
        public async Task GameWithoutMeetings_PlayersCompleteSelfies_EnsureTheyGetMissionHint()
        {
            T.CreateGame(TestGameFlags.Default);
            T.CreateAndStartGame();
            var boy1 = await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            var girl1 = await T.JoinPlayerAndApproveSelfie(2, Gender.Girl);
            T.CheckPlayerReceived(boy1, MsgFormats.MeetingHint);
        }

        [Test]
        public async Task GameWithoutMeetings__EnsureTheyGetMissionHintOnce()
        {
            T.CreateGame(TestGameFlags.Default);
            T.CreateAndStartGame();
            var boy1 = await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            var girl1 = await T.JoinPlayerAndApproveSelfie(2, Gender.Girl);
            var girl2 = await T.JoinPlayerAndApproveSelfie(3, Gender.Girl);
            await T.ProcessPlayerMessage(boy1, "PhotoOfMeeting", "");
            await T.ProcessValidatorMessage("yes");
            await T.ProcessPlayerMessage(boy1, "PhotoOfMeeting", "");
            await T.ProcessValidatorMessage("yes");
            Assert.AreEqual(T.MessagesFor(boy1).Where(x => x.TextFormat == MsgFormats.MeetingHint).Count(), 1);
        }

        [Test]
        public async Task GameWithMeetings_PlayersCompleteMeeting_EnsureTheyGetMissionWithHint()
        {
            T.CreateAndStartGame();
            var boy1 = await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            var girl1 = await T.JoinPlayerAndApproveSelfie(2, Gender.Girl);
            await T.ProcessPlayerMessage(boy1, "PhotoOfMeeting", "");
            await T.ProcessValidatorMessage("yes");

            T.CheckPlayerReceived(boy1, MsgFormats.PhotoAccepted);
            T.CheckPlayerReceived(boy1, MsgFormats.YourTask);
            T.CheckPlayerReceivedLike(boy1, @"Фото с условиями\: Вы .* Вы .*");
            T.CheckPlayerReceived(boy1, MsgFormats.PartnerIsSame);
            T.CheckPlayerReceived(boy1, MsgFormats.MissionHint);

            T.CheckPlayerReceived(girl1, MsgFormats.PhotoAccepted);
            T.CheckPlayerReceived(girl1, MsgFormats.YourTask);
            T.CheckPlayerReceivedLike(girl1, @"Фото с условиями\: Вы .* Вы .*");
            T.CheckPlayerReceived(girl1, MsgFormats.PartnerIsSame);
            T.CheckPlayerReceived(girl1, MsgFormats.MissionHint);
        }

        [
            Test]
        public async Task GameWithMeetings_PlayerCompletesMission_EnsureHeGetMeetingWithoutHint()
        {
            T.CreateAndStartGame();
            var boy1 = await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            var girl1 = await T.JoinPlayerAndApproveSelfie(2, Gender.Girl);
            var girl2 = await T.JoinPlayerAndApproveSelfie(3, Gender.Girl);
            await T.ProcessPlayerMessage(boy1, "PhotoOfMeeting", "");
            await T.ProcessValidatorMessage("yes");
            await T.ProcessPlayerMessage(girl1, "PhotoOfMission", "");
            await T.ProcessValidatorMessage("yes");
        }

        [
            Test]
        public async Task GameWithoutMeetings_PlayerCompletesMission_EnsureHeGetsMissionWithoutHints()
        {
            T.CreateGame(TestGameFlags.Default);
            T.CreateAndStartGame();
            var boy1 = await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            var girl1 = await T.JoinPlayerAndApproveSelfie(2, Gender.Girl);
            var boy2 = await T.JoinPlayerAndApproveSelfie(3, Gender.Boy);
            await T.ProcessPlayerMessage(girl1, "PhotoOfMeeting", "");
            await T.ProcessValidatorMessage("yes");
            await T.ProcessPlayerMessage(boy1, "PhotoOfMission", "");
            await T.ProcessValidatorMessage("yes");

            Assert.LessOrEqual(T.MessagesFor(boy1).Count(x => x.TextFormat == MsgFormats.MissionHint), 1);
            Assert.LessOrEqual(T.MessagesFor(boy2).Count(x => x.TextFormat == MsgFormats.MissionHint), 1);
            Assert.LessOrEqual(T.MessagesFor(girl1).Count(x => x.TextFormat == MsgFormats.MissionHint), 1);

            Assert.LessOrEqual(T.MessagesFor(boy1).Count(x => x.TextFormat == MsgFormats.MeetingHint), 1);
            Assert.LessOrEqual(T.MessagesFor(boy2).Count(x => x.TextFormat == MsgFormats.MeetingHint), 1);
            Assert.LessOrEqual(T.MessagesFor(girl1).Count(x => x.TextFormat == MsgFormats.MeetingHint), 1);
        }

        [
            Test]
        public async Task If2PlayersGotSelfie_EnsureNextMissionIsObjective()
        {
            T.CreateAndStartGame();
            var boy1 = await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            var girl1 = await T.JoinPlayerAndApproveSelfie(2, Gender.Girl);
            Assert.AreEqual(T.CurrentTask(boy1).TaskType, GameTaskType.Meeting);
            await T.ProcessPlayerMessage(boy1, "Photo W Girl", "");
            await T.ProcessValidatorMessage("yes");
            Assert.AreEqual(T.CurrentTask(boy1).TaskType, GameTaskType.Mission);
            Assert.AreEqual(T.GetPartner(boy1), girl1);
        }

        [Test]
        public async Task ChecMissonsAssignedToAnonimousUsers()
        {
            T.CreateGame(TestGameFlags.WithMeetings);
            T.CreateAndStartGame();
            var boy1 = T.GetTestUser(1, true);
            T.ProcessUserMessage(boy1, "/start");
            T.ProcessUserMessage(boy1, "/ready");

            await T.ProcessPlayerMessage(boy1, "ready", "M1");
            Assert.AreEqual(T.CurrentTask(boy1).TaskType, GameTaskType.Selfie);
        }

        [Test]
        public async Task CreateFakeGirl_CheckAssignedToMeetingAndMission()
        {
            T.CreateGame(TestGameFlags.WithMeetings);
            T.CreateAndStartGame();
            var boy1 = await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            await T.ProcessAdminMessage(new MessengerMessage() {PhotoId = "P1", Text = "offline Masha"});
            await T.ProcessValidatorMessage("yes girl");
            Assert.AreEqual(T.CurrentTask(boy1).TaskType, GameTaskType.Meeting);
            await T.ProcessPlayerMessage(boy1, "Photo2", "P2");
            await T.ProcessValidatorMessage("yes");
            Assert.AreEqual(T.CurrentTask(boy1).TaskType, GameTaskType.Mission);
        }

        [Test]
        public async Task CreateFakeBoy_CheckModeratorReceivedSelfie()
        {
            T.CreateGame(TestGameFlags.WithMeetings);
            T.CreateAndStartGame();
            var boy1 = await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            await T.ProcessAdminMessage(new MessengerMessage() {PhotoId = "PhotoFake1", Text = "offline Masha"});
            T.CheckValidatorReceived("PhotoFake1");
        }

        [Test]
        public async Task CreateFakeBoy_KickFakeBoy_CheckNoTasks()
        {
            T.CreateGame(TestGameFlags.WithMeetings);
            T.CreateAndStartGame();
            await T.ProcessAdminMessage(new MessengerMessage() {PhotoId = "PhotoFake1", Text = "offline Petya"});
            T.CheckValidatorReceived("PhotoFake1");
            await T.ProcessValidatorMessage("yes boy");
            await T.ProcessAdminMessage("kick 1");
            var girl2 = await T.JoinPlayerAndApproveSelfie(2, Gender.Girl);
            Assert.IsNull(T.CurrentTask(girl2));
        }
        
        [Test]
        public async Task JoinSomePlayers_CheckStatus()
        {
            T.CreateGame(TestGameFlags.WithMeetings);
            T.CreateAndStartGame();
            var boy1 = await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            var boy2 = await T.JoinPlayerAndApproveSelfie(2, Gender.Boy);
            var boy3 = await T.JoinPlayerAndApproveSelfie(3, Gender.Boy);
            var girl1 = await T.JoinPlayerAndApproveSelfie(4, Gender.Girl);
            var girl2 = await T.JoinPlayerAndApproveSelfie(5, Gender.Girl);
            await T.ProcessPlayerMessage(boy1, "Photo1", "p1");
            await T.ProcessPlayerMessage(boy2, "Photo2", "p1");
            await T.ProcessValidatorMessage("yes");
            await T.ProcessValidatorMessage("yes");
            await T.JoinPlayer(6);
            await T.ProcessAdminMessage("status");
            T.CheckAdminReceivedLike(
                Regex.Escape("<a href='tg://user?id=6'>Name6 LName6</a> 6,  (0:0) Сделай селфи с небом."));
        }

        [Test]
        public async Task GameIsFinished_ThenFullResultsAreSentToAdmin()
        {
            T.CreateGame(TestGameFlags.WithMeetings);
            T.CreateAndStartGame();
            for (int i = 0; i < 20; i++)
                await T.JoinPlayerAndApproveSelfie(i, Gender.Boy);
            await T.ProcessAdminMessage("finish");
            T.CheckAdminReceivedLike(Regex.Escape("<a href='tg://user?id=2'>Name2 LName2</a> : 1"));
        }


        [Test]
        public async Task GameWithoutMeetings_PostSelfie_CheckFirstTaskIsMission()
        {
            T.CreateGame(TestGameFlags.Default);
            T.CreateAndStartGame();
            var boy1 = await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            var girl1 = await T.JoinPlayerAndApproveSelfie(3, Gender.Girl);

            Assert.AreEqual(T.CurrentTask(boy1).TaskType, GameTaskType.Mission);
            Assert.AreEqual(T.CurrentTask(girl1).TaskType, GameTaskType.Mission);
        }

        [Test]
        public async Task PlayerJoinedAfterFinal_CheckReceivedIsTooLate()
        {
            T.CreateAndStartGame();
            await T.ProcessAdminMessage("final");
            var girl1 = await T.JoinPlayer(1);
            T.CheckPlayerReceived(girl1, MsgFormats.TooLateToPlay);
        }

        [Test]
        public async Task AddGameWithNoFinalPlace_WaitTask_SetFinal__CheckReceived_ItWasFinalTask()
        {
            T.CreateGame(TestGameFlags.WithMeetings | TestGameFlags.WithNoFinalActivities);
            T.CreateAndStartGame();
            var girl1 = await T.JoinPlayerAndApproveSelfie(1, Gender.Girl);
            var boy2 = await T.JoinPlayerAndApproveSelfie(2, Gender.Boy);
            var girl3 = await T.JoinPlayerAndApproveSelfie(3, Gender.Girl);
            await T.ProcessAdminMessage("final");
            T.CheckPlayerReceived(girl3, MsgFormats.ItWasFinalTask);
        }

        [Test]
        public async Task AddGameWithNoFinalPlace_SelfieIsInProgress_SetFinal__CheckReceived_TaskCancelled()
        {
            T.CreateGame(TestGameFlags.WithMeetings | TestGameFlags.WithNoFinalActivities);
            T.CreateAndStartGame();
            var girl1 = await T.JoinPlayer(1);
            await T.ProcessAdminMessage("final");
            T.CheckPlayerReceived(girl1, MsgFormats.TaskCancelled);
        }

        [Test]
        public async Task
            AddGameWithNoFinalPlace_MissionInProgress_SetFinal__FinishMission_CheckReceivedItWasFinalTask()
        {
            T.CreateGame(TestGameFlags.WithMeetings | TestGameFlags.WithNoFinalActivities);
            T.CreateAndStartGame();
            var girl1 = await T.JoinPlayerAndApproveSelfie(1, Gender.Girl);
            var boy2 = await T.JoinPlayerAndApproveSelfie(2, Gender.Boy);
            await T.ProcessAdminMessage("final");
            await T.ProcessPlayerMessage(boy2, "Photo1", "P1");
            await T.ProcessValidatorMessage("yes");
            T.CheckPlayerReceived(girl1, MsgFormats.ItWasFinalTask);
            T.CheckPlayerReceived(boy2, MsgFormats.ItWasFinalTask);
        }

        [Test]
        public async Task AddPlayerWithNoTasks_FinishGame_CheckNoExceptions()
        {
            T.CreateAndStartGame();
            await T.JoinPlayer(1);
            var girl1 = await T.JoinPlayerAndApproveSelfie(2, Gender.Girl);
            await T.ProcessAdminMessage("final");
            var girl3 = await T.JoinPlayerAndApproveSelfie(3, Gender.Girl);
            await T.ProcessAdminMessage("finish");
        }

        [Test]
        public async Task AddPlayerWithNoTasks_FinishGame_CheckNoMissionWillbeAssigned()
        {
            T.CreateGame(TestGameFlags.Default);
            T.CreateAndStartGame();
            var boy1 = await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            var girl1 = await T.JoinPlayerAndApproveSelfie(2, Gender.Girl);
            var girl2 = await T.JoinPlayerAndApproveSelfie(3, Gender.Girl);
            await T.ProcessPlayerMessage(boy1, "Photo1", "P1");
            await T.ProcessValidatorMessage("yes");
            await T.ProcessAdminMessage("finish");
        }

        [Test]
        public async Task GameWithoutMeetings_CheckSecondMissionWithDifferentPartner()
        {
            T.CreateGame(TestGameFlags.Default);
            T.CreateAndStartGame();
            var boy1 = await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            var girl1 = await T.JoinPlayerAndApproveSelfie(2, Gender.Girl);
            var girl2 = await T.JoinPlayerAndApproveSelfie(3, Gender.Girl);

            var p1 = T.GetPartner(boy1);
            await T.ProcessPlayerMessage(boy1, "Photo1", "P1");
            await T.ProcessValidatorMessage("yes");
            var p2 = T.GetPartner(boy1);
            Assert.AreNotEqual(p1, p2);
        }

        [Test]
        public async Task CheckParthnersChange()
        {
            T.CreateAndStartGame();
            var boy1 = await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            var boy2 = await T.JoinPlayerAndApproveSelfie(2, Gender.Boy);
            var girl1 = await T.JoinPlayerAndApproveSelfie(3, Gender.Girl);
            var girl2 = await T.JoinPlayerAndApproveSelfie(4, Gender.Girl);

            await T.ProcessPlayerMessage(boy1, "Photo Selfie W GirlA", "F1");
            await T.ProcessPlayerMessage(boy2, "Photo Selfie W GirlB", "F2");
            var boy1P = T.GetPartner(boy1);
            var boy2P = T.GetPartner(boy2);

            await T.ProcessValidatorMessage("yes");
            await T.ProcessValidatorMessage("yes");

            await T.ProcessPlayerMessage(boy1, "Photo Objective W GirlA", "F3");
            await T.ProcessValidatorMessage("yes");
            await T.ProcessPlayerMessage(boy2, "Photo Objective W GirlB", "F4");
            await T.ProcessValidatorMessage("yes");

            var newBoy1P = T.GetPartner(boy1);
            var newBoy2P = T.GetPartner(boy2);

            Assert.AreEqual(boy2P, newBoy1P);
            Assert.AreEqual(boy1P, newBoy2P);
        }

        [
            Test]
        public async Task IfFinal_LonelyPlayersGotMission()
        {
            T.CreateAndStartGame();
            var boy1 = await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            var boy2 = await T.JoinPlayerAndApproveSelfie(2, Gender.Boy);
            var girl1 = await T.JoinPlayerAndApproveSelfie(3, Gender.Girl);
            await T.ProcessPlayerMessage(girl1, "Photo Selfie W GirlA", "F1");
            await T.ProcessValidatorMessage("yes");
            await T.ProcessAdminMessage("final");
            await T.ProcessAdminMessage("force");
            Assert.AreEqual(T.CurrentTask(boy2).TaskType, GameTaskType.FinalSingle);
        }

        [
            Test]
        public async Task IfAdminKicksPlayer_TaskGotCanceled()
        {
            T.CreateAndStartGame();
            var boy1 = await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            var girl1 = await T.JoinPlayerAndApproveSelfie(3, Gender.Girl);
            await T.ProcessAdminMessage("kick @" + boy1.TgmUserName);
            Assert.AreEqual(T.CurrentTaskId(girl1), null);
            Assert.AreEqual(T.CurrentTaskId(boy1), null);
            T.CheckPlayerReceived(boy1, MsgFormats.TaskCancelled);
            T.CheckPlayerReceived(boy1, MsgFormats.YouLeftTheGame);
            T.CheckPlayerReceived(girl1, MsgFormats.TaskCancelled);
            T.CheckPlayerReceived(girl1, MsgFormats.YouWillGetNewTaskInAMinute);
        }

        [Test]
        public async Task IfAdminSoftKicksPlayer_NewTaskNotAssigned()
        {
            T.CreateAndStartGame();
            var boy1 = await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            var girl1 = await T.JoinPlayerAndApproveSelfie(2, Gender.Girl);
            var girl2 = await T.JoinPlayerAndApproveSelfie(3, Gender.Girl);
            await T.ProcessAdminMessage("softkick @" + boy1.TgmUserName);
            Assert.IsNotNull(T.CurrentTaskId(girl1));
            Assert.IsNotNull(T.CurrentTaskId(boy1));
            await T.ProcessPlayerMessage(boy1, "Photo2", "M2");
            await T.ProcessValidatorMessage("yes");
            Assert.IsNull(T.CurrentTaskId(boy1));
        }

        [Test]
        public async Task CheckGameTesterWorks()
        {
            T.CreateGame(TestGameFlags.WithMeetings);
            T.CreateAndStartGame();
            var gameLogs = T.InScope(x =>
            {
                return new GameSampler(x.Resolve<SnapmeContext>()).TestGame(T.gameId.Value);
            });
            Assert.IsNotNull(gameLogs);
        }

        [Test]
        public async Task SendMessageAll_CheckPlayersReceived()
        {
            T.CreateAndStartGame();
            var boy1 = await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            var girl1 = await T.JoinPlayerAndApproveSelfie(2, Gender.Girl);
            var girl2 = await T.JoinPlayerAndApproveSelfie(3, Gender.Girl);
            await T.ProcessAdminMessage("message-players Hello all,Guys!");
            T.CheckPlayerReceived(boy1, "Hello all,Guys!");
            T.CheckPlayerReceived(girl1, "Hello all,Guys!");
            T.CheckPlayerReceived(girl2, "Hello all,Guys!");
        }

        [Test]
        public async Task SendShuffle_CheckPlayersReceived()
        {
            
            T.CreateAndStartGame();
            var boy1 = await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            var girl1 = await T.JoinPlayerAndApproveSelfie(2, Gender.Girl);
            var girl2 = await T.JoinPlayerAndApproveSelfie(3, Gender.Girl);
            await T.ProcessAdminMessage("shuffle 3 XPhoto1 XPhoto2 XPhoto3");
        }


        [Test]
        public async Task IfFeedbackRepliesWithSoftKick_PlayerKicked()
        {
            T.CreateAndStartGame();
            var boy1 = await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            var girl1 = await T.JoinPlayerAndApproveSelfie(2, Gender.Girl);
            var girl2 = await T.JoinPlayerAndApproveSelfie(3, Gender.Girl);
            await T.ProcessPlayerMessage(boy1, "BAD WORDS", "BW");
            await T.ProcessValidatorMessage(new MessengerMessage
                {Text = "softkick", ReplyTo = new MessangerMsgInfo("BW")});
            Assert.IsNotNull(T.CurrentTaskId(girl1));
            Assert.IsNotNull(T.CurrentTaskId(boy1));
            await T.ProcessPlayerMessage(boy1, "Photo2", "M2");
            await T.ProcessValidatorMessage("yes");
            Assert.IsNull(T.CurrentTaskId(boy1));
        }

        [Test]
        public async Task IfFinal_PairsDoNotBreakUp()
        {
            T.CreateAndStartGame();
            var boy1 = await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            var boy2 = await T.JoinPlayerAndApproveSelfie(2, Gender.Boy);
            var girl1 = await T.JoinPlayerAndApproveSelfie(3, Gender.Girl);
            var firstParthner = T.GetPartner(girl1);
            await T.ProcessAdminMessage("final");
            await T.ProcessPlayerMessage(girl1, "Photo Selfie W GirlA", "F1");
            await T.ProcessValidatorMessage("yes");
            Assert.AreEqual(T.CurrentTask(girl1).TaskType, GameTaskType.Final);
            Assert.AreEqual(T.GetPartner(girl1), firstParthner);
        }

        [Test]
        public async Task CheckTasks()
        {
            T.CreateAndStartGame();
            await T.ProcessAdminMessage("test-tasks");
            T.CheckAdminReceivedLike("Сделай селфи");
        }

        [Test]
        public async Task AdminSendsResults_CheckPlayersReceivedAndAdminNotified()
        {
            T.CreateAndStartGame();
            var boy1 = await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            var girl1 = await T.JoinPlayerAndApproveSelfie(3, Gender.Girl);
            await T.ProcessPlayerMessage(girl1, "Photo Selfie W GirlA", "F1");
            await T.ProcessValidatorMessage("yes");
            await T.ProcessAdminMessage("send-results");
            Assert.AreEqual(T.MessagesFor(boy1).Count(x => x.TextFormat == MsgFormats.CurrentResults), 1);
            Assert.AreEqual(T.MessagesFor(boy1).Count(x => x.TextFormat == MsgFormats.GameResults), 0);
        }

        [Test]
        public async Task ModeratorUpdatesPhase_CheckNextMissionInNextPhase()
        {
            T.CreateAndStartGame();
            var boy1 = await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            var girl2 = await T.JoinPlayerAndApproveSelfie(2, Gender.Girl);
            var girl3 = await T.JoinPlayerAndApproveSelfie(3, Gender.Girl);

            await T.ProcessPlayerMessage(boy1, "Photo", "F1");
            await T.ProcessAdminMessage("phase 2");
            await T.ProcessValidatorMessage("yes");
            await T.ProcessAdminMessage("status");
            await T.ProcessPlayerMessage(boy1, "PhotoX2", "X2");
            Assert.AreEqual(T.CurrentTaskPhase(boy1), 2);
        }

        [Test]
        public async Task ModeratorFinishesGame_CheckPlayersReceiveStats()
        {
            T.CreateAndStartGame();
            var boy = await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            var girl = await T.JoinPlayerAndApproveSelfie(2, Gender.Girl);
            var boy2 = await T.JoinPlayerAndApproveSelfie(3, Gender.Boy);

            await T.ProcessPlayerMessage(boy, "Photo 1", "F1"); // SELFIE 1+2
            await T.ProcessValidatorMessage("yes");

            await T.ProcessPlayerMessage(girl, "Photo 2", "F2"); // MISSION 1+2
            await T.ProcessValidatorMessage("yes");

            await T.ProcessPlayerMessage(girl, "Photo 3", "F3"); // MISSION 1+3
            await T.ProcessValidatorMessage("yes");

            await T.ProcessAdminMessage("finish");
            var lastMessage = T.MessagesFor(boy).LastOrDefault();
            Assert.IsTrue(lastMessage?.ToString().Contains("Name3 LName3 : 2"));
        }

        [
            Test]
        public async Task ModeratorFinishesGame_CheckPlayersReceivesHisScore()
        {
            T.CreateAndStartGame();
            var boy = await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            await T.ProcessAdminMessage("finish");
            T.CheckPlayerReceived(boy, MsgFormats.GameFinished);
            T.CheckPlayerReceived(boy, MsgFormats.YourScorePScore, 1);
        }

        [
            Test]
        public async Task AdminSetFinal_CheckAssignedMissonsAreFinal()
        {
            T.CreateAndStartGame();
            var player1 = await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            var player2 = await T.JoinPlayerAndApproveSelfie(2, Gender.Girl);
            var player3 = await T.JoinPlayerAndApproveSelfie(3, Gender.Girl);
            var player4 = await T.JoinPlayerAndApproveSelfie(4, Gender.Girl);
            await T.ProcessPlayerMessage(player1, "Photo1", "F1");
            await T.ProcessAdminMessage("final");
            await T.ProcessValidatorMessage("yes");
            Assert.AreEqual(T.CurrentTask(player1)?.TaskType, GameTaskType.Final);
        }

        [Test]
        public async Task CompleteFinalMission_CheckNoMoreMissonsAssigned()
        {
            T.CreateAndStartGame();
            var player1 = await T.JoinPlayerAndApproveSelfie(1, Gender.Boy);
            var player2 = await T.JoinPlayerAndApproveSelfie(2, Gender.Girl);
            var player3 = await T.JoinPlayerAndApproveSelfie(3, Gender.Girl);
            var player4 = await T.JoinPlayerAndApproveSelfie(4, Gender.Girl);
            await T.ProcessPlayerMessage(player1, "Photo1", "F1");

            await T.ProcessAdminMessage("final");
            await T.ProcessValidatorMessage("yes");

            await T.ProcessPlayerMessage(player1, "Photo2", "F2");
            await T.ProcessValidatorMessage("yes");

            T.CheckPlayerReceived(player1, MsgFormats.ItWasFinalTask);
            Assert.AreEqual(T.CurrentTask(player1), null);
        }



        [Test]
        public async Task AddCustom()
        {
//            var text = File.ReadAllText("c:/temp/j.txt");
//            var serializeObject = JsonConvert.SerializeObject(text);
//            File.WriteAllText("C:/temp/out.txt", serializeObject);
         //   var msgSegments = MsgParseHelper.Parse("Привет :-]");
        }

        [Test]
        public async Task AddCustomStartNoSlashMsgBeforeGame_JoinWithNoGameCheckCustomMessageReceived()
        {
            T.CreateGame();
            T.ImportSpreadSheet(new ContentSpreadsheetRecord("start", "Привееет друг!"));
            var testUser = T.ProcessUserMessage(1, "start");
            T.CheckPlayerReceived(testUser, "Привееет друг!");
        }

        [Test]
        public async Task AddCustomStartMsgInGame_JoinWithNoGameCheckCustomMessageReceived()
        {
            T.CreateGame();
            T.CreateAndStartGame();
            T.ImportSpreadSheet(new ContentSpreadsheetRecord("/startInGame", "Привееет друг!"));
            var testUser = T.ProcessUserMessage(1, "/start");
            T.CheckPlayerReceived(testUser, "Привееет друг!");
        }


        [Test]
        public async Task AddCustomStartMsgInGame_JoinWithNoGameCheckCustomMessageNoSlashReceived()
        {
            T.CreateGame();
            T.CreateAndStartGame();
            T.ImportSpreadSheet(new ContentSpreadsheetRecord("/startInGame", "Привееет друг!"));
            var testUser = T.ProcessUserMessage(1, "/start");
            T.CheckPlayerReceived(testUser, "Привееет друг!");
        }
    }
}