﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using NUnit.Framework;
using Snapme.Bots;
using Snapme.Bots.Messages;
using Snapme.Bots.Telegram.Parsed;

namespace Snapme.Tests
{
    
    
    [TestFixture]
    public class MessageCombinerTest
    {
        private void AssertMessages(CombinedMessage[] actual, params CombinedMessage[] expected)
        {
            Assert.AreEqual(
                JsonConvert.SerializeObject(expected, Formatting.None),
                JsonConvert.SerializeObject(actual, Formatting.None));
        }
        
        [Test]
        public void SimpleTextMessage()
        {
            AssertMessages(
                MessageCombiner.GetCombinedMessages("Hello!"), 
                new CombinedMessage {TextMessage = new TextMessage {Text = "Hello!"}});
        }
        
        [Test]
        public void TextStartsLikeButton()
        {
            AssertMessages(
                MessageCombiner.GetCombinedMessages("[btn:Hello"), 
                new CombinedMessage {TextMessage = new TextMessage {Text = "[btn:Hello"}});
        }
        
        [Test]
        public void TextWithSquareBraces()
        {
            AssertMessages(
                MessageCombiner.GetCombinedMessages(":-[]..."), 
                new CombinedMessage {TextMessage = new TextMessage {Text = ":-[]..."}});
        }

        [Test]
        public void PhotoMessage()
        {
            AssertMessages(
                MessageCombiner.GetCombinedMessages("[img:123]"), 
                new CombinedMessage {Photo = new PhotoMessage {  PhotoId = "123"}});
        }
        
        [Test]
        public void TextAndPhoto()
        {
            AssertMessages(
                MessageCombiner.GetCombinedMessages("Hello! [img:123]"), 
                new CombinedMessage {TextMessage = new TextMessage { Text = "Hello!"}},
                new CombinedMessage {Photo = new PhotoMessage { PhotoId = "123" }});
        }
        
        [Test]
        public void PhotoWithCaption()
        {
            AssertMessages(
                MessageCombiner.GetCombinedMessages("[img:123] Nice photo"), 
                new CombinedMessage {Photo = new PhotoMessage { PhotoId = "123", Text = "Nice photo"}});
        }

        [Test]
        public void PhotoAndText()
        {
            AssertMessages(
                MessageCombiner.GetCombinedMessages(
                    "[img:123]\n" +
                    "---\n" +
                    "Nice photo"),
                new CombinedMessage {Photo = new PhotoMessage { PhotoId = "123" }},
                new CombinedMessage {TextMessage = new TextMessage { Text = "Nice photo"}});
        }
        
        [Test]
        public void TextPhotoTextPhotoText()
        {
            AssertMessages(
                MessageCombiner.GetCombinedMessages("One [img:A] Two [img:B] Three"), 
                new CombinedMessage {TextMessage = new TextMessage { Text = "One"}},
                new CombinedMessage {AlbumMessage = new AlbumMessage
                {
                    Items = { new AlbumItem { PhotoId = "A", Text = "Two" }, new AlbumItem { PhotoId = "B", Text = "Three" }}
                }});
        }

        [Test]
        public void AlbumWithTwoPhotos()
        {
            AssertMessages(
                MessageCombiner.GetCombinedMessages("[img:1] [img:2] Cool photos"), 
                new CombinedMessage {AlbumMessage = new AlbumMessage { Items  = new List<AlbumItem>{
                        new AlbumItem { PhotoId = "1" },
                        new AlbumItem { PhotoId = "2", Text = "Cool photos"}}}});
        }
        
        [Test]
        public void AlbumWithTreePhotos()
        {
            AssertMessages(
                MessageCombiner.GetCombinedMessages("[img:1] [img:2] [img:3] Cool photos"), 
                new CombinedMessage {AlbumMessage = new AlbumMessage { Items  = new List<AlbumItem>{
                    new AlbumItem { PhotoId = "1" },
                    new AlbumItem { PhotoId = "2" },
                    new AlbumItem { PhotoId = "3", Text = "Cool photos"}}}});
        }

        [Test]
        public void TextWithLinkPreview()
        {
            AssertMessages(
                MessageCombiner.GetCombinedMessages("[preview] Visit our website snapmegame.com"), 
                new CombinedMessage {TextMessage = new TextMessage { Text = "Visit our website snapmegame.com", LinkPreview = true}});
        }
        
        [Test]
        public void ReplyButton()
        {
            AssertMessages(
                MessageCombiner.GetCombinedMessages("Press ok! [btn:ok]"), 
                new CombinedMessage {TextMessage = new TextMessage { Text = "Press ok!", Buttons = new List<MsgSegment>
                    {
                        new MsgSegment {Buttons = new List<MsgSegmentButton>{ new MsgSegmentButton {BtnText = "ok"}} }
                    }
                }});
        }
        
        [Test]
        public void ReplyButtonsInRow()
        {
            AssertMessages(
                MessageCombiner.GetCombinedMessages("Sure? [btn:yes|no]"), 
                new CombinedMessage {TextMessage = new TextMessage { Text = "Sure?", Buttons = 
                    {
                        new MsgSegment {Buttons = { new MsgSegmentButton {BtnText = "yes"}, new MsgSegmentButton {BtnText = "no"}}}
                    }
                }});
        }
        
        [Test]
        public void ReplyButtonsInSeparateLines()
        {
            AssertMessages(
                MessageCombiner.GetCombinedMessages("Sure? [btn:yes][btn:no]"), 
                new CombinedMessage {TextMessage = new TextMessage { Text = "Sure?", Buttons =
                    {
                        new MsgSegment {Buttons = { new MsgSegmentButton {BtnText = "yes"}} },
                        new MsgSegment {Buttons = { new MsgSegmentButton {BtnText = "no"}} }
                    }
                }});
        }
        
        [Test]
        public void InlineButton()
        {
            AssertMessages(
                MessageCombiner.GetCombinedMessages("Press when ready: [btn:ready;I am ready!]"), 
                new CombinedMessage {TextMessage = new TextMessage 
                { 
                    Text = "Press when ready:", 
                    Buttons = new List<MsgSegment> { new MsgSegment { Buttons = new List<MsgSegmentButton>
                            {
                                new MsgSegmentButton {BtnText = "I am ready!", BtnCmd = "ready"}
                            }
                        }
                    }
                }});
        }
  

        [Test]
        public void AlbumAndButtons()
        {
            AssertMessages(
                MessageCombiner.GetCombinedMessages("[img:1][img:2][img:3][btn:ok]"),
                new CombinedMessage
                {
                    AlbumMessage = new AlbumMessage
                    {
                        Items = { new AlbumItem {PhotoId = "1"}, new AlbumItem {PhotoId = "2"} }
                    }
                },
                new CombinedMessage
                {
                    Photo = new PhotoMessage
                    {
                        PhotoId = "3", Buttons = { new MsgSegment {Buttons = {new MsgSegmentButton {BtnText = "ok"}}}}
                    }
                });
        }

        [Test]
        public void TextWithSquareBracket()
        {
            AssertMessages(
                MessageCombiner.GetCombinedMessages("Hello :-]"), 
                new CombinedMessage {TextMessage = new TextMessage { Text = "Hello :-]"}});
        }

        [Test]
        public void EscapedMessage()
        {
            AssertMessages(
                MessageCombiner.GetCombinedMessages("plaintext:[img:1]"), 
                new CombinedMessage {TextMessage = new TextMessage { Text = "[img:1]"}});
        }
    }
}