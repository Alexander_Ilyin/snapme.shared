using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Autofac;
using NUnit.Framework;
using Snapme.Bots.Messages;
using Snapme.Bots.Texts;
using Snapme.Data;
using Snapme.Data.Bots;
using Snapme.Data.Games;
using Snapme.Import;
using Snapme.Testing;

namespace Snapme.Tests
{
    public class ImportTest : BotTestBase
    {
        
        [Test]
        public async Task AddCustomStartMsg_JoinWithNoGameCheckCustomMessageReceived()
        {
            T.ImportSpreadSheet(new ContentSpreadsheetRecord("/start", "Привееет друг!"));
            var testUser = T.ProcessUserMessage(1, "/start");
            T.CheckPlayerReceived(testUser, "Привееет друг!");
        }

        [Test]
        public async Task AddCustomStartMsgBeforeGame_JoinWithNoGameCheckCustomMessageReceived()
        {
            T.CreateGame();
            T.ImportSpreadSheet(new ContentSpreadsheetRecord("/start", "Привееет друг!"));
            var testUser = T.ProcessUserMessage(1, "/start");
            T.CheckPlayerReceived(testUser, "Привееет друг!");
        }
    }
}