﻿using System;
using NUnit.Framework;
using Snapme.Import;

namespace Snapme.Tests
{
    
    public class SpreadsheetReaderTest
    {
        // https://docs.google.com/spreadsheets/d/1yIYau2TqE12aQx_u7HoqSOCaA3OVYlvE9IoDkrLLfoE/edit#gid=430111404

        [Test]
        public void ReadWordsTest()
        {
            var reader = new SpreadsheetReader();
            var words = reader.GetTrinityWords("1yIYau2TqE12aQx_u7HoqSOCaA3OVYlvE9IoDkrLLfoE", "Words");
            foreach (var word in words)
            {
                Console.WriteLine(word);
            }
            CollectionAssert.AreEquivalent(words, new [] { "Шок", "Звезда", "Толпа" });
            
        }
        
        
        
    }
}