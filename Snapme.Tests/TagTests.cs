using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using Snapme.Bots.Messages;
using Snapme.Bots.Texts;
using Snapme.Data.Bots;
using Snapme.Import;

namespace Snapme.Tests
{
    public class TagTests : BotTestBase
    {

        [Test]
        public async Task SendHelp_CheckReceived()
        {
            await T.ProcessFeedbackMessage("/help");
            T.CheckFeedbackReceivedLike("contentTag mytag mycontent");
            T.CheckFeedbackReceivedLike("Reply /tag mytag");
            T.CheckFeedbackReceivedLike("Reply /untag mytag");
        }
        
        [Test]
        public async Task TagUser_ShowUsers_CheckTag()
        {
            var testUser = T.GetTestUser(1);
            T.ProcessUserMessage(testUser, "/start");
            await T.ProcessFeedbackMessage("/tag 1 aaa");
            await T.ProcessFeedbackMessage("/users");
            
            T.CheckFeedbackReceived("1. <a href='tg://user?id=1'>Name1 LName1</a> tags:aaa");
        }
        
        [Test]
        public async Task TagUserWithReply_ShowUsers_CheckTag()
        {
            var testUser = T.GetTestUser(1);
            T.ProcessUserMessage(testUser, "/start");
            await T.ProcessFeedbackMessage("/tag 1 aaa");
            
            await T.ProcessFeedbackMessage("/users");
            
            T.CheckFeedbackReceived("1. <a href='tg://user?id=1'>Name1 LName1</a> tags:aaa");
        }
        
        [Test]
        public async Task TagWithReply()
        {
            var user = T.GetTestUser(1);
            T.ProcessUserMessage(user, T.Simple("/start", "0001"));
            await T.ProcessFeedbackMessage(new MessengerMessage {ReplyTo = new MessangerMsgInfo("0001"), Text = "/tag bbb"});
            
            await T.ProcessFeedbackMessage("/users");
            
            T.CheckFeedbackReceived("1. <a href='tg://user?id=1'>Name1 LName1</a> tags:bbb");
        }
        
        [Test]
        public async Task UntagWithReply()
        {
            var user = T.GetTestUser(1);
            T.ProcessUserMessage(user, T.Simple("/start", "0001"));
            await T.ProcessFeedbackMessage(new MessengerMessage {ReplyTo = new MessangerMsgInfo("0001"), Text = "/tag bbb"});
            await T.ProcessFeedbackMessage(new MessengerMessage {ReplyTo = new MessangerMsgInfo("0001"), Text = "/untag bbb"});
            
            await T.ProcessFeedbackMessage("/users");
            
            T.CheckFeedbackReceived("1. <a href='tg://user?id=1'>Name1 LName1</a>");
        }

        [Test]
        public async Task TagUser_SendContentTagged_CheckReceived()
        {
            var testUser = T.GetTestUser(1);
            T.ProcessUserMessage(testUser, "/start");
            await T.ProcessFeedbackMessage("tag 1 game-msk");
            T.ImportSpreadSheet(new ContentSpreadsheetRecord {Type = "a", Description = "b c d"});
            await T.ProcessFeedbackMessage("content-tag game-msk a");
            Assert.IsTrue(T.MessagesFor(testUser).Any(x => x.ToString().Contains("b c d")));
        }

        [Test]
        public async Task AddUser_SendContentTagged_CheckNotReceived()
        {
            var testUser = T.GetTestUser(1);
            T.ProcessUserMessage(testUser, "/start");
            T.ImportSpreadSheet(new ContentSpreadsheetRecord {Type = "a", Description = "b c d"});
            await T.ProcessFeedbackMessage("content-tag game-msk a");
            Assert.IsFalse(T.MessagesFor(testUser).Any(x => x.ToString().Contains("b c d")));
        }

        [Test]
        public async Task TagUser_SendMsgTagged_CheckReceived()
        {
            var testUser = T.GetTestUser(1);
            T.ProcessUserMessage(testUser, "/start");
            await T.ProcessFeedbackMessage("tag 1 game-msk");
            await T.ProcessFeedbackMessage("message-tag #game-msk Hello everybody!");
            T.CheckPlayerReceived(testUser, "Hello everybody!");
        }

        [Test]
        public async Task TagUser_UntagUser_SendMsgTagged_CheckNotReceived()
        {
            var testUser = T.GetTestUser(1);
            T.ProcessUserMessage(testUser, "/start");
            await T.ProcessFeedbackMessage("tag 1 game-msk");
            await T.ProcessFeedbackMessage("untag 1 game-msk");
            await T.ProcessFeedbackMessage("message-tag game-msk Hello everybody!");
            Assert.IsFalse(T.MessagesFor(testUser).Any(x => x.ToString().Contains("Hello")));
        }

        [Test]
        public async Task Tag2Users_SendMsgTagged_CheckReceived()
        {
            var testUser1 = T.GetTestUser(1);
            var testUser2 = T.GetTestUser(2);
            var testUser3 = T.GetTestUser(3);
            T.ProcessUserMessage(testUser1, "/start");
            T.ProcessUserMessage(testUser2, "/start");
            T.ProcessUserMessage(testUser3, "/start");
            await T.ProcessFeedbackMessage("tag 1, 2 game-msk");
            await T.ProcessFeedbackMessage("message-tag game-msk Hello everybody!");
            T.CheckPlayerReceived(testUser1, "Hello everybody!");

            Assert.IsFalse(T.MessagesFor(testUser3).Any(x => x.ToString().Contains("Hello")));
        }

        [Test]
        public async Task TagUser_CreateGameWithTag_CheckUserCanJoin()
        {
            var testUser = T.GetTestUser(1);
            T.ProcessUserMessage(testUser, "/start");
            await T.ProcessFeedbackMessage("tag 1 game-msk");
            T.CreateGame();
            await T.ProcessAdminMessage("/start-tag game-msk");
            T.CheckPlayerReceived(testUser, MsgFormats.GameStarted);
        }

        [Test]
        public async Task CreateGameWithTag_TagUser_CheckUserCanJoin()
        {
            T.CreateGame();
            await T.ProcessAdminMessage("/start-tag game-msk");
            var testUser = T.GetTestUser(1);
            T.ProcessUserMessage(testUser, "/start");
            await T.ProcessFeedbackMessage("tag 1 game-msk");
            T.CheckPlayerReceived(testUser, MsgFormats.GameStarted);
        }

        [Test]
        public async Task TagUser_CreateGameAll_CheckUserReceivedInviteOnce()
        {
            var testUser = T.GetTestUser(1);
            T.ProcessUserMessage(testUser, "/start");
            T.CreateGame();
            await T.ProcessAdminMessage("/start-all");
            await T.ProcessFeedbackMessage("tag 1 game-msk");
            T.CheckPlayerReceivedOnce(testUser, MsgFormats.GameStarted);
        }

        [Test]
        public async Task TagUser_CreateGameTag_CreateGameAll_CheckUserReceivedInviteOnce()
        {
            var testUser = T.GetTestUser(1);
            T.ProcessUserMessage(testUser, "/start");
            await T.ProcessFeedbackMessage("tag 1 game-msk");
            T.CreateGame();
            await T.ProcessAdminMessage("/start-tag game-msk");
            await T.ProcessAdminMessage("/start-all");
            T.CheckPlayerReceivedOnce(testUser, MsgFormats.GameStarted);
        }

        [Test]
        public async Task TagUser_CreateGameAll_CreateGameTag_CheckUserReceivedInviteOnce()
        {
            var testUser = T.GetTestUser(1);
            T.ProcessUserMessage(testUser, "/start");
            await T.ProcessFeedbackMessage("tag 1 game-msk");
            T.CreateGame();
            await T.ProcessAdminMessage("/start-all");
            await T.ProcessAdminMessage("/start-tag game-msk");
            T.CheckPlayerReceivedOnce(testUser, MsgFormats.GameStarted);
        }

        [Test]
        public async Task CreateGameAll_TagUser_CheckUserReceivedInviteOnce()
        {
            T.CreateGame();
            await T.ProcessAdminMessage("/start-all");
            var testUser = T.GetTestUser(1);
            T.ProcessUserMessage(testUser, "/start");
            await T.ProcessFeedbackMessage("tag 1 game-msk");
            T.CheckPlayerReceivedOnce(testUser, MsgFormats.StartInGame);
            T.CheckPlayerNotReceived(testUser, MsgFormats.GameStarted);
        }
        
        [Test]
        public async Task StartTag_NoTag_CheckNoStarted()
        {
            T.CreateGame();
            var testUser = T.GetTestUser(1);
            await T.ProcessFeedbackMessage("tag 1 game-msk");
            await T.ProcessAdminMessage("/startTag");
            T.CheckPlayerNotReceived(testUser, MsgFormats.StartInGame);
            T.CheckPlayerNotReceived(testUser, MsgFormats.GameStarted);
            T.CheckAdminNotReceived(MsgFormats.GameStartedAdmin);
        }

        [Test]
        public async Task SendTag_CheckTagAssigned()
        {
            T.CreateGame();
            var testUser = T.GetTestUser(1);
            await T.ProcessPlayerMessage(testUser,"/t_boy");
            await T.ProcessFeedbackMessage("/users");
        }

        [Test]
        public async Task StartTag_CheckTagListed()
        {
            var testUser = T.GetTestUser(1);
            T.ProcessUserMessage(testUser, "/start");
            await T.ProcessFeedbackMessage("tag 1 game-msk");
            await T.ProcessFeedbackMessage("/users");
        }
        
        [Test]
        public async Task CreateUser_StartGame_CheckReceivedStarted()
        {
            var testUser = T.GetTestUser(1);
            T.ProcessUserMessage(testUser, "/start");
            T.CreateGame();
            await T.ProcessAdminMessage("startall");
        }

        [Test]
        public async Task CreateGameWithTag_JoinUntagged_CheckNoInviteAtStart()
        {
            T.CreateGame();
            await T.ProcessAdminMessage("/start-tag MSK");
            var testUser = T.GetTestUser(1);
            T.ProcessUserMessage(testUser, "/start");
            T.CheckPlayerNotReceived(testUser, MsgFormats.StartInGame);
        }

        [Test]
        public async Task CreateGameWithTag_JoinUntagged_CheckNoInvite()
        {
            T.CreateGame();
            await T.ProcessAdminMessage("/start-all");
            var testUser = T.GetTestUser(1);
            T.ProcessUserMessage(testUser, "/start");
            await T.ProcessFeedbackMessage("tag 1 game-msk");
            T.CheckPlayerNotReceived(testUser, MsgFormats.GameStarted);
        }
    }
}