using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using Snapme.Bots.Texts;
using Snapme.Data.Bots;
using Snapme.Import;
using Snapme.Testing;

namespace Snapme.Tests
{
    public class NetworkingTests : BotTestBase
    {
        [Test]
        public async Task TypeNetworking_CheckDialogWizard()
        {
            var testUser = T.GetTestUser(1);
            await FillProfile(testUser);
        }

        [Test]
        public async Task TypeNetworking_CheckDialog_TypeNext_CheckReceivedNoProfiles()
        {
            var testUser = T.GetTestUser(1);
            await FillProfile(testUser);
            await T.ProcessPlayerMessage(testUser, "networking-next");
            T.CheckPlayerReceived(testUser, MsgFormats.NoOtherNentworkingUsersYet);
        }

        [Test]
        public async Task Type2Users_CheckSecondReceivedProfileOfFirst()
        {
            var testUser1 = T.GetTestUser(1);
            var testUser2 = T.GetTestUser(2);
            await FillProfile(testUser1);
            await FillProfile(testUser2);
            await T.ProcessPlayerMessage(testUser2, "networking-next");
            T.CheckPlayerReceivedLike(testUser2,"Дизайнер2");

        }
        
        [Test]
        public async Task Type2Users_Skip_CheckNoMoreButtonReceived()
        {
            var testUser1 = T.GetTestUser(1);
            var testUser2 = T.GetTestUser(2);
            await FillProfile(testUser1);
            await FillProfile(testUser2);
            await T.ProcessPlayerMessage(testUser2, "networking-skip 1");
            T.CheckPlayerReceived(testUser2, MsgFormats.NoOtherNentworkingUsersYet);
        }
        
        [Test]
        public async Task Type2Users_Like_CheckLikeReceived()
        {
            var testUser1 = T.GetTestUser(1);
            var testUser2 = T.GetTestUser(2);
            await FillProfile(testUser1);
            await FillProfile(testUser2);
            await T.ProcessPlayerMessage(testUser2, "networking-like 1");
            T.CheckPlayerReceived(testUser2, MsgFormats.NoOtherNentworkingUsersYet);
        }

        [Test]
        public async Task Type2Users_DisableProfile_CheckNotShown()
        {
            var testUser1 = T.GetTestUser(1);
            var testUser2 = T.GetTestUser(2);
            await FillProfile(testUser1);
            await FillProfile(testUser2);            
            await T.ProcessPlayerMessage(testUser2, "networking-like 1");
            await T.ProcessPlayerMessage(testUser1, "networkingedit");
            await T.ProcessPlayerMessage(testUser1, "networking-disable");
            await T.ProcessPlayerMessage(testUser2, "networking-disable");
            var testUser3 = T.GetTestUser(3);
            await FillProfile(testUser3);            
            await T.ProcessPlayerMessage(testUser3, "networking-next");
            T.CheckPlayerReceived(testUser3, MsgFormats.NoOtherNentworkingUsersYet);
        }
        
        [Test]
        public async Task DisableProfile_EnableProfile_EditProfile_CheckUpdated()
        {
            var testUser1 = T.GetTestUser(1);
            await FillProfile(testUser1);
            await T.ProcessPlayerMessage(testUser1, "networking-disable");
            await T.ProcessPlayerMessage(testUser1, "networkingenable");
            await T.ProcessPlayerMessage(testUser1, "networkingedit");
            await T.ProcessPlayerMessage(testUser1, "networking-update-position");
            await T.ProcessPlayerMessage(testUser1, "Дворник на вокзале");
            var testUser2 = T.GetTestUser(2);
            await FillProfile(testUser2);
            await T.ProcessPlayerMessage(testUser2, "networking-next");

            T.CheckPlayerReceivedLike(testUser2, "Дворник");
        }

        [Test]
        public async Task SkipPhoto_CheckProceeds()
        {
            var testUser = T.GetTestUser(1);
            await T.ProcessPlayerMessage(testUser, "/startnetworking");
            T.CheckPlayerReceived(testUser, MsgFormats.SendUsSelfie);
            await T.ProcessPlayerMessage(testUser, "Photo " + testUser.PlayerNum);
            await T.ProcessPlayerMessage(testUser, "networking-skip-q");
            T.CheckPlayerReceived(testUser, MsgFormats.WhatIsYourPosition);
        }
        
        [Test]
        public async Task Like1User_CheckOtherAppears()
        {
            var testUser1 = T.GetTestUser(1);
            var testUser2 = T.GetTestUser(2);
            await FillProfile(testUser1);
            await FillProfile(testUser2);            
            var testUser3 = T.GetTestUser(3);
            await FillProfile(testUser3);            
            await T.ProcessPlayerMessage(testUser2, "networking-like 1");
            T.CheckPlayerReceivedLike(testUser2, "Дизайнер3");
        }
        
        [Test]
        public async Task Skip1User_CheckOtherAppears()
        {
            var testUser1 = T.GetTestUser(1);
            var testUser2 = T.GetTestUser(2);
            await FillProfile(testUser1);
            await FillProfile(testUser2);            
            var testUser3 = T.GetTestUser(3);
            await FillProfile(testUser3);            
            await T.ProcessPlayerMessage(testUser2, "networking-skip 1");
            T.CheckPlayerReceivedLike(testUser2, "Дизайнер3");
        }
        
        [Test]
        public async Task SkipPhoto_CheckReceivedOk()
        {
            var testUser = T.GetTestUser(1);
            await T.ProcessPlayerMessage(testUser, "/networking");
            await T.ProcessPlayerMessage(testUser, "networking-skip-q");
            await T.ProcessPlayerMessage(testUser, "networking-skip-q");
            await T.ProcessPlayerMessage(testUser, "networking-skip-q");
            await T.ProcessPlayerMessage(testUser, "networking-skip-q");
            var testUser2 = T.GetTestUser(2);
            await FillProfile(testUser2);
            await T.ProcessPlayerMessage(testUser2, "networking-next");
            
            Assert.IsTrue(T.MessagesFor(testUser2).Count(m => !m.ToString().Contains("img"))==1);
        }
        
        private async Task FillProfile(TestUser testUser)
        {
            await T.ProcessPlayerMessage(testUser, "/startnetworking");
            T.CheckPlayerReceived(testUser, MsgFormats.NetworkingBegin);

            T.CheckPlayerReceived(testUser, MsgFormats.SendUsSelfie);
            await T.ProcessPlayerMessage(testUser, "PhotoC" + testUser.PlayerNum);

            T.CheckPlayerReceived(testUser, MsgFormats.WhatIsYourPosition);
            await T.ProcessPlayerMessage(testUser, "Дизайнер" + testUser.PlayerNum);

            T.CheckPlayerReceived(testUser, MsgFormats.WhatAreYouGreatAt);
            await T.ProcessPlayerMessage(testUser, "Я крут во всём" + testUser.PlayerNum);

            T.CheckPlayerReceived(testUser, MsgFormats.WhatIsYourInterest);
            await T.ProcessPlayerMessage(testUser, "Ищу работу" + testUser.PlayerNum);



        }
    }
}