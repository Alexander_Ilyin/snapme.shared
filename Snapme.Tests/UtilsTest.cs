using System;
using System.Linq;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using NUnit.Framework;
using Snapme.Bots;
using Snapme.Bots.Telegram;
using Snapme.Bots.Telegram.Parsed;
using Snapme.Import;

namespace Snapme.Tests
{
    public class UtilsTest
    {
        [Test]
        public void CombineTest()
        {
            var message = "3234 [img:im1] sqeqe qwe qwe qwe qwe qweqwe qwe qweqwe qwe " +
                          "qeqweq we qwe qwe qwe qwe qwe qwe" +
                          "qweqweqweqwe qwe qweqwe qwe qwe qweqwe" +
                          "qweqweqweqweqweqweqweqwe qweqwe qweqwe qwe" +
                          "qweqweqwe qwe qwe qwe qwe qweq weqwe qwe" +
                          "qweqweqweqwe qweqwe qweqweqweqwqwer qwrqw eqwer " +
                          "qwerqwerqwrqwerqweqwe qwed[btn:ssss] [btn:ssss] qweqwe [img:im1] sd[btn:ssss]";
            var combinedMessages = MessageCombiner.Combine("wer", MessageCombiner.Parse(message));
            Console.WriteLine(JsonConvert.SerializeObject(combinedMessages, Formatting.Indented));
        }

        [Test]
        public void CombineVideoTest1()
        {
            var message = "[img:im1][vn:V1]AA[btn:ssss]";
            var combinedMessages = MessageCombiner.Combine("-", MessageCombiner.Parse(message));
            Console.WriteLine(JsonConvert.SerializeObject(combinedMessages, Formatting.Indented));
            Assert.AreEqual(combinedMessages[0].Photo.PhotoId, "im1");
            Assert.AreEqual(combinedMessages[1].VideoNote.VideoId, "V1");
            Assert.AreEqual(combinedMessages[2].TextMessage.Text, "AA");
            Assert.AreEqual(combinedMessages[2].TextMessage.Buttons[0].Buttons[0].BtnText, "ssss");
        }

        [Test]
        public void CombineVideoTest2()
        {
            var message = "[img:im1][vn:V1][btn:ssss]";
            var combinedMessages = MessageCombiner.Combine("-", MessageCombiner.Parse(message));
            Console.WriteLine(JsonConvert.SerializeObject(combinedMessages, Formatting.Indented));
            Assert.AreEqual(combinedMessages[0].Photo.PhotoId, "im1");
            Assert.AreEqual(combinedMessages[1].VideoNote.VideoId, "V1");
            Assert.AreEqual(combinedMessages[1].VideoNote.Buttons[0].Buttons[0].BtnText, "ssss");
        }

        [Test]
        public void ParseLinkTest()
        {
            Assert.AreEqual("111", TelegramMessenger.ExtractUserIdFromLink("uid=111; Ilyin Aleksandr  :wer"));
        }

        [Test]
        public void ParseUsersCmdParams()
        {
            Assert.AreEqual(1, CommandParser.ParseUsersCmdParams(" 1, 21,3  tag1").Nums[0]);
            Assert.AreEqual(21, CommandParser.ParseUsersCmdParams(" 1, 21,3  tag1").Nums[1]);
            Assert.AreEqual(3, CommandParser.ParseUsersCmdParams(" 1, 21,3  tag1").Nums[2]);
            Assert.AreEqual("tag1", CommandParser.ParseUsersCmdParams(" 1, 21,3  tag1").Parameter);
        }
    }
}