using System;
using System.IO;
using System.Linq;
using Autofac;
using NUnit.Framework;
using Snapme.Common;
using Snapme.Common.DI;
using Snapme.Data.Games;
using Snapme.Import;
using Snapme.Testing;

namespace Snapme.Tests
{
    public class BotTestBase
    {
        public TgmBotTester T { get; private set; }

        public virtual string BotTitle => "@TestBot";

        [SetUp]
        public virtual void Setup()
        {
            RandomHelper.TestMode = true;
            LogHelper.InitLogger();
            var builder = new SnapmeBuilder(new[]
            {
                GetType().Assembly,
                typeof(Game).Assembly,
                typeof(BotTestBase).Assembly
            }.Distinct().ToArray());
            var containerBuilder = builder.CreateBuilder("Tests", Guid.NewGuid().ToString());
            containerBuilder.RegisterType<TestSpreadsheetReader>().AsImplementedInterfaces().SingleInstance();
            containerBuilder.RegisterType<TestMessengerFactory>().AsImplementedInterfaces().SingleInstance();
            var container = containerBuilder.Build();
            T = new TgmBotTester(container, BotTitle);
        }

        [TearDown]
        public virtual void TearDown()
        {
            var stringWriter = new StringWriter();
            T.LogAllMessages(null,stringWriter);
            var newResult = stringWriter.ToString();
            Console.WriteLine(newResult);

            var actualPath = Path.Combine(FsUtils.FindPath("./TestOutput"),
                TestContext.CurrentContext.Test.Name + ".txt");

            var expectedPath = Path.Combine(FsUtils.FindPath("./TestOutput"),
                TestContext.CurrentContext.Test.Name + ".expected.txt");

            File.WriteAllText(actualPath, newResult);
            if (File.Exists(expectedPath))
            {
                var oldResult = File.ReadAllText(expectedPath);
                //   Assert.AreEqual(oldResult, newResult);
            }
            else
            {
                File.WriteAllText(expectedPath, newResult);
            }
        }

        public void CheckUserOutput(TestUser testUser, string version=null)
        {
            CheckOutput(testUser.ChatId, version);
        }

        public void CheckOutput(long chatId, string version)
        {
            var stringWriter = new StringWriter();
            T.LogAllMessages(chatId, stringWriter);
            var newResult = stringWriter.ToString();
            Console.WriteLine(newResult);

            var suffix = ".Chat" + chatId + "_v_" + version;
            var actualPath = Path.Combine(FsUtils.FindPath("./TestOutput"),
                TestContext.CurrentContext.Test.Name + suffix + ".txt");

            var expectedPath = Path.Combine(FsUtils.FindPath("./TestOutput"),
                TestContext.CurrentContext.Test.Name + suffix + ".expected.txt");

            File.WriteAllText(actualPath, newResult);
            if (File.Exists(expectedPath))
            {
                var oldResult = File.ReadAllText(expectedPath);
                Assert.AreEqual(oldResult, newResult);
            }
            else
            {
                File.WriteAllText(expectedPath, newResult);
            }
        }
    }
}