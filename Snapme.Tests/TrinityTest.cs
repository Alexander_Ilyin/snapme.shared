﻿using NUnit.Framework;
using Snapme.Trinity;

namespace Snapme.Tests
{
    public class TrinityTest : BotTestBase
    {
        public override void Setup()
        {
            base.Setup();

            T.ProcessFeedbackMessage("/CreateAndStartTrinityGame");
            T.CheckFeedbackReceived("Trinity game created and started");

            T.SpreadsheetReader.Words = new [] {"Мох", "Улитка", "Баклажан"};
            T.ProcessFeedbackMessage("/import");
        }
        
        [Test]
        public void NoExplanations_Join_AsksForVideoCheck()
        {
            var user = T.GetTestUser(1);

            T.ProcessUserMessage(1, "/JoinTrinityGame");
            
            T.CheckPlayerReceived(user, TrinityStrings.WelcomeToGame);
            T.CheckPlayerReceived(user, TrinityStrings.VideoCheckInstructions);
        }
        
        [Test]
        public void NoExplanations_SendVideoCheck_AskForFirstExplanation()
        {
            var user = T.GetTestUser(1);

            T.ProcessUserMessage(1, "/JoinTrinityGame");
            T.ProcessUserVideoNote(user, "vn1");
            
            T.CheckPlayerReceived(user, TrinityStrings.FirstExplanationInstructions);
            T.CheckPlayerReceived(user, TrinityStrings.GetReadyForExplanation);
        }
        
        [Test]
        public void NoExplanations_SendVideoCheck_ReadyForExplanation_SendsWord()
        {
            var user = T.GetTestUser(1);

            T.ProcessUserMessage(user, "/JoinTrinityGame");
            T.ProcessUserVideoNote(user, "vn0");
            T.ProcessUserMessage(user, "ready");
            
            T.CheckPlayerReceived(user, TrinityStrings.WordForExplanationIsX, "Мох");
        }
        
        [Test]
        public void SubmitExplanation_SendsGotExplanationAndNewExplanationTask()
        {
            var user = T.GetTestUser(1);

            T.ProcessUserMessage(user, "/JoinTrinityGame");
            T.ProcessUserVideoNote(user, "vn0");
            T.ProcessUserMessage(user, "ready");
            
            T.ProcessUserVideoNote(user, "vn1");
            
            T.CheckPlayerReceived(user, TrinityStrings.GotYourExplanation);
            T.CheckPlayerNotReceived(user, TrinityStrings.FirstExplanationInstructions);
            T.CheckPlayerReceived(user, TrinityStrings.GetReadyForExplanation);
        }
        
        [Test]
        public void GivesDifferentWordsForExplanation()
        {
            var user = T.GetTestUser(1);

            T.ProcessUserMessage(user, "/JoinTrinityGame");
            T.ProcessUserVideoNote(user, "vn0");
            
            T.ProcessUserMessage(user, "ready");
            T.ProcessUserVideoNote(user, "vn1");
            
            T.ProcessUserMessage(user, "ready");
            T.ProcessUserVideoNote(user, "vn2");
            
            T.ProcessUserMessage(user, "ready");
            T.ProcessUserVideoNote(user, "vn3");
            
            T.CheckPlayerReceived(user, TrinityStrings.WordForExplanationIsX, "Мох");
            T.CheckPlayerReceived(user, TrinityStrings.WordForExplanationIsX, "Улитка");
            T.CheckPlayerReceived(user, TrinityStrings.WordForExplanationIsX, "Баклажан");
        }
        
        [Test]
        public void SecondPlayersJoins_SendReadyForGuess()
        {
            var user = T.GetTestUser(1);
            var user2 = T.GetTestUser(2);

            T.ProcessUserMessage(user, "/JoinTrinityGame");
            T.ProcessUserVideoNote(user, "vn0");
            T.ProcessUserMessage(user, "ready");
            T.ProcessUserVideoNote(user, "vn1");
            
            T.ProcessUserMessage(user2, "/JoinTrinityGame");
            
            T.CheckPlayerReceived(user2, TrinityStrings.WelcomeToGame);
            T.CheckPlayerReceived(user2, TrinityStrings.FirstGuessInstructions);
            T.CheckPlayerReceived(user2, TrinityStrings.GetReadyForGuess);
        }
        
        [Test]
        public void SecondPlayersJoinsAndReadyForGuess_SendVideoExplanation()
        {
            var user = T.GetTestUser(1);
            var user2 = T.GetTestUser(2);

            T.ProcessUserMessage(user, "/JoinTrinityGame");
            T.ProcessUserVideoNote(user, "vn0");
            T.ProcessUserMessage(user, "ready");
            T.ProcessUserVideoNote(user, "vn1");
            
            T.ProcessUserMessage(user2, "/JoinTrinityGame");
            T.ProcessUserMessage(user2, "ready");
            
            T.CheckPlayerReceivedVideoNote(user2, "vn1");
        }
        
        [Test]
        public void SecondPlayerGetsUnusedWordForExplanation()
        {
            var user = T.GetTestUser(1);
            var user2 = T.GetTestUser(2);

            T.ProcessUserMessage(user, "/JoinTrinityGame");
            T.ProcessUserVideoNote(user, "vn0");
            T.ProcessUserMessage(user, "ready");

            T.ProcessUserMessage(user2, "/JoinTrinityGame");
            T.ProcessUserVideoNote(user2, "vn1");
            T.ProcessUserMessage(user2, "ready");
            
            T.CheckPlayerReceived(user, TrinityStrings.WordForExplanationIsX, "Мох");
            T.CheckPlayerReceived(user2, TrinityStrings.WordForExplanationIsX, "Улитка");
        }

    }
}