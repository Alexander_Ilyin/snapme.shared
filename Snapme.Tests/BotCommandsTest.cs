﻿using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using Snapme.Bots.Messages;
using Snapme.Import;

namespace Snapme.Tests
{
    public class BotCommandsTest : BotTestBase
    {
        [Test]
        public async Task BotSendsPhotoIdToFeedback()
        {
            await T.ProcessFeedbackPhoto("photo 1");
            T.CheckFeedbackReceived("plaintext:[img:photo 1]");
        }
        
        [Test]
        public async Task BotSendsVideoNoteIdToFeedback()
        {
            await T.ProcessFeedbackVideNote("vn1");
            T.CheckFeedbackReceived("plaintext:[vn:vn1]");
        }
        
        [Test]
        public async Task BotUsersCommand()
        {
            var user = T.GetTestUser(1, true);
            T.ProcessUserMessage(user, T.Simple("/start"));

            await T.ProcessFeedbackMessage("/users");

            T.CheckFeedbackReceived("1. <a href='tg://user?id=1'>Name1 LName1</a>");
            T.CheckFeedbackReceived("Total: 1");
        }
        
        [Test]
        public async Task BotUsersBatchTestCommand()
        {
            foreach (var i in Enumerable.Range(1, 25))
            {
                var user = T.GetTestUser(i, true);
                T.ProcessUserMessage(user, T.Simple("/start"));
            }

            await T.ProcessFeedbackMessage("/users");

            T.CheckFeedbackReceived(
                string.Join('\n', Enumerable.Range(1, 20).Select(i => $"{i}. <a href='tg://user?id={i}'>Name{i} LName{i}</a>")));
            
            T.CheckFeedbackReceived(
                string.Join('\n', Enumerable.Range(21, 5).Select(i => $"{i}. <a href='tg://user?id={i}'>Name{i} LName{i}</a>")));
            
            T.CheckFeedbackReceived("Total: 25");
        }
        
        [Test]
        public async Task SendBroadCast()
        {
            T.ImportSpreadSheet(new ContentSpreadsheetRecord("megaMsg", "МЕГА СООБЩЕНИЕ"));

            var user = T.GetTestUser(1);
            T.ProcessUserMessage(user, new MessengerMessage {PhotoId = "P1"});
            
            await T.ProcessFeedbackMessage("content-all megaMsg");
            T.CheckPlayerReceived(user, "МЕГА СООБЩЕНИЕ");
        }
    }
}