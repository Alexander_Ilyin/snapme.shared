namespace Snapme.Testing
{
    public class TestUser
    {
        private readonly int playerNum;
        private readonly int chatId;
        private string tgmUserName;

        public string TgmUserName
        {
            get => tgmUserName;
            set => tgmUserName = value;
        }

        private readonly long tgmUserId;

        public long TgmUserId => tgmUserId;

        public int PlayerNum => playerNum;

        public int ChatId => chatId;


        public TestUser(int playerNum, int chatId, string tgmUserName, long tgmUserId)
        {
            this.playerNum = playerNum;
            this.chatId = chatId;
            this.tgmUserName = tgmUserName;
            this.tgmUserId = tgmUserId;
        }
    }
}