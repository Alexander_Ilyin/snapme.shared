using System;

namespace Snapme.Testing
{
    [Flags]
    public enum TestGameFlags
    {
        Default = 0,
        WithMeetings = 1,
        RequireUserName = 2,
        WithNoFinalActivities = 4,
        WithNoActivities = 8,
        SendResultsToPlayers = 16
    }
}