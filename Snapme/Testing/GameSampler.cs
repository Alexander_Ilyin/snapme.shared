using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Autofac.Core.Lifetime;
using Snapme.Bots;
using Snapme.Bots.Games;
using Snapme.Bots.Hosting;
using Snapme.Bots.Services;
using Snapme.Common;
using Snapme.Common.DI;
using Snapme.Data;
using Snapme.Data.Bots;
using Snapme.Data.Games;
using Snapme.Import;

namespace Snapme.Testing
{
    public class GameSampler : IRequestBound
    {
        private readonly SnapmeContext context;

        public GameSampler(SnapmeContext context)
        {
            this.context = context;
        }

        public Task<string> TestGame(Guid gameId)
        {
            return TestGame(gameId, "tests");
        }

        public async Task<string> TestGame(Guid gameId, string profile)
        {
            var tasks = context.GameTasks.Where(x => x.GameId == gameId && !x.IsDelete).ToList();
            var builder = new SnapmeBuilder(new[]
            {
                GetType().Assembly,
                typeof(Game).Assembly
            });
            var containerBuilder = builder.CreateBuilder(profile, Guid.NewGuid().ToString());
            containerBuilder.RegisterType<TestSpreadsheetReader>().As<ISpreadsheetReader>().SingleInstance();
            containerBuilder.RegisterType<TestMessengerFactory>().As<ITelegramMessengerFactory>().SingleInstance();
            var container = containerBuilder.Build();

            var tester = new TgmBotTester(container);
            tester.CreateGame(TestGameFlags.WithNoActivities);

            tester.InScope(s =>
            {
                var game = s.Resolve<GameService>().GetGame(tester.gameId.Value);
                var realSettings = context.Games.Find(gameId).GetSettings();

                var gameSettings = game.GetSettings();
                gameSettings.WithMeetings = realSettings.WithMeetings;
                gameSettings.RequireUserName = realSettings.RequireUserName;
                game.SetSettings(gameSettings);
                foreach (var activity in tasks)
                {
                    s.Resolve<SnapmeContext>().AddNew(new Activity
                    {
                        GameId = game.Id,
                        Level = activity.Level,

                        DescriptionRu = activity.DescriptionRu,
                        DescriptionEn = activity.DescriptionEn,
                        ActivityType = activity.ActivityType,
                        Phase = activity.Phase,
                    });
                    s.Resolve<SnapmeContext>().SaveChanges();
                }
            });
            tester.CreateAndStartGame();
            var fakeGameId = tester.gameId;
            var players = new List<TestUser>();
            for (var i = 0; i < 10; i++)
                if (i == 0)
                    players.Add(await tester.JoinPlayerAndApproveSelfie(i, Gender.Boy));
                else
                    players.Add(await tester.JoinPlayerAndApproveSelfie(i, Gender.Girl));

            for (var i = 0; i < 10; i++)
            {
                await tester.ProcessPlayerMessage(players[0], "Photo" + i, "P" + i);
                await tester.ProcessValidatorMessage("yes");
            }

            await tester.ProcessAdminMessage("phase 2");
            for (var i = 0; i < 10; i++)
            {
                await tester.ProcessPlayerMessage(players[0], "Photo" + i, "P" + i);
                await tester.ProcessValidatorMessage("yes");
            }

            await tester.ProcessAdminMessage("phase 3");
            for (var i = 0; i < 10; i++)
            {
                await tester.ProcessPlayerMessage(players[0], "Photo" + i, "P" + i);
                await tester.ProcessValidatorMessage("yes");
            }

            await tester.ProcessAdminMessage("final");
            for (var i = 0; i < 10; i++)
            {
                await tester.ProcessPlayerMessage(players[0], "Photo" + i, "P" + i);
                await tester.ProcessValidatorMessage("yes");
            }

            await tester.ProcessAdminMessage("finish");
            var output = new StringWriter();

            tester.InScope(s =>
            {
                var activities = s.Resolve<SnapmeContext>().PlayerTasks
                    .Where(t => t.Assigned.Any(p => p.User.TgmUserId == players[0].TgmUserId) && t.GameId == fakeGameId)
                    .OrderBy(t => t.Created)
                    .Take(20)
                    .ToList();
                var descriptions = activities.Select(a => s.Resolve<GameService>().GetDescription(a));
                foreach (var description in descriptions.Distinct())
                    output.WriteLine("\n" + description);
            });
            return output.ToString();
        }
    }
}