using Snapme.Bots.Hosting;
using Snapme.Bots.Telegram;

namespace Snapme.Testing
{
    public class TestMessengerFactory : ITelegramMessengerFactory
    {
        public ITelegramMessenger CreateMessenger()
        {
            return new TestMessenger();
        }
    }
}