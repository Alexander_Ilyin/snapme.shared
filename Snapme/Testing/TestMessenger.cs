using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework.Internal;
using Snapme.Bots;
using Snapme.Bots.Hosting;
using Snapme.Bots.Messages;
using Snapme.Bots.Telegram;
using Snapme.Common;
using Snapme.Data;
using Snapme.Data.Bots;
using Snapme.Data.Games;

namespace Snapme.Testing
{
    public class TestMessenger : ITelegramMessenger
    {
        public event Action<TelegramMessageEvent> MessageReceived;
        public event Action<TelegramBotEvent> Update;
        public int messageIndex = 1;

        private readonly Dictionary<long, List<TestMesengerLogItem>> messagesByChatId = new Dictionary<long, List<TestMesengerLogItem>>();

        private Dictionary<string, MessengerMessage> messagesById = new Dictionary<string, MessengerMessage>();

        public void TriggerMessageReceived(TelegramMessageEvent e)
        {
            messagesByChatId[e.FromChatId] = messagesByChatId.GetValueOrDefault(e.FromChatId) ?? new List<TestMesengerLogItem>();
            messagesByChatId[e.FromChatId].Add(new TestMesengerLogItem(null, e));
            if (MessageReceived != null)
                MessageReceived(e);
        }

        public IEnumerable<long> FindForwardFromUserIds(MessangerMsgInfo msgInfo)
        {
            return messagesByChatId.SelectMany(x => x.Value).Where(x => x?.Incoming?.Message?.MsgInfo?.Data == msgInfo?.Data)
                .Select(x => x.Incoming.FromUserId).Where(x => x.HasValue).Select(x => x.Value);
        }

        public async Task SendMessage(long targetChatId, OutgoingMessage msg, string language)
        {
            this.Log().Info("Send message to " + targetChatId + " Msg:" + msg);

            messagesByChatId[targetChatId] = messagesByChatId.GetValueOrDefault(targetChatId) ?? new List<TestMesengerLogItem>();
            messagesByChatId[targetChatId].Add(new TestMesengerLogItem(msg, null));
        }

        public void Start(string botKey)
        {
        }

        public void Stop()
        {
        }

        public MessengerMessage Simple(string text, string msgInfo = null)
        {
            var id = msgInfo ?? ("msg_" + messageIndex++);
            
            if (text?.ToLowerInvariant()?.Contains("photo") == true)
                return messagesById[id] = new MessengerMessage()
                {
                    PhotoId = text,
                    MsgInfo = new MessangerMsgInfo(id)
                };
            else
            {
                return this.messagesById[id]= new MessengerMessage { Text = text, MsgInfo = new MessangerMsgInfo(id) };
            }
        }

        public MessengerMessage Photo(string text, string photoId)
        {
            var id = "msg_" + messageIndex++;
            return messagesById[id]= new MessengerMessage { PhotoId = photoId, Text = text, MsgInfo = new MessangerMsgInfo(id) };
        }
        
        public MessengerMessage VideoNote(string vnId)
        {
            var id = "msg_" + messageIndex++;
            return messagesById[id]= new MessengerMessage { VideoNoteId = vnId, MsgInfo = new MessangerMsgInfo(id) };
        }

        public IEnumerable<OutgoingMessage> GetMessagesFor(long chatId)
        {
            return messagesByChatId?.GetItemOrDefault(chatId)?.Where(i => i.Outgoing != null)?.Select(i => i.Outgoing) ?? new OutgoingMessage[0];
        }

        public void LogAllMessages(SnapmeContext context, TgmBot bot, Game game = null, long? chatIdFilter=null,TextWriter output = null)
        {
            output = output ?? Console.Out;
            foreach (var userMessage in messagesByChatId)
            {
                var chatId = userMessage.Key;
                if (chatIdFilter.HasValue && chatIdFilter!=chatId)
                    continue;
                var messages = userMessage.Value;
                
                if (game?.IsAdminChat(chatId) == true)
                {
                    output.WriteLine("AdminChat:");
                    foreach (var me in messages)
                    {
                        if (me.Incoming != null)
                            output.WriteLine("admin:   " + Format(me.Incoming));
                        if (me.Outgoing != null)
                            output.WriteLine("bot:     " + Format(me.Outgoing));
                    }

                    output.WriteLine("==== \n\n");
                    continue;
                }

                if (game?.IsValidatorChat(chatId) == true)
                {
                    output.WriteLine("ValidatorChat:");
                    foreach (var me in messages)
                    {
                        if (me.Incoming != null)
                            output.WriteLine("validator:   " + Format(me.Incoming));
                        if (me.Outgoing != null)
                            output.WriteLine("bot:     " + Format(me.Outgoing));
                    }

                    output.WriteLine("==== \n\n");
                    continue;
                }

                if (bot?.IsFeedbackChat(chatId) == true)
                {
                    output.WriteLine("FeedbackChat:");
                    foreach (var me in messages)
                    {
                        if (me.Incoming != null)
                            output.WriteLine("feedbacker:   " + Format(me.Incoming));
                        if (me.Outgoing != null)
                            output.WriteLine("bot:     " + Format(me.Outgoing));
                    }

                    output.WriteLine("==== \n\n");
                    continue;
                }

                var botUser = context.TgmBotUsers.FirstOrDefault(u => u.TgmChatId == chatId);
                var name = botUser?.User?.Name() ?? chatId.ToString();
                output.WriteLine("User " + name + ":");
                foreach (var me in messages)
                {
                    if (me.Incoming != null)
                        output.WriteLine(name + ": " + Format(me.Incoming));
                    if (me.Outgoing != null)
                        output.WriteLine("bot:    " + Format(me.Outgoing));
                }

                output.WriteLine("==== \n\n");
            }
        }

        private string Format(OutgoingMessage outgoing)
        {
            if (outgoing.ForwardedMsg!=null)
            {
                var messengerMessage = messagesById.GetItemOrDefault(outgoing.ForwardedMsg.Data);
                if (messengerMessage != null)
                    return "Forwarded-" + messengerMessage;
            }
            return outgoing.ToString();
            
        }
        private string Format(TelegramMessageEvent incoming)
        {
            return incoming.ToString();
        }

        
    }
}