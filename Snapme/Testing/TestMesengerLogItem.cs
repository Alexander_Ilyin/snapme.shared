using Snapme.Bots.Messages;
using Snapme.Bots.Telegram;

namespace Snapme.Testing
{
    public class TestMesengerLogItem
    {
        public TestMesengerLogItem(OutgoingMessage outgoing, TelegramMessageEvent incoming)
        {
            Outgoing = outgoing;
            Incoming = incoming;
        }

        public OutgoingMessage Outgoing { get; set; }
        public TelegramMessageEvent Incoming { get; set; }
    }
}