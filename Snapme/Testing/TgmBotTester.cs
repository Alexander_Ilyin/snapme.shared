using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Autofac;
using Autofac.Core.Lifetime;
using NUnit.Framework;
using NUnit.Framework.Internal;
using Snapme.Bots;
using Snapme.Bots.Background;
using Snapme.Bots.Hosting;
using Snapme.Bots.Messages;
using Snapme.Bots.Processors;
using Snapme.Bots.Services;
using Snapme.Bots.Telegram;
using Snapme.Bots.Texts;
using Snapme.Common;
using Snapme.Data;
using Snapme.Data.Bots;
using Snapme.Data.Games;
using Snapme.Import;
using Snapme.Trinity;

namespace Snapme.Testing
{
    public class TgmBotTester
    {
        private readonly Dictionary<TestUser, int> msgSkipIndex = new Dictionary<TestUser, int>();
        readonly SnapmeContainer snapmeContainer;
        private Guid botId;
        private TestMessenger gameMessenger;
        public Guid? gameId;
        private List<TestUser> testUsers = new List<TestUser>();
        private int validateChatId = 2000;
        private int adminChatId = 1000;
        private int feedbackChatId = 3000;
        private TelegramBotHost telegramBotHost;

        public TgmBotTester(IContainer container, string botTitle = "@TestBot")
        {
            snapmeContainer = new SnapmeContainer(container);
            botId = InScope(s =>
            {
                var tgmBot = s.Resolve<GameService>().CreateBot();
                tgmBot.FeedbackChatId = feedbackChatId;
                tgmBot.Title = botTitle;
                return tgmBot.Id;
            });
            telegramBotHost = new TelegramBotHost(snapmeContainer);
            telegramBotHost.StartBot(botId).Wait();
            gameMessenger = (TestMessenger) telegramBotHost.GetMessenger(botId);
        }

        public Guid CreateGame(
            TestGameFlags flags = TestGameFlags.WithMeetings | TestGameFlags.RequireUserName |
                                  TestGameFlags.SendResultsToPlayers)
        {
            InScope(async s =>
            {
                await Task.CompletedTask;
                var gameService = s.Resolve<GameService>();
                var game = gameService.CreateGame(botId);

                var settings = new GameSettings
                {
                    AdminChatId = adminChatId,
                    ValidationChatIds = new List<long> { validateChatId }
                };
                if ((TestGameFlags.WithMeetings & flags) == TestGameFlags.WithMeetings)
                    settings.WithMeetings = true;
                if ((TestGameFlags.RequireUserName & flags) == TestGameFlags.RequireUserName)
                    settings.RequireUserName = true;
                if ((TestGameFlags.SendResultsToPlayers & flags) == TestGameFlags.SendResultsToPlayers)
                    settings.SendResultsToPlayers = true;
                game.SetSettings(settings);
                game.MustBeProcessedByBot = true;
                ((TestSpreadsheetReader)s.Resolve<ISpreadsheetReader>()).LoadTasks(flags);
                gameId = game.Id;
                gameService.SaveChanges();
                await s.Resolve<GameTaskImporter>().ImportTasks(game.Id, false);
                gameService.SaveChanges();
            });
            return gameId.Value;
        }

        public void CreateAndStartGame()
        {
            if (gameId == null)
                CreateGame();
            ProcessAdminMessage("start-all");
        }

        public TestSpreadsheetReader SpreadsheetReader => (TestSpreadsheetReader) snapmeContainer.GetSingleton<ISpreadsheetReader>();

        public void InScope(Action<ILifetimeScope> action)
        {
            snapmeContainer.RunWithMessenger(this.gameMessenger, this.botId, async (s) =>
            {
                action(s);
                s.Resolve<SnapmeContext>().SaveChanges();
            }).Wait();
        }

        public T InScope<T>(Func<ILifetimeScope, T> action)
        {
            T result = default(T);
            snapmeContainer.RunWithMessenger(this.gameMessenger, this.botId, async (s) =>
            {
                result = action(s);
                s.Resolve<SnapmeContext>().SaveChanges();
            }).Wait();
            return result;
        }

        public async Task<TestUser> JoinAnonymousPlayer(int playerNum)
        {
            var testUser = GetTestUser(playerNum, true);
            ProcessUserMessage(testUser, gameMessenger.Simple("/start"));
            ProcessUserMessage(testUser, gameMessenger.Simple("/ready"));
            return testUser;
        }

        public async Task<TestUser> JoinPlayer(int playerNum = 1)
        {
            var testUser = GetTestUser(playerNum, false);
            ProcessUserMessage(testUser, gameMessenger.Simple("/start"));
            ProcessUserMessage(testUser, gameMessenger.Simple("/ready"));
            return testUser;
        }

        public TestUser ProcessUserMessage(int playerNum, string text)
        {
            var testUser = GetTestUser(playerNum, false);
            ProcessUserMessage(testUser, gameMessenger.Simple(text));
            return testUser;
        }

        public TestUser GetTestUser(int playerNum, bool noTgmName = false)
        {
            var user = testUsers.FirstOrDefault(x => x.PlayerNum == playerNum);
            if (user == null)
            {
                user = new TestUser(playerNum, 100 + playerNum, noTgmName ? null : "tgmU" + playerNum, playerNum);
                testUsers.Add(user);
            }
            return user;
        }

        public void ProcessUserMessage(TestUser user, string text)
        {
            ProcessUserMessage(user, Simple(text));
        }

        public void ProcessUserMessage(TestUser user, MessengerMessage messengerMessage)
        {
            gameMessenger.TriggerMessageReceived(new TelegramMessageEvent()
            {
                FromUserId = user.PlayerNum,
                FromChatId = user.ChatId,
                Message = messengerMessage,

                FromUser = new TgmUserInfo()
                {
                    Language = "",
                    FirstName = "Name" + user.PlayerNum,
                    LastName = "LName" + user.PlayerNum,
                    TgmUserName = user.TgmUserName,
                    TgmUserId = user.TgmUserId
                }
            });
        }

        private void ReceiveGroupMessage(int chatId, MessengerMessage messengerMessage)
        {
            gameMessenger.TriggerMessageReceived(new TelegramMessageEvent()
            {
                FromGroupChat = true,
                FromChatId = chatId,
                Message = messengerMessage,
                FromUser = new TgmUserInfo()
                {
                    Language = "",
                    FirstName = "LNameXXX",
                    LastName = "LNameZZ",
                    TgmUserName = "AaA" + chatId,
                    TgmUserId = 0
                }
            });
        }

        public async Task ProcessPlayerMessage(TestUser testUser, MessengerMessage messengerMessage)
        {
            ProcessUserMessage(testUser, messengerMessage);
        }

        public async Task ProcessPlayerMessage(TestUser player, string text)
        {
            await ProcessPlayerMessage(player, Simple(text));
        }

        public async Task ProcessPlayerMessage(TestUser player, string text, string messageId)
        {
            await ProcessPlayerMessage(player, Simple(text, messageId));
        }

        public async Task ProcessPlayerPhoto(TestUser player, string text, string photoId)
        {
            await ProcessPlayerMessage(player, gameMessenger.Photo(text, photoId));
        }
        
        public async Task ProcessUserVideoNote(TestUser player, string vnId)
        {
            await ProcessPlayerMessage(player, gameMessenger.VideoNote(vnId));
        }

        public Task ProcessAdminMessage(string text)
        {
            return ProcessAdminMessage(new MessengerMessage { Text = text });
        }

        public Task ProcessValidatorMessage(string text)
        {
            return ProcessValidatorMessage(new MessengerMessage { Text = text });
        }

        public Task ProcessFeedbackMessage(string text)
        {
            return ProcessFeedbackMessage(new MessengerMessage { Text = text });
        }

        public Task ProcessFeedbackPhoto(string photoId)
        {
            return ProcessFeedbackMessage(new MessengerMessage { PhotoId = photoId });
        }

        public Task ProcessFeedbackVideNote(string vnId)
        {
            return ProcessFeedbackMessage(new MessengerMessage { VideoNoteId = vnId });
        }

        public async Task ProcessAdminMessage(MessengerMessage messengerMessage)
        {
            ReceiveGroupMessage(adminChatId, messengerMessage);
        }

        public async Task ProcessValidatorMessage(MessengerMessage messengerMessage)
        {
            ReceiveGroupMessage(validateChatId, messengerMessage);
        }

        public async Task ProcessFeedbackMessage(MessengerMessage messengerMessage)
        {
            ReceiveGroupMessage(feedbackChatId, messengerMessage);
        }

        public async Task<TestUser> JoinPlayerAndApproveSelfie(int playerNum, Gender gender)
        {
            var player = await JoinPlayer(playerNum);
            await ProcessPlayerMessage(player, "Photo of " + playerNum, "FW:" + "Photo of " + playerNum);
            await ProcessValidatorMessage("yes " + (gender == Gender.Boy ? "boy" : "girl"));
            return player;
        }


        public List<OutgoingMessage> MessagesFor(TestUser p)
        {
            return gameMessenger.GetMessagesFor(p.ChatId)
                .Skip(msgSkipIndex.GetItemOrDefault(p)).ToList();
        }

        public void CheckValidatorReceived(string msg)
        {
            var messages = gameMessenger.GetMessagesFor(validateChatId);
            Assert.IsTrue(messages.Any(x => x.ToString().Contains(msg)), "Validator did not receive message" + msg);
        }

        public void CheckValidatorReceivedPhoto(string id)
        {
            var messages = gameMessenger.GetMessagesFor(validateChatId);
            Assert.IsTrue(messages.Any(x => x.ToString().Contains("[img:" + id + "]")), "Validator did not receive message");
        }

        public void CheckValidatorReceived(MsgFormat format)
        {
            var messages = gameMessenger.GetMessagesFor(validateChatId);
            Assert.IsTrue(messages.Any(x => x.TextFormat == format || x.Text == format.GetFormat(Languages.Russian)),
                "Validator did not receive message");
        }

        public void RunBackgroundActions()
        {
            InScope(s =>
            {
                var gameForBot = s.Resolve<GameService>().GetCurrentGameForBot(botId);
                s.Resolve<GameMissionAssigner>().AssignMissionsAndNotifyPlayers(gameForBot.Id).Wait();
            });
        }

        public TestUser GetPartner(TestUser player)
        {
            return InScope(s =>
            {
                var user = s.Resolve<SnapmeContext>().TgmBotUsers.Single(x => x.TgmChatId == player.ChatId);
                var p = s.Resolve<GameService>().GetPlayer(gameId.Value, user.UserId);
                var partner = s.Resolve<GameService>().GetPartner(p, p.CurrentTask);
                return testUsers.FirstOrDefault(x => x.TgmUserId == partner.User.TgmUserId);
            });
        }

        public int GetPhase(GameTask gameTask)
        {
            return gameTask?.GetActivities().Max(x => x.Phase ?? 1) ?? -1;
        }

        public void CheckPlayerReceived(TestUser player, string msg)
        {
            var messages = MessagesFor(player);
            var idx = messages.FindIndex(x => x.GetText(Languages.Russian) == msg);
            SkipMessages(player, idx);
            Assert.IsTrue(idx >= 0, "Player did not receive " + msg);
        }

        public void CheckPlayerReceivedPhoto(TestUser player, string photo, MsgFormat msg, params object[] args)
        {
            var s = msg == null ? null : string.Format(msg.GetFormat(Languages.Russian), args);

            var messages = MessagesFor(player);
            var idx = messages.FindIndex(x => x.PhotoId == photo && (s == null || x.GetText(Languages.Russian) == s));
            SkipMessages(player, idx);
            Assert.IsTrue(idx >= 0, "Player did not receive " + photo);
        }
        
        public void CheckPlayerReceivedVideoNote(TestUser player, string vnId)
        {
            var messages = MessagesFor(player);
            var idx = messages.FindIndex(m => m.VideoNoteId == vnId);
            SkipMessages(player, idx);
            Assert.IsTrue(idx >= 0, "Player did not receive VideoNote " + vnId);
        }
        

        public void CheckPlayerReceivedLike(TestUser player, string regex)
        {
            var messages = MessagesFor(player);
            var idx = messages.FindIndex(x => Regex.IsMatch(x.GetText(Languages.Russian) ?? "", regex));
            SkipMessages(player, idx);
            Assert.IsTrue(idx >= 0, "Player did not receive " + regex);
        }


        public void CheckPlayerReceived(TestUser player, MsgFormat msg, params object[] args)
        {
            var s = string.Format(msg.GetFormat(Languages.Russian), args);
            var messages = MessagesFor(player);
            var idx = messages.FindIndex(x => x.GetText(Languages.Russian) == s);
            SkipMessages(player, idx);
            Assert.IsTrue(idx >= 0, "Player did not receive " + s);
        }
        
        public void CheckPlayerNotReceived(TestUser player, MsgFormat msg, params object[] args)
        {
            var s = string.Format(msg.GetFormat(Languages.Russian), args);
            var messages = MessagesFor(player);
            var c = messages.Count(x => x.GetText(Languages.Russian) == s);
            Assert.IsTrue(c == 0, "Player receive " + s + c + " times");
        }

        public void CheckPlayerReceivedOnce(TestUser player, MsgFormat msg, params object[] args)
        {
            var s = string.Format(msg.GetFormat(Languages.Russian), args);
            var messages = MessagesFor(player);
            var c = messages.Count(x => x.GetText(Languages.Russian) == s);
            Assert.IsTrue(c == 1, "Player receive " + s + c + " times");
        }

        private void SkipMessages(TestUser player, int idx)
        {
            msgSkipIndex[player] = msgSkipIndex.GetItemOrDefault(player) + idx + 1;
        }
        
        public List<OutgoingMessage> GetValidatorMessages()
        {
            return gameMessenger.GetMessagesFor(validateChatId).ToList();
        }

        public List<OutgoingMessage> GetFeedbackMessages()
        {
            return gameMessenger.GetMessagesFor(feedbackChatId).ToList();
        }

        public void LogAllMessages(long? chatId=null, StringWriter output = null)
        {
            InScope(s =>
            {
                gameMessenger.LogAllMessages(
                    s.Resolve<SnapmeContext>(),
                    s.Resolve<GameService>().GetBot(botId),
                    gameId == null ? null : s.Resolve<GameService>().GetGame(gameId.Value), chatId, output);
            });
        }

        public Guid? CurrentTaskId(TestUser player)
        {
            return InScope(s =>
            {
                var user = s.Resolve<SnapmeContext>().TgmBotUsers.Single(x => x.TgmChatId == player.ChatId);
                var p = s.Resolve<GameService>().GetPlayer(gameId.Value, user.UserId);
                return p.CurrentTaskId;
            });
        }

        public GameTask CurrentTask(TestUser player)
        {
            return InScope(s =>
            {
                var user = s.Resolve<SnapmeContext>().TgmBotUsers.Single(x => x.TgmChatId == player.ChatId);
                var p = s.Resolve<GameService>().GetPlayer(gameId.Value, user.UserId);
                return p.CurrentTask;
            });
        }

        public int CurrentTaskPhase(TestUser player)
        {
            return InScope(s =>
            {
                var user = s.Resolve<SnapmeContext>().TgmBotUsers.Single(x => x.TgmChatId == player.ChatId);
                var p = s.Resolve<GameService>().GetPlayer(gameId.Value, user.UserId);
                return GetPhase(p.CurrentTask);
            });
        }

        public object SelfiePhotoId(TestUser player)
        {
            return InScope(s =>
            {
                var user = s.Resolve<SnapmeContext>().TgmBotUsers.Single(x => x.TgmChatId == player.ChatId);
                var p = s.Resolve<GameService>().GetPlayer(gameId.Value, user.UserId);
                return p.SelfiePhotoId;
            });
        }

        public void ImportSpreadSheet(params ContentSpreadsheetRecord[] spreadsheet)
        {
            InScope(s =>
            {
                ((TestSpreadsheetReader)s.Resolve<ISpreadsheetReader>()).SetContent(spreadsheet);
                s.Resolve<ContentImporter>().ImportContent(botId, true).Wait();
            });
            InScope(x => { x.Resolve<BotContentService>().ResetCache(botId); });
        }

        public MessengerMessage Simple(string s, string id = null)
        {
            return gameMessenger.Simple(s, id);
        }

        public void CheckAdminReceivedLike(string s)
        {
            var messages = gameMessenger.GetMessagesFor(adminChatId)
                .ToList();
            ;
            var idx = messages.FindIndex(x => Regex.IsMatch(x.GetText(Languages.Russian) ?? "", s));
            Assert.IsTrue(idx >= 0, "admin did not receive " + s);
        }

        public void CheckFeedbackReceivedLike(string s)
        {
            var messages = gameMessenger.GetMessagesFor(feedbackChatId)
                .ToList();

            var idx = messages.FindIndex(x => Regex.IsMatch(x.GetText(Languages.Russian) ?? "", s));
            Assert.IsTrue(idx >= 0, "feedback did not receive " + s);
        }


        public void CheckFeedbackReceived(string s)
        {
            var messages = gameMessenger.GetMessagesFor(feedbackChatId);
            Assert.IsTrue(messages.Any(m => m.GetText(Languages.Russian) == s), $"Message '{s}' was not received");
        }

        public void CheckAdminNotReceived(MsgFormat msg, params object[] args)
        {
            var s = string.Format(msg.GetFormat(Languages.Russian), args);
            var messages = gameMessenger.GetMessagesFor(adminChatId).ToList();
            var idx = messages.FindIndex(m => m.GetText(Languages.Russian) == s);
            Assert.IsFalse(idx >= 0, "Admin received " + s);
        }
    }
}