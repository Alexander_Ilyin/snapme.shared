﻿using System;

namespace Snapme.Trinity.Data
{
    public class TrinityExplanation
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public Guid GameId { get; set; }
        
        public Guid PlayerId { get; set; }
        public virtual TrinityPlayer Player { get; set; }

        public DateTime Recorded { get; set; }

        public Guid WordId { get; set; }
        public virtual TrinityWord Word { get; set; }
        
        public string VnId { get; set; }

        public TrinityTaskStatus Status { get; set; }
    }
}