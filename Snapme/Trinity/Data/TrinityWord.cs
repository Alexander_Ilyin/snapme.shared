﻿using System;

namespace Snapme.Trinity.Data
{
    public class TrinityWord
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public Guid GameId { get; set; }

        public string Word { get; set; }
        public bool IsDeleted { get; set; }
    }
}