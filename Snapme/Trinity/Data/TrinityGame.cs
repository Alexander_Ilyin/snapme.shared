﻿using System;
using Snapme.Data.Bots;

namespace Snapme.Trinity.Data
{
    public class TrinityGame
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public DateTime Created { get; set; }

        public bool IsActive { get; set; }
        public Guid TgmBotId { get; set; }
        public virtual TgmBot TgmBot { get; set; }
    }
}