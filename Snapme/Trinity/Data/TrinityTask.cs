﻿using System;

namespace Snapme.Trinity.Data
{
    public class TrinityTask
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public Guid GameId { get; set; }
        
        public Guid PlayerId { get; set; }
        public virtual TrinityPlayer Player { get; set; }
        
        public Guid ExplanationId { get; set; }
        public virtual TrinityExplanation Explanation { get; set; }

        public TrinityTaskStatus Status { get; set; }
    }
}