﻿namespace Snapme.Trinity.Data
{
    public enum TrinityTaskStatus
    {
        Active = 0,
        Solved = 1,
        Verifying = 2,
        Cancelled = 3
    }
}