﻿using System;
using Snapme.Data.Bots;

namespace Snapme.Trinity.Data
{
    public class TrinityPlayer
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public Guid GameId { get; set; }
        
        public Guid UserId { get; set; }
        public virtual User User { get; set; }

        public bool CanRecordVn { get; set; }

        public TrinityStep CurrentStep { get; set; }
    }

    public enum TrinityStep
    {
        None = 0,
        VideoCheck = 1,
        ReadyForGuess = 2,
        Guess = 3,
        ReadyForExplanation = 4,
        Explanation = 5
    }
}