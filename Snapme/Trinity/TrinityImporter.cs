﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Snapme.Bots.Messages;
using Snapme.Bots.Telegram;
using Snapme.Common;
using Snapme.Common.DI;
using Snapme.Data;
using Snapme.Import;

namespace Snapme.Trinity
{
    public class TrinityImporter : IRequestBound
    {
        private readonly TrinityService trinityService;
        private readonly ISpreadsheetReader spreadsheetReader;
        private readonly ITelegramMessenger telegramMessenger;

        public TrinityImporter(TrinityService trinityService, ISpreadsheetReader spreadsheetReader, ITelegramMessenger telegramMessenger, SnapmeContext snapmeContext)
        {
            this.trinityService = trinityService;
            this.spreadsheetReader = spreadsheetReader;
            this.telegramMessenger = telegramMessenger;
        }

        public async Task ImportWords(Guid botId, bool reportIfNoChanges)
        {
            var game = trinityService.GetCurrentGameForBot(botId);
            if (game == null)
                return;
            
            var feedbackChatId = game.TgmBot.FindFeedbackChatId();
            try
            {
                var words = spreadsheetReader.GetTrinityWords(game.TgmBot.GetSettings().InfoSpreadsheetId, "Words");
                if ((trinityService.ImportWords(game.Id, words) || reportIfNoChanges) && feedbackChatId != null)
                    await telegramMessenger.SendMessage(feedbackChatId.Value, OutgoingMessage.Simple("Imported word list"));
            }
            catch (Exception e)
            {
                if (feedbackChatId != null)
                    await telegramMessenger.SendMessage(feedbackChatId.Value, OutgoingMessage.Simple("Failed to import words " + e.Message));
                this.Log().Error("Error importing", e);
            }

        }
    }
}