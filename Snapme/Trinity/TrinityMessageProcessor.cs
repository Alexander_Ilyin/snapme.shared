﻿using System.Threading.Tasks;
using Snapme.Bots.Background;
using Snapme.Bots.Helpers;
using Snapme.Bots.Messages;
using Snapme.Bots.Processors;
using Snapme.Bots.Services;
using Snapme.Bots.Telegram;
using Snapme.Bots.Texts;
using Snapme.Common;
using Snapme.Data;
using Snapme.Trinity.Data;

namespace Snapme.Trinity
{
    public class TrinityMessageProcessor : DefaultMessageProcessor
    {
        private readonly TrinityService trinityService;
        private readonly TrinityImporter importer;
        
        public TrinityMessageProcessor(ITelegramMessenger messenger, UserService userService, BotContentService botContentService, 
            SnapmeContext snapmeContext, TrinityService trinityService, MessageProcessorContext context, ContentImporter contentImporter,
            TrinityImporter importer) 
            : base(messenger, userService, botContentService, snapmeContext, context, contentImporter)
        {
            this.trinityService = trinityService;
            this.importer = importer;
        }

        public static bool IsCreateGameCommand(string message) => message != null && 
                                                                  message.Trim(' ', '/').ToLower() == "createandstarttrinitygame";
        
        private string MessageText => msg.Message.Text ?? "";
        
        protected override async Task ProcessUserMessage()
        {
            var game = trinityService.GetCurrentGameForBot(context.Bot.Id);
            var player = trinityService.GetPlayer(game, user);
            
            if (player == null)
            {
                if (MessageText.ToLower().Trim('/', ' ') == "jointrinitygame")
                {
                    player = trinityService.CreatePlayer(game, user);
                    await SendMessage(msg.FromChatId, OutgoingMessage.Format(TrinityStrings.WelcomeToGame));
                }
                else
                {
                    await base.ProcessUserMessage();
                    return;
                }
            }

            switch (player.CurrentStep)
            {
                case TrinityStep.ReadyForGuess:
                    if (MessageText == "ready")
                    {
                        var task = trinityService.CreateNextTask(player);
                        await SendMessage(msg.FromChatId, OutgoingMessage.VideoNote(task.Explanation.VnId));
                    }
                    break;

                case TrinityStep.Guess:
                {
                    var words = MessageText.SplitRemoveEmpty(' ');
                    if (words.Length == 1)
                    {
                        var success = trinityService.AttemptGuess(player, words[0]);
                        if (success)
                            await SendMessage(msg.FromChatId, OutgoingMessage.Format(TrinityStrings.CorrectWord));
                        else
                            await SendMessage(msg.FromChatId, OutgoingMessage.Format(TrinityStrings.IncorrectWord));
                    }
                    break;
                }
                
                case TrinityStep.VideoCheck:
                    if (msg.Message.VideoNoteId != null)
                    {
                        player.CanRecordVn = true;
                        player.CurrentStep = TrinityStep.None;
                    }
                    break;
                
                case TrinityStep.ReadyForExplanation:
                    if (MessageText == "ready")
                    {
                        var explanation = trinityService.CreateExplanation(player);
                        await SendMessage(msg.FromChatId, OutgoingMessage.Format(TrinityStrings.WordForExplanationIsX, explanation.Word.Word));
                        player.CurrentStep = TrinityStep.Explanation;
                    }
                    break;
                
                case TrinityStep.Explanation:
                    if (msg.Message.VideoNoteId != null)
                    {
                        trinityService.ResolveExplanation(player, msg.Message.VideoNoteId);
                        player.CurrentStep = TrinityStep.None;
                        await SendMessage(msg.FromChatId, OutgoingMessage.Format(TrinityStrings.GotYourExplanation));
                    }
                    break;
            }

            if (player.CurrentStep == TrinityStep.None)
            {
                if (trinityService.ShouldGiveGuessTask(player))
                {
                    player.CurrentStep = TrinityStep.ReadyForGuess;
                    await SendMessage(msg.FromChatId, OutgoingMessage.Format(TrinityStrings.FirstGuessInstructions));
                    await SendMessage(msg.FromChatId, OutgoingMessage.Format(TrinityStrings.GetReadyForGuess));
                }
                else if (!player.CanRecordVn)
                {
                    player.CurrentStep = TrinityStep.VideoCheck;
                    await SendMessage(msg.FromChatId, OutgoingMessage.Format(TrinityStrings.VideoCheckInstructions));
                }
                else if (trinityService.GetWordForExplanation(player) != null)
                {
                    player.CurrentStep = TrinityStep.ReadyForExplanation;
                    if (trinityService.ExplanationCount(player) == 0)
                        await SendMessage(msg.FromChatId, OutgoingMessage.Format(TrinityStrings.FirstExplanationInstructions));
                    await SendMessage(msg.FromChatId, OutgoingMessage.Format(TrinityStrings.GetReadyForExplanation));
                }
                else
                    await SendMessage(msg.FromChatId, OutgoingMessage.Format(TrinityStrings.NoTaskForNow));
            }
            
            snapmeContext.SaveChanges();
        }

        protected override async Task ProcessFeedbackRegularMessage()
        {
            await base.ProcessFeedbackRegularMessage();

            if (msg.Message.Text != null && IsCreateGameCommand(msg.Message.Text))
            {
                trinityService.CreateAndStartGame(context.Bot.Id);
                await SendMessage(FeedbackChatId, OutgoingMessage.Simple("Trinity game created and started"));
            }
        }
    
        protected override async Task ProcessFeedbackRegularMessageImport()
        {
            await base.ProcessFeedbackRegularMessageImport();
            await importer.ImportWords(context.Bot.Id, true);
        }
    }
}