﻿using Snapme.Bots.Texts;

namespace Snapme.Trinity
{
    public class TrinityStrings
    {
        
        public static readonly MsgFormat WelcomeToGame =
            Ru("Добро пожаловать в нашу игру");
        
        
        public static readonly MsgFormat FirstGuessInstructions =
            Ru("Сейчас ты получишь сообщение от одного из игроков. Он объяснит значение слова, а ты должен его угадать и написать мне");
        
        public static MsgFormat GetReadyForGuess =
            Ru("Когда будешь готов угадывать слово, нажми \"Поехали\" [btn:ready;Поехали!]");
        
        public static readonly MsgFormat CorrectWord = Ru("Правильно");
        
        public static readonly MsgFormat IncorrectWord = Ru("Не правильно, попробуй ещё раз");
        
        
        public static readonly MsgFormat VideoCheckInstructions =
            Ru("Давай попробуем записать круглое видео. Запиши что-нибудь и отправь мне");


        public static readonly MsgFormat FirstExplanationInstructions =
            Ru("Сейчас я скажу тебе слово. Твоя задача - записать видео и объяснить его значение. Нельзя использовать однокоренные слова.");
               
        public static readonly MsgFormat GetReadyForExplanation =       
            Ru("Если готов записывать видео про слово, нажми \"Поехали\" [btn:ready;Поехали!]");
        
        public static readonly MsgFormat WordForExplanationIsX =       
            Ru("Слово - {0}. У тебя 30 секунд!");
        
        public static readonly MsgFormat GotYourExplanation =       
            Ru("Окей!");
        
        public static readonly MsgFormat NoTaskForNow =       
            Ru("Сейчас для тебя нет задания, заходи чуть позже");
        
        
        private static MsgFormat Ru(string text)
        {
            return new MsgFormat("NoCode", text);
        }
    }
}