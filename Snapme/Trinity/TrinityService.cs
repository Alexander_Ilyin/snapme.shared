﻿using System;
using System.Linq;
using Snapme.Common;
using Snapme.Common.DI;
using Snapme.Data;
using Snapme.Data.Bots;
using Snapme.Trinity.Data;

namespace Snapme.Trinity
{
    public class TrinityService  : IRequestBound
    {
        private readonly SnapmeContext context;

        public TrinityService(SnapmeContext context)
        {
            this.context = context;
        }
        
        public TrinityGame GetCurrentGameForBot(Guid botId)
        {
            var games = context.TrinityGames.Where(g => g.IsActive && g.TgmBotId == botId);
            if (games.Count() > 1)
                this.Log().Error(games.Count() + " games found for bot " + botId);
            return games.FirstOrDefault();
        }


        public TrinityGame CreateAndStartGame(Guid botId)
        {
            var game = context.AddNew(new TrinityGame { IsActive = true, TgmBotId = botId });
            context.SaveChanges();
            return game;
        }
        
        public TrinityGame GetGame(Guid gameId)
        {
            return context.TrinityGames.Single(g => g.Id == gameId);
        }

        public TrinityPlayer CreatePlayer(TrinityGame game, TgmBotUser user)
        {
            var player = new TrinityPlayer
            {
                GameId = game.Id,
                UserId = user.UserId,
                CanRecordVn = false
            };
            context.Add(player);
            context.SaveChanges();
            return player;
        }

        public TrinityPlayer GetPlayer(TrinityGame game, TgmBotUser user)
        {
            return context.TrinityPlayers.SingleOrDefault(p => p.GameId == game.Id && p.UserId == user.UserId);
        }

        public bool ShouldGiveGuessTask(TrinityPlayer player)
        {
            return ExplanationCount(player) >= TasksCount(player) / 5 && GetExplanationForPlayer(player) != null;
        }

        public int ExplanationCount(TrinityPlayer player)
        {
            return context.TrinityExplanations.Count(e => e.PlayerId == player.Id);
        }

        public int TasksCount(TrinityPlayer player)
        {
            return context.TrinityTasks.Count(t => t.PlayerId == player.Id);
        }

        public TrinityExplanation GetExplanationForPlayer(TrinityPlayer player)
        {
            return context.TrinityExplanations
                .Where(e => e.GameId == player.GameId)
                .Where(e => e.VnId != null)
                .Where(e => e.PlayerId != player.Id)
                .Where(e => !context.TrinityTasks.Any(t => t.ExplanationId == e.Id && t.PlayerId == player.Id))
                .OrderBy(e => context.TrinityTasks.Count(t => t.ExplanationId == e.Id))
                .FirstOrDefault();
        }

        public TrinityTask CreateNextTask(TrinityPlayer player)
        {
            var explanation = GetExplanationForPlayer(player);

            if (explanation == null)
                return null;

            var task = new TrinityTask
            {
                GameId = player.GameId,
                PlayerId = player.Id,
                ExplanationId = explanation.Id,
                Status = TrinityTaskStatus.Active
            };
            player.CurrentStep = TrinityStep.Guess;
            
            return context.AddNew(task);
            
        }

        public bool AttemptGuess(TrinityPlayer player, string word)
        {
            var task = context.TrinityTasks.SingleOrDefault(t => t.PlayerId == player.Id && t.Status == TrinityTaskStatus.Active);
            if (task != null && task.Explanation.Word.Word == word)
            {
                task.Status = TrinityTaskStatus.Solved;
                player.CurrentStep = TrinityStep.None;
                return true;
            }
            return false;
        }

        public object GetCurrentExplanation(TrinityGame game, TrinityPlayer player)
        {
            return context.TrinityExplanations
                .Where(e => e.GameId == game.Id)
                .SingleOrDefault(e => e.PlayerId == player.Id && e.Status == TrinityTaskStatus.Active);
        }
        
        public TrinityExplanation CreateExplanation(TrinityPlayer player)
        {
            var word = GetWordForExplanation(player);

            if (word == null)
            {
                this.Log().Warn("No available explanations");
                return null;
            }
            var explanation = new TrinityExplanation
            {
                Status = TrinityTaskStatus.Active,
                PlayerId = player.Id,
                WordId = word.Id,
                GameId = player.GameId,
            };
            return context.AddNew(explanation);
        }

        public TrinityWord GetWordForExplanation(TrinityPlayer player)
        {
            return context.TrinityWords
                .Where(w => w.GameId == player.GameId && !w.IsDeleted)
                .Where(w => !context.TrinityExplanations.Any(e => e.WordId == w.Id && e.PlayerId == player.Id))
                .OrderBy(w => context.TrinityExplanations.Count(e => e.WordId == w.Id))
                .FirstOrDefault();
        }

        public void ResolveExplanation(TrinityPlayer player, string vnId)
        {
            var explanation = context.TrinityExplanations.Single(e => e.PlayerId == player.Id && e.Status == TrinityTaskStatus.Active);
            explanation.VnId = vnId;
            explanation.Status = TrinityTaskStatus.Solved;
        }

        public bool ImportWords(Guid gameId, string[] words)
        {
            var somethingChanged = false;
            var currentRecords = context.TrinityWords.Where(w => w.GameId == gameId).ToDictionary(w => w.Word);
            
            foreach (var word in words)
            {
                if (currentRecords.ContainsKey(word))
                {
                    currentRecords[word].IsDeleted = false;
                    currentRecords.Remove(word);
                }
                else
                {
                    context.AddNew(new TrinityWord {GameId = gameId, Word = word});
                    somethingChanged = true;
                }
            }
            foreach (var record in currentRecords.Values)
            {
                record.IsDeleted = true;
                somethingChanged = true;
            }
            context.SaveChanges();
            return somethingChanged;
        }

    }
}