using System;

namespace Snapme.Common
{
    public class RandomHelper
    {
        private static readonly Random r = new Random();
        public static bool TestMode;

        public static int Next(int max)
        {
            if (TestMode)
                return max > 1 ? 1 : 0;
            return r.Next(max);
        }
    }
}