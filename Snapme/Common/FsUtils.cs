using System;
using System.IO;

namespace Snapme.Common
{
    public class FsUtils
    {
        public static string FindPath(string path)
        {
            int iteration = 0;
            var prefix = "./";
            while (true)
            {
                iteration++;
                if (iteration > 40)
                {
                    typeof(FsUtils).Log().Error("Can't find " + path);
                    return null;
                }
                var combined = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, prefix, path);
                if (Directory.Exists(combined))
                    return combined;
                if (File.Exists(combined))
                    return combined;
                prefix = "../" + prefix;
                if (prefix.Length > 40)
                    break;
            }
            typeof(FsUtils).Log().Error("Can't find " + path);
            return null;
        }
    }
}