using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Snapme.Common
{
    public static class NetExtensions
    {
        public static void DoNotWait(this Task task)
        {
        }
        
        public static IEnumerable<List<T>> Batches<T>(this IEnumerable<T> items, int size)
        {
            List<T> batch = new List<T>();
            foreach (var item in items)
            {
                batch.Add(item);
                if (batch.Count == size)
                {
                    yield return batch;
                    batch = new List<T>();
                }
            }
            if (batch.Count>0)
                yield return batch;
        }

        public static string[] SplitRemoveEmpty(this string s, char separator)
        {
            return s.Split(separator, StringSplitOptions.RemoveEmptyEntries);
        }
        
        public static string[] SplitRemoveEmpty(this string s, params char[] separator)
        {
            return s.Split(separator, StringSplitOptions.RemoveEmptyEntries);
        }
        
        public static string[] SplitRemoveEmpty(this string s, char separator, int count)
        {
            return s.Split(separator, count, StringSplitOptions.RemoveEmptyEntries);
        }
        
        public static string ToSimpleString(this TimeSpan? s)
        {
            if (s == null)
                return null;
            return Convert.ToInt32(s.Value.Minutes + 60*s.Value.Hours) + ":" + Convert.ToInt32(s.Value.Seconds);
        }
        
        public static bool IsEmptyOrNull(this string s)
        {
            return string.IsNullOrEmpty(s);
        }

        public static int? TryParseInt(this string s)
        {
            if (s.IsEmptyOrNull())
                return null;
            int a;
            if (int.TryParse(s, out a))
                return a;
            return null;
        }

        public static long? TryParseLong(this string s)
        {
            if (s.IsEmptyOrNull())
                return null;
            long a;
            if (long.TryParse(s, out a))
                return a;
            return null;
        }

        public static string EmptyIfNull(this string s)
        {
            if (s == null)
                return "";
            return s;
        }

        public static T GetItemOrDefault<TK, T>(this IDictionary<TK, T> v, TK k)
        {
            T val;
            if (v.TryGetValue(k, out val))
                return val;
            return default(T);
        }

        public static int? IndexOfOrNull<T>(this IEnumerable<T> items, Func<T, bool> check)
        {
            var x = -1;
            foreach (var item in items)
            {
                x++;
                if (check(item))
                    return x;
            }

            return null;
        }

        public static int? ToInt(this double? v)
        {
            if (v.HasValue)
                return Convert.ToInt32(v.Value);
            return null;
        }

        public static TV GetOrAdd<TK, TV>(this IDictionary<TK, TV> items, TK k, Func<TV> getNew)
        {
            if (items.ContainsKey(k))
                return items[k];
            return items[k] = getNew();
        }

        public static T FindWithAlmostMaxOrDefault<T>(this IEnumerable<T> items, Func<T, int> getScore)
        {
            var res = default(T);
            var best = int.MinValue;
            foreach (var item in items)
            {
                var score = getScore(item) * 10000;
                if (score <= 0)
                    continue;
                if (score >= 8) score += RandomHelper.Next(500);
                if (score >= best)
                {
                    best = score;
                    res = item;
                }
            }

            return res;
        }

        public static T FindWithMaxOrDefault<T>(this IEnumerable<T> items, Func<T, int> getScore)
        {
            var res = default(T);
            var best = int.MinValue;
            foreach (var item in items)
            {
                var score = getScore(item);
                if (score <= 0)
                    continue;
                if (score >= best)
                {
                    best = score;
                    res = item;
                }
            }

            return res;
        }

        public static string JoinStr<T, K>(this IEnumerable<T> items, Func<T, K> getKey, string separator = null)
        {
            var s = new StringBuilder();
            foreach (var item in items)
            {
                if (s.Length > 0)
                    s.Append(separator ?? ";");
                s.Append(getKey(item));
            }

            return s.ToString();
        }
        public static Dictionary<TKey, TSource> ToDictionarySafe<TSource, TKey>(this IEnumerable<TSource> source,
            Func<TSource, TKey> keySelector)
        {
            return ToDictionarySafe<TSource, TKey, TSource>(source, keySelector, x=>x);
        }

        public static Dictionary<TKey, TValue> ToDictionarySafe<TSource, TKey, TValue>(this IEnumerable<TSource> source,
            Func<TSource, TKey> keySelector, Func<TSource, TValue> valueSelector)
        {
            if (source == null) 
                return new Dictionary<TKey, TValue>();

            var dictionary = new Dictionary<TKey, TValue>();
            foreach (var item in source)
            {
                var key = keySelector(item);
                if (key==null)
                    continue;
                if (!dictionary.ContainsKey(key))
                {
                    dictionary.Add(key, valueSelector(item));
                }
            }

            return dictionary;
        }
        
        public static Dictionary<TKey, List<T>> ToDictionaryList<TKey, T>(this IEnumerable<T> items,
            Func<T, TKey> getKey)
        {
            var dictionary = new Dictionary<TKey, List<T>>();
            foreach (var item in items)
            {
                var key = getKey(item);
                if (!dictionary.ContainsKey(key))
                    dictionary[key] = new List<T> {item};
                else
                    dictionary[key].Add(item);
            }

            return dictionary;
        }

        public static IEnumerable<IEnumerable<T>> GroupByCount<T>(this IEnumerable<T> items, int count)
        {
            while (items.Any())
            {
                yield return items.Take(count);
                items = items.Skip(count);
            }
        }
    }
}