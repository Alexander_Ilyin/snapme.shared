using Microsoft.EntityFrameworkCore;
using ProxiesExtensions = Microsoft.EntityFrameworkCore.ProxiesExtensions;

namespace Snapme.Common
{
    public static class EFExtensions
    {
        public static T AddNew<T>(this DbContext context, T t) where T : class
        {
            var proxy = ProxiesExtensions.CreateProxy<T>(context);
            foreach (var propertyInfo in t.GetType().GetProperties())
            {
                var value = propertyInfo.GetValue(t);
                if (value != null)
                    propertyInfo.SetValue(proxy, value);
            }

            context.Add(proxy);
            return proxy;
        }

        public static T Proxy<T>(this DbContext context, T t) where T : class
        {
            var proxy = context.CreateProxy<T>();
            foreach (var propertyInfo in t.GetType().GetProperties())
            {
                var value = propertyInfo.GetValue(t);
                if (value != null)
                    propertyInfo.SetValue(proxy, value);
            }

            context.Attach(proxy);
            return proxy;
        }
    }
}