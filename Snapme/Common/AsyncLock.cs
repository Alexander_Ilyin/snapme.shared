using System;
using System.Threading.Tasks;

namespace Snapme.Common
{
    public class AsyncLock
    {
        private volatile Task current;

        public Task<T> Run<T>(Func<Task<T>> func)
        {
            lock (this)
            {
                if (current == null)
                {
                    var task = func();
                    current = task;
                    return task;
                }
                else
                {
                    var task = InSequence(current, func);
                    current = task;
                    return task;
                }
            }
        }

        private async Task<T> InSequence<T>(Task current, Func<Task<T>> func)
        {
            try
            {
                await current;
            }
            catch (Exception ex)
            {
                this.Log().Error(ex);
            }

            return await func();
        }
    }
}