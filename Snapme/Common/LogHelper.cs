using System.IO;
using System.Reflection;
using log4net;
using log4net.Config;

namespace Snapme.Common
{
    public static class LogHelper
    {
        public static ILog Log<T>(this T o)
        {
            return LogManager.GetLogger(typeof(T));
        }

        public static void InitLogger()
        {
            var logDir = Path.GetFullPath(FsUtils.FindPath("logs"));
            GlobalContext.Properties["LogDir"] = logDir;
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            var logCfgPath = FsUtils.FindPath("Configs/log4net.config");
            XmlConfigurator.Configure(logRepository, new FileInfo(logCfgPath));
        }
    }
}