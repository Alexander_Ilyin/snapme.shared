using System.IO;
using Microsoft.Extensions.Configuration;

namespace Snapme.Common
{
    public class ConfigHelper
    {
        public static IConfiguration GetConfig(string profileName)
        {
            var path = FsUtils.FindPath("./Configs");
            if (string.IsNullOrEmpty(profileName))
                profileName = File.ReadAllText(Path.Combine(path, "./profile")).Trim();
            var builder = new ConfigurationBuilder()
                .AddJsonFile(Path.Combine(path, profileName + ".json"), true, true);
            return builder.Build();
        }
    }
}