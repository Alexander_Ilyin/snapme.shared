using System.Text.RegularExpressions;

namespace Snapme.Common
{
    public class GrammarFixer
    {
        public static string FixGrammar(string text)
        {
            text = Regex.Replace(text, @"(?<!\.)\.\.", ".");
            text = Regex.Replace(text, @"\?\.", "?");
            text = Regex.Replace(text, @"\!\.", "!");
            text = Regex.Replace(text, @" +", " ");
            text = Regex.Replace(text, @"\s*\.", ".");
            text = Regex.Replace(text, @"\.(\w)", ". $1");
            if (!text.EndsWith(".") && !text.EndsWith("!") && !text.EndsWith("?"))
                text += ".";
            return text;
        }
    }
}