using System;

namespace Snapme.Common
{
    public class DateHelper
    {
        private static DateTime? last;
        private static DateTime? testValue;

        public static void SetUtcNowForTests(DateTime? testValue)
        {
            lock (typeof(DateHelper))
                DateHelper.testValue = testValue;
        }

        public static DateTime UtcNow()
        {
            lock (typeof(DateHelper))
            {
                if (testValue != null)
                    return testValue.Value;
                var l = last;
                var now = DateTime.UtcNow;
                while (now <= l)
                    now = now.AddMilliseconds(1);
                last = now;
                return now;
            }
        }
    }
}