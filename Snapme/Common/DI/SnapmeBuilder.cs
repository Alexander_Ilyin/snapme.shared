﻿using System;
using System.Linq;
using System.Reflection;
using Autofac;
using Microsoft.Extensions.Caching.Memory;
using Snapme.Bots.Hosting;
using Snapme.Data;
using Snapme.Import;

namespace Snapme.Common.DI
{
    public class SnapmeBuilder
    {
        private readonly Assembly[] assemblies;


        public SnapmeBuilder(Assembly[] assemblies)
        {
            this.assemblies = assemblies.Distinct().ToArray();
        }

        public SnapmeContainer CreateContainer(string profile = null)
        {
            var builder = CreateBuilder(profile);
            var container = builder.Build();
            return new SnapmeContainer(container);
        }

        public ContainerBuilder CreateBuilder(string profile = null, string testId = null)
        {
            var config = ConfigHelper.GetConfig(profile);
            var builder = new ContainerBuilder();
            Configure(builder, assemblies);
            builder.Register(x =>
            {
                var snapmeContext = new SnapmeContext(testId, config);
                return snapmeContext;
            }).AsSelf().InstancePerLifetimeScope();
            builder.Register(x => { return new MemoryCache(new MemoryCacheOptions()); }).AsSelf().SingleInstance();
            builder.RegisterType<SpreadsheetReader>().As<ISpreadsheetReader>().SingleInstance();
            return builder;
        }

        public ContainerBuilder Configure(ContainerBuilder builder, params Assembly[] assemblies)
        {
            bool IsSingleton(Type x)
            {
                return typeof(ISingleton).IsAssignableFrom(x);
            }

            bool IsRequestBound(Type x)
            {
                return typeof(IRequestBound).IsAssignableFrom(x);
            }

            bool IsOneOff(Type x)
            {
                return typeof(IOneOff).IsAssignableFrom(x);
            }

            builder.RegisterAssemblyTypes(assemblies)
                .Where(IsOneOff)
                .AsImplementedInterfaces()
                .AsSelf()
                .InstancePerDependency();

            builder.RegisterAssemblyTypes(assemblies)
                .Where(IsSingleton)
                .AsImplementedInterfaces()
                .AsSelf()
                .SingleInstance();

            builder.RegisterAssemblyTypes(assemblies)
                .Where(IsRequestBound)
                .AsSelf()
                .AsImplementedInterfaces()
                .InstancePerLifetimeScope();
            return builder;
        }
    }
}