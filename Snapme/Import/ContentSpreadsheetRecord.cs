namespace Snapme.Import
{
    public class ContentSpreadsheetRecord
    {
        public ContentSpreadsheetRecord()
        {
        }

        public ContentSpreadsheetRecord(string type, string description)
        {
            Type = type;
            Description = description;
        }

        public string Type { get; set; }
        public string Description { get; set; }
    }
}