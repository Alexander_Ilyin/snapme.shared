using System.Collections.Generic;
using Snapme.Data.Bots;
using Snapme.Data.Games;

namespace Snapme.Import
{
    public interface ISpreadsheetReader
    {
        string[] GetTrinityWords(string sheetId, string listName);
        List<ContentSpreadsheetRecord> GetContent(TgmBotSettings tgmBotSettings);
        List<GameTaskSpreadsheetRecord> GetTasks(TgmBotSettings tgmBotSettings, GameSettings settings);
    }
}