using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core.Internal;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Snapme.Common;
using Snapme.Common.DI;
using Snapme.Data.Bots;
using Snapme.Data.Games;

namespace Snapme.Import
{
    public class SpreadsheetReader : ISpreadsheetReader, ISingleton
    {
        private readonly Lazy<SheetsService> sheetService = new Lazy<SheetsService>(CreateSheetService());

        private static SheetsService CreateSheetService()
        {
            var credential = GoogleCredential.FromFile(FsUtils.FindPath(@"./Configs/service-cred.json"))
                .CreateScoped(SheetsService.Scope.SpreadsheetsReadonly);
            var initializer = new BaseClientService.Initializer { ApplicationName = "snapme", HttpClientInitializer = credential };
            return new SheetsService(initializer);
        }

        public string[] GetTrinityWords(string sheetId, string listName)
        {
            var data = GetRawData(sheetId, listName);
            return data.Where(r => r.Count > 0).Select(r => r[0].ToString()).Where(s => !string.IsNullOrWhiteSpace(s)).ToArray();
        }

        public List<ContentSpreadsheetRecord> GetContent(TgmBotSettings tgmBotSettings)
        {
            var spreadsheetId = tgmBotSettings.InfoSpreadsheetId;
            var sheetName = tgmBotSettings.InfoSpreadsheedListName;

            if (spreadsheetId.IsEmptyOrNull())
                return new List<ContentSpreadsheetRecord>();
            if (sheetName.IsEmptyOrNull())
                return new List<ContentSpreadsheetRecord>();
            var rawData = RetryHelper.Get(() => GetRawData(spreadsheetId, sheetName));
            if (rawData == null)
                return null;
            return ReadContentValues(spreadsheetId, rawData);
        }

        public List<GameTaskSpreadsheetRecord> GetTasks(TgmBotSettings tgmBotSettings, GameSettings settings)
        {
            if (tgmBotSettings.InfoSpreadsheetId.IsEmptyOrNull())
                return new List<GameTaskSpreadsheetRecord>();
            if (settings.TaskSpreadsheedListName.IsEmptyOrNull())
                return new List<GameTaskSpreadsheetRecord>();
            var rawData = GetRawData(tgmBotSettings.InfoSpreadsheetId, settings.TaskSpreadsheedListName);
            if (rawData == null)
                return null;
            return ReadTaskValues(rawData);
        }

        public IList<IList<object>> GetRawData(string sheetId, string listName)
        {
            try
            {
                var service = sheetService.Value;
                {
                    var spreadsheetId = sheetId;
                    var range = listName + "!A:H";
                    var request = service.Spreadsheets.Values.Get(spreadsheetId, range);

                    var response = request.Execute();
                    var values = response.Values;
                    return values;
                }
            }
            catch (Exception e)
            {
                this.Log().ErrorFormat("Error reading spreadsheet '{0}' / '{1}': {2}", sheetId, listName, e.Message);
                throw;
            }
        }
        
        public List<ContentSpreadsheetRecord> ReadContentValues(string sheedId, IList<IList<object>> rawData)
        {
            var headerRow = rawData.FirstOrDefault(r => r.Any(c => c.ToString().EmptyIfNull().ToLower().Contains("Command".ToLower())));
            if (headerRow == null)
            {
                this.Log().Error("Failed to find headers row in " + sheedId);
                return null;
            }

            var cmdIndex = headerRow.IndexOfOrNull(c => c.ToString().EmptyIfNull().ToLower().Contains("Command".ToLower()));
            var descIndex = headerRow.IndexOfOrNull(c => c.ToString().EmptyIfNull().ToLower().Contains("Description".ToLower()));
            
            return rawData.Skip(rawData.IndexOf(headerRow) + 1)
                .Select(r => new ContentSpreadsheetRecord { Type = GetCol(r, cmdIndex), Description = GetCol(r, descIndex) })
                .Where(r => !r.Type.IsNullOrEmpty())
                .ToList();
        }

        private string GetCol(IList<object> row, int? colIndex)
        {
            if (colIndex == null || row == null || row.Count <= colIndex.Value)
                return null;
            return row[colIndex.Value].ToString();
        }

        private List<GameTaskSpreadsheetRecord> ReadTaskValues(IList<IList<object>> values)
        {
            var headerRow = values.FirstOrDefault(r => r.Any(c => c.ToString().EmptyIfNull().ToLower().Contains("TaskType".ToLower())));
            if (headerRow == null)
            {
                this.Log().Error("Failed to find headers row");
                return new List<GameTaskSpreadsheetRecord>();
            }

            var taskTypeIndex = headerRow.IndexOfOrNull(c => c.ToString().EmptyIfNull().ToLower().Contains("TaskType".ToLower()));
            var phaseIndex = headerRow.IndexOfOrNull(c => c.ToString().EmptyIfNull().ToLower().Contains("Phase".ToLower()));
            var levelIndex = headerRow.IndexOfOrNull(c => c.ToString().EmptyIfNull().ToLower().Contains("Level".ToLower()));
            var ruDescriptionIndex = headerRow.IndexOfOrNull(c => c.ToString().EmptyIfNull().ToLower().Contains("Description-RU".ToLower()));
            var enDescriptionIndex = headerRow.IndexOfOrNull(c => c.ToString().EmptyIfNull().ToLower().Contains("Description-EN".ToLower()));

            return values.Skip(values.IndexOf(headerRow) + 1)
                .Select(row => new GameTaskSpreadsheetRecord
                {
                    Type = GetCol(row, taskTypeIndex),
                    Phase = GetCol(row, phaseIndex),
                    Level = GetCol(row, levelIndex),
                    DescriptionRu = GetCol(row, ruDescriptionIndex),
                    DescriptionEn = GetCol(row, enDescriptionIndex)
                })
                .Where(r => !string.IsNullOrEmpty(r.Type)).ToList();
        }
    }

    public class RetryHelper
    {
        public static T Get<T>(Func<T> func)
        {
            for (int i = 0; i < 3; i++)
            {
                try
                {
                    var res = func();
                    return res;
                }
                catch(Exception ex)
                {
                    typeof(RetryHelper).Log().Error(ex);
                }
            }
            return func();
        }
    }
}