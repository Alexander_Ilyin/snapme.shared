namespace Snapme.Import
{
    public class GameTaskSpreadsheetRecord
    {
        public string Type { get; set; }
        public string Phase { get; set; }
        public string Level { get; set; }
        public string DescriptionRu { get; set; }
        public string DescriptionEn { get; set; }
    }
}