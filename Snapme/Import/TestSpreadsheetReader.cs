using System.Collections.Generic;
using System.Linq;
using Snapme.Data.Bots;
using Snapme.Data.Games;
using Snapme.Testing;

namespace Snapme.Import
{
    public class TestSpreadsheetReader : ISpreadsheetReader
    {
        List<GameTaskSpreadsheetRecord> Tasks = new List<GameTaskSpreadsheetRecord>();
        List<ContentSpreadsheetRecord> Content = new List<ContentSpreadsheetRecord>();


        public string[] Words { get; set; } = new string[0];

        public string[] GetTrinityWords(string sheetId, string listName)
        {
            return Words;
        }

        public List<ContentSpreadsheetRecord> GetContent(TgmBotSettings tgmBotSettings)
        {
            return Content;
        }

        public List<GameTaskSpreadsheetRecord> GetTasks(TgmBotSettings tgmBotSettings, GameSettings settings)
        {
            return Tasks;
        }

        public void LoadTasks(TestGameFlags flags)
        {
            Tasks.Clear();
            if ((flags & TestGameFlags.WithNoActivities) != 0)
                return;

            AddActivity("Сделай селфи с Лениным", ActivityType.SelfieTask);
            AddActivity("Сделай селфи с небом", ActivityType.SelfieTask);

            if ((flags & TestGameFlags.WithMeetings) != 0)
            {
                AddActivity("Сделайте селфи со знаком Stop", ActivityType.MeetingTask);
                AddActivity("Сделайте селфи со светофором", ActivityType.MeetingTask);
            }

            AddActivity("Вы на фоне Пушкина", ActivityType.MissionTaskPlace);
            AddActivity("Вы на фоне Оперного", ActivityType.MissionTaskPlace);
            AddActivity("Вы играете в гольф", ActivityType.MissionTaskActivity);
            AddActivity("Вы играете вниз головой", ActivityType.MissionTaskActivity);
            AddActivity("Вы играете на трубе", ActivityType.MissionTaskActivity);
            AddActivity("Вы танцуете", ActivityType.MissionTaskActivity, 2);
            AddActivity("Ты играешь в домино", ActivityType.MissionSingleTask);
            AddActivity("Ты играешь в тетрис", ActivityType.MissionSingleTask, 2);

            if ((flags & TestGameFlags.WithNoFinalActivities) == 0)
                AddActivity("Ты одиноко стоишь у памятника", ActivityType.FinalSingleTask);
            if ((flags & TestGameFlags.WithNoFinalActivities) == 0)
                AddActivity("Ты на потолке", ActivityType.FinalTaskPlace);
        }

        public void AddActivity(string descriptionRu,
            ActivityType gameTaskType, int phase = 1)
        {
            Tasks.Add(new GameTaskSpreadsheetRecord
            {
                DescriptionRu = descriptionRu,
                Type = gameTaskType.ToString(),
                Phase = phase.ToString(),
            });
        }

        public void SetContent(ContentSpreadsheetRecord[] spreadsheet)
        {
            this.Content = spreadsheet.ToList();
        }
    }
}