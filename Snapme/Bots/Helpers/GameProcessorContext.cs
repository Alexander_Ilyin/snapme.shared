using System;
using System.Collections.Generic;
using Snapme.Bots.Services;
using Snapme.Common.DI;
using Snapme.Data.Games;

namespace Snapme.Bots.Helpers
{
    public class GameProcessorContext : IRequestBound
    {
        private Lazy<long[]> _validatorChatIds;
        private Lazy<long?> _adminChatId;
        private Lazy<Game> _game;

        public long[] ValidatorChatIds => _validatorChatIds.Value;
        public long? AdminChatId => _adminChatId.Value;
        public Game Game => _game.Value;

        public GameProcessorContext(MessageProcessorContext messageProcessorContext, GameService gameService)
        {
            _game = new Lazy<Game>(() => { return gameService.GetCurrentGameForBot(messageProcessorContext.Bot.Id); });
            _validatorChatIds = new Lazy<long[]>(() => { return (this._game.Value.GetSettings().ValidationChatIds ?? new List<long>()).ToArray(); });
            _adminChatId = new Lazy<long?>(() => { return this._game.Value.GetSettings().AdminChatId; });
        }
    }
}