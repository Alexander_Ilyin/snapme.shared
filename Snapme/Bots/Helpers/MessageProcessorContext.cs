using Snapme.Bots.Telegram;
using Snapme.Common.DI;
using Snapme.Data.Bots;

namespace Snapme.Bots.Helpers
{
    public class MessageProcessorContext : IRequestBound
    {
        private TgmBotUser botUser;
        private TelegramMessageEvent msg;
        private TgmBot bot;

        public void Init(TgmBot bot, TgmBotUser botUser, TelegramMessageEvent msg)
        {
            this.bot = bot;
            this.botUser = botUser;
            this.msg = msg;
        }

        public TgmBot Bot => bot;
        public TgmBotUser BotUser => botUser;
        public TelegramMessageEvent Msg => msg;
    }
}