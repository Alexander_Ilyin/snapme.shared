using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Snapme.Bots.Games;
using Snapme.Bots.Messages;
using Snapme.Bots.Services;
using Snapme.Bots.Telegram;
using Snapme.Bots.Texts;
using Snapme.Common;
using Snapme.Common.DI;
using Snapme.Data;
using Snapme.Data.Bots;
using Snapme.Data.Games;

namespace Snapme.Bots.Background
{
    public class GameMissionAssigner : IRequestBound
    {
        private readonly GameService gameService;
        private readonly ITelegramMessenger telegramMessenger;
        private readonly SnapmeContext snapmeContext;

        public GameMissionAssigner(GameService gameService, ITelegramMessenger telegramMessenger, SnapmeContext snapmeContext)
        {
            this.gameService = gameService;
            this.telegramMessenger = telegramMessenger;
            this.snapmeContext = snapmeContext;
        }

        public async Task AssignMissionsAndNotifyPlayers(Guid gameId, bool skipWaiting = false)
        {
            var game = gameService.GetGame(gameId);
            if (game.GameStatus == GameStatus.Finished || game.GameStatus == GameStatus.New)
                return;

            var sw = Stopwatch.StartNew();
            var freePlayers = gameService.GetFreePlayers(game);
            this.Log().DebugFormat("Perf-GetFreePlayers took {0}ms", sw.ElapsedMilliseconds);
            sw.Reset();
            sw.Start();
            var completeTasks = gameService.GetCompleteTasksOfFreePlayers(gameId, freePlayers);
            this.Log().DebugFormat("Perf-GetCompleteTasksOfFreePlayers took {0}ms", sw.ElapsedMilliseconds);
            sw.Reset();
            sw.Start();

            var missions = await gameService.AssignMissionToFreePlayers(gameId, freePlayers, completeTasks, skipWaiting);
            await NotifyPlayersOfAssignedMissions(gameId, missions, completeTasks);
            this.Log().DebugFormat("Perf-AssignMissionToFreePlayers took {0}ms", sw.ElapsedMilliseconds);
            sw.Reset();
            sw.Start();
            await gameService.SaveChangesAsync();
            this.Log().DebugFormat("Perf-AssignMissionToFreePlayers-SaveChanges took {0}ms", sw.ElapsedMilliseconds);
        }

        private async Task NotifyPlayersOfAssignedMissions(Guid gameId, List<GameTask> missions,
            Dictionary<User, List<DetachedGameTask>> completeTasks)
        {
            foreach (var mission in missions)
            foreach (var assignee in mission.Assigned)
            {
                var completePlayerTasks = completeTasks.GetItemOrDefault(assignee.User) ?? new List<DetachedGameTask>();

                var completeCount = completePlayerTasks?.Count ?? 0;
                var player = gameService.GetPlayer(mission.GameId, assignee.UserId);

                var lastTask = completePlayerTasks.LastOrDefault();
                var lastPartnerUserId = lastTask?.GetPartnerFor(player.UserId);
                var partner = gameService.GetPartner(player, mission);

                if (partner != null && partner.UserId != lastPartnerUserId)
                {
                    if (!string.IsNullOrEmpty(partner.SelfiePhotoId))
                        if (!partner.User.IsFake)
                            await SendToPlayer(player, OutgoingMessage.Photo(partner.SelfiePhotoId,
                                MsgFormats.YourPartnerPPartnerUserIdPPartnerUsername,
                                partner.User.TgmUserId, partner.User.Name()
                            ));
                        else
                            await SendToPlayer(player, OutgoingMessage.Photo(partner.SelfiePhotoId,
                                MsgFormats.YourPartnerOfflinePPartnerUserIdPPartnerUsername,
                                partner.User.Name()
                            ));


                    bool sendMeetingHint = mission.TaskType == GameTaskType.Meeting && completeCount <= 1;

                    if ((mission.TaskType == GameTaskType.Final || mission.TaskType == GameTaskType.Mission) &&
                        completeCount <= 1 &&
                        gameService.GetGame(gameId).GetSettings().WithMeetings != true)
                        sendMeetingHint = true;

                    if (sendMeetingHint)
                        if (partner.User.IsFake)
                            await SendToPlayer(player, MsgFormats.MeetingHintOffline);
                        else
                            await SendToPlayer(player, MsgFormats.MeetingHint);
                }


                await SendToPlayer(player, completeCount == 0 ? MsgFormats.FirstTask : MsgFormats.YourTask);
                await SendToPlayer(player,
                    OutgoingMessage.Simple("<b>" + gameService.GetDescription(mission) + "</b>"));

                if (partner != null && lastPartnerUserId == partner.UserId)
                    await SendToPlayer(player, MsgFormats.PartnerIsSame);

                if (mission.IsMission() && completePlayerTasks.All(t => !t.IsMission()))
                    await SendToPlayer(player, MsgFormats.MissionHint);

                else if (mission.TaskType == GameTaskType.Selfie &&
                         completePlayerTasks.All(t => t.TaskType != GameTaskType.Selfie))
                    await SendToPlayer(player, MsgFormats.SelfieHint);
            }
        }

        private async Task SendToPlayer(Player player, OutgoingMessage message)
        {
            var botId = gameService.GetGame(player.GameId).TgmBotId;
            var botUser = await snapmeContext.TgmBotUsers.FindAsync(botId, player.User.Id);
            if (botUser.TgmChatId.HasValue)
                await telegramMessenger.SendMessage(botUser.TgmChatId.Value, message, Languages.Russian);
            else
                this.Log().Error("Error in user no tgmId");
        }

        private async Task SendToPlayer(Player player, MsgFormat message, params object[] parameters)
        {
            await SendToPlayer(player, OutgoingMessage.Format(message, parameters));
        }
    }
}