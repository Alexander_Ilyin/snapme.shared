using System;
using System.Threading.Tasks;
using Snapme.Bots.Games;
using Snapme.Bots.Messages;
using Snapme.Bots.Services;
using Snapme.Bots.Telegram;
using Snapme.Common;
using Snapme.Common.DI;
using Snapme.Import;

namespace Snapme.Bots.Background
{
    public class GameTaskImporter : IRequestBound
    {
        private readonly ISpreadsheetReader spreadsheetReader;
        private readonly GameService gameService;
        private readonly GameTaskImportService gameTaskImportService;
        private readonly ITelegramMessenger telegramMessenger;

        public GameTaskImporter(ISpreadsheetReader spreadsheetReader, GameService gameService,
            GameTaskImportService gameTaskImportService, ITelegramMessenger telegramMessenger)
        {
            this.spreadsheetReader = spreadsheetReader;
            this.gameService = gameService;
            this.gameTaskImportService = gameTaskImportService;
            this.telegramMessenger = telegramMessenger;
        }

        public async Task ImportTasks(Guid gameId, bool reportIfNoChanges)
        {
            var game = gameService.GetGame(gameId);
            var feedbackChatId = game.TgmBot.FindFeedbackChatId();
            try
            {
                var records = spreadsheetReader.GetTasks(game.TgmBot.GetSettings(), game.GetSettings());
                if (records != null)
                {
                    var importResult = gameTaskImportService.Import(game, records);
                    if (reportIfNoChanges || importResult.HasChanges)
                        if (feedbackChatId != null)
                            await telegramMessenger.SendMessage(feedbackChatId.Value, OutgoingMessage.Simple("Imported tasks"));
                }
                else if (feedbackChatId != null)
                    await telegramMessenger.SendMessage(feedbackChatId.Value, OutgoingMessage.Simple("Failed to extract tasks data"));
            }
            catch (Exception e)
            {
                if (feedbackChatId != null)
                    await telegramMessenger.SendMessage(feedbackChatId.Value, OutgoingMessage.Simple("Failed to extract content data " + e.Message));
                this.Log().Error("Error importing", e);
            }
        }
    }
}