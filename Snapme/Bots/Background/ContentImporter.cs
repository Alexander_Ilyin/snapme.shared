using System;
using System.Threading.Tasks;
using Snapme.Bots.Messages;
using Snapme.Bots.Telegram;
using Snapme.Bots.Texts;
using Snapme.Common;
using Snapme.Common.DI;
using Snapme.Data;
using Snapme.Import;

namespace Snapme.Bots.Background
{
    public class ContentImporter : IRequestBound
    {
        private readonly SnapmeContext snapmeContext;
        private readonly ISpreadsheetReader spreadsheetReader;
        private readonly BotContentService botContentService;
        private readonly ITelegramMessenger telegramMessenger;

        public ContentImporter(SnapmeContext snapmeContext, ISpreadsheetReader spreadsheetReader,
            BotContentService botContentService, ITelegramMessenger telegramMessenger)
        {
            this.snapmeContext = snapmeContext;
            this.spreadsheetReader = spreadsheetReader;
            this.botContentService = botContentService;
            this.telegramMessenger = telegramMessenger;
        }

        public async Task ImportContent(Guid botId, bool reportIfNoChanges)
        {
            var tgmBot = snapmeContext.TgmBots.Find(botId);
            var feedbackChatId = tgmBot.FindFeedbackChatId();
            try
            {
                {
                    var records = spreadsheetReader.GetContent(tgmBot.GetSettings());
                    if (records != null)
                    {
                        var importResult = botContentService.ImportRecords(tgmBot, records);
                        if (importResult == ImportResult.Imported || reportIfNoChanges)
                            if (feedbackChatId.HasValue)
                                await telegramMessenger.SendMessage(feedbackChatId.Value, OutgoingMessage.Simple("Imported content"));
                    }
                    else
                    {
                        if (feedbackChatId.HasValue)
                            await telegramMessenger.SendMessage(feedbackChatId.Value, OutgoingMessage.Simple("Failed to extract contentdata"));
                    }
                }
            }
            catch (Exception e)
            {
                if (feedbackChatId.HasValue)
                    await telegramMessenger.SendMessage(feedbackChatId.Value,
                        OutgoingMessage.Simple("Failed to extract contentdata " + e.Message));
                this.Log().Error("Error importing", e);
            }
        }
    }
}