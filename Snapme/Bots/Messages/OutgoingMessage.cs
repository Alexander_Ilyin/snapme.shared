using System.Collections.Generic;
using System.Linq;
using System.Text;
using Snapme.Bots.Texts;
using Snapme.Common;
using Snapme.Data.Bots;

namespace Snapme.Bots.Messages
{
    public class OutgoingMessage
    {
        public string Text { get; set; }
        public string PhotoId { get; set; }
        public string VideoNoteId { get; set; }
        public MsgFormat TextFormat { get; set; }
        public object[] TextFormatParams { get; set; }
        public MessangerMsgInfo ForwardedMsg { get; set; }
        public MessangerMsgInfo ReplyTo { get; set; }

        public static OutgoingMessage Forwarded(MessangerMsgInfo forwardInfo)
        {
            return new OutgoingMessage
            {
                ForwardedMsg = forwardInfo
            };
        }

        public static OutgoingMessage Forwarded(MessengerMessage message)
        {
            return new OutgoingMessage
            {
                ForwardedMsg = message.MsgInfo
            };
        }

        public static OutgoingMessage Format(MsgFormat s, params object[] parameters)
        {
            return new OutgoingMessage
            {
                TextFormat = s,
                TextFormatParams = parameters
            };
        }


        public static OutgoingMessage Simple(string s)
        {
            return new OutgoingMessage { Text = s };
        }

        public OutgoingMessage InReplyTo(MessengerMessage serviceMessage)
        {
            ReplyTo = serviceMessage.ReplyTo;
            return this;
        }

        public static OutgoingMessage Photo(string photoId, MsgFormat caption, params object[] p)
        {
            return new OutgoingMessage
            {
                PhotoId = photoId,
                TextFormat = caption,
                TextFormatParams = p
            };
        }

        public static OutgoingMessage Photos(IEnumerable<string> photoIds)
        {
            return Simple(string.Join("", photoIds.Select(i => $"[img:{i}]")));
        }

        public static OutgoingMessage VideoNote(string vnId)
        {
            return Simple($"[vn:{vnId}]");
        }

        public string GetText(string lang)
        {
            if (Text != null)
                return Text;
            if (TextFormat != null)
                return string.Format(TextFormat.GetFormat(lang), TextFormatParams ?? new object[0]);
            return null;
        }

        public override string ToString()
        {
            if (ForwardedMsg != null)
                return "Forwarded-" + ForwardedMsg.ToString();
            if (PhotoId != null)
                return "[img:" + PhotoId + "]" + GetText(Languages.Russian);
            if (VideoNoteId != null)
                return "[vn:" + VideoNoteId + "]";
            var text = GetText(Languages.Russian);
            if (ReplyTo != null)
                text += "/ReplyTo:" + ReplyTo.Data;
            return text;
        }

        public static OutgoingMessage KeyboardUpdate(MessangerMsgInfo msgInfo, string newMarkup)
        {
            return new OutgoingMessage
            {
                Text = newMarkup,
                UpdateKeyboardOf = msgInfo
            };
        }

        public MessangerMsgInfo UpdateKeyboardOf { get; set; }
    }
}