namespace Snapme.Bots.Messages
{
    public class MsgSegmentButton
    {
        public string BtnText { get; set; }
        public string BtnCmd { get; set; }
    }
}