namespace Snapme.Bots.Messages
{
    public class MessangerMsgInfo
    {
        public string Data;

        public MessangerMsgInfo(string data)
        {
            Data = data;
        }

        public override string ToString()
        {
            return "Msg:" + Data;
        }
    }
}