using System;
using System.Linq;
using Snapme.Common;

namespace Snapme.Bots.Messages
{
    public class MessengerMessage
    {
        public string Text { get; set; }
        public MessangerMsgInfo ReplyTo { get; set; }
        public MessangerMsgInfo MsgInfo { get; set; }
        public string PhotoId { get; set; }
        public string VideoNoteId { get; set; }
        public string AudioId { get; set; }
        public string VoiceId { get; set; }
        public bool IsCallbackQuery { get; set; }

        public MessangerCommand ParseAsCommand()
        {
            var args = (Text ?? "").Split(' ', 2, StringSplitOptions.RemoveEmptyEntries);
            var action = (args.FirstOrDefault() ?? "").ToLowerInvariant().Trim('/');
            if (action.IndexOf('@') >= 0)
                action = action.Substring(0, action.IndexOf('@'));
            return new MessangerCommand
            {
                Action = action.Replace("-",""),
                Parameter = args.Skip(1).FirstOrDefault() ?? ""
            };
        }

        public bool IsVideoNote()
        {
            return !VideoNoteId.IsEmptyOrNull();
            
        }
        public bool IsAudio()
        {
            return !AudioId.IsEmptyOrNull();
            
        }
        public bool IsVoice()
        {
            return !VoiceId.IsEmptyOrNull();
            
        }
        public bool IsPhoto()
        {
            return !PhotoId.IsEmptyOrNull();
        }

        public override string ToString()
        {
            var res = "";
            res += Text;
            if (IsPhoto())
                res += "[img:" + PhotoId + "]";
            if (IsVideoNote())
                res += "[vn:" + VideoNoteId + "]";
            if (ReplyTo != null)
                res += "/ReplyTo:" + ReplyTo.Data;
            return res;
        }

    }
}