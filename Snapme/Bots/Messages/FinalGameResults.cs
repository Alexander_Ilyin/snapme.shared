using System.Collections.Generic;

namespace Snapme.Bots.Messages
{
    public class FinalGameResults
    {
        public List<FinalGameResultsItem> Items = new List<FinalGameResultsItem>();
    }
}