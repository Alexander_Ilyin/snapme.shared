namespace Snapme.Bots.Messages
{
    public class MessangerCommand
    {
        public string Action { get; set; } = "";
        public string Parameter { get; set; } = "";
    }
}