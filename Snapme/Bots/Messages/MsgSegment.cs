using System.Collections.Generic;

namespace Snapme.Bots.Messages
{
    public class MsgSegment
    {
        public string ImageUrl { get; set; }
        public string Text { get; set; }
        public bool LinkPreview { get; set; }
        public List<MsgSegmentButton> Buttons { get; set; } = new List<MsgSegmentButton>();
        public string VideoNoteUrl { get; set; }
        public string AudioUrl { get; set; }
        public string VoiceUrl { get; set; }
    }
}