using Snapme.Data.Bots;

namespace Snapme.Bots.Messages
{
    public class FinalGameResultsItem
    {
        public User User { get; set; }
        public int Score { get; set; }
    }
}