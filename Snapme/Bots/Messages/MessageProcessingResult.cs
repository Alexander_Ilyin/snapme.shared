namespace Snapme.Bots.Messages
{
    public enum MessageProcessingResult
    {
        MessageNotProcessed,
        MessageProcessingFinished,
    }
}