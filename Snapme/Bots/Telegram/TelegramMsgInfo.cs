using Telegram.Bot.Types;

namespace Snapme.Bots.Telegram
{
    public class TelegramMsgInfo
    {
        public long? ForwardFromChatId;

        public TelegramMsgInfo()
        {
        }

        public TelegramMsgInfo(Message message)
        {
            ChatId = message.Chat.Id;
            MessageId = message.MessageId;
            MessageText = message.Text;

            ForwardFromChatId = message?.ForwardFromChat?.Id;
            ForwardFromMessageId = message?.ForwardFromMessageId;
            ForwardFromUserId = message?.ForwardFrom?.Id;

            if (message.ReplyToMessage != null)
                ReplyTo = new TelegramMsgInfo(message.ReplyToMessage);
        }

        public string MessageText { get; set; }

        public int? ForwardFromUserId { get; set; }

        public TelegramMsgInfo ReplyTo { get; set; }

        public long ChatId { get; set; }
        public int MessageId { get; set; }
        public int? ForwardFromMessageId { get; set; }
    }
}