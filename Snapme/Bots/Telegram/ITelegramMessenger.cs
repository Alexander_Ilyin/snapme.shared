using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Snapme.Bots.Hosting;
using Snapme.Bots.Messages;
using Snapme.Data.Bots;

namespace Snapme.Bots.Telegram
{
    public interface ITelegramMessenger
    {
        event Action<TelegramMessageEvent> MessageReceived;
        event Action<TelegramBotEvent> Update;
        IEnumerable<long> FindForwardFromUserIds(MessangerMsgInfo msgInfo);
        Task SendMessage(long targetChatId, OutgoingMessage msg, string language=Languages.Russian);
        void Start(string botKey);
        void Stop();
    }
}