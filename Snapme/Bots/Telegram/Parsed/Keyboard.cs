using System.Collections.Generic;
using System.Linq;
using Snapme.Bots.Messages;
using Telegram.Bot.Types.ReplyMarkups;

namespace Snapme.Bots.Telegram.Parsed
{
    public class Keyboard
    {
        public List<MsgSegment> Buttons = new List<MsgSegment>();

    }
}