using System.Collections.Generic;
using Snapme.Bots.Messages;

namespace Snapme.Bots.Telegram
{
    public class VideoMessage
    {
        public string VideoId { get; set; }
        public List<MsgSegment> Buttons { get; set; } = new List<MsgSegment>();
    }
}