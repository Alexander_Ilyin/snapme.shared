using System.Collections.Generic;
using Snapme.Bots.Messages;

namespace Snapme.Bots.Telegram
{
    public class AudioMessage
    {
        public string AudioId { get; set; }
        public List<MsgSegment> Buttons { get; set; } = new List<MsgSegment>();
    }
}