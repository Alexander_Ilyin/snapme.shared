using System.Collections.Generic;
using Snapme.Bots.Messages;

namespace Snapme.Bots.Telegram.Parsed
{
    public class TextMessage
    {

        public bool LinkPreview { get; set;  }
        public string Text { get; set; }
        public List<MsgSegment> Buttons { get; set; } = new List<MsgSegment>();

    }
}