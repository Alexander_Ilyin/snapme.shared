using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Snapme.Bots.Telegram.Parsed
{
    public class AlbumItem
    {
        public string Text { get; set; }
        public string PhotoId { get; set; }

        public IAlbumInputMedia ToMedia()
        {
            return new InputMediaPhoto() {Caption = Text, Media = new InputMedia(PhotoId), ParseMode = ParseMode.Html};
        }
    }
}