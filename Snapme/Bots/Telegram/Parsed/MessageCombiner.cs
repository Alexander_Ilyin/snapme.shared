using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Castle.Core.Internal;
using log4net;
using Snapme.Bots.Messages;

namespace Snapme.Bots.Telegram.Parsed
{
    public static class MessageCombiner
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(MessageCombiner));


        public static CombinedMessage[] GetCombinedMessages(string text)
        {
            return text.Split("---").SelectMany(ParseAndCombine).ToArray();
        }

        private static List<CombinedMessage> ParseAndCombine(string s)
        {
            var segments = Parse(s.Trim());
            var messages = Combine(s, segments);
            return messages;
        }

        public static MsgSegment[] Parse(string text)
        {
            if (text.StartsWith(@"plaintext:"))
                return new[] { new MsgSegment { Text = text.Substring(10) } };

            var linkPreview = text.Contains("[preview]", StringComparison.OrdinalIgnoreCase);
            if (linkPreview)
                text = text.Replace("[preview]", "", StringComparison.OrdinalIgnoreCase);

            var segments = Regex.Split(text, @"(\[btn:.*?\])|(\[img:.*?\])|(\[vn:.*?\])|(\[audio:.*?\])|(\[voice:.*?\])").Where(x=>!x.IsNullOrEmpty()).ToList();
            var res = new List<MsgSegment>();
            foreach (var segment in segments)
            {
                 var x = segment;
                if (x.StartsWith("[audio:") && x.EndsWith("]"))
                {
                    res.Add(new MsgSegment { AudioUrl = x.Substring("[audio:".Length).Trim(']') });
                }
                if (x.StartsWith("[voice:") && x.EndsWith("]"))
                {
                    res.Add(new MsgSegment { VoiceUrl = x.Substring("[voice:".Length).Trim(']') });
                }
                if (x.StartsWith("[vn:") && x.EndsWith("]"))
                {
                    res.Add(new MsgSegment { VideoNoteUrl = segment.Substring("[vn:".Length).Trim(']') });
                }
                else if (segment.StartsWith("[img:") && segment.EndsWith("]"))
                {
                    res.Add(new MsgSegment { ImageUrl = segment.Substring("[img:".Length).Trim(']') });
                }
                else if (segment.StartsWith("[btn:") && segment.EndsWith("]"))
                {
                    var btnText = segment.Substring("[btn:".Length).Trim(']');
                    var strings = btnText.Split("|", StringSplitOptions.RemoveEmptyEntries);
                    res.Add(new MsgSegment
                    {
                        Buttons = strings.Select(s =>
                        {
                            var parts = s.Split(";", StringSplitOptions.RemoveEmptyEntries);
                            return parts.Length == 1 ? new MsgSegmentButton { BtnText = parts[0] } : new MsgSegmentButton { BtnText = parts[1], BtnCmd = parts[0] };
                        }).ToList()
                    });
                }
                else
                {
                    res.Add(new MsgSegment { Text = segment.Trim(), LinkPreview = linkPreview });
                }
            }

            return res.ToArray();
        }

        public static List<CombinedMessage> Combine(string sourceText, MsgSegment[] segments)
        {
            var res = new List<CombinedMessage>();
            CombinedMessage current = null;
            foreach (var segment in segments)
            {
                if (segment.Text != null)
                {
                    if (String.IsNullOrWhiteSpace(segment.Text))
                        continue;
                    if (current == null)
                    {
                        current = new CombinedMessage();
                        res.Add(current);
                        current.TextMessage = new TextMessage() { Text = segment.Text };
                    }
                    else if (current.VideoNote != null)
                    {
                        current = new CombinedMessage();
                        res.Add(current);
                        current.TextMessage = new TextMessage() { Text = segment.Text };
                    }
                    else if (current.Photo != null)
                    {
                        if ((current.Photo.Text?.Length ?? 0) + segment.Text.Length > 200)
                        {
                            current = new CombinedMessage();
                            res.Add(current);
                            current.TextMessage = new TextMessage() { Text = segment.Text };
                        }
                        else
                            current.Photo.Text += segment.Text;
                    }
                    else if (current.AlbumMessage != null)
                    {
                        var albumItem = current.AlbumMessage.Items.Last();
                        if ((albumItem?.Text?.Length ?? 0) + segment.Text.Length > 200)
                        {
                            current = new CombinedMessage();
                            res.Add(current);
                            current.TextMessage = new TextMessage { Text = segment.Text };
                        }
                        else
                            albumItem.Text += segment.Text;
                    }
                }

                if (segment.AudioUrl != null)
                {
                    current = new CombinedMessage();
                    res.Add(current);
                    current.Audio = new AudioMessage() { AudioId = segment.AudioUrl };
                }
                if (segment.VoiceUrl != null)
                {
                    current = new CombinedMessage();
                    res.Add(current);
                    current.Voice = new VoiceMessage() { VoiceId = segment.VoiceUrl };
                }
                if (segment.VideoNoteUrl != null)
                {
                    current = new CombinedMessage();
                    res.Add(current);
                    current.VideoNote = new VideoMessage() { VideoId = segment.VideoNoteUrl };
                }
                if (segment.ImageUrl != null)
                {
                    if (current == null || current.TextMessage != null)
                    {
                        current = new CombinedMessage();
                        res.Add(current);
                        current.Photo = new PhotoMessage { PhotoId = segment.ImageUrl };
                    }
                    else if (current.Photo != null)
                    {
                        current.AlbumMessage = new AlbumMessage();
                        current.AlbumMessage.Items.Add(new AlbumItem { PhotoId = current.Photo.PhotoId, Text = current.Photo.Text });
                        current.AlbumMessage.Items.Add(new AlbumItem { PhotoId = segment.ImageUrl });
                        current.Photo = null;
                    }
                    else if (current.AlbumMessage != null)
                    {
                        current.AlbumMessage.Items.Add(new AlbumItem { PhotoId = segment.ImageUrl });
                    }
                }
                if (segment.Buttons != null && segment.Buttons.Count > 0)
                {
                    var target = current ?? res.LastOrDefault();
                    if (target == null)
                    {
                        Log.Error("Invalid message " + sourceText);
                        continue;
                    }
                    current = null;

                    if (target.AlbumMessage != null)
                    {
                        var last = target.AlbumMessage.Items.Last();
                        target.AlbumMessage.Items.Remove(last);
                        res.Add(new CombinedMessage()
                        {
                            Photo = new PhotoMessage()
                            {
                                PhotoId = last.PhotoId,
                                Text = last.Text,
                                Buttons = new List<MsgSegment>() { segment }
                            }
                        });
                    }
                    else if (target.Voice != null)
                    {
                        target.Voice.Buttons.Add(segment);
                        
                    }
                    else if (target.Audio != null)
                    {
                        target.Audio.Buttons.Add(segment);
                    }
                    else if (target.VideoNote != null)
                    {
                        target.VideoNote.Buttons.Add(segment);
                    }
                    else if (target.Photo != null)
                    {
                        target.Photo.Buttons.Add(segment);
                    }
                    else if (target.TextMessage != null)
                    {
                        target.TextMessage.Buttons.Add(segment);
                    }
                }

                if (segment.LinkPreview && current.TextMessage != null)
                    current.TextMessage.LinkPreview = true;
            }
            return res;
        }
    }
}