using System.Collections.Generic;
using Snapme.Bots.Messages;

namespace Snapme.Bots.Telegram
{
    public class VoiceMessage
    {
        public string VoiceId { get; set; }
        public List<MsgSegment> Buttons { get; set; } = new List<MsgSegment>();
    }
}