using System.Collections.Generic;
using Snapme.Bots.Messages;

namespace Snapme.Bots.Telegram.Parsed
{
    public class PhotoMessage
    {
        public string Text { get; set; }
        public string PhotoId { get; set; }
        public List<MsgSegment> Buttons { get; set; } = new List<MsgSegment>();
    }
}