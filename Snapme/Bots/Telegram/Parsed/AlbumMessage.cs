using System.Collections.Generic;

namespace Snapme.Bots.Telegram.Parsed
{
    public class AlbumMessage
    {
        public List<AlbumItem> Items { get; set; } = new List<AlbumItem>();
    }
}