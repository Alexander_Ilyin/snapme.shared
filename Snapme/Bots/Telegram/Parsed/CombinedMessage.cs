using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Snapme.Bots.Messages;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InputFiles;
using Telegram.Bot.Types.ReplyMarkups;

namespace Snapme.Bots.Telegram.Parsed
{
    public class CombinedMessage
    {
        public AlbumMessage AlbumMessage { get; set; }
        public PhotoMessage Photo { get; set; }
        public TextMessage TextMessage { get; set; }
        public VideoMessage VideoNote { get; set; }
        public AudioMessage Audio { get; set; }
        public VoiceMessage Voice { get; set; }

        public static IReplyMarkup GetMarkup(IEnumerable<MsgSegment> segments)
        {
            if (segments == null)
                return null;
            segments = segments.ToList();
            if (!segments.Any())
                return null;
            var withoutCommands = segments.Any(x => x.Buttons.Any(b => b.BtnCmd == null && b.BtnText != null));
            
            if (withoutCommands)
            {
                var keyboardButtons = segments.Select(x => x.Buttons.Select(b => new KeyboardButton(b.BtnText)).ToArray());
                return new ReplyKeyboardMarkup(keyboardButtons) {OneTimeKeyboard = true};
            }
            else
            {
                var keyboardButtons = segments.Select(s =>
                    s.Buttons.Select(b => InlineKeyboardButton.WithCallbackData(b.BtnText, b.BtnCmd)).ToArray());
                return new InlineKeyboardMarkup(keyboardButtons);
            }
        }

        public async Task Send(long targetChatId, TelegramBotClient bot, int replyTo)
        {
            if (AlbumMessage != null)
            {
                await bot.SendMediaGroupAsync(AlbumMessage.Items.Select(x => new InputMediaPhoto()
                {
                    Caption = x.Text,
                    ParseMode = ParseMode.Html,
                    Media = new InputMedia(x.PhotoId)
                }), targetChatId, replyToMessageId: replyTo);
            }
            else if (Voice != null)
            {
                await bot.SendVoiceAsync(targetChatId, new InputOnlineFile(Voice.VoiceId),
                    replyToMessageId: replyTo, replyMarkup: GetMarkup(Voice.Buttons));
                
            }
            else if (Audio != null)
            {
                await bot.SendAudioAsync(targetChatId, new InputOnlineFile(Audio.AudioId),
                    replyToMessageId: replyTo, replyMarkup: GetMarkup(Audio.Buttons));
            }
            else if (Voice != null)
            {
                await bot.SendAudioAsync(targetChatId, new InputOnlineFile(Voice.VoiceId),
                    replyToMessageId: replyTo, replyMarkup: GetMarkup(Voice.Buttons));
            }
            else if (VideoNote != null)
            {
                await bot.SendVideoNoteAsync(targetChatId, new InputOnlineFile(VideoNote.VideoId),
                    replyToMessageId: replyTo, replyMarkup: GetMarkup(VideoNote.Buttons));
            }
            else if (Photo != null)
            {
                await bot.SendPhotoAsync(targetChatId, new InputOnlineFile(Photo.PhotoId), caption: Photo.Text,
                    replyToMessageId: replyTo, parseMode: ParseMode.Html, replyMarkup: GetMarkup(Photo.Buttons));
            }
            else if (TextMessage != null)
            {
                await bot.SendTextMessageAsync(targetChatId, TextMessage.Text,
                    replyToMessageId: replyTo, parseMode: ParseMode.Html, replyMarkup: GetMarkup(TextMessage.Buttons),
                    disableWebPagePreview:!TextMessage.LinkPreview);
            }
        }
    }

}