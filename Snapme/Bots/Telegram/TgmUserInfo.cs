namespace Snapme.Bots.Telegram
{
    public class TgmUserInfo
    {
        public string TgmUserName { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Language { get; set; }
        public long TgmUserId { get; set; }
    }
}