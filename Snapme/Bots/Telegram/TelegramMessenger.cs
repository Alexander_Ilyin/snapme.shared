using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Snapme.Bots.Hosting;
using Snapme.Bots.Messages;
using Snapme.Bots.Telegram.Parsed;
using Snapme.Bots.Texts;
using Snapme.Common;
using Snapme.Data.Bots;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InputFiles;
using Telegram.Bot.Types.ReplyMarkups;

namespace Snapme.Bots.Telegram
{
    public class TelegramMessenger : ITelegramMessenger
    {
        private TelegramBotClient bot;
        private ConcurrentDictionary<long, AsyncLock> sendLocks = new ConcurrentDictionary<long, AsyncLock>();
        public event Action<TelegramMessageEvent> MessageReceived = x => { };
        public event Action<TelegramBotEvent> Update = x => { };

        public TelegramBotClient Bot => bot;

        public void Start(string botKey)
        {
            try
            {
                if (botKey == null)
                    throw new ArgumentNullException(nameof(botKey));
                bot = new TelegramBotClient(botKey);
                bot.OnMessage += (z, x) => OnMessage(z, x);
                bot.OnReceiveError += OnReceiveError;
                bot.OnReceiveGeneralError += OnGenError;
                bot.OnUpdate += (x, e) => OnUpdate(x, e);
                bot.StartReceiving();
            }
            catch (Exception ex)
            {
                this.Update(new TelegramBotEvent(ex));
                this.Log().Error("Error in bot start", ex);
                throw;
            }
        }

        public void Stop()
        {
            bot?.StopReceiving();
        }

        public IEnumerable<long> FindForwardFromUserIds(MessangerMsgInfo msgInfo)
        {
            var telegramMsgInfo = JsonConvert.DeserializeObject<TelegramMsgInfo>(msgInfo.Data);
            while (telegramMsgInfo != null)
            {
                var forwardFromUserId = telegramMsgInfo.ForwardFromUserId;
                if (forwardFromUserId != null)
                    yield return forwardFromUserId.Value;
                if (telegramMsgInfo.MessageText != null)
                {
                    var extracted = ExtractUserIdFromLink(telegramMsgInfo.MessageText);
                    var l = extracted.TryParseLong();
                    if (l != null)
                        yield return l.Value;
                }

                telegramMsgInfo = telegramMsgInfo.ReplyTo;
            }
        }

        public static string ExtractUserIdFromLink(string s)
        {
            var match = Regex.Match(s, @"uid\=(.*?)\;", RegexOptions.IgnoreCase);
            if (match.Success)
                return match.Groups[1].Value;
            return null;
        }


        public Task SendMessage(long targetChatId, OutgoingMessage msg, string language)
        {
            var sendLock = sendLocks.GetOrAdd(targetChatId, new AsyncLock());
            sendLock.Run(async () =>
            {
                try
                {
                    await SendMessagePrivate(targetChatId, msg, language);
                }
                catch (Exception e)
                {
                    var telegramBotEvent = new TelegramBotEvent(e);
                    telegramBotEvent.FailedChatId = targetChatId;
                    this.Update(telegramBotEvent);
                    this.Log().Error("Err sending to " + targetChatId, e);
                }

                return 1;
            });
            return Task.CompletedTask;
        }

        private async Task SendMessagePrivate(long targetChatId, OutgoingMessage msg, string language)
        {
            this.Log().Info("Send message to " + targetChatId + " Msg:" + msg);

            if (msg.PhotoId != null)
            {
                await bot.SendPhotoAsync(targetChatId,
                    new InputOnlineFile(msg.PhotoId), GetText(msg, Languages.Russian),
                    ParseMode.Html);
                return;
            }

            if (msg.ForwardedMsg != null)
            {
                var forwardedInfo = JsonConvert.DeserializeObject<TelegramMsgInfo>(msg.ForwardedMsg.Data);
                await bot.ForwardMessageAsync(targetChatId, forwardedInfo.ChatId, forwardedInfo.MessageId);
                return;
            }

            if (msg.UpdateKeyboardOf != null)
            {
                var forwardedInfo = JsonConvert.DeserializeObject<TelegramMsgInfo>(msg.UpdateKeyboardOf.Data);
                var segments = MessageCombiner.Parse(msg.Text.Trim());
                var buttons = segments.Where(x => x.Buttons.Any());
                var replyKb = CombinedMessage.GetMarkup(buttons);
                await bot.EditMessageReplyMarkupAsync(targetChatId, forwardedInfo.MessageId,
                    replyKb as InlineKeyboardMarkup);
                return;
            }

            {
                var replyTo = msg.ReplyTo == null
                    ? null
                    : JsonConvert.DeserializeObject<TelegramMsgInfo>(msg.ReplyTo.Data);

                var text = GetText(msg, language);

                if (!String.IsNullOrEmpty(text))
                    foreach (var message in MessageCombiner.GetCombinedMessages(text))
                        await message.Send(targetChatId, bot, replyTo?.ChatId == targetChatId ? replyTo?.MessageId ?? 0 : 0);
                else
                {
                    this.Log().Error("Tried to send invalid message, no text"+ msg);
                }
            }
        }

        private void OnMessage(object sender, MessageEventArgs messageEventArgs)
        {
            if (messageEventArgs.Message.From.IsBot)
                return;
            MessageReceived(GetEventFromMessage(messageEventArgs));
        }

        private async Task OnUpdate(object sender, UpdateEventArgs e)
        {
            try
            {
                if (e.Update?.CallbackQuery != null && e.Update?.CallbackQuery.Data != null)
                    MessageReceived(GetEventFromQuery(e));

                if (e.Update?.Type == UpdateType.Message && e.Update?.Message?.Type == MessageType.GroupCreated ||
                    e.Update?.Message?.Type == MessageType.ChatMembersAdded)
                    if (e.Update.Message?.Chat?.Type == ChatType.Group ||
                        e.Update.Message?.Chat?.Type.ToString()?.Contains("Group") == true)
                    {
                        var @event = new TelegramBotEvent
                        {
                            EventDate = DateHelper.UtcNow(),
                            Description = "Added to group:" + e.Update.Message.Chat.Id + " " +
                                          e.Update.Message?.Chat?.Title,
                            JoinedGroupId = e.Update.Message.Chat.Id,
                            JoinedGroupTitle = e.Update.Message?.Chat?.Title
                        };
                        Update(@event);
                    }
            }
            catch (Exception ex)
            {
                this.Update(new TelegramBotEvent(ex));
                this.Log().Error("Error in update", ex);
            }
        }

        private TelegramMessageEvent GetEventFromQuery(UpdateEventArgs m)
        {
            var res = new TelegramMessageEvent();

            if (m.Update?.CallbackQuery.Message?.Chat?.Type == ChatType.Private)
            {
                var msg = m.Update.CallbackQuery.Message;
                var user = m.Update.CallbackQuery.From;
                res.FromUser = new TgmUserInfo();
                res.FromUser.TgmUserName = user.Username;
                res.FromUser.LastName = user.LastName;
                res.FromUser.FirstName = user.FirstName;
                res.FromUser.Language = Languages.FromTgmCode(user.LanguageCode);
                res.FromUser.TgmUserId = user.Id;
            }

            res.FromUserId = m.Update.CallbackQuery.From.Id;
            res.Message = new MessengerMessage
            {
                IsCallbackQuery = true,
                Text = m.Update.CallbackQuery.Data,
                MsgInfo = new MessangerMsgInfo(
                    JsonConvert.SerializeObject(new TelegramMsgInfo(m.Update.CallbackQuery.Message)))
            };
            res.FromGroupChat = m.Update?.CallbackQuery?.Message?.Chat?.Type == ChatType.Group ||
                                m.Update?.CallbackQuery?.Message?.Chat?.Type == ChatType.Supergroup;
            res.FromChatId = m.Update.CallbackQuery.Message.Chat.Id;
            return res;
        }


        private TelegramMessageEvent GetEventFromMessage(MessageEventArgs m)
        {
            var res = new TelegramMessageEvent();
            if (m.Message.Chat.Type == ChatType.Private)
            {
                res.FromUser = new TgmUserInfo();
                res.FromUser.TgmUserName = m.Message.From.Username;
                res.FromUser.LastName = m.Message.From.LastName;
                res.FromUser.FirstName = m.Message.From.FirstName;
                res.FromUser.TgmUserId = m.Message.From.Id;
                res.FromUser.Language = Languages.FromTgmCode(m.Message.From.LanguageCode);
            }

            var msg = m.Message;
            res.Message = new MessengerMessage
            {
                Text = msg.Text ?? msg.Caption,
                PhotoId = (msg.Photo ?? new PhotoSize[0]).Select(x => x.FileId).FirstOrDefault(),
                ReplyTo = msg.ReplyToMessage == null
                    ? null
                    : new MessangerMsgInfo(JsonConvert.SerializeObject(new TelegramMsgInfo(msg.ReplyToMessage))),
                MsgInfo = new MessangerMsgInfo(JsonConvert.SerializeObject(new TelegramMsgInfo(msg))),
                VideoNoteId = msg.VideoNote?.FileId,
                AudioId  = msg.Audio?.FileId,
                VoiceId  = msg.Voice?.FileId
            };
            res.FromGroupChat =
                m.Message.Chat.Type == ChatType.Group ||
                m.Message.Chat.Type == ChatType.Supergroup;
            res.FromChatId = m.Message.Chat.Id;
            res.FromUserId = m.Message.From.Id;
            return res;
        }


        private void OnGenError(object sender, ReceiveGeneralErrorEventArgs e)
        {
            this.Update(new TelegramBotEvent(e.Exception));
            this.Log().Error("Error in Tgm bot:" + bot.BotId, e.Exception);
        }

        private void OnReceiveError(object sender, ReceiveErrorEventArgs e)
        {
            this.Update(new TelegramBotEvent(e.ApiRequestException));
            this.Log().Error("Error in Tgm bot:" + bot.BotId, e.ApiRequestException);
        }


        private string GetText(OutgoingMessage msg, string language)
        {
            if (!msg.Text.IsEmptyOrNull())
                return msg.Text;
            if (msg.TextFormat != null)
                return string.Format(msg.TextFormat.GetFormat(language),
                    msg.TextFormatParams ?? new object[0]);
            return null;
        }


        private string Format(MsgFormat format, string lang, params object[] vals)
        {
            return string.Format(format.GetFormat(lang), vals);
        }
    }
}