using Snapme.Bots.Messages;

namespace Snapme.Bots.Telegram
{
    public class TelegramMessageEvent
    {
        public long? FromUserId { get; set; }
        public long FromChatId { get; set; }
        public bool FromGroupChat { get; set; }
        public TgmUserInfo FromUser { get; set; }
        public MessengerMessage Message { get; set; }

        public override string ToString()
        {
            return $"{Message}";
        }
    }
}