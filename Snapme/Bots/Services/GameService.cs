using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Snapme.Bots.Games;
using Snapme.Bots.Messages;
using Snapme.Bots.Texts;
using Snapme.Common;
using Snapme.Common.DI;
using Snapme.Data;
using Snapme.Data.Bots;
using Snapme.Data.Games;
using Snapme.Data.Info;

namespace Snapme.Bots.Services
{
    public class GameService : IRequestBound
    {
        private readonly SnapmeContext context;

        public GameService(SnapmeContext context)
        {
            this.context = context;
        }

        public Game CreateGame(Guid botId)
        {
            var entry = context.AddNew(new Game { TgmBotId = botId, Created = DateHelper.UtcNow() });
            return entry;
        }

        public async Task<List<GameTask>> AssignMissionToFreePlayers(Guid gameId,
            List<Player> freePlayers,
            Dictionary<User, List<DetachedGameTask>> completeTasks, bool skipWaiting = false)
        {
            LogFreePlayers(freePlayers);
            var game = GetGame(gameId);
            this.Log().InfoFormat("----AssignMissionToFreePlayers, phase:" + game.Phase);

            var activities = context.GameTasks
                .Where(t => t.GameId == gameId &&
                            !t.IsDelete &&
                            (t.Phase == null || t.Phase == game.Phase)).ToArray();


            var assignJob = new PlayerTaskAssignJob(context, activities.ToArray(), completeTasks, freePlayers,
                game.GetSettings().LongWaitPeriodInSeconds ?? 200);
            var assignMissions = assignJob.AssignMissions(skipWaiting);

            await context.SaveChangesAsync();
            var result = assignMissions.Values.Distinct().ToList();
            this.Log().InfoFormat("---------");
            return result;
        }

        private void LogFreePlayers(List<Player> freePlayers)
        {
            this.Log().InfoFormat("Free players:" + freePlayers.JoinStr(p => p.User.TgmUserName));
        }

        private void LogMissions(Dictionary<Player, GameTask> assignMissions)
        {
            foreach (var m in assignMissions.Values.Distinct())
                this.Log().InfoFormat("Mission assigned: {0} {1}", GetDescription(m), m.GetPlayerNames());
        }

        public Dictionary<User, List<DetachedGameTask>> GetCompleteTasksOfFreePlayers(Guid gameId,
            List<Player> freePlayers)
        {
            var userIds = freePlayers.Select(x => x.UserId).ToList();

            var assignees = context.GameTaskAssignees.Where(x =>
                x.GameTask.GameId == gameId && userIds.Contains(x.UserId)).Select(a => new
            {
                a.UserId,
                a.GameTaskId,
                a.GameTask.Created,
                a.GameTask.Finished,
                a.GameTask.TaskType
            }).ToArray();


            var gameTaskIds = assignees.Select(x => x.GameTaskId).Distinct().ToArray();
            var gameTaskActivities = context.GameTaskActivities.Where(x => gameTaskIds.Contains(x.GameTaskId)).Select(
                x => new
                {
                    ActivityId = x.Activity.Id,
                    x.GameTaskId
                }).ToList().ToDictionaryList(x => x.GameTaskId);
            var assigneeByTaskId = assignees.ToDictionaryList(a => a.GameTaskId);

            var playerTasks = new List<DetachedGameTask>();
            foreach (var assigneeByTask in assigneeByTaskId)
            {
                var gameTaskId = assigneeByTask.Key;
                var activities = gameTaskActivities.GetItemOrDefault(gameTaskId);
                if (activities == null)
                {
                    this.Log().Error(new Exception("Invalid tasks in game " + gameId));
                    continue;
                }

                var assigneeList = assigneeByTask.Value;
                var first = assigneeList.First();
                var finished = first.Finished;
                if (finished == null)
                {
                }
                var task = new DetachedGameTask(first.TaskType, first.Created, finished,
                    activities.Select(x => x.ActivityId).ToArray(),
                    assigneeList.Select(x => x.UserId).ToArray());
                playerTasks.Add(task);
            }

            var result = new Dictionary<User, List<DetachedGameTask>>();

            foreach (var task in playerTasks.OrderBy(t => t.Created))
            foreach (var userId in task.AssignedUserIds)
            {
                var user = context.Users.Find(userId);
                if (!result.ContainsKey(user))
                    result[user] = new List<DetachedGameTask>();
                result[user].Add(task);
            }

            return result;
        }

        public void AcceptTask(GameTask task)
        {
            task.GameTaskStatus = GameTaskStatus.Accepted;
            task.Finished = DateHelper.UtcNow();
            task.Assigned.ForEach(p =>
            {
                var player = GetPlayer(task.GameId, p.UserId);
                player.CurrentTaskId = null;
                player.OnStatusUpdate();
            });
            context.SaveChanges();
        }

        public Player Join(Guid gameId, User user)
        {
            var player = context.Players.Find(gameId, user.Id);
            if (player != null)
                return player;
            var entry = context.AddNew(new Player
            {
                GameId = gameId,
                UserId = user.Id,
            });
            context.SaveChanges();
            return entry;
        }

        public Game GetGame(Guid gameId)
        {
            return context.Games.Find(gameId);
        }

        public Task SaveChangesAsync()
        {
            return context.SaveChangesAsync();
        }

        public void SaveChanges()
        {
            context.SaveChanges();
        }

        public GameTask GetNextForModeration(Guid gameId)
        {
            return context.PlayerTasks
                .Where(t => t.GameId == gameId && t.GameTaskStatus == GameTaskStatus.ReadyForValidation)
                .OrderBy(t => t.ModeratorPriority)
                .ThenBy(x => x.Id)
                .FirstOrDefault();
        }

        public GameTask GetCurrentlyModerated(string validatorsChatId, Guid gameId)
        {
            return context.PlayerTasks
                .Where(t => t.WaitingForValidatorId == validatorsChatId && t.GameId == gameId &&
                            t.GameTaskStatus == GameTaskStatus.Validating)
                .OrderBy(t => t.ModeratorPriority)
                .ThenBy(t => t.Id)
                .FirstOrDefault();
        }

        public Player GetPlayer(Guid gameId, Guid userId)
        {
            return context.Players.Find(gameId, userId);
        }

        public void RejectTask(GameTask gameTask, string reason)
        {
            gameTask.GameTaskStatus = GameTaskStatus.NotReady;
            if (!string.IsNullOrEmpty(reason))
                gameTask.ModeratorRejectReason = reason;
            context.SaveChanges();
        }

        public void SetReadyForValidation(Guid? taskId, string photoId)
        {
            var task = context.PlayerTasks.Find(taskId);
            task.ModeratorPriority = (DateHelper.UtcNow() - new DateTime(2019, 1, 1)).TotalMilliseconds;
            task.GameTaskStatus = GameTaskStatus.ReadyForValidation;
            task.TaskPhotoId = photoId;
            context.SaveChanges();
        }

        public void SetValidating(GameTask gameTask, string validatorId)
        {
            gameTask.GameTaskStatus = GameTaskStatus.Validating;
            gameTask.WaitingForValidatorId = validatorId;
            context.SaveChanges();
        }

        public List<Player> GetPlayers(GameTask gameTask)
        {
            return gameTask.Assigned.Select(a => GetPlayer(gameTask.GameId, a.UserId)).ToList();
        }

        public IEnumerable<Player> GetAllPlayers(Guid gameId)
        {
            return context.Players.Where(p => p.GameId == gameId).ToArray();
        }

        public Player GetPartner(Player player, GameTask gameTask)
        {
            var link = gameTask.Assigned.FirstOrDefault(x => x.UserId != player.UserId);
            if (link == null)
                return null;
            return GetPlayer(player.GameId, link.UserId);
        }

        public List<Player> GetGamePlayers(Guid gameId)
        {
            return context.Players.Where(p => p.GameId == gameId).ToList();
        }

        public string[] GetAcceptedMissionPhotos(Guid gameId)
        {
            return context.PlayerTasks
                .Where(t => t.GameId == gameId && t.TaskType != GameTaskType.Selfie && t.GameTaskStatus == GameTaskStatus.Accepted)
                .Select(t => t.TaskPhotoId)
                .ToArray();
        }

        public string[] GetAcceptedSelfiePhotos(Guid gameId)
        {
            return context.PlayerTasks
                .Where(t => t.GameId == gameId && t.TaskType == GameTaskType.Selfie && t.GameTaskStatus == GameTaskStatus.Accepted)
                .Select(t => t.TaskPhotoId)
                .ToArray();
        }

        public FinalGameResults GetGameResults(Guid gameId)
        {
            var results = new FinalGameResults();
            var byTasks = context.PlayerTasks
                .Where(t => t.GameTaskStatus == GameTaskStatus.Accepted && t.GameId == gameId)
                .SelectMany(t => t.Assigned)
                .GroupBy(t => t.User)
                .Select(g => new FinalGameResultsItem
                {
                    User = g.Key,
                    Score = g.Count(),
                }).ToArray();
            var resDictionary = byTasks.ToDictionary(x => x.User.Id);

            var extraScored = context.Players.Where(x => x.ExtraScore > 0 && x.GameId == gameId).ToArray();
            if (extraScored.Any())
                foreach (var item in resDictionary.Values)
                    item.Score *= 5;

            foreach (var player in extraScored)
            {
                var resultsItem = resDictionary.GetOrAdd(player.UserId,
                    () => new FinalGameResultsItem() { User = player.User, Score = 0 });
                resultsItem.Score += player.ExtraScore;
            }

            results.Items = resDictionary.Values.OrderByDescending(x => x.Score).ToList();
            return results;
        }


        public Activity[] GetGameTasks(Guid gameId)
        {
            return context.GameTasks.Where(t => t.GameId == gameId && !t.IsDelete).ToArray();
        }

        public string GetDescription(GameTask gameTask)
        {
            var activities = gameTask.GetActivities();
            var s = activities.Select(a => a.GetDescription(Languages.Russian)).JoinStr(x => x, ".");
            switch (gameTask.TaskType)
            {
                case GameTaskType.Selfie:
                case GameTaskType.Meeting:
                    return GrammarFixer.FixGrammar(s);
                default:
                    return GrammarFixer.FixGrammar(
                        string.Format(MsgFormats.PhotoWithRequirementsPRequirements.GetFormat(Languages.Russian), s));
            }
        }

        public Player FindPlayer(string tgmUserName)
        {
            return context.Players.FirstOrDefault(p => p.User.TgmUserName == tgmUserName);
        }

        public async Task CancelTask(Player player)
        {
            if (player.CurrentTask == null)
                return;
            player.CurrentTask.GameTaskStatus = GameTaskStatus.Canceled;
            var partner = GetPartner(player, player.CurrentTask);
            player.CurrentTaskId = null;
            if (partner != null)
                partner.CurrentTaskId = null;

            await context.SaveChangesAsync();
        }

        public async Task Kick(Player player)
        {
            await CancelTask(player);
            player.Status = PlayerStatus.Kicked;
            player.OnStatusUpdate();
            await context.SaveChangesAsync();
        }

        public async Task SoftKick(Player player)
        {
            player.Status = PlayerStatus.SoftKicked;
            player.OnStatusUpdate();
            await context.SaveChangesAsync();
        }

        public TgmBot CreateBot()
        {
            var bot = context.AddNew(new TgmBot());
            var content = context.AddNew(new BotContent());
            content.TgmBotId = bot.Id;
            return bot;
        }

        public TgmBot GetBot(Guid id)
        {
            return context.TgmBots.Find(id);
        }

        public void ActivatePlayer(Player player)
        {
            if (player.Status != PlayerStatus.NotJoined)
                return;
            player.Status = PlayerStatus.Active;
            SaveChanges();
            ;
        }

        public Task<List<Player>> GetAllPlayersForAnnonce(Guid gameId)
        {
            return context.Players.Where(x => (x.SelfiePhotoId != null || x.ExtraScore > 0)
                                              && x.GameId == gameId).ToListAsync();
        }

        public List<Player> GetFreePlayers(Game game)
        {
            var userNameRequired = game.GetSettings().RequireUserName == true;
            if (userNameRequired)
                return context.Players
                    .Where(p =>
                        p.Status == PlayerStatus.Active &&
                        p.GameId == game.Id && p.CurrentTaskId == null
                        && (p.User.TgmUserName != null))
                    .ToList();
            else
                return context.Players
                    .Where(p =>
                        p.Status == PlayerStatus.Active &&
                        p.GameId == game.Id && p.CurrentTaskId == null)
                    .ToList();
        }

        public bool HasFinalActvities(Guid gameId)
        {
            return context.GameTasks.Any(x =>
                x.GameId == gameId &&
                (x.ActivityType == ActivityType.FinalTaskPlace ||
                 x.ActivityType == ActivityType.FinalSingleTask)
                && !x.IsDelete);
        }

        public IEnumerable<Player> GetActivePlayersWithoutTasks(Guid gameId)
        {
            return context.Players.Where(x => x.GameId == gameId &&
                                              x.CurrentTaskId == null && x.SelfiePhotoId != null);
        }

        public IEnumerable<Player> GetPlayersWithSelfieTask(Guid gameId)
        {
            return context.Players.Where(x =>
                x.GameId == gameId && x.CurrentTask.Activities.Any(a => a.GameTask.TaskType == GameTaskType.Selfie));
        }

        public class StatusItem
        {
            public string Players;
            public string TaskType;
            public TimeSpan? WaitTime;
            public string TaskDescription;

            public StatusItem(string players, string taskType, TimeSpan? waitTime, string taskDescription)
            {
                Players = players;
                TaskType = taskType;
                WaitTime = waitTime;
                TaskDescription = taskDescription;
            }
        }

        public string[] CollectStatus(Guid gameId, string query)
        {
            var players = context.Players.Where(x => x.GameId == gameId).ToList();
            var toSkip = new HashSet<Guid>();
            var items = new List<StatusItem>();
            foreach (var player in players)
            {
                if (toSkip.Contains(player.UserId))
                    continue;
                if (player.Status == PlayerStatus.Kicked || player.Status == PlayerStatus.SoftKicked ||
                    player.Status == PlayerStatus.NotJoined)
                    continue;

                var botUser = context.TgmBotUsers.Find(player.Game.TgmBotId, player.UserId);
                if (!botUser.IsMatch(query))
                    continue;
                if (player.CurrentTaskId == null)
                {
                    items.Add(new StatusItem(FormatPlayerLink(player), "WaitForTask", player.GetIdleTime(), null));
                    continue;
                }

                if (player.CurrentTask != null)
                {
                    var partner = GetPartner(player, player.CurrentTask);
                    if (partner != null)
                        toSkip.Add(partner.UserId);
                    items.Add(new StatusItem(FormatPlayerLink(player) + ", " + FormatPlayerLink(partner),
                        player.CurrentTask.TaskType.ToString(),
                        player.GetIdleTime(), GetDescription(player.CurrentTask)));
                }
            }

            var output = new List<string>();
            foreach (var group in items.GroupBy(x => x.TaskType).OrderByDescending(x => GetOrder(x.Key)))
            {
                if (output.Count > 0)
                    output.Add("\n");
                output.Add($"<b>{group.Key}</b> ({group.Count()}):\n");
                foreach (var statusItem in group.OrderByDescending(x => x.WaitTime))
                    output.Add(string.Format("{0} ({1}) {2}\n", statusItem.Players,
                        statusItem.WaitTime.ToSimpleString(),
                        statusItem.TaskDescription));
            }

            return output.Batches(20).Select(x => x.JoinStr(z => z, "")).ToArray();
        }

        private string FormatPlayerLink(Player player)
        {
            if (player == null)
                return "";
            var tgmBotUser = context.TgmBotUsers.Find(player.Game.TgmBotId, player.UserId);
            return (player.User.FormatLink() + " " + tgmBotUser?.UserNumber).Trim();
        }

        private int GetOrder(string tskType)
        {
            if (tskType == "NotJoined")
                return -1000;
            if (tskType == "WaitForTask")
                return 5000;
            if (Enum.TryParse(tskType, out GameTaskType parsed))
                return (int)parsed;
            return 400;
        }

        public async Task EnsureAllBotUsersArePlayers(Guid gameId)
        {
            var game = GetGame(gameId);
            if (game.TgmBotId == null)
            {
                this.Log().Error("Invalid game, no botid in EnsureAllBotUsersArePlayers " + gameId);
                return;
            }

            var tgmBotId = game.TgmBotId.Value;
            var users = context.TgmBotUsers.Where(bu =>
                bu.BotId == tgmBotId &&
                context.Players.Where(p => p.GameId == gameId).All(p => p.UserId != bu.UserId)
            ).Select(x => x.User).ToList();
            foreach (var u in users)
                Join(gameId, u);
        }

        public bool GameIsNotRunning(Guid? gameId)
        {
            if (gameId == null)
                return true;
            var game = GetGame(gameId.Value);
            return game.GameStatus == GameStatus.New || game.GameStatus == GameStatus.Finished;
        }
        public bool GameIsRunning(Guid? gameId)
        {
            return !GameIsNotRunning(gameId);
        }

        public Player FindPlayerByNumber(Guid gameId, int value)
        {
            return context.Players
                .FirstOrDefault(x => context.TgmBotUsers.Any(bu =>
                    bu.UserId == x.UserId &&
                    x.GameId == gameId &&
                    bu.UserNumber == value &&
                    bu.BotId == x.Game.TgmBotId));
        }

        public string DescribePlayersWithSelfie(GameTask mission)
        {
            var sb = new StringBuilder();
            foreach (var assignee in mission.Assigned)
            {
                var player = GetPlayer(mission.GameId, assignee.UserId);
                if (!player.SelfiePhotoId.IsEmptyOrNull())
                    sb.Append($"[img:{player.SelfiePhotoId}]");
                sb.Append(assignee.User.FormatLink());
            }

            return sb.ToString();
        }

        public Game GetCurrentGameForBot(Guid botId)
        {
            var games = context.Games.Where(g => g.MustBeProcessedByBot && g.TgmBotId == botId);
            if (games.Count() > 1)
                this.Log().Error(games.Count() + " games found for bot " + botId);
            return games.OrderBy(x => x.Created).FirstOrDefault();
        }
    }
}