using System;
using System.Linq;
using Snapme.Common;
using Snapme.Common.DI;
using Snapme.Data;
using Snapme.Data.Bots;
using Snapme.Data.Networking;

namespace Snapme.Bots.Services
{
    public class NetworkingService : IRequestBound
    {
        private readonly SnapmeContext context;

        public NetworkingService(SnapmeContext context)
        {
            this.context = context;
        }

        public NetworkingSession GetOrCreateNetworking(Guid botId)
        {
            var session = context.NetworkingSessions.Local.FirstOrDefault(x => x.IsActive && x.TgmBotId == botId);
            if (session != null)
                return session;
            session = context.NetworkingSessions.FirstOrDefault(x => x.TgmBotId == botId && x.IsActive);
            if (session != null)
                return session;

            session = context.AddNew(new NetworkingSession()
            {
                TgmBotId = botId,
                IsActive = true
            });
            context.SaveChanges();
            return session;
        }
        
        public NetworkingProfile GetOrCreateProfile(TgmBotUser user)
        {
            var networkingSession = this.GetOrCreateNetworking(user.BotId);
            var profile = context.NetworkingProfiles.Find(networkingSession.Id, user.UserId);
            if (profile != null)
                return profile;

            profile = context.AddNew(new NetworkingProfile()
            {
                NetworkingSessionId = networkingSession.Id,
                UserId = user.UserId
            });
            context.SaveChanges();
            return profile;
        }
        
        public void SetUserName(TgmBotUser user, string name)
        {
            var profile = GetOrCreateProfile(user);
            profile.Name = name;
            context.SaveChanges();
        }
        
        public void SetUserCompany(TgmBotUser user, string company)
        {
            var profile = GetOrCreateProfile(user);
            profile.Position= company;
            context.SaveChanges();
        }

        public void SetUserInterest(TgmBotUser user, string v)
        {
            var profile = GetOrCreateProfile(user);
            profile.Interest = v;
            context.SaveChanges();
        }
        

        public NetworkingProfile GetNextProfile(Guid sessionId,Guid userId)
        {
            var networkingSession = context.NetworkingSessions.Find(sessionId);
            var users = context.NetworkingProfiles
                .Where(x => x.IsActive &&
                     x.NetworkingSessionId == sessionId)
                .OrderByDescending(x=>x.Created)
                .Select(x=>context.TgmBotUsers.
                    SingleOrDefault(b=>b.UserId==x.UserId && b.BotId==networkingSession.TgmBotId)).ToList();

            var profile = GetOrCreateProfile(context.TgmBotUsers.Find(networkingSession.TgmBotId, userId));
            var liked = profile.GetLiked().ToDictionarySafe(x=>x);
            var disliked = profile.GetDisliked().ToDictionarySafe(x=>x);
            
            TgmBotUser result = users.FirstOrDefault(x =>
            {
                if (x.UserId == userId)
                    return false;
                if (liked.ContainsKey(x.UserNumber))
                    return false;
                if (disliked.ContainsKey(x.UserNumber))
                    return false;
                return true;
            });
            if (result!=null)
                return GetOrCreateProfile(result);
            return null;
        }

        public void BeginNetworking(TgmBotUser botUser)
        {
            botUser.CurrentDialogType = "networking";
            context.SaveChanges();
        }
    }
}