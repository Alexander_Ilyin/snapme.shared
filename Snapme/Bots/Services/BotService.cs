using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Snapme.Common.DI;
using Snapme.Data;

namespace Snapme.Bots.Services
{
    public class BotService : IRequestBound
    {
        private readonly SnapmeContext snapmeContext;

        public BotService(SnapmeContext snapmeContext)
        {
            this.snapmeContext = snapmeContext;
        }

        public async Task StopBot(Guid botId)
        {
            var tgmBot = snapmeContext.TgmBots.Find(botId);
            var tgmBotSettings = tgmBot.GetSettings();
            tgmBotSettings.Stopped = true;
            tgmBot.SetSettings(tgmBotSettings);
            await snapmeContext.SaveChangesAsync();
        }

        public Task<string> GetBotKey(Guid botId)
        {
            var tgmBot = snapmeContext.TgmBots.Find(botId);
            return Task.FromResult(tgmBot.BotTgmKey);
        }

        public async Task<Guid[]> GetBotIdsToStart()
        {
            var tgmBots = await snapmeContext.TgmBots.Where(x => !x.IsDelete).ToListAsync();
            tgmBots = tgmBots.Where(b => !b.GetSettings().Stopped).ToList();
            return tgmBots.Select(b => b.Id).ToArray();
        }

        public async Task StartBot(Guid botId)
        {
            var tgmBot = snapmeContext.TgmBots.Find(botId);
            var tgmBotSettings = tgmBot.GetSettings();
            tgmBotSettings.Stopped = false;
            tgmBot.SetSettings(tgmBotSettings);
            await snapmeContext.SaveChangesAsync();
        }
    }
}