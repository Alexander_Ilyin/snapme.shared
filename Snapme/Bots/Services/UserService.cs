using System;
using System.Linq;
using Snapme.Common;
using Snapme.Common.DI;
using Snapme.Data;
using Snapme.Data.Bots;

namespace Snapme.Bots.Services
{
    public class UserService : IRequestBound
    {
        private readonly SnapmeContext context;

        public UserService(SnapmeContext context)
        {
            this.context = context;
        }

        public User GetOrCreateUser(long tgmUserId)
        {
            var user = context.Users.SingleOrDefault(u => u.TgmUserId == tgmUserId);
            if (user == null)
            {
                var entry = context.AddNew(new User
                {
                    TgmUserId = tgmUserId
                });
                context.SaveChanges();
                return entry;
            }

            context.SaveChanges();
            return user;
        }

        public TgmBotUser GetOrCreateFakeUser(Guid botId, string name)
        {
            var userId = Guid.NewGuid();
            var user = context.AddNew(new User
            {
                Id = userId,
                TgmUserId = -1
            });

            user.IsFake = true;
            user.FirstName = name;
            user.TgmUserName = "FakeTgm-" + user.Id.GetHashCode();
            var botUser = context.TgmBotUsers.FirstOrDefault(u => u.BotId == botId && u.UserId == user.Id);
            if (botUser == null)
            {
                var tgmBot = context.TgmBots.Find(botId);
                context.Entry(tgmBot).Reload();
                tgmBot.UserSequenceNum++;

                botUser = context.AddNew(new TgmBotUser
                {
                    BotId = botId,
                    UserId = user.Id,
                    TgmChatId = -1,
                    UserNumber = tgmBot.UserSequenceNum
                });
            }

            context.SaveChanges();
            return botUser;
        }

        public TgmBotUser GetOrCreateBotUser(Guid botId, long chatId, long fromId)
        {
            var user = GetOrCreateUser(fromId);
            var botUser = context.TgmBotUsers.FirstOrDefault(u => u.BotId == botId && u.UserId == user.Id);
            if (botUser == null)
            {
                var tgmBot = context.TgmBots.Find(botId);
                context.Entry(tgmBot).Reload();
                tgmBot.UserSequenceNum++;
                botUser = context.AddNew(new TgmBotUser
                {
                    BotId = botId,
                    UserId = user.Id,
                    TgmChatId = chatId,
                    UserNumber = tgmBot.UserSequenceNum
                });
                this.Log().InfoFormat("Create user, tgmId: " + fromId + " chatId:" + chatId);
            }

            context.SaveChanges();
            return botUser;
        }

        public TgmBotUser FindBotUser(long[] tgmUserIds, Guid botId)
        {
            return context.TgmBotUsers.FirstOrDefault(x => tgmUserIds.Contains(x.User.TgmUserId) && x.BotId == botId);
        }

        public TgmBotUser FindBotUserByNumber(Guid botId,int i)
        {
            return context.TgmBotUsers.FirstOrDefault(x => x.BotId==botId && x.UserNumber == i);
        }
    }

}