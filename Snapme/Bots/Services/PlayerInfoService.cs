using System;
using System.Collections.Generic;
using System.Linq;
using Snapme.Common;
using Snapme.Common.DI;
using Snapme.Data;
using Snapme.Data.Dto;

namespace Snapme.Bots.Services
{
    public class PlayerInfoService : IRequestBound
    {
        private readonly SnapmeContext context;
        private readonly GameService gameService;

        public PlayerInfoService(SnapmeContext context, GameService gameService)
        {
            this.context = context;
            this.gameService = gameService;
        }

        public List<PlayerInfo> GetPlayersInfo(Guid gameId)
        {
            var players = context.Players.Where(p => p.GameId == gameId).ToArray();
            var res = players.Select(p => new PlayerInfo
            {
                UserId = p.UserId,
                TgmName = p.User.FullName(),
                CurrentTask = p.CurrentTask==null ? "--No task--" : gameService.GetDescription(p.CurrentTask),
                IdleTimeSeconds = (p.GetIdleTime()?.TotalSeconds).ToInt()
            }).ToList();
            return res;
        }
    }
}