using System;
using System.Collections.Generic;
using System.Linq;
using Castle.Core.Internal;
using Snapme.Common;
using Snapme.Common.DI;
using Snapme.Data;
using Snapme.Data.Games;
using Snapme.Import;

namespace Snapme.Bots.Games
{
    public class GameTaskImportService : IRequestBound
    {
        private readonly SnapmeContext context;
        private readonly ISpreadsheetReader spreadsheetImporter;

        public GameTaskImportService(SnapmeContext context, ISpreadsheetReader spreadsheetImporter)
        {
            this.context = context;
            this.spreadsheetImporter = spreadsheetImporter;
        }

        public TaskImportResult Import(Game game)
        {
            if (game.GetSettings().TaskSpreadsheedListName.IsNullOrEmpty())
                throw new Exception("TaskListName is not set");
            var records = spreadsheetImporter.GetTasks(game.TgmBot.GetSettings(), game.GetSettings());
            return Import(game, records);
        }

        public TaskImportResult Import(Game game, List<GameTaskSpreadsheetRecord> newRecords)
        {
            newRecords = newRecords.Where(x => !x.DescriptionEn.IsEmptyOrNull() || !x.DescriptionRu.IsEmptyOrNull()).ToList();
            var importResult = new TaskImportResult();
            var gameId = game.Id;
            var oldRecords = context.GameTasks.Where(t => t.GameId == gameId && !t.IsDelete).ToList();
            if (newRecords.Count == 0)
                return importResult;

            var sorted = oldRecords.Any()
                ? newRecords.OrderByDescending(x => oldRecords.Max(z => GetSimilarity(z, x))).ToList()
                : newRecords;

            foreach (var record in sorted)
            {
                var activityType = TryParseType(record.Type);
                if (activityType == null)
                    continue;
                var phase = TryParsePhase(record.Phase);
                var level = TryParseLevel(record.Level);


                var oldRecord = oldRecords.FirstOrDefault(x => x.ActivityType == activityType && StrHelper.Norm(x.DescriptionRu) == StrHelper.Norm(record.DescriptionRu));

                oldRecord = oldRecord ?? oldRecords.Where(x => x.ActivityType == activityType)
                    .FindWithMaxOrDefault(x => GetSimilarity(x, record));


                var beforeUpdate = oldRecord?.ToLongString();
                if (oldRecord != null)
                {
                    oldRecord.DescriptionEn = record.DescriptionEn;
                    oldRecord.DescriptionRu = record.DescriptionRu;
                    oldRecord.Level = level ?? Level.Medium;
                    oldRecord.Phase = phase;
                    oldRecords.Remove(oldRecord);
                    importResult.Updated.Add(oldRecord);
                    var afterUpdate = oldRecord.ToLongString();
                    if (beforeUpdate != afterUpdate)
                    {
                        if (!importResult.HasChanges)
                            this.Log().Info("Change detected:" + beforeUpdate + "\n Vs \n" + afterUpdate);
                        importResult.NotifyHasChanges();
                    }
                }
                else
                {
                    var gameTask = context.AddNew(new Activity
                    {
                        Level = level ?? Level.Medium,
                        Phase = phase,
                        DescriptionEn = record.DescriptionEn,
                        DescriptionRu = record.DescriptionRu,
                        GameId = gameId,
                        ActivityType = activityType.Value
                    });
                    if (!importResult.HasChanges)
                        this.Log().Info("Change detected new:" + gameTask.ToLongString());
                    importResult.New.Add(gameTask);
                }
            }

            foreach (var oldRecord in oldRecords)
            {
                oldRecord.IsDelete = true;
                if (!importResult.HasChanges)
                    this.Log().Info("Change detected old:" + oldRecord.ToLongString());
                importResult.Deleted.Add(oldRecord);
            }
            return importResult;
        }

        private static int GetSimilarity(Activity x, GameTaskSpreadsheetRecord record)
        {
            var score = 20;
            if (!x.DescriptionRu.IsNullOrEmpty())
                score -= StrHelper.Compute(x.DescriptionRu ?? "", record.DescriptionRu ?? "");
            if (!x.DescriptionEn.IsNullOrEmpty())
                score -= StrHelper.Compute(x.DescriptionEn ?? "", record.DescriptionEn ?? "");
            return score;
        }

        private Level? TryParseLevel(string s)
        {
            if (Enum.TryParse(s, true, out Level r))
                return r;
            return null;
        }

        private int? TryParsePhase(string s)
        {
            if (int.TryParse(s, out var r))
                return r;
            return null;
        }

        private ActivityType? TryParseType(string s)
        {
            if (Enum.TryParse(s, true, out ActivityType r))
                return r;
            return null;
        }
    }

    public class TaskImportResult
    {
        private bool hasChanges;

        public bool HasChanges => hasChanges || Deleted.Any() || New.Any();

        public List<Activity> Updated { get; set; } = new List<Activity>();
        public List<Activity> Deleted { get; set; } = new List<Activity>();
        public List<Activity> New { get; set; } = new List<Activity>();

        public void NotifyHasChanges()
        {
            this.hasChanges = true;
        }
    }
}