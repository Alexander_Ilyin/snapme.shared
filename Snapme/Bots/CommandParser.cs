using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Snapme.Common;

namespace Snapme.Bots
{
    public class CommandParser
    {
        public static ParsedCmdParams ParseUsersCmdParams(string cmdParameter)
        {
            var match = Regex.Match(cmdParameter, @"^\s*(?<n>\d+)(?<n>\s*\,\s*\d+)*\s*(?<p>.*?)$");
            var parsedCmdParams = new ParsedCmdParams
            {
                Nums = match.Groups["n"].Captures.Select(x => x.Value.Trim(',', ' ')).Select(x=>x.TryParseInt().Value).ToList(),
                Parameter = match.Groups["p"].Value
            };
            return parsedCmdParams;
        }
    }

    public class ParsedCmdParams
    {
        public List<int> Nums { get; set; }
        public string Parameter { get; set; }
    }
}