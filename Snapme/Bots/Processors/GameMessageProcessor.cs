using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Snapme.Bots.Background;
using Snapme.Bots.Games;
using Snapme.Bots.Helpers;
using Snapme.Bots.Messages;
using Snapme.Bots.Services;
using Snapme.Bots.Telegram;
using Snapme.Bots.Texts;
using Snapme.Common;
using Snapme.Data;
using Snapme.Data.Bots;
using Snapme.Data.Games;
using Snapme.Import;

namespace Snapme.Bots.Processors
{
    public class GameMessageProcessor : DefaultMessageProcessor
    {
        private readonly GameProcessorImpl gameProcessorImpl;
        private readonly GameProcessorContext gameProcessorContext;
        private readonly GameService gameService;
        private TgmBotUser tgmBotUser => context.BotUser;
        private TelegramMessageEvent msg => context.Msg;
        private TgmBot bot => context.Bot;
        private long? feedbackChatId => bot.FindFeedbackChatId();

        public GameMessageProcessor(ITelegramMessenger messenger, UserService userService, BotContentService botContentService, SnapmeContext snapmeContext, MessageProcessorContext context,
            ISpreadsheetReader spreadsheetReader, ContentImporter contentImporter, GameProcessorImpl gameProcessorImpl, GameProcessorContext gameProcessorContext,
            GameTaskImportService gameTaskImportService, GameService gameService, GameMissionAssigner gameMissionAssigner) : base(messenger, userService, botContentService, snapmeContext,
            context, contentImporter)
        {
            this.gameProcessorImpl = gameProcessorImpl;
            this.gameProcessorContext = gameProcessorContext;
            this.gameService = gameService;
        }

        public long[] validatorChatIds => gameProcessorContext.ValidatorChatIds;
        public long? adminChatId => gameProcessorContext.AdminChatId;
        public Game game => gameProcessorContext.Game;
        public Guid gameId => gameProcessorContext.Game.Id;

        protected override async Task ProcessUserMessage()
        {
            await ProcessUserMessageAddTags();
            try
            {
                if (!msg.Message.IsPhoto() || game.IsRunning())
                    await ProcessUserMessageSendToFeedback();
            }
            catch (Exception ex)
            {
                this.Log().Error(ex);
            }
            var result = await gameProcessorImpl.ProcessUserMessage();
            if (result == MessageProcessingResult.MessageNotProcessed)
                await ProcessUserMessageMain();
        }

        protected override async Task ProcessGroupChatMessage()
        {
            if (validatorChatIds.Contains(msg.FromChatId))
                await gameProcessorImpl.ProcessValidatorMessage(msg.FromChatId.ToString(), msg.Message);
            else if (adminChatId == msg.FromChatId)
                await gameProcessorImpl.ProcessAdminMessage(msg.Message);
            await base.ProcessGroupChatMessage();
        }

        protected override async Task ProcessFeedbackReplyMessage(TgmBotUser forwardedFromUser)
        {
            if (msg.Message.Text != null && Regex.IsMatch(msg.Message.Text, @"^\+\d+$"))
            {
                var player = gameService.Join(gameId, forwardedFromUser.User);
                var extra = msg.Message.Text.Trim('+').TryParseInt();
                if (extra.HasValue)
                {
                    player.ExtraScore += extra.Value;
                    snapmeContext.SaveChanges();
                    await SendMessage(forwardedFromUser.TgmChatId.Value,
                        OutgoingMessage.Format(MsgFormats.YouGotPScore, FormatScores(extra.Value)));
                    await SendMessage(feedbackChatId.Value, OutgoingMessage.Format(MsgFormats.AddedPScore, extra.Value));
                }
                return;
            }
            await base.ProcessFeedbackReplyMessage(forwardedFromUser);
        }

        private string FormatScores(int extraValue)
        {
            switch (extraValue)
            {
                case 11:
                case 21:
                case 1: return extraValue + " балл";
                case 2:
                case 22:
                case 4:    
                case 24:
                case 3:
                case 23: return extraValue + " балла";
                default:
                    return extraValue + " баллов";
            }
        }

        protected override async Task ProcessCheckCommand()
        {
            await SendMessage(feedbackChatId.Value,
                OutgoingMessage.Simple(
                    $"Меня зовут {bot.Title} Это feedback чат\n" +
                    $"Текущая игра {game?.GetTitle() ?? " отсутствует"} \n" +
                    $"Состояние игры {game?.GameStatusStr() ?? "не определено"}"
                ));
            if (game != null)
            {
                var settings = game.GetSettings();
                if (settings.AdminChatId.HasValue)
                    await SendMessage(adminChatId.Value, OutgoingMessage.Simple($"Меня зовут {bot.Title} Это admin chat"));
                else
                    await SendMessage(feedbackChatId.Value, OutgoingMessage.Simple("Admin chat not set"));

                if (settings.ValidationChatIds.Any())
                    foreach (var validationChatId in settings.ValidationChatIds)
                        await SendMessage(validationChatId, OutgoingMessage.Simple($"Меня зовут {bot.Title} Это validate chat"));
                else
                    await SendMessage(feedbackChatId.Value, OutgoingMessage.Simple("ValidationChats not set"));
            }
        }

        protected override async Task OnUsersTagged(List<TgmBotUser> users, string[] tags)
        {
            await base.OnUsersTagged(users, tags);
            await gameProcessorImpl.OnUsersTagged(users);
        }
    }
}