using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Snapme.Bots.Background;
using Snapme.Bots.Helpers;
using Snapme.Bots.Messages;
using Snapme.Bots.Services;
using Snapme.Bots.Telegram;
using Snapme.Bots.Texts;
using Snapme.Common;
using Snapme.Data;
using Snapme.Data.Bots;
using Snapme.Data.Networking;
using Snapme.Import;

namespace Snapme.Bots.Processors
{
    public class NetworkingProcessor : DefaultMessageProcessor
    {
        private readonly NetworkingService service;
        private readonly MessageProcessorContext messageProcessorContext;
        private readonly UserService userService;
        private readonly BotContentService botContentService;
        private NetworkingDialogState state = new NetworkingDialogState();
        private TgmBotUser botUser => messageProcessorContext.BotUser;
        private TelegramMessageEvent msg => messageProcessorContext.Msg;

        public NetworkingProcessor(ITelegramMessenger messenger, UserService userService, BotContentService botContentService, SnapmeContext snapmeContext, MessageProcessorContext context, ISpreadsheetReader spreadsheetReader, ContentImporter contentImporter, NetworkingService service, MessageProcessorContext messageProcessorContext) : base(messenger, userService, botContentService, snapmeContext, context, contentImporter)
        {
            this.service = service;
            this.messageProcessorContext = messageProcessorContext;
            this.userService = userService;
            this.botContentService = botContentService;
        }

        public async Task<MessageProcessingResult> ProcessNetworkingMessage()
        {
            var contentData = await botContentService.GetContent(messageProcessorContext.Bot.Id);
            var contentItem = contentData.GetValue(msg.Message.Text);
            if (!contentItem.IsEmptyOrNull()
                || msg.Message.Text?.ToLowerInvariant().Trim(' ', '/', '!', '.') == "start"
                || msg.Message.Text?.ToLowerInvariant().Trim(' ', '/', '!', '.') == "ready")
            {
                botUser.CurrentDialogType = null;
                botUser.CurrentDialogState = null;
                await snapmeContext.SaveChangesAsync();
                return MessageProcessingResult.MessageNotProcessed;
            }

            LoadState(botUser.CurrentDialogState);
            var dialogProcessResult = await ProcessMessage(msg.Message);
            if (dialogProcessResult == MessageProcessingResult.MessageProcessingFinished)
            {
                botUser.CurrentDialogType = null;
                botUser.CurrentDialogState = null;
            }
            else
            {
                SaveState();
                botUser.CurrentDialogState = SaveState();
                botUser.CurrentDialogType = "networking";
            }
            await snapmeContext.SaveChangesAsync();
            return dialogProcessResult;
        }

        public void LoadState(string s)
        {
            if (s.IsEmptyOrNull())
                return;
            state = JsonConvert.DeserializeObject<NetworkingDialogState>(s);
        }

        public string SaveState()
        {
            return JsonConvert.SerializeObject(state);
        }

        public async Task<MessageProcessingResult> ProcessMessage(MessengerMessage message)
        {
            var userChatId = botUser.GetChatId();

            var command = message.ParseAsCommand();
            if (StrHelper.AlmostEqual("networking-disable", command.Action))
            {
                service.GetOrCreateProfile(botUser).IsActive = false;
                await snapmeContext.SaveChangesAsync();
                await SendMessage( userChatId,
                    OutgoingMessage.Format(MsgFormats.ProfileDisabled));
                return MessageProcessingResult.MessageProcessingFinished;
            }

            if (StrHelper.AlmostEqual("networkingenable", command.Action))
            {
                service.GetOrCreateProfile(botUser).IsActive = true;
                await snapmeContext.SaveChangesAsync();
                await SendMessage( userChatId,
                    OutgoingMessage.Format(MsgFormats.ProfileEnabled));
                return MessageProcessingResult.MessageProcessingFinished;
            }

            if (StrHelper.AlmostEqual("networkingedit", command.Action))
            {
                await SendMessage(userChatId,
                    OutgoingMessage.Format(MsgFormats.WhatYouWantToChange));
                return MessageProcessingResult.MessageProcessingFinished;
            }

            if (StrHelper.AlmostEqual("networking-update-position", command.Action))
                return await InitUpdate(MsgFormats.WhatIsYourPosition, QuestionAsked.WhatIsYourPosition);

            if (StrHelper.AlmostEqual("networking-update-photo", command.Action))
                return await InitUpdate(MsgFormats.SendUsSelfie, QuestionAsked.SendUsSelfie);

            if (StrHelper.AlmostEqual("networking-update-interests", command.Action))
                return await InitUpdate(MsgFormats.WhatIsYourInterest, QuestionAsked.WhatIsYourInterest);

            if (StrHelper.AlmostEqual("networking-update-great-at", command.Action))
                return await InitUpdate(MsgFormats.WhatAreYouGreatAt, QuestionAsked.WhatAreYouGreatAt);

            if (StrHelper.AlmostEqual("networking-like", command.Action))
            {
                var userNum = command.Parameter.TryParseInt();
                if (userNum != null)
                {
                    await AddLike(userNum.Value, message);
                    await ShowNextProfile();
                    return MessageProcessingResult.MessageProcessingFinished;
                }
                else
                    this.Log().Error("Invalid user num" + message.Text);
            }

            if (StrHelper.AlmostEqual("networking-skip", command.Action))
            {
                var userNum = command.Parameter.TryParseInt();
                if (userNum != null)
                {
                    await AddDisLike(userNum.Value, message);
                    await ShowNextProfile();
                    return MessageProcessingResult.MessageProcessingFinished;
                }
                else
                    this.Log().Error("Invalid user num" + message.Text);
            }

            if (StrHelper.AlmostEqual("networking-next", command.Action))
            {
                await ShowNextProfile();
                return MessageProcessingResult.MessageProcessingFinished;
            }

            switch (state.QuestionAsked)
            {
                case QuestionAsked.None:
                    await SendMessage( userChatId,
                        OutgoingMessage.Format(MsgFormats.NetworkingBegin));
                    await SendMessage( userChatId,
                        OutgoingMessage.Format(MsgFormats.SendUsSelfie));
                    state.QuestionAsked = QuestionAsked.SendUsSelfie;
                    snapmeContext.SaveChanges();
                    return MessageProcessingResult.MessageNotProcessed;


                case QuestionAsked.SendUsSelfie:
                    if (String.IsNullOrEmpty(message.PhotoId) &&
                        !StrHelper.AlmostEqual(message.Text, "networking-skip-q"))
                    {
                        await SendMessage(userChatId,
                            OutgoingMessage.Format(MsgFormats.SendUsSelfie));
                        return MessageProcessingResult.MessageNotProcessed;
                    }

                    if (!StrHelper.AlmostEqual(message.Text, "networking-skip-q"))
                    {
                        var profile = service.GetOrCreateProfile(botUser);
                        profile.PhotoId = message.PhotoId;
                    }

                    if (state.JustOneQuestion)
                    {
                        await AfterProfileUpdated(message);
                        return MessageProcessingResult.MessageProcessingFinished;
                    }

                    await SendMessage( userChatId,
                        OutgoingMessage.Format(MsgFormats.WhatIsYourPosition));
                    state.QuestionAsked = QuestionAsked.WhatIsYourPosition;
                    snapmeContext.SaveChanges();
                    return MessageProcessingResult.MessageNotProcessed;

                case QuestionAsked.WhatIsYourPosition:
                    if (String.IsNullOrEmpty(message.Text))
                    {
                        await SendMessage( userChatId,
                            OutgoingMessage.Format(MsgFormats.WhatIsYourPosition));
                        return MessageProcessingResult.MessageNotProcessed;
                    }

                    if (!StrHelper.AlmostEqual(message.Text, "networking-skip-q"))
                    {
                        var profile = service.GetOrCreateProfile(botUser);
                        profile.Position = message.Text;
                    }

                    if (state.JustOneQuestion)
                    {
                        await AfterProfileUpdated(message);
                        return MessageProcessingResult.MessageProcessingFinished;
                    }

                    await SendMessage( userChatId,
                        OutgoingMessage.Format(MsgFormats.WhatAreYouGreatAt));
                    state.QuestionAsked = QuestionAsked.WhatAreYouGreatAt;
                    snapmeContext.SaveChanges();
                    return MessageProcessingResult.MessageNotProcessed;

                case QuestionAsked.WhatAreYouGreatAt:
                    if (String.IsNullOrEmpty(message.Text))
                    {
                        await SendMessage( userChatId,
                            OutgoingMessage.Format(MsgFormats.WhatAreYouGreatAt));
                        return MessageProcessingResult.MessageNotProcessed;
                    }

                    if (!StrHelper.AlmostEqual(message.Text, "networking-skip-q"))
                    {
                        var profile = service.GetOrCreateProfile(botUser);
                        profile.GreatAt = message.Text;
                    }

                    if (state.JustOneQuestion)
                    {
                        await AfterProfileUpdated(message);
                        return MessageProcessingResult.MessageProcessingFinished;
                    }

                    await SendMessage( userChatId,
                        OutgoingMessage.Format(MsgFormats.WhatIsYourInterest));
                    state.QuestionAsked = QuestionAsked.WhatIsYourInterest;
                    snapmeContext.SaveChanges();
                    return MessageProcessingResult.MessageNotProcessed;

                case QuestionAsked.WhatIsYourInterest:
                {
                    if (String.IsNullOrEmpty(message.Text))
                    {
                        await SendMessage( userChatId,
                            OutgoingMessage.Format(MsgFormats.WhatIsYourInterest));
                        return MessageProcessingResult.MessageNotProcessed;
                    }

                    var profile = service.GetOrCreateProfile(botUser);
                    if (!StrHelper.AlmostEqual(message.Text, "networking-skip-q"))
                        profile.Interest = message.Text;

                    if (state.JustOneQuestion)
                    {
                        await AfterProfileUpdated(message);
                        return MessageProcessingResult.MessageProcessingFinished;
                    }

                    profile.IsActive = true;
                    snapmeContext.SaveChanges();
                    await ShowMyProfile();
                    return MessageProcessingResult.MessageProcessingFinished;
                }
            }

            return MessageProcessingResult.MessageProcessingFinished;
        }

        private async Task AfterProfileUpdated(MessengerMessage message)
        {
            if (!StrHelper.AlmostEqual(message.Text, "networking-skip-q"))
            {
                await ShowMyProfile();
            }
            else
                await SendMessage( botUser.GetChatId(),
                    OutgoingMessage.Format(MsgFormats.ProfileNotUpdated));
        }

        private async Task<MessageProcessingResult> InitUpdate(MsgFormat question,
            QuestionAsked questionAsked)
        {
            await SendMessage( botUser.GetChatId(),
                OutgoingMessage.Format(question));
            state.QuestionAsked = questionAsked;
            state.JustOneQuestion = true;
            snapmeContext.SaveChanges();
            return MessageProcessingResult.MessageNotProcessed;
        }

        private async Task AddLike(int userNum, MessengerMessage message)
        {
            var user = userService.FindBotUserByNumber(messageProcessorContext.Bot.Id, userNum);
            var otherUser = service.GetOrCreateProfile(user);
            var profile = service.GetOrCreateProfile(botUser);
            profile.AddLike(userNum);
            await NotifyLiked(otherUser);
            await SendMessage( botUser.GetChatId(),
                OutgoingMessage.KeyboardUpdate(message.MsgInfo,
                    "[btn:networking-skip " + userNum + ";Пропустить|networking-like " + userNum + ";Связаться👍]"));
            snapmeContext.SaveChanges();
        }

        private async Task AddDisLike(int userNum, MessengerMessage message)
        {
            var profile = service.GetOrCreateProfile(botUser);
            profile.AddDislike(userNum);
            await SendMessage( botUser.GetChatId(),
                OutgoingMessage.KeyboardUpdate(message.MsgInfo,
                    "[btn:networking-skip " + userNum + ";Пропустить✖|networking-like " + userNum + ";Связаться]"));
            snapmeContext.SaveChanges();
        }

        private async Task NotifyLiked(NetworkingProfile otherUser)
        {
            if (!otherUser.IsActive)
                return;
            var likedBy = botUser;
            if (otherUser.GetNotifiedBy().Contains(likedBy.UserNumber))
                return;
            otherUser.AddNofifiedBy(likedBy.UserNumber);
            snapmeContext.SaveChanges();
            var otherBotUser = snapmeContext.TgmBotUsers.Find(messageProcessorContext.Bot.Id, otherUser.UserId);

            await SendMessage( otherBotUser.GetChatId(),
                OutgoingMessage.Format(MsgFormats.OtherUserWantsToContactYou_PUser, likedBy.User.FormatLink()));

            await SendProfileToLikedUser(otherBotUser, service.GetOrCreateProfile(likedBy));
            
            await SendMessage( otherBotUser.GetChatId(),
                OutgoingMessage.Format(MsgFormats.ClickHisNameToChat));

        }

        private async Task ShowMyProfile()
        {
            var myProfile = service.GetOrCreateProfile(botUser);
            await SendMessage( botUser.GetChatId(),
                OutgoingMessage.Format(
                    MsgFormats.Profile_PPhoto_PName_PPosition_PInterest_PGreatAt,
                    FormatPhoto(myProfile.PhotoId), myProfile.User.FormatLink(), myProfile.Position,
                    myProfile.Interest, myProfile.GreatAt));
            
        }
        private async Task ShowNextProfile()
        {
            var profile = service.GetOrCreateProfile(botUser);
            var nextProfile = service.GetNextProfile(profile.NetworkingSessionId, botUser.UserId);
            if (nextProfile != null)
                await SendProfileWithButtons(nextProfile);
            else
                await SendMessage(botUser.GetChatId(),
                    OutgoingMessage.Format(MsgFormats.NoOtherNentworkingUsersYet));
        }

        private async Task SendProfileWithButtons(NetworkingProfile nextProfile)
        {
            var tgmBotUser = snapmeContext.TgmBotUsers.Find(messageProcessorContext.Bot.Id, nextProfile.User.Id);
            await SendMessage( botUser.GetChatId(),
                OutgoingMessage.Format(
                    MsgFormats.NetworkProfileWithButtons_PPhoto_PName_PPosition_PInterest_PGreatAt_PUserNum,
                    FormatPhoto(nextProfile.PhotoId), tgmBotUser.User.Name(), nextProfile.Position,
                    nextProfile.Interest, nextProfile.GreatAt, tgmBotUser.UserNumber));
        }

        private object FormatPhoto(string photoId)
        {
            if (string.IsNullOrWhiteSpace(photoId))
                return null;
            return "[img:" + photoId + "]";
        }
        


        private async Task SendProfileToLikedUser(TgmBotUser liked, NetworkingProfile likeAuthor)
        {
            await SendMessage( liked.GetChatId(),
                OutgoingMessage.Format(MsgFormats.OtherUserProfile_PPhoto_PName_PPosition_PInterest_PCompetence,
                    FormatPhoto(likeAuthor.PhotoId), likeAuthor.User.FormatLink(), likeAuthor.Position,
                    likeAuthor.Interest,
                    likeAuthor.GreatAt));
        }

        private enum QuestionAsked
        {
            None = 0,
            WhatIsYourPosition = 2,
            SendUsSelfie = 3,
            WhatIsYourInterest = 4,
            WhatAreYouGreatAt = 5,
        }

        private class NetworkingDialogState
        {
            public QuestionAsked QuestionAsked { get; set; }
            public bool JustOneQuestion { get; set; }
        }

    }
}