using System;
using System.Linq;
using System.Threading.Tasks;
using Snapme.Bots.Helpers;
using Snapme.Bots.Messages;
using Snapme.Bots.Services;
using Snapme.Bots.Telegram;
using Snapme.Common;
using Snapme.Common.DI;
using Snapme.Data;
using Snapme.Data.Bots;
using Snapme.Trinity;

namespace Snapme.Bots.Processors
{
    public class TelegramMessageProcessor : IRequestBound
    {
        private readonly UserService userService;
        private readonly NetworkingService networkingService;
        private readonly SnapmeContext snapmeContext;
        private readonly NetworkingProcessor networkingProcessor;
        private readonly TrinityMessageProcessor trinityProcessor;
        private readonly DefaultMessageProcessor defaultProcessor;
        private readonly GameMessageProcessor gameMessageMessageProcessor;
        private readonly GameService gameService;
        private readonly MessageProcessorContext processorContext;
        private readonly ITelegramMessenger messenger;

        public TelegramMessageProcessor(UserService userService, NetworkingService networkingService,
            SnapmeContext snapmeContext, NetworkingProcessor networkingProcessor, TrinityMessageProcessor trinityProcessor,
            DefaultMessageProcessor defaultProcessor, GameMessageProcessor gameMessageMessageProcessor, GameService gameService,
            MessageProcessorContext processorContext, ITelegramMessenger messenger)
        {
            this.userService = userService;
            this.networkingService = networkingService;
            this.snapmeContext = snapmeContext;
            this.networkingProcessor = networkingProcessor;
            this.trinityProcessor = trinityProcessor;
            this.defaultProcessor = defaultProcessor;
            this.gameMessageMessageProcessor = gameMessageMessageProcessor;
            this.gameService = gameService;
            this.processorContext = processorContext;
            this.messenger = messenger;
        }

        public async Task ProcessMessage(Guid botId, TelegramMessageEvent msg)
        {
            TgmBotUser botUser = null;
            if (!msg.FromGroupChat)
            {
                botUser = userService.GetOrCreateBotUser(botId, msg.FromChatId, msg.FromUserId.Value);
                if (msg.FromUser != null)
                {
                    botUser.User.LastName = msg.FromUser.LastName;
                    botUser.User.FirstName = msg.FromUser.FirstName;
                    botUser.User.TgmUserName = msg.FromUser.TgmUserName;
                    botUser.User.Language = Languages.FromTgmCode(msg.FromUser.Language);
                    snapmeContext.SaveChanges();
                }
            }
            var bot = snapmeContext.TgmBots.Find(botId);
            processorContext.Init(bot, botUser, msg);
            if (processorContext.Msg.Message?.Text == "id".Norm())
            {
                await messenger.SendMessage(
                    processorContext.Msg.FromChatId, OutgoingMessage.Simple("Chat Id:" + processorContext.Msg.FromChatId));
                return;
            }
            if (!msg.FromGroupChat)
            {
                var botUser1 = processorContext.BotUser;
                if (string.Equals(msg.Message.Text?.Trim('/', ' '), "startnetworking", StringComparison.OrdinalIgnoreCase))
                    networkingService.BeginNetworking(botUser1);
                if (botUser1.CurrentDialogType == "networking" || msg.Message.Text?.StartsWith("networking") == true)
                {
                    var result = await networkingProcessor.ProcessNetworkingMessage();
                    if (result == MessageProcessingResult.MessageProcessingFinished)
                        return;
                }
            }

            if (TrinityMessageProcessor.IsCreateGameCommand(msg.Message.Text) ||
                snapmeContext.TrinityGames.Any(g => g.TgmBotId == processorContext.Bot.Id))
                await trinityProcessor.Process();
            else
            {
                var game = gameService.GetCurrentGameForBot(processorContext.Bot.Id);
                if (game != null && game.CanRun())
                    await gameMessageMessageProcessor.Process();
                else
                    await defaultProcessor.Process();
                await snapmeContext.SaveChangesAsync();
            }
        }
    }
}