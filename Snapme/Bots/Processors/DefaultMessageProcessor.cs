using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Snapme.Bots.Background;
using Snapme.Bots.Helpers;
using Snapme.Bots.Messages;
using Snapme.Bots.Services;
using Snapme.Bots.Telegram;
using Snapme.Bots.Texts;
using Snapme.Common;
using Snapme.Common.DI;
using Snapme.Data;
using Snapme.Data.Bots;
using static System.String;

namespace Snapme.Bots.Processors
{
    public class DefaultMessageProcessor : IRequestBound
    {
        private readonly ITelegramMessenger messenger;
        private readonly UserService userService;
        private readonly BotContentService botContentService;
        protected readonly SnapmeContext snapmeContext;
        protected readonly MessageProcessorContext context;
        protected TgmBotUser user => context.BotUser;
        protected TelegramMessageEvent msg => context.Msg;
        private readonly ContentImporter contentImporter;
        
        protected long FeedbackChatId => context.Bot.FindFeedbackChatId().Value;

        public DefaultMessageProcessor(ITelegramMessenger messenger, UserService userService,
            BotContentService botContentService, SnapmeContext snapmeContext, MessageProcessorContext context, ContentImporter contentImporter)
        {
            this.messenger = messenger;
            this.userService = userService;
            this.botContentService = botContentService;
            this.snapmeContext = snapmeContext;
            this.context = context;
            this.contentImporter = contentImporter;
        }

        public async Task Process()
        {
            if (msg.FromGroupChat)
                await ProcessGroupChatMessage();
            else
                await ProcessUserMessage();
        }

        protected virtual async Task ProcessFeedbackReplyMessage(TgmBotUser forwardedFromUser)
        {
            var text = msg.Message.Text;
            if (IsNullOrEmpty(text))
                return;
            if (!text.StartsWith("/"))
            {
                await SendMessage(forwardedFromUser.TgmChatId.Value, OutgoingMessage.Simple(text));
                var feedbackChat = context.Bot.FindFeedbackChatId().Value;
                await SendMessage(feedbackChat, OutgoingMessage.Simple("Sent:"));
                await SendMessage(feedbackChat, OutgoingMessage.Simple(text));
            }
            else
            {
                if (text.StartsWith("/tag"))
                {
                    var tag = text.Replace("/tag", "").Trim('#', ' ');
                    forwardedFromUser.AddTags(tag);
                    await snapmeContext.SaveChangesAsync();
                    await SendMessage(FeedbackChatId, OutgoingMessage.Simple("Tagged " + tag));
                }
                else if (text.StartsWith("/untag"))
                {
                    var tag = text.Replace("/untag", "").Trim('#', ' ');
                    forwardedFromUser.RemoveTags(tag);
                    await snapmeContext.SaveChangesAsync();
                    await SendMessage(FeedbackChatId, OutgoingMessage.Simple("UnTagged " + tag));
                }
                else if (text.StartsWith("/content"))
                {
                    var tag = text.Replace("/content", "").Trim(' ');
                    var botContentData = await botContentService.GetContent(context.Bot.Id);
                    var value = botContentData.GetValue(tag);
                    if (IsNullOrEmpty(value))
                        await SendMessage(FeedbackChatId, OutgoingMessage.Simple("Content " + tag + " not found"));
                    else
                    {
                        await SendMessage(forwardedFromUser.TgmChatId.Value, OutgoingMessage.Simple(value));
                        await SendMessage(FeedbackChatId, OutgoingMessage.Simple("Sent:"));
                        await SendMessage(FeedbackChatId, OutgoingMessage.Simple(value));
                    }
                }
            }
        }

        protected virtual async Task ProcessFeedbackRegularMessage()
        {
            if (msg.Message.IsPhoto())
            {
                await SendMessage(FeedbackChatId, OutgoingMessage.Simple("plaintext:[img:" + msg.Message.PhotoId + "]"));
            }
            else if (msg.Message.IsVideoNote())
            {
                await SendMessage(FeedbackChatId, OutgoingMessage.Simple("plaintext:[vn:" + msg.Message.VideoNoteId + "]"));
            }
            else if (msg.Message.IsAudio())
            {
                await SendMessage(FeedbackChatId, OutgoingMessage.Simple("plaintext:[audio:" + msg.Message.AudioId + "]"));
            }
            else if (msg.Message.IsVoice())
            {
                await SendMessage(FeedbackChatId, OutgoingMessage.Simple("plaintext:[voice:" + msg.Message.VoiceId + "]"));
            }
            else
            {
                var cmd = msg.Message.ParseAsCommand();
                switch (cmd.Action)
                {
                    case "untag":
                    {
                        var parsed = CommandParser.ParseUsersCmdParams(cmd.Parameter);
                        var users = parsed.Nums.Select(n => userService.FindBotUserByNumber(context.Bot.Id, n)).ToList();
                        var tags = parsed.Parameter.SplitRemoveEmpty(' ', ',', ';', '\n')
                            .Select(x => x.Trim('#')).ToArray();
                        foreach (var user in users)
                            user.RemoveTags(tags);
                        await snapmeContext.SaveChangesAsync();
                        await SendMessage(FeedbackChatId, OutgoingMessage.Simple($"Untagged {users.Count} users with tags {tags.JoinStr(t => t, ",")}"));
                    }
                        break;
                    case "tag":
                    {
                        var parsed = CommandParser.ParseUsersCmdParams(cmd.Parameter);
                        var users = parsed.Nums.Select(n => userService.FindBotUserByNumber(context.Bot.Id, n)).ToList();
                        var tags = parsed.Parameter.SplitRemoveEmpty(' ', ',', ';', '\n')
                            .Select(x => x.Trim('#')).ToArray();
                        foreach (var user in users)
                            user.AddTags(tags);

                        await snapmeContext.SaveChangesAsync();
                        await OnUsersTagged(users, tags);
                        await SendMessage(FeedbackChatId, OutgoingMessage.Simple($"Tagged {users.Count} users with tags {tags.JoinStr(t => t, ",")}"));
                        break;
                    }
                    case "import":
                    {
                        await ProcessFeedbackRegularMessageImport();
                        break;
                    }
                    case "users":
                    {
                        var lines = snapmeContext.TgmBotUsers.Where(u => u.BotId == context.Bot.Id)
                            .Include(bu => bu.User).ToArray()
                            .OrderBy(x => x.UserNumber).ToArray()
                            .Where(bu => bu.IsMatch(cmd.Parameter))
                            .Select(bu => (bu.UserNumber > 0 ? (bu.UserNumber + ". ") : "") + bu.User.FormatLink() +
                                          (bu.Tags.IsEmptyOrNull() ? "" : " tags:" + bu.Tags))
                            .ToArray();

                        foreach (var batch in lines.Batches(20))
                            await SendMessage(FeedbackChatId, OutgoingMessage.Simple(batch.JoinStr(x => x, "\n")));
                        await SendMessage(FeedbackChatId, OutgoingMessage.Simple("Total: " + lines.Length));
                        break;
                    }
                    case "check":
                    {
                        await ProcessCheckCommand();
                        break;
                    }
                    case "contentall":
                    {
                        var contentData = await botContentService.GetContent(context.Bot.Id);
                        var contentItem = contentData.GetValue(cmd.Parameter ?? "");
                        if (!contentItem.IsEmptyOrNull())
                        {
                            var count = await SendAll("all", contentItem);
                            await SendMessage(FeedbackChatId, OutgoingMessage.Format(MsgFormats.SentMessageToNUsers, count));
                        }
                        else
                            await SendMessage(FeedbackChatId, OutgoingMessage.Format(MsgFormats.ContentNotFound));

                        break;
                    }
                    case "contenttag":
                    {
                        string[] split = cmd.Parameter.SplitRemoveEmpty(' ', 2);
                        var contentData = await botContentService.GetContent(context.Bot.Id);
                        var contentItem = contentData.GetValue(split[1] ?? "");
                        var tag = split[0].Trim('#');
                        if (!contentItem.IsEmptyOrNull())
                        {
                            var count = await SendAll(tag, contentItem);
                            await SendMessage(FeedbackChatId, OutgoingMessage.Format(MsgFormats.SentMessageToNUsers, count));
                        }
                        else
                            await SendMessage(FeedbackChatId, OutgoingMessage.Format(MsgFormats.ContentNotFound));
                        break;
                    }
                    case "messagetag":
                    {
                        {
                            string[] split = cmd.Parameter.SplitRemoveEmpty(' ', 2);
                            if (split.Length != 2)
                            {
                                await SendMessage(FeedbackChatId, OutgoingMessage.Simple("Invalid format"));
                                return;
                            }
                            var tag = split[0].Trim('#');
                            var count = await SendAll(tag, split[1]);
                            await SendMessage(FeedbackChatId, OutgoingMessage.Format(MsgFormats.SentMessageToNUsers, count));
                        }
                        break;
                    }
                    case "messageuser":
                    {
                        string[] split = cmd.Parameter.SplitRemoveEmpty(' ', 2);
                        if (split.Length != 2)
                        {
                            await SendMessage(FeedbackChatId, OutgoingMessage.Simple("Invalid format"));
                            return;
                        }

                        var userNum = split[0].TryParseInt();
                        if (!userNum.HasValue)
                        {
                            await SendMessage(FeedbackChatId, OutgoingMessage.Simple("Invalid format"));
                            return;
                        }

                        var user = userService.FindBotUserByNumber(context.Bot.Id, userNum.Value);
                        if (user == null)
                        {
                            await SendMessage(FeedbackChatId, OutgoingMessage.Simple("User " + userNum + " not found!"));
                            return;
                        }

                        await SendMessage(user.GetChatId(), OutgoingMessage.Simple(split[1]));
                    }
                        break;
                    case "messageall":
                        if (!cmd.Parameter.IsEmptyOrNull())
                        {
                            var count = await SendAll("all", cmd.Parameter);
                            await SendMessage(FeedbackChatId, OutgoingMessage.Format(MsgFormats.SentMessageToNUsers, count));
                        }

                        break;
                    case "h":
                    case "help":
                        await SendMessage(FeedbackChatId, OutgoingMessage.Format(MsgFormats.HelpFeedback));
                        break;
                }
            }
        }

        protected virtual async Task ProcessFeedbackRegularMessageImport()
        {
            await contentImporter.ImportContent(context.Bot.Id, true);
        }

        private async Task<int> SendAll(string tag, string message)
        {
            int count = 0;
            var botUsers = snapmeContext.TgmBotUsers.Where(x => x.BotId == context.Bot.Id);
            foreach (var botUser in botUsers)
            {
                try
                {
                    if (botUser.IsMatchForTag(tag))
                        if (!botUser.User.IsFake && botUser.TgmChatId != null)
                        {
                            await SendMessage(botUser.TgmChatId.Value, OutgoingMessage.Simple(message));
                            count++;
                        }
                }
                catch (Exception ex)
                {
                    this.Log().Error("Er", ex);
                }
            }
            return count;
        }

        protected virtual async Task ProcessCheckCommand()
        {
            var tgmBot = snapmeContext.TgmBots.Find(context.Bot.Id);
            await SendMessage(tgmBot.FindFeedbackChatId().Value,
                OutgoingMessage.Simple($"Меня зовут {tgmBot.Title} Это feedback чат. Это всё что я могу сказать.\n"));
        }

        protected virtual Task OnUsersTagged(List<TgmBotUser> users, string[] tags)
        {
            return Task.CompletedTask;
        }

        protected virtual async Task ProcessUserMessage()
        {
            await ProcessUserMessageAddTags();
            try
            {
                await ProcessUserMessageSendToFeedback();
            }
            catch (Exception ex)
            {
                this.Log().Error(ex);
            }
            await ProcessUserMessageMain();
        }

        protected virtual async Task ProcessUserMessageMain()
        {
            var contentData = await botContentService.GetContent(context.Bot.Id);
            var contentItem = contentData.GetValue(msg.Message.Text);
            if (!contentItem.IsEmptyOrNull())
                await SendMessage(msg.FromChatId, OutgoingMessage.Simple(contentItem));
        }

        protected virtual bool SkipSendingToFeedback(string text)
        {
            if (text?.Norm() == "start".Norm())
                return true;
            if (text?.Norm() == "ready".Norm())
                return true;
            if (text?.Norm().StartsWith("done") == true)
                return true;
            return false;
        }

        protected virtual async Task ProcessUserMessageSendToFeedback()
        {
            if (SkipSendingToFeedback(msg.Message.Text))
                return;
            var feedbackChatId = context.Bot.FindFeedbackChatId();
            if (feedbackChatId == null)
                return;
            if (msg.Message.IsCallbackQuery && !SkipSendingToFeedback(msg.Message.Text))
            {
                var text = Format("user <a href='tg://user?id={0}'>{1} {2}</a> queried {3}",
                    msg.FromUser.TgmUserId, msg.FromUser?.FirstName, msg.FromUser?.LastName, msg.Message.Text);
                await SendMessage(feedbackChatId.Value, OutgoingMessage.Simple(text));
            }
            else
            {
                await SendMessage(feedbackChatId.Value, OutgoingMessage.Forwarded(msg.Message));
            }
        }

        protected async Task SendMessage(long chatId, OutgoingMessage msg)
        {
            try
            {
                if (msg.TextFormat != null && !msg.TextFormat.GetCode().IsEmptyOrNull())
                {
                    var contentData = await botContentService.GetContent(context.Bot.Id);
                    var customFormat = contentData.GetValue(msg.TextFormat.GetCode());
                    if (!customFormat.IsEmptyOrNull())
                        msg.TextFormat = msg.TextFormat.Patched(customFormat);
                }

                await messenger.SendMessage(chatId, msg, Languages.Russian);
            }
            catch (Exception ex)
            {
                this.Log().Error(ex);
            }
        }

        protected async Task ProcessUserMessageAddTags()
        {
            if (msg.Message.Text?.Trim('/', ' ')?.StartsWith("t_") == true)
            {
                var tag = msg.Message.Text?.Trim('/', ' ');
                if (user != null)
                {
                    user.AddTags(tag);
                    await snapmeContext.SaveChangesAsync();
                }
            }
        }

        protected virtual async Task ProcessGroupChatMessage()
        {
            var feedbackChatId = context.Bot.FindFeedbackChatId();
            if (feedbackChatId != null && msg.FromChatId == feedbackChatId)
            {
                if (msg.Message.ReplyTo != null)
                {
                    var forwardedFromIds = messenger.FindForwardFromUserIds(msg.Message.ReplyTo).ToArray();
                    var forwardedFromUser = userService.FindBotUser(forwardedFromIds, context.Bot.Id);
                    if (forwardedFromUser != null)
                    {
                        await ProcessFeedbackReplyMessage(forwardedFromUser);
                        return;
                    }
                }
                await ProcessFeedbackRegularMessage();
            }
        }
    }
}