using System;
using System.Collections.Generic;
using System.Linq;
using Snapme.Common;
using Snapme.Data;
using Snapme.Data.Bots;
using Snapme.Data.Games;

namespace Snapme.Bots.Games
{
    public class PlayerTaskAssignJob
    {
        private readonly Dictionary<User, List<DetachedGameTask>> completeTasks;
        private readonly SnapmeContext context;
        private readonly List<Player> freePlayers;
        private readonly Activity[] activities;
        private readonly int longWaitPeriodInSeconds;
        readonly DateTime now = DateHelper.UtcNow();
        private readonly Dictionary<Player, GameTask> newMissions = new Dictionary<Player, GameTask>();
        private bool isCityGame;

        public PlayerTaskAssignJob(SnapmeContext context, Activity[] activities,
            Dictionary<User, List<DetachedGameTask>> completeTasks,
            List<Player> freePlayers, int longWaitPeriodInSeconds)
        {
            this.isCityGame = activities.Any(x=>x.ActivityType==ActivityType.FinalTaskPlace);
            this.context = context;
            this.activities = activities;
            this.completeTasks = completeTasks;
            this.longWaitPeriodInSeconds = longWaitPeriodInSeconds;
            this.freePlayers = freePlayers.ToList();
        }

        public Dictionary<Player, GameTask> AssignMissions(bool force)
        {
            var somePlayer = freePlayers.FirstOrDefault();
            if (somePlayer == null)
                return newMissions;
            var game = somePlayer.Game;
            if (game.GameStatus == GameStatus.Finished || game.GameStatus == GameStatus.New)
                return newMissions;

            var withMeetings = game.GetSettings().WithMeetings == true;
            var noFinalActions = activities.All(x =>
                x.ActivityType != ActivityType.FinalSingleTask && x.ActivityType != ActivityType.FinalTaskPlace);

            ForFreePlayers(p =>
            {
                if (p.SelfiePhotoId == null)
                {
                    if (game.GameStatus == GameStatus.Started)
                        AssignTask(p, null, GameTaskType.Selfie);
                }
            });
            
            ForFreePlayers(p =>
            {
                var prevTask = GetPrevTask(p);
                if (withMeetings && prevTask!=null && prevTask.IsMeeting())
                {
                    var previousPartner = GetPreviousPartnerIfAvailable(p);
                    if (previousPartner != null)
                    {
                        AssignTask(p, previousPartner, game.GameStatus==GameStatus.Started ? 
                            GameTaskType.Mission : GameTaskType.Final);
                    }
                }
            });
            
            ForFreePlayers(p =>
            {
                GameTaskType nextTaskType;
                if (game.GameStatus == GameStatus.Final)
                    nextTaskType = GameTaskType.Final;
                else
                    nextTaskType = withMeetings ? GameTaskType.Meeting : GameTaskType.Mission;

                if (nextTaskType == GameTaskType.Final)
                    if (FinishedFinal(p) || noFinalActions)
                        return;
                Player partner = null;
                if (game.GameStatus == GameStatus.Final)
                    partner = GetPreviousPartnerIfAvailable(p);
                if (partner == null)
                    partner = FindOtherGenderPartner(p);
                if (partner == null && (IsLongWaiter(p) || force))
                    partner = FindAnyGenderPartner(p);
                if (force || (partner == null && game.GameStatus == GameStatus.Final))
                    partner = FindAnyFreePartner(p);

                if (partner != null)
                    AssignTask(p, partner, nextTaskType);
                else if (game.GameStatus == GameStatus.Final)
                    if (!p.User.IsFake)
                        AssignTask(p, null, GameTaskType.FinalSingle);
            });
            return newMissions;
        }

        private Player FindPartner(Player player, Func<Player, bool> where)
        {
            foreach (var freePlayer in freePlayers.OrderByDescending(x => GetWaitTime(x)))
            {
                if (player == freePlayer)
                    continue;
                if (player.User.IsFake && freePlayer.User.IsFake)
                    continue;
                if (newMissions.ContainsKey(freePlayer))
                    continue;
                if (where(freePlayer))
                    return freePlayer;
            }

            return null;
        }

        private int CommonMissionsCount(Player player, Player partner)
        {
            var r = 0;
            foreach (var m in completeTasks.GetValueOrDefault(player.User) ?? new List<DetachedGameTask>())
                if (m.AssignedUserIds.Any(a => a == partner.UserId))
                    r++;
            return r;
        }

        private Player FindAnyFreePartner(Player player)
        {
            return FindPartner(player, p => true);
        }

        private Player FindAnyGenderPartner(Player player)
        {
            return FindPartner(player, p => CommonMissionsCount(player, p) == 0);
        }

        private Player FindOtherGenderPartner(Player player)
        {
            return FindPartner(player, p => p.User.Gender != player.User.Gender && CommonMissionsCount(player, p) == 0);
        }

        private Player GetPreviousPartnerIfAvailable(Player player)
        {
            var lastTask = completeTasks.GetItemOrDefault(player.User)?.LastOrDefault();
            if (lastTask == null)
                return null;
            var otherPlayerUserId = lastTask.AssignedUserIds.FirstOrDefault(a => a != player.UserId);
            if (otherPlayerUserId == Guid.Empty)
                return null;
            var partner = context.Players.Find(player.GameId, otherPlayerUserId);
            if (newMissions.ContainsKey(partner))
                return null;
            if (partner.Status == PlayerStatus.Active)
                return partner;
            return null;
        }

        private DetachedGameTask GetPrevTask(Player player)
        {
            var lastTask = completeTasks.GetItemOrDefault(player.User)?.LastOrDefault();
            return lastTask;
        }

        private bool FinishedFinal(Player player)
        {
            var lastTaskType = completeTasks.GetItemOrDefault(player.User)?.LastOrDefault()?.TaskType;
            return lastTaskType == GameTaskType.FinalSingle ||
                   lastTaskType == GameTaskType.Final;
        }

        private void AssignTask(Player player, Player partner, GameTaskType taskType)
        {
            var tasks = SelectActivities(taskType, player, partner);
            if (tasks.Count == 0)
            {
                this.Log().Info($"No activities found matching {player} {partner} {taskType}");
                return;
            }

            var playerTaskId = Guid.NewGuid();
            for (var i = 0; i < tasks.Count; i++)
            {
                var selectTask = tasks[i];
                context.Add(new GameTaskActivity
                {
                    Number = i,
                    GameTaskId = playerTaskId,
                    ActivityId = selectTask.Id
                });
            }

            var playerTask = newMissions[player] = context.AddNew(new GameTask
            {
                Id = playerTaskId,
                GameId = player.GameId,
                GameTaskStatus = GameTaskStatus.NotReady,
                Created = DateHelper.UtcNow(),
                TaskType = taskType
            });
            this.Log().Info($"Assign player task {player} {taskType} {playerTaskId} ");
            context.Add(new GameTaskAssignee
            {
                UserId = player.UserId,
                GameTaskId = playerTaskId
            });
            newMissions[player] = playerTask;
            if (partner != null)
            {
                context.Add(new GameTaskAssignee
                {
                    UserId = partner.UserId,
                    GameTaskId = playerTaskId
                });
                newMissions[partner] = playerTask;
                partner.CurrentTaskId = playerTaskId;
                partner.OnStatusUpdate();
                this.Log().Info($"Assign partner task {partner} {taskType} {playerTaskId}");
            }
            player.OnStatusUpdate();
            player.CurrentTaskId = playerTask.Id;
        }


        private List<Activity> SelectActivities(GameTaskType taskType, Player player1, Player partner = null)
        {
            var res = new List<Activity>();
            var level = GetLevel(player1, partner);
            switch (taskType)
            {
                case GameTaskType.Selfie:
                    res.Add(TrySelectActivity(ActivityType.SelfieTask, player1, partner, level, res));
                    break;
                case GameTaskType.Meeting:
                    res.Add(TrySelectActivity(ActivityType.MeetingTask, player1, partner, level, res));
                    break;
                case GameTaskType.MissionSingle:
                    res.Add(TrySelectActivity(ActivityType.MissionSingleTask, player1, partner, level, res));
                    break;
                case GameTaskType.Mission:
                    res.Add(TrySelectActivity(ActivityType.MissionTaskPlace, player1, partner, level, res));
                    res.Add(TrySelectActivity(ActivityType.MissionTaskActivity, player1, partner, level, res));
                    if (GetLevel(player1, partner) != Level.Easy)
                        res.Add(TrySelectActivity(ActivityType.MissionTaskActivity, player1, partner, level, res));
                    break;
                case GameTaskType.FinalSingle:
                    res.Add(TrySelectActivity(ActivityType.FinalSingleTask, player1, partner, level, res));
                    break;
                case GameTaskType.Final:
                    res.Add(TrySelectActivity(ActivityType.FinalTaskPlace, player1, partner, level, res));
                    res.Add(TrySelectActivity(ActivityType.MissionTaskActivity, player1, partner, level, res));
                    res.Add(TrySelectActivity(ActivityType.MissionTaskActivity, player1, partner, level, res));
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(taskType), taskType, null);
            }

            return res.Where(x => x != null).ToList();
        }

        private Level GetLevel(Player player1, Player pathner)
        {
            var missionCount = Math.Max(GetCompleteTaskCount(player1), GetCompleteTaskCount(pathner));
            if (missionCount <= (isCityGame ? 4 : 2))
                return Level.Easy;
            if (missionCount >= 6)
                return Level.Hard;
            return Level.Medium;
        }

        private int GetCompleteTaskCount(Player player)
        {
            if (player == null)
                return 0;
            return completeTasks.GetItemOrDefault(player.User)?.Count ?? 0;
        }

        private Activity TrySelectActivity(ActivityType activityType, Player player1, Player partner, Level level,
            List<Activity> excluded)
        {
            var bestMainTask = activities
                .Where(a => a.ActivityType == activityType)
                .Where(a => !excluded.Contains(a))
                .FindWithAlmostMaxOrDefault(activity =>
                {
                    var score = 300;

                    if (completeTasks.GetValueOrDefault(player1.User)
                            ?.Exists(t => t.ActivityIds.Any(a => a == activity.Id)) == true)
                        score -= 100;
                    if (partner != null)
                        if (completeTasks.GetValueOrDefault(partner.User)
                                ?.Exists(t => t.ActivityIds.Any(y => y == activity.Id)) == true)
                            score -= 100;

                    if (LevelIsVeryDifferent(activity.Level, level))
                        score -= 10;
                    return score;
                });
            return bestMainTask;
        }

        private bool LevelIsVeryDifferent(Level l1, Level l2)
        {
            return Math.Abs(l1 - l2) == 2;
        }

        private bool IsLongWaiter(Player player)
        {
            var waitTime = GetWaitTime(player);
            if (waitTime != null && waitTime >= TimeSpan.FromSeconds(longWaitPeriodInSeconds))
                return true;
            return false;
        }

        private TimeSpan? GetWaitTime(Player player)
        {
            var finishTime = completeTasks.GetValueOrDefault(player.User)?.LastOrDefault()?.Finished;
            return now - finishTime;
        }

        private void ForFreePlayers(Action<Player> action)
        {
            foreach (var freePlayer in freePlayers.OrderByDescending(GetWaitTime))
                if (!newMissions.ContainsKey(freePlayer))
                    action(freePlayer);
        }
    }
}