using System;
using System.Linq;
using Snapme.Data.Games;

namespace Snapme.Bots.Games
{
    public class DetachedGameTask
    {
        private readonly GameTaskType taskType;
        private readonly DateTime? created;
        private readonly DateTime? finished;
        private readonly Guid[] activityIds;
        private readonly Guid[] assignedUserIds;

        public Guid[] AssignedUserIds => assignedUserIds;
        public Guid[] ActivityIds => activityIds;

        public GameTaskType TaskType => taskType;
        

        public DetachedGameTask(GameTaskType taskType, DateTime? created, DateTime? finished,Guid[] activityIds, Guid[] assignedUserIds)
        {
            this.taskType = taskType;
            this.created = created;
            this.finished = finished;
            this.activityIds = activityIds;
            this.assignedUserIds = assignedUserIds;
        }

        public DateTime? Created => created;
        public DateTime? Finished  => finished;

        public bool IsMeeting()
        {
            return taskType == GameTaskType.Meeting;
        }

        public bool IsMission()
        {
            return taskType == GameTaskType.Final || taskType == GameTaskType.FinalSingle || taskType == GameTaskType.Mission
                || taskType == GameTaskType.MissionSingle;
        }

        public Guid? GetPartnerFor(Guid userId)
        {
            var res = AssignedUserIds.FirstOrDefault(x => x != userId);
            if (res == Guid.Empty)
                return null;
            return res;
        }
    }
}