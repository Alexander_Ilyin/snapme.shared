using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Snapme.Bots.Background;
using Snapme.Bots.Helpers;
using Snapme.Bots.Messages;
using Snapme.Bots.Services;
using Snapme.Bots.Texts;
using Snapme.Common;
using Snapme.Common.DI;
using Snapme.Data;
using Snapme.Data.Bots;
using Snapme.Data.Games;
using Snapme.Testing;

namespace Snapme.Bots.Games
{
    public class GameProcessorImpl : IRequestBound
    {
        private readonly GameService gameService;
        private readonly GameMissionAssigner gameMissionAssigner;
        private readonly GameProcessorMessageSender processorMessageSender;
        private readonly UserService userService;
        private readonly SnapmeContext context;
        private readonly GameProcessorContext gameProcessorContext;
        private readonly MessageProcessorContext messageProcessorContext;

        public GameProcessorImpl(GameService gameService,
            GameMissionAssigner gameMissionAssigner,
            GameProcessorMessageSender processorMessageSender, UserService userService, SnapmeContext context,
            GameProcessorContext gameProcessorContext, MessageProcessorContext messageProcessorContext )
        {
            this.gameService = gameService;
            this.gameMissionAssigner = gameMissionAssigner;
            this.processorMessageSender = processorMessageSender;
            this.userService = userService;
            this.context = context;
            this.gameProcessorContext = gameProcessorContext;
            this.messageProcessorContext = messageProcessorContext;
        }

        private Guid gameId => gameProcessorContext.Game.Id;
        private string[] validatorIds => gameProcessorContext.ValidatorChatIds.Select(x=>x.ToString()).ToArray();
        private MessengerMessage receivedMsg => messageProcessorContext.Msg.Message;
        
        public async Task ProcessAdminMessage(MessengerMessage serviceMessage)
        {
            var cmd = serviceMessage.ParseAsCommand();
            switch (cmd.Action)
            {
                case "h":
                case "help":
                    await SendToAdmin(OutgoingMessage.Format(MsgFormats.HelpAdmin));
                    return;
                case "testtasks":
                    var sampler = new GameSampler(context);
                    var testGame = await sampler.TestGame(gameId);
                    await SendToAdmin(OutgoingMessage.Simple(testGame));
                    return;
                case "status":
                    var status = gameService.CollectStatus(gameId, cmd.Parameter);
                    foreach (var s in status)
                        await SendToAdmin(OutgoingMessage.Simple(s));
                    return;
                case "offline":
                    if (cmd.Parameter.IsEmptyOrNull())
                        await SendToAdmin(OutgoingMessage.Simple("Укажите имя"));
                    if (serviceMessage.PhotoId.IsEmptyOrNull())
                        await SendToAdmin(OutgoingMessage.Simple("Укажите photoid"));
                    else
                        await JoinFakeUser(serviceMessage.PhotoId, cmd.Parameter);
                    return;

                case "shuffle":
                    var args = cmd.Parameter.Split(' ', StringSplitOptions.RemoveEmptyEntries);
                    var photoCount = args[0].TryParseInt();
                    if (photoCount != args.Length - 1)
                        await SendToAdmin(
                            OutgoingMessage.Simple("Wrong command, good one is like : shuffle 3 photo1 photo2 photo3"));
                    else
                        await ShufflePlayers(args.Skip(1).ToArray());
                    return;
                case "messageplayers":
                    await SendToPlayers(cmd.Parameter ?? "");
                    return;

                case "kick":
                    await KickPlayer(cmd.Parameter ?? "");
                    return;

                case "softkick":
                    await SoftKickPlayer(cmd.Parameter ?? "");
                    return;

                case "startall":
                    await StartGame("all");
                    return;
                case "starttag":
                    await StartGame(cmd.Parameter);
                    return;
                case "phase":
                {
                    var phase = cmd.Parameter.TryParseInt();
                    if (phase == null)
                        await SendToAdmin(OutgoingMessage.Format(MsgFormats.PhaseNotSet));
                    else
                    {
                        gameService.GetGame(gameId).SetPhase(phase.Value);
                        await SendToAdmin(OutgoingMessage.Format(MsgFormats.PhaseUpdated));
                        await gameService.SaveChangesAsync();
                    }
                    return;
                }
                case "results":
                    await NotifyAdminOfGameResults();
                    return;
                case "sendresults":
                    await NotifyPlayersAndAdminOfGameResults(false);
                    return;
                case "finish":
                    gameService.GetGame(gameId).SetFinished();
                    await gameService.SaveChangesAsync();
                    await NotifyPlayersAndAdminOfGameResults(true);
                    return;
                case "force":
                {
                    await AssignMissionsAndNotifyPlayers(true);
                    await gameService.SaveChangesAsync();
                    await SendToAdmin(OutgoingMessage.Format(MsgFormats.ForceComplete));
                    return;
                }
                case "final":
                {
                    gameService.GetGame(gameId).SetFinal();
                    if (!gameService.HasFinalActvities(gameId))
                    {
                        var idlePlayers = gameService.GetActivePlayersWithoutTasks(gameId);
                        foreach (var idlePlayer in idlePlayers)
                            await SendToPlayer(idlePlayer, OutgoingMessage.Format(MsgFormats.ItWasFinalTask));

                        var newPlayers = gameService.GetPlayersWithSelfieTask(gameId);
                        foreach (var player in newPlayers)
                        {
                            await gameService.CancelTask(player);
                            await SendToPlayer(player, OutgoingMessage.Format(MsgFormats.TaskCancelled));
                        }
                    }
                    else
                        await AssignMissionsAndNotifyPlayers();

                    await gameService.SaveChangesAsync();
                    await SendToAdmin(OutgoingMessage.Format(MsgFormats.PhaseUpdatedToFinal));
                    return;
                }
                case "getphotos":
                    var photos = gameService.GetAcceptedMissionPhotos(gameId);
                    if (!photos.Any())
                        await SendToAdmin(OutgoingMessage.Simple("Нет подходящих фоток"));
                    else
                        foreach (var batch in photos.GroupByCount(10))
                            await SendToAdmin(OutgoingMessage.Photos(batch));
                    return;
                case "getselfies":
                    var selfies = gameService.GetAcceptedSelfiePhotos(gameId);
                    if (!selfies.Any())
                        await SendToAdmin(OutgoingMessage.Simple("Нет подходящих фоток"));
                    else
                        foreach (var batch in selfies.GroupByCount(10))
                            await SendToAdmin(OutgoingMessage.Photos(batch));
                    return;
            }
        }

        private async Task AssignMissionsAndNotifyPlayers(bool skipWait=false)
        {
            await gameMissionAssigner.AssignMissionsAndNotifyPlayers(gameProcessorContext.Game.Id, skipWait);
        }

        private async Task ShufflePlayers(string[] photos)
        {
            var players = await gameService.GetAllPlayersForAnnonce(gameId);
            
            var shuffled = players.OrderBy(x => RandomHelper.TestMode ? x.User.FullName() :"X"+x.User.Id.GetHashCode()).ToArray();
            for (int i = 0; i < shuffled.Length; i++)
            {
                var player = shuffled[i];
                await SendToPlayer(player, OutgoingMessage.Photo(photos[i % photos.Length], null));
            }
        }

        private async Task JoinFakeUser(string photoId, string name)
        {
            var user = userService.GetOrCreateFakeUser(gameService.GetGame(gameId).TgmBotId.Value, name);
            var player = gameService.Join(gameId, user.User);
            gameService.ActivatePlayer(player);
            gameService.SaveChanges();
            await AssignMissionsAndNotifyPlayers();
            gameService.SetReadyForValidation(player.CurrentTaskId, photoId);
            await ProcessValidatorQueue();
        }

        private async Task SendToPlayers(string msg)
        {
            if (string.IsNullOrWhiteSpace(msg))
            {
                await SendToAdmin(OutgoingMessage.Simple("Message not set"));
                return;
            }

            var players = await gameService.GetAllPlayersForAnnonce(gameId);
            foreach (var player in players)
                if (player.SelfiePhotoId != null)
                    try
                    {
                        await SendToPlayer(player, OutgoingMessage.Simple(msg));
                    }
                    catch (Exception e)
                    {
                        this.Log().Error(e);
                        throw;
                    }
        }

        private async Task SoftKickPlayer(string tgmUserName)
        {
            Player player = null;
            if (tgmUserName.StartsWith("@"))
                player = gameService.FindPlayer(tgmUserName.Trim('@'));
            if (tgmUserName.TryParseInt() != null)
                player = gameService.FindPlayerByNumber(gameId, tgmUserName.TryParseInt().Value);
            if (player == null)
            {
                await SendToAdmin(OutgoingMessage.Format(MsgFormats.UserNotFoundPUser, tgmUserName));
                return;
            }
            await gameService.SoftKick(player);
            await SendToAdmin(OutgoingMessage.Format(MsgFormats.PlayerKicked));
        }

        private async Task KickPlayer(string tgmUserName)
        {
            Player player = null;
            if (tgmUserName.StartsWith("@"))
                player = gameService.FindPlayer(tgmUserName.Trim('@'));
            if (tgmUserName.TryParseInt() != null)
                player = gameService.FindPlayerByNumber(gameId, tgmUserName.TryParseInt().Value);

            if (player == null)
                await SendToAdmin(OutgoingMessage.Format(MsgFormats.UserNotFoundPUser, tgmUserName));
            else
                await KickPlayer(player);
        }

        private async Task KickPlayer(Player player)
        {
            var canceledTask = player.CurrentTask;
            var partner = player.CurrentTask == null ? null : gameService.GetPartner(player, player.CurrentTask);
            await gameService.Kick(player);
            {
                if (canceledTask != null)
                    await SendToPlayer(player, MsgFormats.TaskCancelled);
                await SendToPlayer(player, MsgFormats.YouLeftTheGame);
                await SendToAdmin(OutgoingMessage.Format(MsgFormats.PlayerKicked));
            }
            if (partner != null)
            {
                await SendToPlayer(partner, MsgFormats.TaskCancelled);
                await AssignMissionsAndNotifyPlayers();
                if (partner.CurrentTask == null)
                    await SendToPlayer(partner, MsgFormats.YouWillGetNewTaskInAMinute);
            }
        }

        public async Task ProcessValidatorMessage(string validatorId, MessengerMessage serviceMessage)
        {
            var cmd = serviceMessage.ParseAsCommand();
            if (cmd.Action == "current")
            {
                var current = gameService.GetCurrentlyModerated(validatorId, gameId);
                if (current != null)
                {
                    await SendToValidator(validatorId, OutgoingMessage.Photo(current.TaskPhotoId, null));
                    var description = gameService.GetDescription(current);
                    await SendToValidator(validatorId, OutgoingMessage.Simple(description + GetButtons(current)));
                }
                else
                {
                    await SendToValidator(validatorId, OutgoingMessage.Simple("No items to moderate..."));
                }
            }

            if (cmd.Action == "kick")
                if (serviceMessage.ReplyTo != null)
                {
                    var user = processorMessageSender.FindUser(serviceMessage.ReplyTo);
                    if (user != null)
                    {
                        await KickPlayer(gameService.GetPlayer(gameId, user.Id));
                        await SendToValidator(validatorId, OutgoingMessage.Simple("Kicked " + user.FullName()));
                    }
                }

            if (cmd.Action == "kick-partner")
                if (serviceMessage.ReplyTo != null)
                {
                    var user = processorMessageSender.FindUser(serviceMessage.ReplyTo);
                    if (user != null)
                    {
                        var player = gameService.GetPlayer(gameId, user.Id);
                        var partner = gameService.GetPartner(player, player.CurrentTask);
                        if (partner != null)
                        {
                            await KickPlayer(partner);
                            await SendToValidator(validatorId,
                                OutgoingMessage.Simple("Kicked " + partner.User.FullName()));
                        }
                    }
                }

            if (cmd.Action == "softkick")
                if (serviceMessage.ReplyTo != null)
                {
                    var user = processorMessageSender.FindUser(serviceMessage.ReplyTo);
                    if (user != null)
                    {
                        await gameService.SoftKick(gameService.GetPlayer(gameId, user.Id));
                        await SendToValidator(validatorId, OutgoingMessage.Simple("Soft Kicked " + user.FullName()));
                    }
                }

            var task = gameService.GetCurrentlyModerated(validatorId, gameId);
            if (task == null)
                return;

            if (cmd.Action == "yes")
            {
                var game = gameService.GetGame(gameId);
                if (game.GameStatus == GameStatus.Finished)
                    return;
                var players = gameService.GetPlayers(task);
                if (task.TaskType == GameTaskType.Selfie)
                {
                    var player = players.Single();
                    var user = player.User;
                    var gender = ParseGender(cmd.Parameter);
                    if (gender == null)
                    {
                        await SendToValidator(validatorId, OutgoingMessage.Format(MsgFormats.SpecifyBoyGirl));
                        return;
                    }

                    player.SelfiePhotoId = task.TaskPhotoId;
                    user.Gender = gender;
                    await gameService.SaveChangesAsync();
                }

                gameService.AcceptTask(task);
                foreach (var p in players)
                    await SendToPlayer(p, MsgFormats.PhotoAccepted);

                gameService.SaveChanges();
                
                var isFinalWithNoActivities =
                    game.GameStatus == GameStatus.Final && !gameService.HasFinalActvities(gameId);
                if (!isFinalWithNoActivities)
                    await AssignMissionsAndNotifyPlayers();

                foreach (var p in players)
                    if (p.CurrentTaskId == null)
                    {
                        if (task.IsFinal() || isFinalWithNoActivities)
                            await SendToPlayer(p, MsgFormats.ItWasFinalTask);
                        else
                            await SendToPlayer(p, MsgFormats.YouWillGetNewTaskInAMinute);
                    }

                await ProcessValidatorQueue();
            }
            else if (cmd.Action?.StartsWith("no") == true)
            {
                var game = gameService.GetGame(gameId);
                if (game.GameStatus == GameStatus.Finished)
                    return;

                var activityNumber = cmd.Action.Length > 2 ? cmd.Action.Substring("no".Length).TryParseInt() : null;
                var activityItem = task.Activities.FirstOrDefault(x => x.Number == activityNumber);
                if (activityItem != null)
                {
                    var tasks = task.Assigned.Select(p =>
                    {
                        var player = gameService.GetPlayer(task.GameId, p.UserId);
                        player.OnStatusUpdate();
                        return SendToPlayer(player, MsgFormats.PhotoDeclinedRequirementFailPRequirement,
                            activityItem.Activity.DescriptionRu);
                    });
                    await Task.WhenAll(tasks);
                    gameService.RejectTask(task, "Failed:" + activityItem.Activity.DescriptionRu);
                    //await SendToValidator(validatorId, OutgoingMessage.Format(MsgFormats.YouRejectedPhoto));

                    await ProcessValidatorQueue();
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(cmd.Parameter))
                    {
                        await SendToValidator(validatorId,
                            OutgoingMessage.Format(MsgFormats.DeclineReasonMissing).InReplyTo(serviceMessage));
                        return;
                    }

                    var tasks = task.Assigned.Select(p =>
                    {
                        var player = gameService.GetPlayer(task.GameId, p.UserId);
                        player.OnStatusUpdate();
                        return SendToPlayer(player, MsgFormats.PhotoDeclinedPReason, cmd.Parameter);
                    });
                    await Task.WhenAll(tasks);
                    gameService.RejectTask(task, cmd.Parameter);
                    await ProcessValidatorQueue();
                }
            }
        }


        public async Task<MessageProcessingResult> ProcessUserMessage()
        {
            var p = gameService.Join(gameId, messageProcessorContext.BotUser.User);

            if (p.User.TgmUserName.IsEmptyOrNull() && p.Game.GetSettings().RequireUserName == true)
            {
                await SendToPlayer(p, MsgFormats.SetUsername);
                await SendToPlayer(p, MsgFormats.WriteWhenReady);
                return MessageProcessingResult.MessageProcessingFinished;
            }

            if (receivedMsg.Text?.StartsWith("/start") == true)
            {
                await SendToPlayer(p, MsgFormats.Start);
                if (p.Game.IsStarted())
                {
                    if (p.Game.GetTags().Any(x => p.GetBotUser(context).IsMatchForTag(x)))
                    {
                        p.IsNotifiedGameStarted = true;
                        await SendToPlayer(p, MsgFormats.StartInGame);
                    }

                    p.IsWelcomeReceived = true;
                    gameService.SaveChanges();
                }
                return MessageProcessingResult.MessageProcessingFinished;
            }

            if (p.Status == PlayerStatus.NotJoined &&
                Regex.IsMatch(receivedMsg.Text ?? "", @"\s*ready\??\!?\.?\s*", RegexOptions.IgnoreCase))
            {
                if (p.Game.GameStatus == GameStatus.Final || p.Game.GameStatus == GameStatus.Finished)
                    await SendToPlayer(p, OutgoingMessage.Format(MsgFormats.TooLateToPlay));
                else
                    gameService.ActivatePlayer(p);
            }

            if (p.Status == PlayerStatus.Active || p.Status == PlayerStatus.SoftKicked)
            {
                if (p.IsJustJoined())
                    await AssignMissionsAndNotifyPlayers();

                if (receivedMsg.IsPhoto() && p.CurrentTask != null)
                {
                    p.OnStatusUpdate();
                    var currentTask = p.CurrentTask;
                    var partner = gameService.GetPartner(p, p.CurrentTask);

                    if (partner != null)
                    {
                        await SendToPlayer(partner, MsgFormats.YourPartnerSubmittedPhoto);
                        await SendToPlayer(partner, OutgoingMessage.Photo(receivedMsg.PhotoId, null));
                    }
                    var game = gameService.GetGame(gameId);
                    if (game.GameStatus == GameStatus.Finished)
                    {
                        await SendToPlayer(p, MsgFormats.GameFinished);
                        return MessageProcessingResult.MessageProcessingFinished; 
                    }

                    switch (currentTask.GameTaskStatus)
                    {
                        case GameTaskStatus.ReadyForValidation:
                        case GameTaskStatus.NotReady:
                            gameService.SetReadyForValidation(p.CurrentTaskId, receivedMsg.PhotoId);
                            await SendToPlayer(p, MsgFormats.CheckingYourPhoto);
                            await ProcessValidatorQueue();
                            break;
                        case GameTaskStatus.Validating:
                            await SendToPlayer(p, MsgFormats.AlreadyCheckingYourPhoto);
                            break;
                    }
                }
                return MessageProcessingResult.MessageProcessingFinished;

            }
            return MessageProcessingResult.MessageNotProcessed;
        }


        private async Task SendToAdmin(OutgoingMessage msg)
        {
            await processorMessageSender.SendToAdmin(msg);
        }


        private async Task SendToPlayer(Player player, MsgFormat message, params object[] parameters)
        {
            await processorMessageSender.SendToUser(player.User, OutgoingMessage.Format(message, parameters));
        }

        private async Task SendToPlayer(Player player, OutgoingMessage message)
        {
            await processorMessageSender.SendToUser(player.User, message);
        }

        private async Task SendToValidator(string validatorId, OutgoingMessage message)
        {
            await processorMessageSender.SendToValidator(validatorId, message);
        }

        private async Task ProcessValidatorQueue()
        {
            this.Log().Info("ProcessModeratorQueue Begin");
            foreach (var validationChatId in validatorIds)
            {
                var current = gameService.GetCurrentlyModerated(validationChatId, gameId);
                if (current == null)
                {
                    var mission = gameService.GetNextForModeration(gameId);
                    if (mission != null)
                    {
                        if (mission.TaskType!=GameTaskType.Selfie)
                            await SendToValidator(validationChatId, OutgoingMessage.Simple(gameService.DescribePlayersWithSelfie(mission)));
//
                        await SendToValidator(validationChatId,
                            OutgoingMessage.Simple("[img:" + mission.TaskPhotoId + "] " + 
                                                   (mission.TaskType==GameTaskType.Selfie ? mission.Assigned.First().User.FormatLinkWithNum() +"\n" : "")+
                                                   gameService.GetDescription(mission) +
                                                   GetButtons(mission)));
                        gameService.SetValidating(mission, validationChatId);
                        gameService.SaveChanges();
                        this.Log().Info("Sent moderate message, Mission: " + mission.Id);
                    }
                }
            }

            this.Log().Info("ProcessModeratorQueue End");
        }

        private string GetButtons(GameTask mission)
        {
            if (mission.TaskType == GameTaskType.Selfie)
                return
                    "[btn:yes boy|yes girl]" +
                    "[btn:no На селфи должен быть один игрок]" +
                    "[btn:no Фото не селфи|no Условие не выполнено]";

            var activities = mission.GetActivities();
            var i = -1;
            return "[btn:yes]" + activities.JoinStr(a =>
            {
                i++;
                return "[btn:no" + i + " " + a.GetDescription(Languages.Russian)?.Replace(";", "") + "]";
            }, "");
        }

        private async Task NotifyAdminOfGameResults()
        {
            var results = gameService.GetGameResults(gameId).Items.ToList();
            await SendToAdmin(OutgoingMessage.Format(MsgFormats.GameResults));
            foreach (var f in FormatLongGameResults(results, true, Languages.Russian))
                await SendToAdmin(f);
        }

        private async Task NotifyPlayersAndAdminOfGameResults(bool gameIsFinished)
        {
            var players = await gameService.GetAllPlayersForAnnonce(gameId);
            var playersResults = gameService.GetGameResults(gameId).Items;
            var top10results = playersResults.Take(10).ToList();
            var gameResultsItems = playersResults.ToDictionary(x => x.User.Id);
            var sendResultsToPlayers = !gameIsFinished || gameService.GetGame(gameId).GetSettings().SendResultsToPlayers;
            if (gameIsFinished)
            {
                await SendToAdmin(OutgoingMessage.Format(MsgFormats.GameFinished));
                await SendToAdmin(OutgoingMessage.Format(MsgFormats.GameResults));
                await SendToAdmin(FormatFinalGameResults(top10results, true, Languages.Russian));
            }
            else
            {
                await SendToAdmin(OutgoingMessage.Format(MsgFormats.CurrentResults));
                await SendToAdmin(FormatFinalGameResults(top10results, true, Languages.Russian));
            }

            foreach (var player in players)
            {
                var score = gameResultsItems.GetValueOrDefault(player.UserId)?.Score ?? 0;
                if (score > 0)
                {
                    if (gameIsFinished)
                    {
                        await SendToPlayer(player, MsgFormats.GameFinished);
                        await SendToPlayer(player, MsgFormats.YourScorePScore, score);
                        if (sendResultsToPlayers)
                        {
                            await SendToPlayer(player, MsgFormats.GameResults);
                            await SendToPlayer(player, FormatFinalGameResults(top10results, false, Languages.Russian));
                        }
                    }
                    else
                    {
                        await SendToPlayer(player, MsgFormats.CurrentResults);
                        await SendToPlayer(player, FormatFinalGameResults(top10results, false, Languages.Russian));
                    }
                }
            }

            if (sendResultsToPlayers)
                await SendToAdmin(OutgoingMessage.Format(MsgFormats.ResultsSentToPlayers));
            else
                await SendToAdmin(OutgoingMessage.Format(MsgFormats.ResultsAreNotSentToPlayers));
        }

        public OutgoingMessage[] FormatLongGameResults(List<FinalGameResultsItem> results, bool formatLinks,
            string language)
        {
            return results.Select(winner =>
                    string.Format(MsgFormats.PlayerScorePPlayerPScore.GetFormat(language),
                        (formatLinks ? winner.User.FormatLink() : winner.User.Name()) +
                        (winner.User.IsFake ? " (offline)" : ""),
                        winner.Score)).Batches(20).Select(b => b.JoinStr(x => x, "\n"))
                .Select(s => OutgoingMessage.Simple(s)).ToArray();
        }

        public OutgoingMessage FormatFinalGameResults(List<FinalGameResultsItem> results, bool formatLinks,
            string language)
        {
            var str = results.Select(winner =>
                    string.Format(MsgFormats.PlayerScorePPlayerPScore.GetFormat(language),
                        (formatLinks ? winner.User.FormatLink() : winner.User.Name()) +
                        (winner.User.IsFake ? " (offline)" : ""),
                        winner.Score))
                .JoinStr(x => x, "\n");
            return OutgoingMessage.Simple(str);
        }

        private Gender? ParseGender(string cmdParameter)
        {
            if (cmdParameter?.ToLowerInvariant() == "boy")
                return Gender.Boy;
            if (cmdParameter?.ToLowerInvariant() == "girl")
                return Gender.Girl;
            return null;
        }

        public async Task StartGame(string tag)
        {
            if (string.IsNullOrWhiteSpace(tag))
            {
                await SendToAdmin(OutgoingMessage.Simple("Tag not set!"));
                return;
            }

            var game = gameService.GetGame(gameId);
            game.SetStarted(tag);
            await gameService.SaveChangesAsync();
            await gameService.EnsureAllBotUsersArePlayers(gameId);
            await NotifyPlayersGameStarted();
            await AssignMissionsAndNotifyPlayers();
            await SendToAdmin(OutgoingMessage.Format(MsgFormats.GameStartedAdmin));
        }

        private async Task NotifyPlayersGameStarted()
        {
            var game = gameService.GetGame(gameId);
            foreach (var player in gameService.GetAllPlayers(gameId))
            {
                if (player.IsNotifiedGameStarted)
                    continue;
                if (game.GetTags().Any(x => player.GetBotUser(context).IsMatchForTag(x)))
                {
                    await SendToPlayer(player, MsgFormats.GameStarted);
                    player.IsNotifiedGameStarted = true;
                }
            }

            await gameService.SaveChangesAsync();
        }


        public async Task OnUsersTagged(IEnumerable<TgmBotUser> users)
        {
            var game = gameService.GetGame(gameId);
            if (game.GameStatus == GameStatus.Started)
            {
                foreach (var user in users)
                {
                    var player = gameService.GetPlayer(gameId, user.UserId);
                    if (player.IsNotifiedGameStarted)
                        continue;

                    if (game.GetTags().Any(x => player.GetBotUser(context).IsMatchForTag(x)))
                    {
                        await SendToPlayer(player, MsgFormats.GameStarted);
                        player.IsNotifiedGameStarted = true;
                    }
                }
            }

            await gameService.SaveChangesAsync();
        }
    }
}