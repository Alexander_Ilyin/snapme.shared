using System;
using System.Linq;
using System.Threading.Tasks;
using Snapme.Bots.Helpers;
using Snapme.Bots.Messages;
using Snapme.Bots.Services;
using Snapme.Bots.Telegram;
using Snapme.Common;
using Snapme.Common.DI;
using Snapme.Data;
using Snapme.Data.Bots;

namespace Snapme.Bots.Games
{
    public class GameProcessorMessageSender : IRequestBound
    {
        private readonly SnapmeContext snapmeContext;
        private readonly GameProcessorContext gameProcessorContext;
        private readonly UserService userService;
        private readonly MessageProcessorContext messageProcessorContext;
        private readonly ITelegramMessenger telegramMessenger;

        public GameProcessorMessageSender(SnapmeContext snapmeContext,
            GameProcessorContext gameProcessorContext,
            UserService userService,
            MessageProcessorContext messageProcessorContext, ITelegramMessenger telegramMessenger)
        {
            this.snapmeContext = snapmeContext;
            this.gameProcessorContext = gameProcessorContext;
            this.userService = userService;
            this.messageProcessorContext = messageProcessorContext;
            this.telegramMessenger = telegramMessenger;
        }

        public async Task SendToUser(User user, OutgoingMessage msg)
        {
            var botUser = await snapmeContext.TgmBotUsers.FindAsync(messageProcessorContext.Bot.Id, user.Id);
            if (botUser.TgmChatId.HasValue)
                await telegramMessenger.SendMessage(botUser.TgmChatId.Value, msg, Languages.Russian);
            else
                this.Log().Error("Error in user no tgmId");
        }

        public async Task SendToAdmin(OutgoingMessage outgoingMessage)
        {
            if (!gameProcessorContext.AdminChatId.HasValue)
                throw new Exception("adminChatId Has no Value" + messageProcessorContext.Bot.Id);
            await telegramMessenger.SendMessage(gameProcessorContext.AdminChatId.Value, outgoingMessage);
        }

        public async Task SendToValidator(string validatorId, OutgoingMessage outgoingMessage)
        {
            await telegramMessenger.SendMessage(long.Parse(validatorId), outgoingMessage);
        }

        public User FindUser(MessangerMsgInfo msgInfo)
        {
            var fromUserIds = telegramMessenger.FindForwardFromUserIds(msgInfo);
            return userService.FindBotUser(fromUserIds.ToArray(), messageProcessorContext.Bot.Id)?.User;
        }
    }
}