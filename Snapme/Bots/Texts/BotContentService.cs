using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Snapme.Common;
using Snapme.Common.DI;
using Snapme.Data;
using Snapme.Data.Bots;
using Snapme.Data.Info;
using Snapme.Import;

namespace Snapme.Bots.Texts
{
    public class BotContentService : IRequestBound
    {
        private readonly MemoryCache cache;
        private readonly SnapmeContext context;

        public BotContentService(SnapmeContext context, MemoryCache cache)
        {
            this.context = context;
            this.cache = cache;
        }

        public async Task<BotContentData> GetContent(Guid botId)
        {
            try
            {
                return await cache.GetOrCreateAsync("content-" + botId, async e =>
                {
                    var content = await context.BotContents.Where(x => x.TgmBotId == botId).OrderBy(x => x.Id)
                        .FirstOrDefaultAsync();
                    var items = content?.GetItems()?.ToList();
                    e.SetValue(items);
                    e.AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(60);
                    return new BotContentData(items);
                });
            }
            catch (Exception ex)
            {
                this.Log().Error("Error getting content ", ex);
                return new BotContentData(new List<InfoContentItem>());
            }
        }

        public ImportResult ImportRecords(TgmBot bot, List<ContentSpreadsheetRecord> records)
        {
            if (records == null)
            {
                this.Log().Error("Error - missing records" + bot.Title + " " + bot.Id );
                return ImportResult.NoChanges;
            }
            
            var content = context.BotContents.Where(x => x.TgmBotId == bot.Id).OrderBy(x => x.Id).FirstOrDefault();
            if (content == null)
            {
                content = context.AddNew(new BotContent());
                content.TgmBotId = bot.Id;
            }

            var existingItems = content.GetItems().ToDictionarySafe(x => x.Type.Norm());
            if (existingItems.Count == 0 && records.Count == 0)
                return ImportResult.NoChanges;
            
            var beforeUpdate = content.SerializedItems;

            var resultItems = records
                .Select(r => new InfoContentItem { Type = r.Type, Content = r.Description})
                .OrderBy(i => i.Type)
                .ToList();
            
            content.SetItems(resultItems);
            var contentChanged = beforeUpdate != content.SerializedItems;
            
            if (contentChanged)
                this.Log().InfoFormat("Importing content. Existing: {0}, New: {1}, Result: {2}, Changed: {3}", 
                    existingItems.Count, records.Count, resultItems.Count, contentChanged);
            
            if (contentChanged)
            {
                context.SaveChanges();
                ResetCache(bot.Id);
                return ImportResult.Imported;
            }
            return ImportResult.NoChanges;
        }

        public void ResetCache(Guid botId)
        {
            cache.Remove("content-" + botId);
        }
    }

    public enum ImportResult
    {
        NoChanges = 0,
        Imported = 1
    }
}