using System.Collections.Concurrent;
using System.Reflection;
using Snapme.Bots.Messages;

namespace Snapme.Bots.Texts
{
    public static class MsgFormats
    {
        public static MsgFormat UserNotFoundPUser =
            Ru("Пользователь не найден {0}")
                .En("User not found {0}");

        public static MsgFormat Start =
            Ru("Время и место начала игры мы сообщим в этом чате!")
                .En("Place and time of start will be provided later");

        public static MsgFormat YouGotPScore =
            Ru("Ура! У тебя +{0}.")
                .En("You got +{0} scores.");

        public static MsgFormat AddedPScore =
            Ru("Добавим +{0}.")
                .En("Added +{0} scores.");

        public static MsgFormat ContentNotFound =
            Ru("Контент не найден")
                .En("Content not found");

        public static MsgFormat StartInGame =
            Ru("Добро пожаловать в игру! Если ты на месте и готов нажми [btn:ready;Поехали!]")
                .En("Welcome to the game! If you are ready, press [btn:ready;I'M Ready!]");

        public static MsgFormat GameStarted =
            Ru("Игра началась! Если ты на месте и готов нажми [btn:ready;Поехали!] ")
                .En("Game started! If you are ready, press [btn:ready;I'M Ready!]");

        public static MsgFormat GameStartedAdmin =
            Ru("Игра стартовала")
                .En("Game started!");

        public static MsgFormat ResultsSentToPlayers =
            Ru("Результаты отправлены игрокам ")
                .En("Results are sent to players");

        public static MsgFormat ResultsAreNotSentToPlayers =
            Ru("Результаты НЕ отправлены игрокам ")
                .En("Results are NOT sent to players");

        public static MsgFormat CheckingYourPhoto =
            Ru("Проверяем!").En("Checking your photo...");

        public static MsgFormat AlreadyCheckingYourPhoto =
            Ru("Фото уже проверяется").En("Your photo is already being cheked...");

        public static MsgFormat WriteWhenReady = Ru("Напиши сюда, когда готово.")
            .En("Please write here when it's done");

        public static MsgFormat SetUsername =
            Ru("Чтобы другие игроки могли с тобой связаться, открой настройки и заполни Username (в меню Settings).")
                .En(
                    "Please open Settings window in app's main menu and fill your unique username. This is needed to allow other players to contact you during the game.");

        public static MsgFormat GameFinished = Ru("Игра окончена!").En("Game finished!");

        public static MsgFormat YourScorePScore = Ru("Твой результат - {0}").En("Your score - {0}");

        public static MsgFormat CurrentTopTenPlayersResults =
            Ru("Tоп-10 игроков:")
                .En("Current top players:");

        public static MsgFormat GameResults =
            Ru("Результат игры:")
                .En("Game results:");


        public static MsgFormat MissionHint =
            Ru(
                    "Фото - не обязательно селфи. На нём должны быть оба партнёра и по фото должно быть очевидно, что условия выполнены.")
                .En(
                    "Partner for the next task is the same. Photo is not necessary a selfie. We should see all task requirements are met on the photo.");

        public static MsgFormat MeetingHint =
            Ru(
                    "Нажми на ник партнёра, чтобы открыть чат. Ваша цель - встретиться и вместе выполнить задание. Фото должен отправть любой из вас.")
                .En(
                    "Press on partner's username to start a chat. Your goal is to meet each other and complete the task together. Фото должен отправть любой из вас.");

        public static MsgFormat MeetingHintOffline =
            Ru(
                    "Твой партнер Offline, если ты не сможешь его найти, напиши мне.")
                .En(
                    "Your partner is Offline, tell me if you can't find him.");

        public static MsgFormat SelfieHint = Ru("Фото присылай в этот чат.");

        public static MsgFormat YourPartnerPPartnerUserIdPPartnerUsername =
            Ru("Твой партнёр - <a href='tg://user?id={0}'>{1}</a>")
                .En("Your partner - <a href='tg://user?id={0}'>{1}</a>").AsHtml();

        public static MsgFormat YourPartnerOfflinePPartnerUserIdPPartnerUsername =
            Ru("Твой партнёр - {0}")
                .En("Your partner - {0}").AsHtml();

        public static MsgFormat YourPartnerSubmittedPhoto =
            Ru("Ваш партнёр отправил фото").En("Your partner submitted photo");

        public static MsgFormat YouWillGetNewTaskInAMinute =
            Ru("Через несколько минут вы получите следующее задание.")
                .En("You'll get new task in a few minutes...");


        public static MsgFormat TaskCancelled = Ru("Задание отменено").En("Task is cancelled");

        public static MsgFormat ItWasFinalTask = Ru("Это было финальное задание, ждём остальных и подводим итоги!"
        ).En("This was the final task. Let's wait for others to complete and sum up results!");


        public static MsgFormat PhotoWithRequirementsPRequirements =
            Ru("Фото с условиями: {0}").En("Take following photo:{0}");

        public static MsgFormat FirstTask = Ru("Первое задание:").En("First task:");

        public static MsgFormat YourTask = Ru("Ваше задание:").En("Your task:");

        public static MsgFormat PartnerIsSame = Ru("Партнер тот же.").En("Partner is the same.");

        public static MsgFormat YouLeftTheGame = Ru("Вы вышли из игры. Спасибо за участие.")
            .En("You left the game. Thank you for playing.");

        public static MsgFormat PhotoDeclinedPReason =
            Ru("Фото отклонено: {0}").En("Photo declined:{0}");

        public static MsgFormat PhotoDeclinedRequirementFailPRequirement =
            Ru("Фото отклонено. Не выполнено условие: {0}").En("Photo declined. Requirement is not met:{0}");

        public static MsgFormat PhotoAccepted = Ru("Фото принято!").En("Photo accepted!");

        public static MsgFormat DeclineReasonMissing =
            Ru("Не указана причина").En("Please provide reason");

        public static MsgFormat PlayerScorePPlayerPScore = Ru("{0} : {1}").En("{0} : {1}");

        public static MsgFormat PhaseNotSet = Ru("фаза не указана").En("phase not set");
        public static MsgFormat PhaseUpdated = Ru("фаза обновлена").En("phase updated");
        public static MsgFormat PhaseUpdatedToFinal = Ru("финал объявлен").En("final declared");

        public static MsgFormat SpecifyBoyGirl = Ru("Давай конкретнее, yes boy или yes girl?")
            .En("yes boy or yes girl?");

        public static MsgFormat YouAcceptedPhoto = Ru("Вы приняли фото")
            .En("you accepted photo");

        public static MsgFormat YouRejectedPhoto = Ru("Вы отклонили фото")
            .En("you rejected photo");

        static MsgFormats()
        {
            var fields = typeof(MsgFormats).GetFields(BindingFlags.Static | BindingFlags.Public);
            foreach (var field in fields)
            {
                var value = field.GetValue(null);
                ((MsgFormat) value).SetCode(field.Name);
            }
        }

        public static MsgFormat TooLateToPlay = Ru("Прости, но эта игра уже заканчивается...")
            .En("Sorry, but this game is almost finished");


        public static MsgFormat CurrentResults = Ru("Промежуточные итоги:")
            .En("For now the scores are:");

        public static MsgFormat YourTicketReceived = Ru("Спасибо. Номер билета получен.")
            .En("Thanks. Ticket number accepted.");

        public static MsgFormat SentMessageToNUsers = Ru("Сообщение отправлено {0} пользователям.")
            .En("Message sent to {0} users.");

        public static MsgFormat PlayerKicked = Ru("Игрок отстранен.")
            .En("Player kicked.");

        public static MsgFormat HelpFeedback = Ru(
                "<b>Команды в Feedback чате:</b>\n" +
                "/h Показвает этот текст\n" +
                "/id Показвает id текущего чата\n" +
                "/startnetworking - В основном чате!\n" +
                "/messageAll Всем привет!\n" +
                "/messageTag mytag Всем привет у кого mytag!\n" +
                "/contentTag mytag mycontent\n" +
                "/contentAll mycontent\n" +
                "/users Denis - ищет по строке \n" +
                "/users - полный список пользователей\n" +
                "/tag 1,2,4 mytag - добавляет mytag user-ам, номера из /users\n" +
                "/untag 1,2,4 mytag- убирвает mytag, номера из /users\n" +
                "/import- импорт из Google Docs\n " +
                "/check - проверяет состояние бота\n " +
                "<b>Команды в Reply:</b>\n" +
                "Reply +5 добавляет очки игроку\n" +
                "Reply /tag mytag\n" +
                "Reply /untag mytag\n" +
                "Reply /content mycontent\n" +
                "Reply Привет!\n"
            )
            .En("No ready yet");

        public static MsgFormat HelpAdmin = Ru(
                "<b>Команды в Admin чате:</b>\n" +
                "/testTasks примеры заданий\n" +
                "/status - статус игроков\n" +
                "/offline + (фото и подпись) - создает offline игрока\n" +
                "/messagePlayers Всем привет!\n" +
                "/kick @telegramName - отстраняет игрока\n" +
                "/kick 7 - отстраняет игрока по номеру\n" +
                "/softkick @telegramName - отстраняет игрока\n" +
                "/softkick 34 - отстраняет игрока по номеру\n" +
                "<b>startAll</b> - стартует игру для всех\n" +
                "/startTag sometag - стартует для имеющих тег\n" +
                "/phase 4 - меняет фазу\n" +
                "/results - показывает все результаты админу\n" +
                "<b>sendResults</b> - посылает результаты игрокам\n" +
                "<b>force</b> - раздает задания всем кто ждет\n" +
                "<b>final</b> - дает финальное задание игрокам\n" +
                "<b>finish</b> - полностью завершает игру\n" +
                "/getselfies - получить все селфи\n" +
                "/getphotos - получить все фотки" 
            )
            .En("No ready yet");

        public static MsgFormat ForceComplete = Ru("Команда Force выполнена.")
            .En("Force command complete.");

        public static MsgFormat WhatIsYourPosition = Ru(
                "<b>Компания и позиция (2/4)</b>\nГде и кем ты работаешь? Например <em>Директор в ООО 'Креативные Сайты'</em>")
            .En("Where do you work? ");

        public static MsgFormat WhatIsYourInterest = Ru(
                "<b>Что ты ищешь? (4/4)\n</b>Что или кого ты хотел бы найти здесь? " +
                "Чем другие участники могут тебе помочь? Например <em>Ищу партнёров в регионах, ищу маркетолога</em>");

        public static MsgFormat WhatAreYouGreatAt = Ru(
            "<b>Что ты делаешь? (3/4)</b>\nЧем ты и твоя компания занимаетесь? " +
            "Что можете предложить другим участникам? Например \"Делаю сайты, " +
            "консультирую по продвижению в интернете\"" +
            "");
        
        public static MsgFormat SendUsSelfie = Ru("<b>Фото (1/4)</b>\nСделай селфи и пришли его мне," +
                                                    " чтобы другие участники знали как ты выглядишь. " +
                                                    "Можешь загрузить фото, но свежее селфи лучше");

        
        public static MsgFormat NoOtherNentworkingUsersYet =
            Ru(
                    "У нас пока нет других пользователей, которых я бы мог показать, но через некоторое время ты можешь поискать ещё [btn:networking-next;Поискать ещё]")
                .En("There are no other users yet, but you can [btn:networking-next;Search again]");

        public static MsgFormat OtherUserProfile_PPhoto_PName_PPosition_PInterest_PCompetence = Ru(
                "{0}{1}, {2}---\n<em>---Предлагает:</em> {4}\n<em>Ищет:</em> {3}");


        public static MsgFormat NetworkProfileWithButtons_PPhoto_PName_PPosition_PInterest_PGreatAt_PUserNum = Ru(
            "{0}{1}, {2}\n---<em>Предлагает:</em> {4}\n" +
            "<em>Ищет:</em> {3}\n [btn:networking-skip {5};Пропустить|networking-like {5};Связаться]");

        public static MsgFormat Profile_PPhoto_PName_PPosition_PInterest_PGreatAt = Ru(
            "Отлично, твой профиль будет выглядеть так:---{0}{1}, {2}---\n<em>Предлагает:</em> {4}\n<em>Ищет:</em> {3}\n " +
            "---В любой момент ты сможешь изменить его, нажав на эту кнопку: [btn:networkingedit;Изменить профиль]---" +
            "Я буду присылать тебе профили других пользователей, а  ты сможешь пообщать с теми, кто будет тебе интересен" +
            "[btn:networking-next;Начать нетворкинг]");


        public static MsgFormat OtherUserWantsToContactYou_PUser = Ru(
                "{0} хочет пообщаться c тобой:")
            .En("{0} wants to discuss something with you");

        public static MsgFormat ProfileUpdated = Ru(
                "Профиль обновлён.[btn:networkingedit;Изменить еще что-то][btn:networking-next;Начать нетворкинг]")
            .En("Profile updated.[btn:networkingedit;Edit something else][btn:networking-next;Begin networking]");

        public static MsgFormat ProfileNotUpdated = Ru(
                "Профиль не обновлён.[btn:networkingedit;Изменить еще что-то]")
            .En("Profile is not updated.[btn:networkingedit;Edit something else]");

        public static MsgFormat ProfileDisabled = Ru(
                "Нетворкинг отключен.[btn:networkingenable;Включить обратно]")
            .En("Profile is disabled.[btn:networkingenable;Enable]");

        public static MsgFormat ProfileEnabled = Ru(
                "Нетворкинг включен.[btn:networkingedit;Изменить профиль][btn:networking-next;Начать нетворкинг]")
            .En("Profile is enabled.[btn:networkingedit;Edit profile][btn:networking-next;Begin networking]");

        public static MsgFormat WhatYouWantToChange = Ru("Что хочешь поменять?" +
                                                           "[btn:networking-update-photo;Фото]" +
                                                           "[btn:networking-update-position;Компания и позиция]" +
                                                           "[btn:networking-update-interests;Что ты ищешь]" +
                                                           "[btn:networking-update-great-at;Что ты делаешь]" +
                                                           "[btn:networking-disable;Отключить нетворкинг]"
        );

        public static MsgFormat NetworkingBegin =
            Ru(
                "Я познакомю тебя с другими участниками, которые могу быть полезны тебе. Для начала, ответь на несколько вопросов:");

        public static MsgFormat ClickHisNameToChat = Ru(
            "Нажми на его имя, чтобы открыть чат");

        private static MsgFormat Ru(string text)
        {
            return new MsgFormat("NoCode", text);
        }
    }
}