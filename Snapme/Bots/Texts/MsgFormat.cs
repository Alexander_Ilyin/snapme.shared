using System.Collections.Generic;
using Snapme.Data.Bots;

namespace Snapme.Bots.Texts
{
    public class MsgFormat
    {
        private readonly Dictionary<string, string> formatValues = new Dictionary<string, string>();
        private string code;

        public MsgFormat(string code, string russianFormat)
        {
            this.code = code;
            formatValues[Languages.Russian] = russianFormat;
        }

        public bool IsHtml { get; private set; }

        public MsgFormat En(string f)
        {
            formatValues[Languages.English] = f;
            return this;
        }

        public string GetFormat(string lang)
        {
            return formatValues[lang ?? Languages.Russian] ?? formatValues[Languages.Russian] ?? "...";
        }

        public override string ToString()
        {
            return $"{nameof(code)}: {code} " + GetFormat(Languages.Russian);
        }

        public MsgFormat AsHtml()
        {
            IsHtml = true;
            return this;
        }

        public string GetCode()
        {
            return code;
        }

        public MsgFormat Patched(string betterFormat)
        {
            return new MsgFormat(code, betterFormat);
        }

        public void SetCode(string code)
        {
            this.code = code;
        }
    }
}