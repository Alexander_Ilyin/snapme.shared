using System.Collections.Generic;
using System.Linq;
using Snapme.Common;
using Snapme.Data.Info;

namespace Snapme.Bots.Texts
{
    public class BotContentData
    {
        private readonly Dictionary<string, List<InfoContentItem>> dic;

        public BotContentData(List<InfoContentItem> items)
        {
            var newDic = new Dictionary<string, List<InfoContentItem>>();
            if (items != null)
            {
                foreach (var item in items)
                {
                    var list = newDic.GetOrAdd(item.Type.ToLowerInvariant().Trim('/'), () => new List<InfoContentItem>());
                    list.Add(item);
                }
            }
            dic = newDic;
        }

        public string GetValue(string key)
        {
            if (key.IsEmptyOrNull())
                return null;
            var contentItems = dic.GetItemOrDefault(key.ToLowerInvariant().Trim('/'));
            if (contentItems==null || contentItems.Count == 0)
                return null;
            return contentItems[RandomHelper.Next(contentItems.Count - 1)].Content;
        }
    }
}