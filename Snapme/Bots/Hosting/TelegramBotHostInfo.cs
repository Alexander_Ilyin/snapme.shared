using System.Collections.Generic;
using System.Linq;
using Snapme.Data.Dto;

namespace Snapme.Bots.Hosting
{
    public class TelegramBotHostInfo
    {
        public bool IsRunning { get; set; }
        public List<TelegramBotEvent> LastEvents { get; set; } = new List<TelegramBotEvent>();
        public string LastError { get; set; }

        public BotInfoDto ToDto()
        {
            return new BotInfoDto
            {
                LastError = LastError,
                LastEvents = LastEvents.Select(x => x.ToDto()).ToList(),
                IsRunning = IsRunning
            };
        }
    }
}