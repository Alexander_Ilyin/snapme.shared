using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using Snapme.Bots.Background;
using Snapme.Bots.Processors;
using Snapme.Bots.Services;
using Snapme.Bots.Telegram;
using Snapme.Common;
using Snapme.Common.DI;
using Snapme.Trinity;

namespace Snapme.Bots.Hosting
{
    public class TelegramBotHost : ISingleton
    {
        private readonly ConcurrentDictionary<Guid, TelegramBotHostItem> bots =
            new ConcurrentDictionary<Guid, TelegramBotHostItem>();

        private readonly SnapmeContainer container;
        private readonly TimerFactory timerFactory;
        private readonly ITelegramMessengerFactory messengerFactory;

        public TelegramBotHost(SnapmeContainer container)
        {
            this.container = container;
            messengerFactory = container.GetSingleton<ITelegramMessengerFactory>();
            timerFactory = container.GetSingleton<TimerFactory>();
        }

        public async Task StopBot(Guid botId)
        {
            bool stopped = false;
            lock (this)
            {
                var botHostItem = bots.GetItemOrDefault(botId);
                if (botHostItem == null)
                    return;
                try
                {
                    botHostItem.CancelToken.Cancel();
                    botHostItem.TelegramMessenger?.Stop();
                    botHostItem.TelegramMessenger = null;
                    stopped = true;
                }
                catch (Exception e)
                {
                    botHostItem.Notify(new TelegramBotEvent(e));
                }
            }
            if (stopped)
                await container.Run(async scope =>
                    await scope.Resolve<BotService>().StopBot(botId)
                );
        }

        public async Task StartBot(Guid botId)
        {
            await container.Run(async scope =>
                await scope.Resolve<BotService>().StartBot(botId)
            );
            var botKey = await container.Get(scope => scope.Resolve<BotService>().GetBotKey(botId));
            lock (this)
            {
                var botHostItem = bots.GetItemOrDefault(botId);
                if (botHostItem == null)
                    bots[botId] = botHostItem = new TelegramBotHostItem();
                if (botHostItem.TelegramMessenger != null)
                    return;
                var telegramMessenger = messengerFactory.CreateMessenger();
                var lastActivityDate = DateHelper.UtcNow();
                try
                {
                    botHostItem.CancelToken = new CancellationTokenSource();
                    telegramMessenger.Update += e => { botHostItem.Notify(e); };
                    telegramMessenger.MessageReceived += e =>
                    {
                        lastActivityDate = DateHelper.UtcNow();
                        container.RunWithMessenger(telegramMessenger, botId,
                            async scope => await scope.Resolve<TelegramMessageProcessor>().ProcessMessage(botId, e)).DoNotWait();
                    };
                    telegramMessenger.Start(botKey);
                    botHostItem.TelegramMessenger = telegramMessenger;

                    telegramMessenger.Start(botKey);

                    void RunTimerForActiveBot(Func<ILifetimeScope, Task> action)
                    {
                        timerFactory.StartTimer(botHostItem.CancelToken.Token, TimeSpan.FromSeconds(30),
                            () =>
                            {
                                if (DateHelper.UtcNow() - lastActivityDate < TimeSpan.FromMinutes(30))
                                    container.RunWithMessenger(telegramMessenger, botId, action).DoNotWait();
                            }).DoNotWait();
                    }

                    RunTimerForActiveBot(async scope => await scope.Resolve<TrinityImporter>().ImportWords(botId, false));
                    RunTimerForActiveBot(async scope => await scope.Resolve<ContentImporter>().ImportContent(botId, false));
                    RunTimerForActiveBot(async scope =>
                    {
                        var currentGame = scope.Resolve<GameService>().GetCurrentGameForBot(botId);
                        if (currentGame != null)
                            await scope.Resolve<GameMissionAssigner>().AssignMissionsAndNotifyPlayers(currentGame.Id);
                    });
                    RunTimerForActiveBot(async scope =>
                    {
                        var currentGame = scope.Resolve<GameService>().GetCurrentGameForBot(botId);
                        if (currentGame != null)
                            await scope.Resolve<GameTaskImporter>().ImportTasks(currentGame.Id, false);
                    });

                    botHostItem.TelegramMessenger = telegramMessenger;
                }
                catch (Exception e)
                {
                    telegramMessenger?.Stop();
                    this.Log().Error("Error stating bot for " + botId, e);
                }
            }
        }

        public TelegramBotHostInfo GetBotInfo(Guid botId)
        {
            lock (this)
            {
                if (bots.ContainsKey(botId))
                    return bots[botId].GetInfo();
                return new TelegramBotHostInfo
                {
                    IsRunning = false,
                    LastError = null
                };
            }
        }

        public async Task StartAll()
        {
            try
            {
                Guid[] botIds = await container.Get(scope => scope.Resolve<BotService>().GetBotIdsToStart());
                foreach (var botId in botIds)
                    await StartBot(botId);
            }
            catch (Exception ex)
            {
                this.Log().Error("Err", ex);
            }
        }

        public ITelegramMessenger GetMessenger(Guid botId)
        {
            lock (this)
            {
                if (bots.ContainsKey(botId))
                    return bots[botId].TelegramMessenger;
            }
            return null;
        }
    }
}