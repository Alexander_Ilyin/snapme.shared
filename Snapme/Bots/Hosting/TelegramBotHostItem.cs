using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Snapme.Bots.Telegram;

namespace Snapme.Bots.Hosting
{
    public class TelegramBotHostItem
    {
        private readonly Queue<TelegramBotEvent> latestEvents = new Queue<TelegramBotEvent>();
        public ITelegramMessenger TelegramMessenger { get; set; }
        public string LastError { get; private set; }
        public CancellationTokenSource CancelToken { get; set; }

        public List<TelegramBotEvent> GetLatestEvents()
        {
            lock (latestEvents)
            {
                var list = latestEvents.ToList();
                list.Reverse();
                return list;
            }
        }

        public TelegramBotHostInfo GetInfo()
        {
            return new TelegramBotHostInfo
            {
                IsRunning = TelegramMessenger != null,
                LastError = LastError,
                LastEvents = GetLatestEvents()
            };
        }

        public void Notify(TelegramBotEvent botEvent)
        {
            lock (latestEvents)
            {
                if (latestEvents.Count > 100)
                    latestEvents.Dequeue();
                if (botEvent.Error != null)
                    LastError = botEvent.Error.ToString();
                latestEvents.Enqueue(botEvent);
            }
        }
    }
}