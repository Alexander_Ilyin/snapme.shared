using System;
using Snapme.Common;
using Snapme.Data.Dto;

namespace Snapme.Bots.Hosting
{
    public class TelegramBotEvent
    {
        public TelegramBotEvent()
        {
        }

        public TelegramBotEvent(Exception exception)
        {
            Error = exception;
            Description = exception.ToString();
        }

        public DateTime EventDate { get; set; } = DateHelper.UtcNow();
        public Exception Error { get; set; }
        public string Description { get; set; }

        public long? JoinedGroupId { get; set; }
        public string JoinedGroupTitle { get; set; }
        public long? FailedChatId { get; set; }

        public TelegramBotEventDto ToDto()
        {
            return new TelegramBotEventDto
            {
                Error = Error?.ToString(),
                Description = Description,
                EventDate = EventDate,
                JoinedGroupId = JoinedGroupId?.ToString(),
                JoinedGroupTitle = JoinedGroupTitle
            };
        }

        public bool IsUserBlockedError()
        {
            if (Error?.Message?.Contains("blocked", StringComparison.OrdinalIgnoreCase) == true)
                return true;
            return false;
        }
    }
}