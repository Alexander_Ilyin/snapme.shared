using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Snapme.Bots.Messages;
using Snapme.Bots.Telegram;
using Snapme.Bots.Texts;
using Snapme.Common;
using Snapme.Data.Bots;

namespace Snapme.Bots.Hosting
{
    public class WrappedTelegramMessenger : ITelegramMessenger
    {
        private readonly ITelegramMessenger inner;
        private readonly BotContentService botContentService;
        private readonly Guid? botId;

        public WrappedTelegramMessenger(ITelegramMessenger inner, BotContentService botContentService, Guid? botId)
        {
            this.inner = inner;
            this.botContentService = botContentService;
            this.botId = botId;
        }

        public event Action<TelegramMessageEvent> MessageReceived
        {
            add => inner.MessageReceived += value;
            remove => inner.MessageReceived -= value;
        }

        public event Action<TelegramBotEvent> Update
        {
            add => inner.Update += value;
            remove => inner.Update -= value;
        }

        public IEnumerable<long> FindForwardFromUserIds(MessangerMsgInfo msgInfo)
        {
            return inner.FindForwardFromUserIds(msgInfo);
        }

        public async Task SendMessage(long chatId, OutgoingMessage msg, string language = Languages.Russian)
        {
            try
            {
                if (!this.botId.HasValue)
                    await inner.SendMessage(chatId, msg, language);
                else
                {
                    if (msg.TextFormat != null && !msg.TextFormat.GetCode().IsEmptyOrNull())
                    {
                        var contentData = await botContentService.GetContent(this.botId.Value);
                        var customFormat = contentData.GetValue(msg.TextFormat.GetCode());
                        if (!customFormat.IsEmptyOrNull())
                            msg.TextFormat = msg.TextFormat.Patched(customFormat);
                    }
                    await inner.SendMessage(chatId, msg, language);
                }
            }
            catch (Exception ex)
            {
                this.Log().Error(ex);
            }
        }

        public void Start(string botKey)
        {
            inner.Start(botKey);
        }

        public void Stop()
        {
            inner.Stop();
        }
    }
}