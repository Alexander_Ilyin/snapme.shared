using System;
using System.Threading.Tasks;
using Autofac;
using Autofac.Core.Lifetime;
using Snapme.Bots.Telegram;
using Snapme.Bots.Texts;
using Snapme.Common;
using Snapme.Data;

namespace Snapme.Bots.Hosting
{
    public class SnapmeContainer
    {
        private readonly IContainer container;
        private readonly AsyncLock asyncLock = new AsyncLock();

        public SnapmeContainer(IContainer container)
        {
            this.container = container;
        }

        private Task<T> GetImpl<T>(Func<ILifetimeScope, Task<T>> getData, ITelegramMessenger telegramMessenger = null, Guid? botId=null)
        {
            return asyncLock.Run(async () =>
            {
                var scope = container.BeginLifetimeScope(MatchingScopeLifetimeTags.RequestLifetimeScopeTag, cfg =>
                {
                    cfg.Register(c => new WrappedTelegramMessenger(telegramMessenger, c.Resolve<BotContentService>(), botId))
                        .As<ITelegramMessenger>();
                });
                T data;
                try
                {
                    data = await getData(scope);
                }
                catch (Exception e)
                {
                    this.Log().Error(e);
                    throw;
                }
                finally
                {
                    scope.Dispose();
                }
                return data;
            });
        }

        public Task<T> Get<T>(Func<ILifetimeScope, Task<T>> getData)
        {
            return GetImpl(getData);
        }

        public async Task RunWithMessenger(ITelegramMessenger telegramMessenger, Guid botId, Func<ILifetimeScope, Task> transaction)
        {
            await GetImpl(async s =>
            {
//                var hashCode = s.Resolve<SnapmeContext>().GetHashCode();
//                this.Log().Info("SnapmeContextID:"+hashCode);
                await transaction(s);
                return 0;
            }, telegramMessenger, botId);
        }

        public async Task Run(Func<ILifetimeScope, Task> transaction)
        {
            await GetImpl(async s =>
            {
                await transaction(s);
                return 0;
            });
        }

        public T GetSingleton<T>()
        {
            return container.Resolve<T>();
        }
    }
}