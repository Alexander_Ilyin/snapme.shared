using Snapme.Bots.Telegram;
using Snapme.Common.DI;

namespace Snapme.Bots.Hosting
{
    public class TelegramMessengerFactory : ITelegramMessengerFactory, ISingleton
    {
        public ITelegramMessenger CreateMessenger()
        {
            return new TelegramMessenger();
        }
    }
}