using Snapme.Bots.Telegram;

namespace Snapme.Bots.Hosting
{
    public interface  ITelegramMessengerFactory
    {
        ITelegramMessenger CreateMessenger();
    }
}