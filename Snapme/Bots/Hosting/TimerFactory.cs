using System;
using System.Threading;
using System.Threading.Tasks;
using Snapme.Common;
using Snapme.Common.DI;

namespace Snapme.Bots.Hosting
{
    public class TimerFactory : ISingleton
    {
        public async Task StartTimer(CancellationToken token, TimeSpan interval, Action action)
        {
            await Task.Delay(1000, token);
            while (!token.IsCancellationRequested)
            {
                try
                {
                    action();
                }
                catch (Exception e)
                {
                    this.Log().Error(e);
                }
                await Task.Delay(interval, token);
            }
        }
    }
}