﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Snapme.Migrations
{
    public partial class NetworkingDates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "SystemUsers",
                keyColumn: "Id",
                keyValue: new Guid("17bfed56-a04e-478f-a990-f673dac9b812"));

            migrationBuilder.DeleteData(
                table: "SystemUsers",
                keyColumn: "Id",
                keyValue: new Guid("68acac0f-12e6-41c2-b0a0-5ba50098e33d"));

            migrationBuilder.AddColumn<DateTime>(
                name: "Created",
                table: "NetworkingProfiles",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.InsertData(
                table: "SystemUsers",
                columns: new[] { "Id", "Email", "Password" },
                values: new object[,]
                {
                    { new Guid("31d8578d-40c3-46b6-8c39-2c8ee61b3a52"), "kofman", "sn0pme987" },
                    { new Guid("b0f1a5c3-a75f-4c4b-9267-c11748ac1b8c"), "ilyin", "sn0pme987" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "SystemUsers",
                keyColumn: "Id",
                keyValue: new Guid("31d8578d-40c3-46b6-8c39-2c8ee61b3a52"));

            migrationBuilder.DeleteData(
                table: "SystemUsers",
                keyColumn: "Id",
                keyValue: new Guid("b0f1a5c3-a75f-4c4b-9267-c11748ac1b8c"));

            migrationBuilder.DropColumn(
                name: "Created",
                table: "NetworkingProfiles");

            migrationBuilder.InsertData(
                table: "SystemUsers",
                columns: new[] { "Id", "Email", "Password" },
                values: new object[,]
                {
                    { new Guid("17bfed56-a04e-478f-a990-f673dac9b812"), "kofman", "sn0pme987" },
                    { new Guid("68acac0f-12e6-41c2-b0a0-5ba50098e33d"), "ilyin", "sn0pme987" }
                });
        }
    }
}
