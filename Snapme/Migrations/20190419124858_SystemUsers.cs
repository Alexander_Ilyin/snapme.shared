﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Snapme.Migrations
{
    public partial class SystemUsers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SystemUsers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SystemUsers", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "SystemUsers",
                columns: new[] { "Id", "Email", "Password" },
                values: new object[,]
                {
                    { new Guid("d3e30312-7df2-4550-b0d3-3380d4c48135"), "kofman", "sn0pme987" },
                    { new Guid("8689f0ae-d06d-4a18-a7df-c5bc09b53f3a"), "ilyin", "sn0pme987" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SystemUsers");
        }
    }
}
