﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Snapme.Migrations
{
    public partial class Networking : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "SystemUsers",
                keyColumn: "Id",
                keyValue: new Guid("cbd4f308-79d1-4510-abfb-97c8ddada849"));

            migrationBuilder.DeleteData(
                table: "SystemUsers",
                keyColumn: "Id",
                keyValue: new Guid("d3631b1b-e2f2-49f4-8ed3-9882df822c1d"));

            migrationBuilder.AddColumn<string>(
                name: "CurrentDialogState",
                table: "TgmBotUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CurrentDialogType",
                table: "TgmBotUsers",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "NetworkingSessions",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    TgmBotId = table.Column<Guid>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NetworkingSessions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_NetworkingSessions_TgmBots_TgmBotId",
                        column: x => x.TgmBotId,
                        principalTable: "TgmBots",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "NetworkingProfiles",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    NetworkingSessionId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    PhotoId = table.Column<string>(nullable: true),
                    Interest = table.Column<string>(nullable: true),
                    SerializedLiked = table.Column<string>(nullable: true),
                    SerializedDisliked = table.Column<string>(nullable: true),
                    SerializedNotified = table.Column<string>(nullable: true),
                    Position = table.Column<string>(nullable: true),
                    GreatAt = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NetworkingProfiles", x => new { x.NetworkingSessionId, x.UserId });
                    table.ForeignKey(
                        name: "FK_NetworkingProfiles_NetworkingSessions_NetworkingSessionId",
                        column: x => x.NetworkingSessionId,
                        principalTable: "NetworkingSessions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_NetworkingProfiles_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "SystemUsers",
                columns: new[] { "Id", "Email", "Password" },
                values: new object[,]
                {
                    { new Guid("17bfed56-a04e-478f-a990-f673dac9b812"), "kofman", "sn0pme987" },
                    { new Guid("68acac0f-12e6-41c2-b0a0-5ba50098e33d"), "ilyin", "sn0pme987" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_NetworkingProfiles_UserId",
                table: "NetworkingProfiles",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_NetworkingSessions_TgmBotId",
                table: "NetworkingSessions",
                column: "TgmBotId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "NetworkingProfiles");

            migrationBuilder.DropTable(
                name: "NetworkingSessions");

            migrationBuilder.DeleteData(
                table: "SystemUsers",
                keyColumn: "Id",
                keyValue: new Guid("17bfed56-a04e-478f-a990-f673dac9b812"));

            migrationBuilder.DeleteData(
                table: "SystemUsers",
                keyColumn: "Id",
                keyValue: new Guid("68acac0f-12e6-41c2-b0a0-5ba50098e33d"));

            migrationBuilder.DropColumn(
                name: "CurrentDialogState",
                table: "TgmBotUsers");

            migrationBuilder.DropColumn(
                name: "CurrentDialogType",
                table: "TgmBotUsers");

            migrationBuilder.InsertData(
                table: "SystemUsers",
                columns: new[] { "Id", "Email", "Password" },
                values: new object[,]
                {
                    { new Guid("cbd4f308-79d1-4510-abfb-97c8ddada849"), "kofman", "sn0pme987" },
                    { new Guid("d3631b1b-e2f2-49f4-8ed3-9882df822c1d"), "ilyin", "sn0pme987" }
                });
        }
    }
}
