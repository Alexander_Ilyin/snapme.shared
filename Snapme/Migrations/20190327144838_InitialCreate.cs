﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Snapme.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                "PlayerTasks",
                table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    GameId = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: true),
                    Finished = table.Column<DateTime>(nullable: true),
                    TaskType = table.Column<int>(nullable: false),
                    GameTaskStatus = table.Column<int>(nullable: false),
                    TaskPhotoId = table.Column<string>(nullable: true),
                    ModeratorRejectReason = table.Column<string>(nullable: true),
                    ModeratorPriority = table.Column<double>(nullable: false),
                    WaitingForValidationChatId = table.Column<long>(nullable: true)
                },
                constraints: table => { table.PrimaryKey("PK_PlayerTasks", x => x.Id); });

            migrationBuilder.CreateTable(
                "TgmBots",
                table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    BotTgmKey = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    IsDelete = table.Column<bool>(nullable: false),
                    FeedbackChatId = table.Column<long>(nullable: true),
                    SerializedMeta = table.Column<string>(nullable: true)
                },
                constraints: table => { table.PrimaryKey("PK_TgmBots", x => x.Id); });

            migrationBuilder.CreateTable(
                "Users",
                table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Gender = table.Column<int>(nullable: true),
                    TgmUserName = table.Column<string>(nullable: true),
                    TgmUserId = table.Column<int>(nullable: false),
                    IsAdmin = table.Column<bool>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Language = table.Column<string>(nullable: true)
                },
                constraints: table => { table.PrimaryKey("PK_Users", x => x.Id); });

            migrationBuilder.CreateTable(
                "BotContents",
                table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    TgmBotId = table.Column<Guid>(nullable: true),
                    SerializedItems = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BotContents", x => x.Id);
                    table.ForeignKey(
                        "FK_BotContents_TgmBots_TgmBotId",
                        x => x.TgmBotId,
                        "TgmBots",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                "Games",
                table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    FinishTime = table.Column<DateTime>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    GameStatus = table.Column<int>(nullable: false),
                    Phase = table.Column<int>(nullable: false),
                    TgmBotId = table.Column<Guid>(nullable: true),
                    MustBeProcessedByBot = table.Column<bool>(nullable: false),
                    SerializedSettings = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Games", x => x.Id);
                    table.ForeignKey(
                        "FK_Games_TgmBots_TgmBotId",
                        x => x.TgmBotId,
                        "TgmBots",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                "GameTaskAssignee",
                table => new
                {
                    GameTaskId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameTaskAssignee", x => new {x.GameTaskId, x.UserId});
                    table.ForeignKey(
                        "FK_GameTaskAssignee_PlayerTasks_GameTaskId",
                        x => x.GameTaskId,
                        "PlayerTasks",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        "FK_GameTaskAssignee_Users_UserId",
                        x => x.UserId,
                        "Users",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                "TgmBotUsers",
                table => new
                {
                    BotId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    TgmChatId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TgmBotUsers", x => new {x.BotId, x.UserId});
                    table.ForeignKey(
                        "FK_TgmBotUsers_TgmBots_BotId",
                        x => x.BotId,
                        "TgmBots",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        "FK_TgmBotUsers_Users_UserId",
                        x => x.UserId,
                        "Users",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                "GameTasks",
                table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    GameId = table.Column<Guid>(nullable: false),
                    DescriptionRu = table.Column<string>(nullable: true),
                    DescriptionEn = table.Column<string>(nullable: true),
                    ActivityType = table.Column<int>(nullable: false),
                    Phase = table.Column<int>(nullable: true),
                    Level = table.Column<int>(nullable: false),
                    IsDelete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameTasks", x => x.Id);
                    table.ForeignKey(
                        "FK_GameTasks_Games_GameId",
                        x => x.GameId,
                        "Games",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                "Players",
                table => new
                {
                    GameId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    SelfiePhotoId = table.Column<string>(nullable: true),
                    CurrentTaskId = table.Column<Guid>(nullable: true),
                    LastUpdate = table.Column<DateTime>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    IsWelcomeReceived = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Players", x => new {x.GameId, x.UserId});
                    table.ForeignKey(
                        "FK_Players_PlayerTasks_CurrentTaskId",
                        x => x.CurrentTaskId,
                        "PlayerTasks",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        "FK_Players_Games_GameId",
                        x => x.GameId,
                        "Games",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        "FK_Players_Users_UserId",
                        x => x.UserId,
                        "Users",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                "GameTaskActivity",
                table => new
                {
                    GameTaskId = table.Column<Guid>(nullable: false),
                    ActivityId = table.Column<Guid>(nullable: false),
                    Number = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameTaskActivity", x => new {x.GameTaskId, x.ActivityId});
                    table.ForeignKey(
                        "FK_GameTaskActivity_GameTasks_ActivityId",
                        x => x.ActivityId,
                        "GameTasks",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        "FK_GameTaskActivity_PlayerTasks_GameTaskId",
                        x => x.GameTaskId,
                        "PlayerTasks",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                "IX_BotContents_TgmBotId",
                "BotContents",
                "TgmBotId");

            migrationBuilder.CreateIndex(
                "IX_Games_TgmBotId",
                "Games",
                "TgmBotId");

            migrationBuilder.CreateIndex(
                "IX_GameTaskActivity_ActivityId",
                "GameTaskActivity",
                "ActivityId");

            migrationBuilder.CreateIndex(
                "IX_GameTaskAssignee_UserId",
                "GameTaskAssignee",
                "UserId");

            migrationBuilder.CreateIndex(
                "IX_GameTasks_GameId",
                "GameTasks",
                "GameId");

            migrationBuilder.CreateIndex(
                "IX_Players_CurrentTaskId",
                "Players",
                "CurrentTaskId");

            migrationBuilder.CreateIndex(
                "IX_Players_UserId",
                "Players",
                "UserId");

            migrationBuilder.CreateIndex(
                "IX_TgmBotUsers_UserId",
                "TgmBotUsers",
                "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                "BotContents");

            migrationBuilder.DropTable(
                "GameTaskActivity");

            migrationBuilder.DropTable(
                "GameTaskAssignee");

            migrationBuilder.DropTable(
                "Players");

            migrationBuilder.DropTable(
                "TgmBotUsers");

            migrationBuilder.DropTable(
                "GameTasks");

            migrationBuilder.DropTable(
                "PlayerTasks");

            migrationBuilder.DropTable(
                "Users");

            migrationBuilder.DropTable(
                "Games");

            migrationBuilder.DropTable(
                "TgmBots");
        }
    }
}