﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Snapme.Migrations
{
    public partial class TrinityGame : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TrinityGames",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    TgmBotId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TrinityGames", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TrinityGames_TgmBots_TgmBotId",
                        column: x => x.TgmBotId,
                        principalTable: "TgmBots",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TrinityPlayers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    GameId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    CanRecordVn = table.Column<bool>(nullable: false),
                    CurrentStep = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TrinityPlayers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TrinityPlayers_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TrinityWords",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    GameId = table.Column<Guid>(nullable: false),
                    Word = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TrinityWords", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TrinityExplanations",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    GameId = table.Column<Guid>(nullable: false),
                    PlayerId = table.Column<Guid>(nullable: false),
                    Recorded = table.Column<DateTime>(nullable: false),
                    WordId = table.Column<Guid>(nullable: false),
                    VnId = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TrinityExplanations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TrinityExplanations_TrinityPlayers_PlayerId",
                        column: x => x.PlayerId,
                        principalTable: "TrinityPlayers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TrinityExplanations_TrinityWords_WordId",
                        column: x => x.WordId,
                        principalTable: "TrinityWords",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TrinityTasks",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    GameId = table.Column<Guid>(nullable: false),
                    PlayerId = table.Column<Guid>(nullable: false),
                    ExplanationId = table.Column<Guid>(nullable: false),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TrinityTasks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TrinityTasks_TrinityExplanations_ExplanationId",
                        column: x => x.ExplanationId,
                        principalTable: "TrinityExplanations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TrinityTasks_TrinityPlayers_PlayerId",
                        column: x => x.PlayerId,
                        principalTable: "TrinityPlayers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TrinityExplanations_PlayerId",
                table: "TrinityExplanations",
                column: "PlayerId");

            migrationBuilder.CreateIndex(
                name: "IX_TrinityExplanations_WordId",
                table: "TrinityExplanations",
                column: "WordId");

            migrationBuilder.CreateIndex(
                name: "IX_TrinityGames_TgmBotId",
                table: "TrinityGames",
                column: "TgmBotId");

            migrationBuilder.CreateIndex(
                name: "IX_TrinityPlayers_UserId",
                table: "TrinityPlayers",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_TrinityTasks_ExplanationId",
                table: "TrinityTasks",
                column: "ExplanationId");

            migrationBuilder.CreateIndex(
                name: "IX_TrinityTasks_PlayerId",
                table: "TrinityTasks",
                column: "PlayerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TrinityGames");

            migrationBuilder.DropTable(
                name: "TrinityTasks");

            migrationBuilder.DropTable(
                name: "TrinityExplanations");

            migrationBuilder.DropTable(
                name: "TrinityPlayers");

            migrationBuilder.DropTable(
                name: "TrinityWords");
        }
    }
}
