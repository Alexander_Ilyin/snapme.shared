﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Snapme.Migrations
{
    public partial class ValidatorIds : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "WaitingForValidationChatId",
                table: "PlayerTasks");

            migrationBuilder.AlterColumn<long>(
                name: "TgmUserId",
                table: "Users",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<string>(
                name: "WaitingForValidatorId",
                table: "PlayerTasks",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "WaitingForValidatorId",
                table: "PlayerTasks");

            migrationBuilder.AlterColumn<int>(
                name: "TgmUserId",
                table: "Users",
                nullable: false,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<long>(
                name: "WaitingForValidationChatId",
                table: "PlayerTasks",
                nullable: true);
        }
    }
}
