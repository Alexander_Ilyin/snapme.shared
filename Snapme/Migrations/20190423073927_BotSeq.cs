﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Snapme.Migrations
{
    public partial class BotSeq : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GameTaskActivity_GameTasks_ActivityId",
                table: "GameTaskActivity");

            migrationBuilder.DropForeignKey(
                name: "FK_GameTaskActivity_PlayerTasks_GameTaskId",
                table: "GameTaskActivity");

            migrationBuilder.DropForeignKey(
                name: "FK_GameTaskAssignee_PlayerTasks_GameTaskId",
                table: "GameTaskAssignee");

            migrationBuilder.DropForeignKey(
                name: "FK_GameTaskAssignee_Users_UserId",
                table: "GameTaskAssignee");

            migrationBuilder.DropPrimaryKey(
                name: "PK_GameTaskAssignee",
                table: "GameTaskAssignee");

            migrationBuilder.DropPrimaryKey(
                name: "PK_GameTaskActivity",
                table: "GameTaskActivity");

            migrationBuilder.DeleteData(
                table: "SystemUsers",
                keyColumn: "Id",
                keyValue: new Guid("8689f0ae-d06d-4a18-a7df-c5bc09b53f3a"));

            migrationBuilder.DeleteData(
                table: "SystemUsers",
                keyColumn: "Id",
                keyValue: new Guid("d3e30312-7df2-4550-b0d3-3380d4c48135"));

            migrationBuilder.RenameTable(
                name: "GameTaskAssignee",
                newName: "GameTaskAssignees");

            migrationBuilder.RenameTable(
                name: "GameTaskActivity",
                newName: "GameTaskActivities");

            migrationBuilder.RenameIndex(
                name: "IX_GameTaskAssignee_UserId",
                table: "GameTaskAssignees",
                newName: "IX_GameTaskAssignees_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_GameTaskActivity_ActivityId",
                table: "GameTaskActivities",
                newName: "IX_GameTaskActivities_ActivityId");

            migrationBuilder.AddColumn<int>(
                name: "UserNumber",
                table: "TgmBotUsers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserSequenceNum",
                table: "TgmBots",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_GameTaskAssignees",
                table: "GameTaskAssignees",
                columns: new[] { "GameTaskId", "UserId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_GameTaskActivities",
                table: "GameTaskActivities",
                columns: new[] { "GameTaskId", "ActivityId" });

            migrationBuilder.InsertData(
                table: "SystemUsers",
                columns: new[] { "Id", "Email", "Password" },
                values: new object[,]
                {
                    { new Guid("26e249d7-3dac-4c8e-8e4c-85da5a71ce66"), "kofman", "sn0pme987" },
                    { new Guid("a9fb2cde-0fc4-447b-8561-b03f1b1a72ec"), "ilyin", "sn0pme987" }
                });

            migrationBuilder.AddForeignKey(
                name: "FK_GameTaskActivities_GameTasks_ActivityId",
                table: "GameTaskActivities",
                column: "ActivityId",
                principalTable: "GameTasks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GameTaskActivities_PlayerTasks_GameTaskId",
                table: "GameTaskActivities",
                column: "GameTaskId",
                principalTable: "PlayerTasks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GameTaskAssignees_PlayerTasks_GameTaskId",
                table: "GameTaskAssignees",
                column: "GameTaskId",
                principalTable: "PlayerTasks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GameTaskAssignees_Users_UserId",
                table: "GameTaskAssignees",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GameTaskActivities_GameTasks_ActivityId",
                table: "GameTaskActivities");

            migrationBuilder.DropForeignKey(
                name: "FK_GameTaskActivities_PlayerTasks_GameTaskId",
                table: "GameTaskActivities");

            migrationBuilder.DropForeignKey(
                name: "FK_GameTaskAssignees_PlayerTasks_GameTaskId",
                table: "GameTaskAssignees");

            migrationBuilder.DropForeignKey(
                name: "FK_GameTaskAssignees_Users_UserId",
                table: "GameTaskAssignees");

            migrationBuilder.DropPrimaryKey(
                name: "PK_GameTaskAssignees",
                table: "GameTaskAssignees");

            migrationBuilder.DropPrimaryKey(
                name: "PK_GameTaskActivities",
                table: "GameTaskActivities");

            migrationBuilder.DeleteData(
                table: "SystemUsers",
                keyColumn: "Id",
                keyValue: new Guid("26e249d7-3dac-4c8e-8e4c-85da5a71ce66"));

            migrationBuilder.DeleteData(
                table: "SystemUsers",
                keyColumn: "Id",
                keyValue: new Guid("a9fb2cde-0fc4-447b-8561-b03f1b1a72ec"));

            migrationBuilder.DropColumn(
                name: "UserNumber",
                table: "TgmBotUsers");

            migrationBuilder.DropColumn(
                name: "UserSequenceNum",
                table: "TgmBots");

            migrationBuilder.RenameTable(
                name: "GameTaskAssignees",
                newName: "GameTaskAssignee");

            migrationBuilder.RenameTable(
                name: "GameTaskActivities",
                newName: "GameTaskActivity");

            migrationBuilder.RenameIndex(
                name: "IX_GameTaskAssignees_UserId",
                table: "GameTaskAssignee",
                newName: "IX_GameTaskAssignee_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_GameTaskActivities_ActivityId",
                table: "GameTaskActivity",
                newName: "IX_GameTaskActivity_ActivityId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_GameTaskAssignee",
                table: "GameTaskAssignee",
                columns: new[] { "GameTaskId", "UserId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_GameTaskActivity",
                table: "GameTaskActivity",
                columns: new[] { "GameTaskId", "ActivityId" });

            migrationBuilder.InsertData(
                table: "SystemUsers",
                columns: new[] { "Id", "Email", "Password" },
                values: new object[,]
                {
                    { new Guid("d3e30312-7df2-4550-b0d3-3380d4c48135"), "kofman", "sn0pme987" },
                    { new Guid("8689f0ae-d06d-4a18-a7df-c5bc09b53f3a"), "ilyin", "sn0pme987" }
                });

            migrationBuilder.AddForeignKey(
                name: "FK_GameTaskActivity_GameTasks_ActivityId",
                table: "GameTaskActivity",
                column: "ActivityId",
                principalTable: "GameTasks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GameTaskActivity_PlayerTasks_GameTaskId",
                table: "GameTaskActivity",
                column: "GameTaskId",
                principalTable: "PlayerTasks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GameTaskAssignee_PlayerTasks_GameTaskId",
                table: "GameTaskAssignee",
                column: "GameTaskId",
                principalTable: "PlayerTasks",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GameTaskAssignee_Users_UserId",
                table: "GameTaskAssignee",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
