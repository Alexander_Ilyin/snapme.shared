﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Snapme.Migrations
{
    public partial class BanInfo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "UserBannedBot",
                table: "TgmBotUsers",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "UserBannedBotDetails",
                table: "TgmBotUsers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserBannedBot",
                table: "TgmBotUsers");

            migrationBuilder.DropColumn(
                name: "UserBannedBotDetails",
                table: "TgmBotUsers");
        }
    }
}
