﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Snapme.Migrations
{
    public partial class ExtraScores : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ExtraScore",
                table: "Players",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ExtraScore",
                table: "Players");
        }
    }
}
