﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Snapme.Migrations
{
    public partial class Indexes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_PlayerTasks_GameId_GameTaskStatus_Id",
                table: "PlayerTasks",
                columns: new[] { "GameId", "GameTaskStatus", "Id" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_PlayerTasks_GameId_GameTaskStatus_Id",
                table: "PlayerTasks");
        }
    }
}
