﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Snapme.Migrations
{
    public partial class AddHashTags : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "SystemUsers",
                keyColumn: "Id",
                keyValue: new Guid("26e249d7-3dac-4c8e-8e4c-85da5a71ce66"));

            migrationBuilder.DeleteData(
                table: "SystemUsers",
                keyColumn: "Id",
                keyValue: new Guid("a9fb2cde-0fc4-447b-8561-b03f1b1a72ec"));

            migrationBuilder.AddColumn<string>(
                name: "Tags",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsNotifiedGameStarted",
                table: "Players",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Tags",
                table: "Games",
                nullable: true);

            migrationBuilder.InsertData(
                table: "SystemUsers",
                columns: new[] { "Id", "Email", "Password" },
                values: new object[,]
                {
                    { new Guid("cbd4f308-79d1-4510-abfb-97c8ddada849"), "kofman", "sn0pme987" },
                    { new Guid("d3631b1b-e2f2-49f4-8ed3-9882df822c1d"), "ilyin", "sn0pme987" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "SystemUsers",
                keyColumn: "Id",
                keyValue: new Guid("cbd4f308-79d1-4510-abfb-97c8ddada849"));

            migrationBuilder.DeleteData(
                table: "SystemUsers",
                keyColumn: "Id",
                keyValue: new Guid("d3631b1b-e2f2-49f4-8ed3-9882df822c1d"));

            migrationBuilder.DropColumn(
                name: "Tags",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "IsNotifiedGameStarted",
                table: "Players");

            migrationBuilder.DropColumn(
                name: "Tags",
                table: "Games");

            migrationBuilder.InsertData(
                table: "SystemUsers",
                columns: new[] { "Id", "Email", "Password" },
                values: new object[,]
                {
                    { new Guid("26e249d7-3dac-4c8e-8e4c-85da5a71ce66"), "kofman", "sn0pme987" },
                    { new Guid("a9fb2cde-0fc4-447b-8561-b03f1b1a72ec"), "ilyin", "sn0pme987" }
                });
        }
    }
}
