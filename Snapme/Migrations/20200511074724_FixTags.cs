﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Snapme.Migrations
{
    public partial class FixTags : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Tags",
                table: "Users");

            migrationBuilder.AddColumn<string>(
                name: "Tags",
                table: "TgmBotUsers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Tags",
                table: "TgmBotUsers");

            migrationBuilder.AddColumn<string>(
                name: "Tags",
                table: "Users",
                nullable: true);
        }
    }
}
