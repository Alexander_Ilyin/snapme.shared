namespace Snapme.Data.Games
{
    public enum ActivityType
    {
        SelfieTask = 0,
        MeetingTask = 10,
        MissionSingleTask = 13,
        MissionTaskActivity = 30,
        MissionTaskPlace = 60,
        FinalSingleTask = 61,
        FinalTaskPlace = 70
    }
}