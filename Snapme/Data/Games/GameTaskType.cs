namespace Snapme.Data.Games
{
    public enum GameTaskType
    {
        Selfie = 0,
        Meeting = 10,
        MissionSingle = 20,
        Mission = 30,
        FinalSingle = 40,
        Final = 50
    }
}