using System;
using Snapme.Data.Bots;

namespace Snapme.Data.Games
{
    public class Activity
    {
        public Guid GameId { get; set; }
        public virtual Game Game { get; set; }

        public Guid Id { get; set; } = Guid.NewGuid();
        public string DescriptionRu { get; set; }
        public string DescriptionEn { get; set; }
        public ActivityType ActivityType { get; set; }
        public int? Phase { get; set; }
        public Level Level { get; set; }
        public bool IsDelete { get; set; }

        public override string ToString()
        {
            return $"{nameof(Id)}: {Id}: {GetDescription(Languages.Russian)}, {nameof(ActivityType)}: {ActivityType}";
        }

        public string ToLongString()
        {
            return
                $"{nameof(GameId)}: {GameId}, {nameof(Id)}: {Id}, {nameof(DescriptionRu)}: {DescriptionRu}, {nameof(DescriptionEn)}: {DescriptionEn}, {nameof(ActivityType)}: {ActivityType}, {nameof(Phase)}: {Phase}, {nameof(Level)}: {Level}";
        }

        public string GetDescription(string lang)
        {
            return (lang == Languages.English ? DescriptionEn : DescriptionRu) ?? DescriptionRu ?? DescriptionEn ?? "...";
        }
    }
}