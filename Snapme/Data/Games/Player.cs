using System;
using System.Linq;
using Snapme.Common;
using Snapme.Data.Bots;

namespace Snapme.Data.Games
{
    public class Player
    {
        public Guid GameId { get; set; }
        public Guid UserId { get; set; }
        public virtual User User { get; set; }
        public virtual Game Game { get; set; }

        public TgmBotUser GetBotUser(SnapmeContext context)
        {
            var botUser = context.TgmBotUsers.Find(Game.TgmBotId, UserId);
            return botUser;
        } 
        public string SelfiePhotoId { get; set; }

        public Guid? CurrentTaskId { get; set; }
        public virtual GameTask CurrentTask { get; set; }

        public DateTime? LastUpdate { get; set; }
        public PlayerStatus Status { get; set; }
        public bool IsWelcomeReceived { get; set; }
        public int ExtraScore { get; set; }
        public bool IsNotifiedGameStarted { get; set; }


        public override string ToString()
        {
            return $"{nameof(GameId)}: {GameId}, {nameof(UserId)}: {UserId}";
        }

        public void OnStatusUpdate()
        {
            LastUpdate = DateHelper.UtcNow();
        }

        public TimeSpan? GetIdleTime()
        {
            return LastUpdate == null ? (TimeSpan?) null : DateHelper.UtcNow() - LastUpdate.Value;
        }

        public bool IsJustJoined()
        {
            return SelfiePhotoId.IsEmptyOrNull() && CurrentTaskId == null;
        }

    }
}