using System;
using Snapme.Data.Bots;

namespace Snapme.Data.Games
{
    public class GameTaskAssignee
    {
        public Guid GameTaskId { get; set; }
        public Guid UserId { get; set; }
        public virtual User User { get; set; }
        public virtual GameTask GameTask { get; set; }
    }
}