using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Snapme.Common;
using Snapme.Data.Bots;
using Snapme.Data.Dto;

namespace Snapme.Data.Games
{
    public class Game
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string Title { get; set; }
        public DateTime FinishTime { get; set; }
        public DateTime Created { get; set; }
        public GameStatus GameStatus { get; set; }
        public int Phase { get; set; } = 1;

        public Guid? TgmBotId { get; set; }
        public virtual TgmBot TgmBot { get; set; }
        public bool MustBeProcessedByBot { get; set; }

        public string GameStatusStr()
        {
            switch (GameStatus)
            {
                case GameStatus.New:
                    return "Не стартована";
                case GameStatus.Started:
                    return "В процессе";
                case GameStatus.Final:
                    return "Завершается";
                case GameStatus.Finished:
                    return "Завершена";
                default:
                    return "?";
            }
        }

        public string SerializedSettings { get; set; }

        public GameSettings GetSettings()
        {
            if (SerializedSettings.IsEmptyOrNull())
                return new GameSettings();
            return JsonConvert.DeserializeObject<GameSettings>(SerializedSettings);
        }

        public void SetSettings(GameSettings settings)
        {
            SerializedSettings = JsonConvert.SerializeObject(settings);
        }

        public bool IsRunning()
        {
            return GameStatus == GameStatus.Started || GameStatus == GameStatus.Final;
        }

        public bool IsStarted()
        {
            return GameStatus == GameStatus.Started;
        }

        public void SetStarted(string tag)
        {
            if (GameStatus == GameStatus.New)
            {
                GameStatus = GameStatus.Started;
                this.Log().Info("Started game " + Id);
            }
            AddTags(new[] { tag ?? "all" });
        }

        public string Tags { get; set; }

        public string[] GetTags()
        {
            return (Tags ?? "").Split(",", StringSplitOptions.RemoveEmptyEntries);
        }

        public void SetTags(string[] tags)
        {
            Tags = tags.Distinct().Where(x => !x.IsEmptyOrNull()).JoinStr(x => x, ",");
        }

        public void AddTags(string[] tags)
        {
            Tags = GetTags().Concat(tags).Distinct().Where(x => !x.IsEmptyOrNull()).JoinStr(x => x, ",");
        }

        public void RemoveTags(string[] tags)
        {
            Tags = GetTags().Where(x => !tags.Contains(x)).Distinct().Where(x => !x.IsEmptyOrNull())
                .JoinStr(x => x, ",");
        }

        public void SetFinal()
        {
            if (GameStatus < GameStatus.Final)
                GameStatus = GameStatus.Final;
        }

        public void SetFinished()
        {
            if (GameStatus < GameStatus.Finished)
            {
                GameStatus = GameStatus.Finished;
                FinishTime = DateHelper.UtcNow();
            }
        }

        public void SetPhase(int phase)
        {
            Phase = phase;
        }

        public void Update(GameDto dto)
        {
            var settings = GetSettings() ?? new GameSettings();
            settings.AdminChatId = dto.AdminChatId.TryParseLong();
            settings.SendResultsToPlayers = dto.SendResultsToPlayers;
            settings.ValidationChatIds = dto.ValidationChatIds.IsEmptyOrNull()
                ? new List<long>()
                : dto.ValidationChatIds.Trim().Split(",").Select(x => x.TryParseLong()).Where(x => x.HasValue)
                    .Select(x => x.Value).ToList();
            settings.TaskSpreadsheedListName = dto.TaskSpreadsheedListName;
            settings.WithMeetings = dto.WithMeetings;
            settings.RequireUserName = dto.RequireUserName;
            settings.LongWaitPeriodInSeconds = dto.LongWaitPeriodInSeconds;
            SetSettings(settings);

            TgmBotId = dto.TgmBotId;
            Title = dto.Title;
            MustBeProcessedByBot = dto.MustBeProcessedByBot;
        }

        public bool IsAdminChat(long key)
        {
            return GetSettings().AdminChatId == key;
        }

        public bool IsValidatorChat(long userMessageKey)
        {
            return GetSettings().ValidationChatIds.Contains(userMessageKey);
        }

        public string GetTitle()
        {
            return Title ?? "Без названия";
        }

        public bool CanRun()
        {
            return GetSettings().AdminChatId != null &&
                   GetSettings().ValidationChatIds != null &&
                   GetSettings().ValidationChatIds.Count >= 1;
        }
    }
}