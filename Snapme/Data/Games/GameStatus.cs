namespace Snapme.Data.Games
{
    public enum GameStatus
    {
        New = 0,
        Started = 10,
        Final = 20,
        Finished = 30
    }
}