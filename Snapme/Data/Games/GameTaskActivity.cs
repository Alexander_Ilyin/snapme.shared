using System;

namespace Snapme.Data.Games
{
    public class GameTaskActivity
    {
        public int Number { get; set; }

        public Guid GameTaskId { get; set; }
        public virtual GameTask GameTask { get; set; }

        public Guid ActivityId { get; set; }
        public virtual Activity Activity { get; set; }
    }
}