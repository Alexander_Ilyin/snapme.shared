using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Snapme.Data.Games
{
    public class GameTask
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public Guid GameId { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Finished { get; set; }

        public virtual List<GameTaskActivity> Activities { get; set; } = new List<GameTaskActivity>();
        public virtual List<GameTaskAssignee> Assigned { get; set; } = new List<GameTaskAssignee>();


        public GameTaskType TaskType { get; set; }
        public GameTaskStatus GameTaskStatus { get; set; }

        public string TaskPhotoId { get; set; }
        public string ModeratorRejectReason { get; set; }
        public double ModeratorPriority { get; set; }
        public string WaitingForValidatorId { get; set; }


        public string GetPlayerNames()
        {
            var sb = new StringBuilder();
            foreach (var player in Assigned)
                sb.Append("@" + player.User.TgmUserName + ";");
            return sb.ToString();
        }

        public bool IsFinal()
        {
            return TaskType == GameTaskType.Final ||
                   TaskType == GameTaskType.FinalSingle;
        }

        public Activity[] GetActivities()
        {
            return Activities.OrderBy(x => x.Number).Select(a => a.Activity).ToArray();
        }

        public bool IsMission()
        {
            var taskType = TaskType;
            return taskType == GameTaskType.Final || taskType == GameTaskType.FinalSingle || taskType == GameTaskType.Mission
                   || taskType == GameTaskType.MissionSingle;        }

        public bool IsMeeting()
        {
            return TaskType == GameTaskType.Meeting;
        }
    }
}