using System.Collections.Generic;

namespace Snapme.Data.Games
{
    public class GameSettings
    {
        public string TaskSpreadsheedListName { get; set; }
        public List<long> ValidationChatIds { get; set; } = new List<long>();
        public long? AdminChatId { get; set; }
        public bool? WithMeetings { get; set; }
        public bool? RequireUserName { get; set; }
        public bool SendResultsToPlayers { get; set; }
        public int? LongWaitPeriodInSeconds { get; set; }

    }
}