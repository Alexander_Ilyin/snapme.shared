namespace Snapme.Data.Games
{
    public enum PlayerStatus
    {
        NotJoined = 0,
        Active = 10,
        Kicked = 20,
        SoftKicked = 30
    }
}