namespace Snapme.Data.Games
{
    public enum GameTaskStatus
    {
        NotReady = 0,
        ReadyForValidation = 10,
        Validating = 20,
        Accepted = 30,
        Canceled = 50
    }
}