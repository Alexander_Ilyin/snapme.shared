using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Snapme.Common;
using Snapme.Data.Bots;
using Snapme.Data.Games;
using Snapme.Data.Info;
using Snapme.Data.Networking;
using Snapme.Trinity.Data;

namespace Snapme.Data
{
    public class OwnerChecker
    {
        private bool isOwned = false;
        void BeginOwn()
        {
            if (isOwned)
                throw new Exception("AAAAAAAAAAAAAAAAAAAAA");
        }

        void EndOwn()
        {
            isOwned = false;
        }
    }
    public class SnapmeContext : DbContext
    {
        public SnapmeContext() : base(GetOptions(null, ConfigHelper.GetConfig(null)))
        {
        }

        public SnapmeContext(string testId, IConfiguration configuration) : base(GetOptions(testId, configuration))
        {
        }

        public DbSet<TgmBotUser> TgmBotUsers { get; set; }
        public DbSet<TgmBot> TgmBots { get; set; }
        public DbSet<BotContent> BotContents { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<Activity> GameTasks { get; set; }
        public DbSet<GameTask> PlayerTasks { get; set; }
        public DbSet<GameTaskAssignee> GameTaskAssignees { get; set; }
        public DbSet<GameTaskActivity> GameTaskActivities { get; set; }
        public DbSet<SystemUser> SystemUsers { get; set; }

        public DbSet<NetworkingSession> NetworkingSessions { get; set; }
        public DbSet<NetworkingProfile> NetworkingProfiles { get; set; }

        public DbSet<TrinityGame> TrinityGames { get; set; }
        public DbSet<TrinityPlayer> TrinityPlayers { get; set; }
        public DbSet<TrinityExplanation> TrinityExplanations { get; set; }
        public DbSet<TrinityTask> TrinityTasks { get; set; }
        public DbSet<TrinityWord> TrinityWords { get; set; }


        public override int SaveChanges()
        {
            
            return base.SaveChanges();
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            return base.SaveChangesAsync(cancellationToken);
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = new CancellationToken())
        {
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        private static DbContextOptions GetOptions(string testId, IConfiguration configuration)
        {
            
            var optionsBuilder = new DbContextOptionsBuilder<SnapmeContext>();
            var settings = configuration.Get<DbSettings>();
            if (!settings.UseInMemoryDb)
                optionsBuilder = optionsBuilder.UseNpgsql(settings.ConnectionString);
            else
                optionsBuilder = optionsBuilder.UseInMemoryDatabase("Db-" + testId);

            
            var options = optionsBuilder
                .UseLazyLoadingProxies()
                
                .Options;
            return options;
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            new SnapmeModelBuilder(modelBuilder).Build();
        }
    }
}