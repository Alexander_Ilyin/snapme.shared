using System;
using Snapme.Data.Bots;

namespace Snapme.Data.Networking
{
    public class NetworkingSession
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        
        public DateTime Created { get; set; } = DateTime.UtcNow;
        public Guid? TgmBotId { get; set; }
        public virtual TgmBot TgmBot { get; set; }
        public bool IsActive { get; set; }
    }
}