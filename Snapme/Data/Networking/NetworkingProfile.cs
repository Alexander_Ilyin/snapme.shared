using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Snapme.Common;
using Snapme.Data.Bots;

namespace Snapme.Data.Networking
{
    public class NetworkingProfile
    {
        public Guid? UserId { get; set; }
        public virtual User User { get; set; }

        public Guid NetworkingSessionId { get; set; }
        public virtual NetworkingSession NetworkingSession { get; set; }

        public string Name { get; set; }
        public string PhotoId { get; set; }
        public string Interest { get; set; }
        public DateTime Created { get; set; } = DateTime.UtcNow;

        public string SerializedLiked { get; set; }
        public string SerializedDisliked { get; set; }
        public string SerializedNotified { get; set; }
        public string Position { get; set; }
        public string GreatAt { get; set; }
        public bool IsActive { get; set; }

        public void SetNotifiedBy(List<int> list)
        {
            SerializedNotified = JsonConvert.SerializeObject(list);
        }

        public List<int> GetNotifiedBy()
        {
            return SerializedNotified.IsEmptyOrNull()
                ? new List<int>()
                : JsonConvert.DeserializeObject<int[]>(SerializedNotified).ToList();
        }

        public void AddNofifiedBy(int userNum)
        {
            var list = GetNotifiedBy();
            list.Add(userNum);
            SetNotifiedBy(list.Distinct().ToList());
        }

        public void AddLike(int userNum)
        {
            var list = GetLiked();
            list.Add(userNum);
            SetLiked(list.Distinct().ToList());
        }

        public void AddDislike(int userNum)
        {
            var list = GetDisliked();
            list.Add(userNum);
            SetDisliked(list.Distinct().ToList());
        }

        public List<int> GetLiked()
        {
            return SerializedLiked.IsEmptyOrNull()
                ? new List<int>()
                : JsonConvert.DeserializeObject<int[]>(SerializedLiked).ToList();
        }

        public void SetLiked(List<int> v)
        {
            SerializedLiked = JsonConvert.SerializeObject(v);
        }

        public List<int> GetDisliked()
        {
            return SerializedDisliked.IsEmptyOrNull()
                ? new List<int>()
                : JsonConvert.DeserializeObject<int[]>(SerializedDisliked).ToList();
        }

        public void SetDisliked(List<int> v)
        {
            SerializedDisliked = JsonConvert.SerializeObject(v);
        }
    }
}