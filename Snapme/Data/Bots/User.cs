using System;
using System.Linq;
using Castle.Core.Internal;
using Snapme.Common;

namespace Snapme.Data.Bots
{
    public class User
    {
        public Gender? Gender { get; set; }
        public Guid Id { get; set; } = Guid.NewGuid();
        public string TgmUserName { get; set; }
        public long TgmUserId { get; set; }
        public bool IsAdmin { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Language { get; set; }
        public bool IsFake { get; set; }
        public override string ToString()
        {
            return $"{nameof(Id)}: {Id}, {nameof(TgmUserName)}: {TgmUserName}, {nameof(TgmUserId)}: {TgmUserId}";
        }

        public string FormatLink()
        {
            return String.Format("<a href='tg://user?id={0}'>{1}</a>", this.TgmUserId, this.Name());
        }
        
        public string FormatLinkWithNum()
        {
            return String.Format("<a href='tg://user?id={0}'>{1}</a>", this.TgmUserId, this.Name());
        }
      
        
        public string FullName()
        {
            return string.Format("{0} {1} {2}", FirstName, LastName,
                TgmUserName.IsNullOrEmpty() ? "" : "@" + TgmUserName).Trim();
        }

        public string Name()
        {
            var name = (FirstName +" " + LastName).Trim();
            if (name=="") 
                return "@" + TgmUserName;
            return name;
        }
    }
}