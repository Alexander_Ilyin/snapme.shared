namespace Snapme.Data.Bots
{
    public class Languages
    {
        public const string Russian = "ru";
        public const string English  = "en";

        public static string FromTgmCode(string fromLanguageCode)
        {
            if (fromLanguageCode?.ToLower()?.Contains("en") == true)
                return English;
            return Russian;
        }
    }
}