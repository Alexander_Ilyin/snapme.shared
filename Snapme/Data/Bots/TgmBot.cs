using System;
using Newtonsoft.Json;
using Snapme.Common;
using Snapme.Data.Dto;

namespace Snapme.Data.Bots
{
    public class TgmBot
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string BotTgmKey { get; set; }
        public string Title { get; set; }
        public bool IsDelete { get; set; }
        public long? FeedbackChatId { get; set; }
        public int UserSequenceNum { get; set; }
        public string SerializedMeta { get; set; }

        public TgmBotSettings GetSettings()
        {
            if (SerializedMeta.IsEmptyOrNull())
                return new TgmBotSettings();
            return JsonConvert.DeserializeObject<TgmBotSettings>(SerializedMeta);
        }

        public void SetSettings(TgmBotSettings settings)
        {
            SerializedMeta = JsonConvert.SerializeObject(settings);
        }

        public void Update(BotDto dto)
        {
            BotTgmKey = dto.BotTgmKey;
            Title = dto.Title;
            FeedbackChatId = dto.FeedbackChatId.TryParseLong();
            var settings = GetSettings();
            settings.InfoSpreadsheetId = dto.InfoSpreadsheetId;
            settings.InfoSpreadsheedListName = dto.InfoSpreadsheetName;
            SetSettings(settings);
        }

        public bool IsFeedbackChat(long id)
        {
            return FeedbackChatId == id;
        }

        public long? FindFeedbackChatId()
        {
            return this.FeedbackChatId;
        }
    }
}