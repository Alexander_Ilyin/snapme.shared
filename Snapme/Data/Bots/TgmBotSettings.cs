namespace Snapme.Data.Bots
{
    public class TgmBotSettings
    {
        public string InfoSpreadsheetId { get; set; }
        public string InfoSpreadsheedListName { get; set; }
        public long? AdminChatId { get; set; }
        public long? ValidateChatId { get; set; }
        public bool Stopped { get; set; }
    }
}