using System;
using System.Linq;
using Snapme.Common;

namespace Snapme.Data.Bots
{
    public class TgmBotUser
    {
        public Guid BotId { get; set; }
        public Guid UserId { get; set; }
        public long GetChatId() => TgmChatId.Value;
        public long? TgmChatId { get; set; }
        public virtual User User { get; set; }
        public virtual TgmBot Bot { get; set; }
        public int UserNumber { get; set; }
        public bool UserBannedBot { get; set; }
        public string UserBannedBotDetails { get; set; }
        public string CurrentDialogState { get; set; }
        public string CurrentDialogType { get; set; }
        public string Tags { get; set; }

        public bool IsMatch(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
                return true;
            return Tags?.Contains(query, StringComparison.InvariantCultureIgnoreCase) == true ||
                   User?.FullName()?.Contains(query, StringComparison.InvariantCultureIgnoreCase) == true;
        }

        public string[] GetTags()
        {
            return (Tags ?? "").Split(",", StringSplitOptions.RemoveEmptyEntries);
        }

        public void SetTags(string[] tags)
        {
            Tags = tags.Distinct().Where(x => !x.IsEmptyOrNull()).JoinStr(x => x, ",");
        }

        public void AddTags(params string[] tags)
        {
            Tags = GetTags().Concat(tags).Distinct().Where(x => !x.IsEmptyOrNull()).JoinStr(x => x, ",");
        }

        public void RemoveTags(params string[] tags)
        {
            Tags = GetTags().Where(x => !tags.Contains(x)).Distinct().Where(x => !x.IsEmptyOrNull()).JoinStr(x => x, ",");
        }

        public bool IsMatchForTag(string tag)
        {
            if (tag == "all")
                return true;
            return this.GetTags().Any(x => tag.Norm() == x.Norm());
        }
    }
}