namespace Snapme.Data
{
    public class DbSettings
    {
        public string ConnectionString { get; set; }
        public bool UseInMemoryDb { get; set; }
    }
}