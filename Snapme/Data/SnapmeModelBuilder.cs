using Microsoft.EntityFrameworkCore;
using Snapme.Data.Bots;
using Snapme.Data.Games;
using Snapme.Data.Networking;

namespace Snapme.Data
{
    public class SnapmeModelBuilder
    {
        private readonly ModelBuilder modelBuilder;

        public SnapmeModelBuilder(ModelBuilder modelBuilder)
        {
            this.modelBuilder = modelBuilder;
        }

        public void Build()
        {
            {
                var entity = modelBuilder.Entity<SystemUser>();
                entity.HasKey(x => x.Id);
//                entity.HasData(new SystemUser[]
//                {
//                    new SystemUser() {Email = "kofman", Password = "sn0pme987"},
//                    new SystemUser() {Email = "ilyin", Password = "sn0pme987"},
//                });
            }
            {
                var entity = modelBuilder.Entity<TgmBotUser>();
                entity.HasKey(x =>
                    new
                    {
                        x.BotId,
                        x.UserId
                    });
            }
            {
                var entity = modelBuilder.Entity<NetworkingProfile>();
                entity.HasKey(x =>
                    new
                    {
                        x.NetworkingSessionId,
                        x.UserId
                    });
            }
            {
                var entity = modelBuilder.Entity<Game>();
                entity.HasKey(x => x.Id);
            }
            modelBuilder.Entity<User>().HasKey(x => x.Id);

            var player = modelBuilder.Entity<Player>();
            player.HasKey(x =>
                new
                {
                    x.GameId,
                    x.UserId
                });

            var mission = modelBuilder.Entity<GameTask>();
            mission.HasKey(x => x.Id);
            mission.ForNpgsqlHasIndex(x => new {x.GameId, x.GameTaskStatus, x.Id});


            mission.OwnsMany(m => m.Activities,
                link => { link.HasKey(m => new {MissionId = m.GameTaskId, ConditionId = m.ActivityId}); });
            mission.OwnsMany(x => x.Assigned, link => { link.HasKey(x => new {MissionId = x.GameTaskId, x.UserId}); });
        }
    }
}