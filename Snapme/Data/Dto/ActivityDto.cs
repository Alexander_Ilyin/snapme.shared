using System;
using Snapme.Common;
using Snapme.Data.Games;

namespace Snapme.Data.Dto
{
    [TScripted]
    public class ActivityDto
    {
        public Guid GameId { get; set; }

        public Guid Id { get; set; } = Guid.NewGuid();
        public string DescriptionRu { get; set; }
        public string DescriptionEn { get; set; }
        public int? Phase { get; set; }
        public Level Level { get; set; }
        public bool IsDelete { get; set; }
        public ActivityType ActivityType { get; set; }
    }
}