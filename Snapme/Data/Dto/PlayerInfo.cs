using System;
using Snapme.Common;

namespace Snapme.Data.Dto
{
    [TScripted]
    public class PlayerInfo
    {
        public Guid UserId { get; set; }
        public string TgmName { get; set; }
        public string CurrentTask { get; set; }
        public int? IdleTimeSeconds { get; set; }
    }
}