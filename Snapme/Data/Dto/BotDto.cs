using System;
using Snapme.Common;

namespace Snapme.Data.Dto
{
    [TScripted]
    public class BotDto
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public bool IsRunning { get; set; }
        public bool IsDelete { get; set; }
        public string BotTgmKey { get; set; }
        public string FeedbackChatId { get; set; }
        public string InfoSpreadsheetId { get; set; }
        public string InfoSpreadsheetName { get; set; }
    }
}