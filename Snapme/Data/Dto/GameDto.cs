using System;
using Snapme.Common;

namespace Snapme.Data.Dto
{
    [TScripted]
    public class GameDto
    {
        public Guid Id { get; set; }

        public string TaskSpreadsheedListName { get; set; }
        public string Title { get; set; }
        public bool IsDelete { get; set; }
        public string AdminChatId { get; set; }
        public string ValidationChatIds { get; set; }
        public Guid? TgmBotId { get; set; }
        public bool MustBeProcessedByBot { get; set; }
        public bool WithMeetings { get; set; }
        public bool RequireUserName { get; set; }
        public bool SendResultsToPlayers { get; set; }
        public int? LongWaitPeriodInSeconds { get; set; }
    }
}