using System;

namespace Snapme.Data.Dto
{
    public class TelegramBotEventDto
    {
        public DateTime EventDate { get; set; }
        public string Error { get; set; }
        public string Description { get; set; }
        public string JoinedGroupId { get; set; }
        public string JoinedGroupTitle { get; set; }
    }
}