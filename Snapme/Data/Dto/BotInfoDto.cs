using System.Collections.Generic;
using Snapme.Common;

namespace Snapme.Data.Dto
{
    [TScripted]
    public class BotInfoDto
    {
        public bool IsRunning { get; set; }
        public List<TelegramBotEventDto> LastEvents { get; set; }
        public string LastError { get; set; }
    }
}