using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Snapme.Data.Bots;

namespace Snapme.Data.Info
{
    public class BotContent
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        public Guid? TgmBotId { get; set; }

        public virtual TgmBot TgmBot { get; set; }

        public string SerializedItems { get; set; }

        public List<InfoContentItem> GetItems()
        {
            if (string.IsNullOrEmpty(SerializedItems))
                return new List<InfoContentItem>();
            return JsonConvert.DeserializeObject<List<InfoContentItem>>(SerializedItems);
        }

        public void SetItems(List<InfoContentItem> list)
        {
            if (list == null || list.Count == 0)
                SerializedItems = "";
            SerializedItems = JsonConvert.SerializeObject(list);
        }
    }
}