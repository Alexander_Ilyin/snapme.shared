namespace Snapme.Data.Info
{
    public class InfoContentItem
    {
        public string Type { get; set; }
        public string Content { get; set; }
    }
}